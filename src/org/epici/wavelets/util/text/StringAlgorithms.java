package org.epici.wavelets.util.text;

/**
 * General class for string manipulation.
 * Contains some common useful algorithms not
 * already found in {@link org.apache.commons.lang3.StringUtils}.
 * Nothing too specialized here.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class StringAlgorithms {
	
	// Deny constructor
	private StringAlgorithms() {}
	
	/**
	 * Test if <b>a</b> is a subsequence of <b>b</b>,
	 * that is, all of the characters in <b>a</b>
	 * appear in <b>b</b> in order.
	 * Characters can have other characters in between,
	 * unlike with substrings, so
	 * {@code "anna"} is a subsequence of {@code "banana"}
	 * but not a substring.
	 * <br>
	 * Returns false if either argument is null.
	 * 
	 * @since 0.2.2
	 * @param a the smaller sequence
	 * @param b the larger sequence
	 * @return if <b>a</b> is a subsequence of <b>b</b>
	 */
	public static boolean isSubsequenceOf(CharSequence a,CharSequence b) {
		// if either is null => result is false
		if(a == null || b == null)return false;
		int alen = a.length(), blen = b.length();
		// a must be same length or shorter to be possible subsequence
		if(alen > blen)return false;
		int aindex = 0, bindex = 0;
		// go through both sequences
		while(aindex < alen && bindex < blen) {
			// only move forward in smaller sequence if match
			if(a.charAt(aindex) == b.charAt(bindex))aindex++;
			// always move forward in bigger sequence
			bindex++;
		}
		// seen all characters in smaller sequence <=> is a subsequence
		return aindex == alen;
	}

}
