/*
 **************************************************************
 * fft.c
 * Douglas L. Jones 
 * University of Illinois at Urbana-Champaign 
 * January 19, 1992 
 * http://cnx.rice.edu/content/m12016/latest/
 * 
 *	 fft: in-place radix-2 DIT DFT of a complex input 
 * 
 *	 input: 
 * n: length of FFT: must be a power of two 
 * m: n = 2**m 
 *	 input/output 
 * x: double array of length n with real part of data 
 * y: double array of length n with imag part of data 
 * 
 *	 Permission to copy and use this program is granted 
 *	 as long as this header is included. 
 *
 * @param x the real array
 * @param y the matching imaginary array
 ****************************************************************/

/*
 * The link in the copyright header is broken.
 * If you want to verify the copyright and license,
 * it's up to you to find a mirror of the original.
 */

package org.epici.wavelets.util.math;

import org.epici.wavelets.util.Bits;

/**
 * Fast Fourier Transform, using Radix-2 Decimation In Time algorithm.
 * <br>
 * See license header for original copyright.
 * <br><br>
 * Aside from adapting it to Java, other changes have been made,
 * notably compliance with the {@link FFT} API, optimizations, and
 * documentation.
 * 
 * @author Douglas L. Jones (see license)
 * @author EPICI
 * @version 1.0
 */
public final class FFTRadix2 extends FFT {
	
	/**
	 * Cached FFT objects. Prevents duplicates to some extent.
	 */
	private static FFTRadix2[] sharedFFTs = new FFTRadix2[30];
	
	/**
	 * Get shared FFT object for known power of 2 size:
	 * <i>2<sup>{@code logSize}</sup></i>
	 * <br><br>
	 * Warning: even if this method is the only way new FFTs
	 * are created, duplicates can still happen. This will occur
	 * if one thread creates an FFT and another one requests the same
	 * size FFT before that first thread updates the cache and the second
	 * thread sees the update to the cache.
	 * 
	 * @param logSize the exponent of 2
	 * @return an FFT object which can process arrays of length 1&lt;&lt;logSie
	 */
	public static FFTRadix2 getFft(int logSize){
		if(logSize<1){
			throw new IllegalArgumentException("FFT length exponent "+Integer.toString(logSize)+" is too small. Minimum value is 1.");
		}else if(logSize>30){
			throw new IllegalArgumentException("FFT length exponent "+Integer.toString(logSize)+" is too large. Maximum value is 30.");
		}else{
			int index = logSize-1;
			if(sharedFFTs[index]==null){
				sharedFFTs[index]=getNewFft(logSize);
			}
			return sharedFFTs[index];
		}
	}
	
	/**
	 * Dereference a shared FFT object
	 * <br>
	 * Only really used to free up memory
	 * 
	 * @param logSize the exponent of 2
	 * 
	 * @see #getFft(int)
	 */
	public static void removeFft(int logSize){
		//Fail-safe
		if(logSize>=1&&logSize<=30){
			sharedFFTs[logSize-1]=null;
		}
	}
	
	/**
	 * Gets a new FFT object for a known power of 2 size:
	 * <i>2<sup>{@code logSize}</sup></i>
	 * <br>
	 * Assumed valid (checks should be done elsewhere)
	 * 
	 * @param logSize the exponent of 2
	 * @return an FFT object which can process arrays of length 1&lt;&lt;m
	 */
	public static FFTRadix2 getNewFft(int logSize){
		return new FFTRadix2(1<<logSize);
	}

	/**
	 * The size which this FFT works for. Does not work for other sizes.
	 */
	public final int size;
	/**
	 * log2 of {@link #size}.
	 */
	public final int logSize;
	
	/**
	 * Lookup table for real part of roots of unity, which corresponds
	 * to cosine.
	 */
	private double[] cos;
	/**
	 * Lookup table for real part of roots of unity, which corresponds
	 * to sine.
	 */
	private double[] sin;
	/**
	 * One side of the pairs which get bit reversed. Gets swapped
	 * with the corresponding element in {@link #bitreverseb}.
	 */
	private int[] bitreversea;
	/**
	 * One side of the pairs which get bit reversed. Gets swapped
	 * with the corresponding element in {@link #bitreversea}.
	 */
	private int[] bitreverseb;
	/**
	 * The number of pairs which must be bit reversed.
	 */
	private int bitreversecount;
	
	public FFTRadix2(int size) {
		this.size = size;

		/*
		 * This FFT requires a power of 2, at least 2, to work.
		 * (length 0 and 1 are trivial anyway, so who cares)
		 */
		if(size<2 || !Bits.isPo2(size))
			throw new IllegalArgumentException("FFT length must be power of 2");
		
		this.logSize = Bits.binLog(size);
		
		int halfSize = size>>1;
		
		/*
		 * We will need "twiddle factors" later, which are
		 * complex roots of unity, which have real and imaginary
		 * components as cosine and sine of a rational times 2pi (a turn).
		 * 
		 * So we precompute all the values we need and put them
		 * in a lookup table.
		 */
		cos = new double[halfSize];
		sin = new double[halfSize];

		for(int i=0; i<size/2; i++) {
			double ip = (double)i/size;
			/*
			 * The turning is backward (clockwise instead of counterclockwise).
			 * That's not a mistake, it's root of unity convention in math.
			 * 
			 * This little section should be optimized somewhat by the compiler,
			 * so this code should do. Even if it's not optimized, it should
			 * only be run once per size since it happens in initialization,
			 * so not a big cost incurred.
			 */
			cos[i] = Math.cos((-2*Math.PI)*ip);
			sin[i] = Math.sin((-2*Math.PI)*ip);
		}
		
		/*
		 * We make an array to track where each element will
		 * be permuted to. It starts as the identity permutation.
		 */
		
		int[] positions = new int[size];
		int i;
		for(i=0;i<size;i++){
			positions[i]=i;
		}
		
		/*
		 * Bit reverse would normally take O(nlogn) or O(nloglogn) at best
		 * Doing it once and creating a lookup table means the first part of FFT,
		 * the bit reverse, is O(n), which is a pretty big deal
		 * 
		 * Update: modern CPU can do it in O(n) anyway, if the JIT will optimize it.
		 * However, without using a lookup table, it has to test if a swap
		 * is needed (otherwise it gets swapped twice, once triggered by each
		 * side of the pair), and branch.
		 * Branches slow the thing down. The branch predictor isn't that smart.
		 * Lookup table of swapped pairs ensures we don't need to branch later.
		 * How big of a deal is it really? That depends on the CPU. And we're not
		 * taking our chances.
		 */
		final int shift = -logSize;// same as 32-logSize for 32-bit int
		for (i=0; i < size; i++) {
			int j = Integer.reverse(i)>>>shift;
			if (i < j) {
				int si = positions[i];
				int sj = positions[j];
				positions[i] = sj;
				positions[j] = si;
			}
		}
		bitreversecount = 0;
		for(i=0;i<size;i++){
			int j = positions[i];
			if(i<j){
				bitreversecount++;
			}
		}
		bitreversea = new int[bitreversecount];
		bitreverseb = new int[bitreversecount];
		int index = 0;
		for(i=0;i<size;i++){
			int j = positions[i];
			if(i<j){
				bitreversea[index]=i;
				bitreverseb[index]=j;
				index++;
			}
		}
	}

	@Override
	public boolean checkBounds(int length) {
		return length==size;
	}

	@Override
	public void fftUnsafe(double[] real, double[] imag) {
		/*
		 * Reordering variables is a micro-optimization.
		 * The first 4 variables or something are special,
		 * read and write for them will be faster.
		 * In anticipation that it will not always be this 4-split,
		 * all variables are ordered approximately so
		 * earlier ones are used more.
		 */
		double t1,t2,ar,ai,c,s;// Temporary values and lookup local
		int ixa,ixb,skipy,skipx, // X and Y axes together, index and skip
			iz, // Z axis index
			ipz,axisx, // Z axis iteration with YZX axis permutation, and X axis length (which is its skip)
			splits; // Tracks the iteration of view
		/*
		 * Load all the instance variables and constants
		 */
		final int size = this.size, halfSize = size>>1, logSize = this.logSize, bitreversecount = this.bitreversecount;
		final int[] bitreversea = this.bitreversea, bitreverseb = this.bitreverseb;
		final double[] sin = this.sin, cos = this.cos;
	
		/*
		 * Bit reverse all the indices.
		 */
		for(iz=0;iz<bitreversecount;iz++){
			ixa = bitreversea[iz];
			ixb = bitreverseb[iz];
			t1 = real[ixa];
			t2 = real[ixb];
			real[ixa] = t2;
			real[ixb] = t1;
			t1 = imag[ixa];
			t2 = imag[ixb];
			imag[ixa] = t2;
			imag[ixb] = t1;
		}

		/*
		 * This is the actual FFT.
		 * 
		 * At each split, the array of length n will be viewed
		 * as some 3D array with dimensions [X, Y=2, Z],
		 * using row-major order.
		 * The first iteration has X=n/2, Z=1.
		 * The last iteration has X=1, Z=n/2.
		 */
		for (splits=0; splits < logSize; splits++) {
			/*
			 * The multipliers, or skip as it is called here, are
			 *    X  Y  Z
			 * [2*Z, Z, 1]
			 * as per row-major order.
			 * We calculate skips for Y and X.
			 * We also calculate the length of the array on the X axis.
			 * You can verify that skipx * axisx = size.
			 */
			skipy = 1 << splits;
			skipx = skipy << 1;
			axisx = halfSize >> splits;
			/*
			 * Here we loop over the Z values.
			 * This is important because each Z value is
			 * associated with a root of unity, which
			 * we will look up from the precomputed table.
			 * 
			 * At the same time, we do a loop over the Z axis
			 * in an alternate YZX axis view. In this view,
			 * the length of the x axis is the skip.
			 * This is used because of some rational magic, see below.
			 */
			for (iz=0,ipz=0; iz < skipy; iz++,ipz+=axisx) {
				/*
				 * Fetch of components of
				 *   W_{2 skipy}^{ iz} = e^[-i   pi( iz/skipy)]
				 * = W_{  skipx}^{ iz} = e^[-i 2 pi( iz/skipx)]
				 * = W_{   size}^{ipz} = e^[-i 2 pi(ipz/ size)]
				 * from lookup table.
				 * This is a particular root of unity,
				 * called the "twiddle factor" in FFT literature.
				 */
				c = cos[ipz];
				s = sin[ipz];
				/*
				 * Here we loop over the X values.
				 */
				for (ixa=iz; ixa < size; ixa+=skipx) {
					/*
					 * The pair occurs along the Y axis.
					 */
					ixb = ixa+skipy;
					/*
					 * Complex number maths that change the pair,
					 * called a "butterfly" in FFT literature.
					 * t := (table) * b
					 * b := a - t
					 * a := a + t
					 * where t=(t1+t2*i), and a and b are from the input array.
					 */
					t1 = c*real[ixb] - s*imag[ixb];
					t2 = s*real[ixb] + c*imag[ixb];
					ar = real[ixa];
					ai = imag[ixa];
					real[ixb] = ar - t1;
					imag[ixb] = ai - t2;
					real[ixa] = ar + t1;
					imag[ixa] = ai + t2;
				}
			}
		}
	}
	
	@Override
	public String toString(){
		return "<Radix-2 DIT DFT for N="+size+">";
	}
}