package org.epici.wavelets.util.math;

/**
 * Some generic floating point utilities and float constants
 * 
 * @author EPICI
 * @version 1.0
 */
public final class Floats {
	
	private Floats(){}
	
	/**
	 * Default comparison epsilon value used for doubles
	 * <br>
	 * Double has 53 mantissa bits, 2^-53 is around 1.1x10^-16, so 10^-12 is a safe value to use
	 */
	public static final double D_EPSILON = 1e-12;
	/**
	 * A tiny value that is still large enough to not mess up math
	 */
	public static final double D_TINY = 1e-30;
	/**
	 * Opposite of tiny: ridiculously large value
	 * <br>
	 * The number here is chosen so that this multiplied by the tiny value is still a very small value
	 */
	public static final double ID_TINY = 1e12;
	/**
	 * Default epsilon value used for floats
	 * <br>
	 * Float has 24 mantissa bits, 2^-24 is around 6.0x10^-8, so 10^-6 is a safe value to use
	 */
	public static final float F_EPSILON = 1e-6f;
	
	/**
	 * <i>log(2)</i>, useful for going between
	 * base <i>e</i> and base <i>2</i>
	 */
	public static final double LOG2 = 0.693147180559945309417232121458176568075500134360255254120680 /*StrictMath.log(2)*/;
	/**
	 * <i>log(10)</i>, useful for going between
	 * base <i>e</i> and base <i>10</i>
	 */
	public static final double LOG10 = 2.30258509299404568401799145468436420760110148862877297603333 /*StrictMath.log(10)*/;
	
	/**
	 * Checks if two values are absolutely or relatively near,
	 * uses default double epsilon value
	 * <br>
	 * Computed as <i>|a-b|&le;max(epsilon,epsilon&middot;max(|a|,|b|))</i>
	 * <br>
	 * Use this instead of equality check
	 * 
	 * @param a the first value
	 * @param b the second value
	 * @return true if the two values are close
	 */
	public static boolean isNear(double a,double b){
		return isNear(a,b,D_EPSILON);
	}
	/**
	 * Checks if two values are absolutely or relatively near,
	 * uses given double epsilon value
	 * <br>
	 * Computed as <i>|a-b|&le;max(epsilon,epsilon&middot;max(|a|,|b|))</i>
	 * <br>
	 * Use this instead of equality check
	 * 
	 * @param a the first value
	 * @param b the second value
	 * @param epsilon custom epsilon value
	 * @return true if they are absolutely or relatively close
	 */
	public static boolean isNear(double a,double b,double epsilon){
		return Math.abs(a-b)
				<=Math.max(epsilon, epsilon
						*Math.max(
								Math.abs(a),
								Math.abs(b)
								));
	}
	/**
	 * Checks if two values are absolutely or relatively near,
	 * uses default float epsilon value
	 * <br>
	 * Computed as <i>|a-b|&le;max(epsilon,epsilon&middot;max(|a|,|b|))</i>
	 * <br>
	 * Use this instead of equality check
	 * 
	 * @param a the first value
	 * @param b the second value
	 * @return true if the two values are close
	 */
	public static boolean isNear(float a,float b){
		return isNear(a,b,F_EPSILON);
	}
	/**
	 * Checks if two values are absolutely or relatively near,
	 * uses given float epsilon value
	 * <br>
	 * Computed as <i>|a-b|&le;max(epsilon,epsilon&middot;max(|a|,|b|))</i>
	 * <br>
	 * Use this instead of equality check
	 * 
	 * @param a the first value
	 * @param b the second value
	 * @param epsilon custom epsilon value
	 * @return true if they are absolutely or relatively close
	 */
	public static boolean isNear(float a,float b,float epsilon){
		return Math.abs(a-b)
				<=Math.max(epsilon, epsilon
						*Math.max(
								Math.abs(a),
								Math.abs(b)
								));
	}
	
	/**
	 * The median value of a,b,c
	 * <br>
	 * More versatile than specific variations like constrain
	 * <br>
	 * If the ordering is somewhat predictable (ex. with constants)
	 * aim for a&lt;b&lt;c
	 * 
	 * @param a first value
	 * @param b second value
	 * @param c third value
	 * @return median of the given values
	 */
	public static float median(float a,float b,float c){
		if(a>c){float d=a;a=c;c=d;}
		return Math.max(a, Math.min(c, b));
	}
	
	/**
	 * The median value of a,b,c
	 * <br>
	 * More versatile than specific variations like constrain
	 * <br>
	 * If the ordering is somewhat predictable (ex. with constants)
	 * aim for a&lt;b&lt;c
	 * 
	 * @param a first value
	 * @param b second value
	 * @param c third value
	 * @return median of the given values
	 */
	public static double median(double a,double b,double c){
		if(a>c){double d=a;a=c;c=d;}
		return Math.max(a, Math.min(c, b));
	}
	
	/**
	 * How even is this value? The result is an
	 * integer which measures evenness. It will be in the
	 * range [0, 23]. A more even number
	 * (higher evenness) will be favored by round to even rules.
	 * Only mantissa is considered.
	 * 
	 * @param value value to test evenness for
	 * @return evenness of this value
	 */
	public static int evenness(float value){
		return Math.min(
				23,
				Integer.numberOfTrailingZeros(
						Float.floatToIntBits(value)
						)
				);
	}
	
	/**
	 * How even is this value? The result is an
	 * integer which measures evenness. It will be in the
	 * range [0, 52]. A more even number
	 * (higher evenness) will be favored by round to even rules.
	 * Only mantissa is considered.
	 * 
	 * @param value value to test evenness for
	 * @return evenness of this value
	 */
	public static int evenness(double value){
		return Math.min(
				52,
				Long.numberOfTrailingZeros(
						Double.doubleToLongBits(value)
						)
				);
	}
	
	/**
	 * Main method, used only for testing
	 * 
	 * @param args ignored
	 */
	public static void main(String[] args){
		//Near equals sanity tests
		System.out.println("--- Near equals, doubles ---");
		System.out.println("1e100\u22481e100+1e85="+isNear(1e100d,1e100d+1e85d)+" (expected true)");
		System.out.println("1e100\u22481e100+1e90="+isNear(1e100d,1e100d+1e90d)+" (expected false)");
		System.out.println("1e-15\u2248-1e-15="+isNear(1e-15d,-1e-15d)+" (expected true)");
		System.out.println("1e-10\u2248-1e-10="+isNear(1e-10d,-1e-10d)+" (expected false)");
		System.out.println("--- Near equals, floats ---");
		System.out.println("1e30\u22481e30+1e20="+isNear(1e30f,1e30f+1e20f)+" (expected true)");
		System.out.println("1e30\u22481e30+1e25="+isNear(1e30f,1e30f+1e25f)+" (expected false)");
		System.out.println("1e-10\u2248-1e-10="+isNear(1e-10f,-1e-10f)+" (expected true)");
		System.out.println("1e-5\u2248-1e-5="+isNear(1e-5f,-1e-5f)+" (expected false)");
		System.out.println("--- Median, doubles ---");
		System.out.println("median(1,2,3)="+median(1d,2d,3d)+","+median(1d,3d,2d)+","+median(2d,1d,3d)+","+median(2d,3d,1d)+","+median(3d,1d,2d)+","+median(3d,2d,1d)+" (expected 2,2,2,2,2,2)");
		System.out.println("median(-1e307,-1,9)="+median(-1e307d,-1d,9d)+","+median(-1e307d,9d,-1d)+","+median(-1d,-1e307d,9d)+","+median(-1d,9d,-1e307d)+","+median(9d,-1e307d,-1d)+","+median(9d,-1d,-1e307d)+" (expected -1,-1,-1,-1,-1,-1)");
		System.out.println("median(0,0,1e-307)="+median(0d,0d,1e-307d)+","+median(0d,1e-307d,0d)+","+median(1e-307d,0d,0d)+" (expected 0,0,0)");
		System.out.println("--- Median, floats ---");
		System.out.println("median(1,2,3)="+median(1f,2f,3f)+","+median(1f,3f,2f)+","+median(2f,1f,3f)+","+median(2f,3f,1f)+","+median(3f,1f,2f)+","+median(3f,2f,1f)+" (expected 2,2,2,2,2,2)");
		System.out.println("median(-1e38,-1,9)="+median(-1e38f,-1f,9f)+","+median(-1e38f,9f,-1f)+","+median(-1f,-1e38f,9f)+","+median(-1f,9f,-1e38f)+","+median(9f,-1e38f,-1f)+","+median(9f,-1f,-1e38f)+" (expected -1,-1,-1,-1,-1,-1)");
		System.out.println("median(0,0,1e-37)="+median(0f,0f,1e-37f)+","+median(0f,1e-37f,0f)+","+median(1e-37f,0f,0f)+" (expected 0,0,0)");
		System.out.println("--- Evenness, doubles ---");
		System.out.println("evenness(1)="+evenness(0.25d)+","+evenness(0.5d)+","+evenness(1d)+","+evenness(2d)+","+evenness(4d)+" (expected 52,52,52,52,52)");
		System.out.println("evenness(3)="+evenness(0.375d)+","+evenness(0.75d)+","+evenness(1.5d)+","+evenness(3d)+","+evenness(6d)+" (expected 51,51,51,51,51)");
		System.out.println("evenness(1/3)="+evenness(1d/3)+","+evenness(2d/3)+","+evenness(4d/3)+","+evenness(8d/3)+","+evenness(16d/3)+" (expected 0,0,0,0,0)");
		System.out.println("evenness(1/5)="+evenness(1d/5)+","+evenness(2d/5)+","+evenness(4d/5)+","+evenness(8d/5)+","+evenness(16d/5)+" (expected 1,1,1,1,1)");
		System.out.println("evenness(1/9)="+evenness(1d/9)+","+evenness(2d/9)+","+evenness(4d/9)+","+evenness(8d/9)+","+evenness(16d/9)+" (expected 2,2,2,2,2)");
		System.out.println("--- Evenness, floats ---");
		System.out.println("evenness(1)="+evenness(0.25f)+","+evenness(0.5f)+","+evenness(1f)+","+evenness(2f)+","+evenness(4f)+" (expected 23,23,23,23,23)");
		System.out.println("evenness(3)="+evenness(0.375f)+","+evenness(0.75f)+","+evenness(1.5f)+","+evenness(3f)+","+evenness(6f)+" (expected 22,22,22,22,22)");
		System.out.println("evenness(1/3)="+evenness(1f/3)+","+evenness(2f/3)+","+evenness(4f/3)+","+evenness(8f/3)+","+evenness(16f/3)+" (expected 0,0,0,0,0)");
		System.out.println("evenness(1/19)="+evenness(1f/19)+","+evenness(2f/19)+","+evenness(4f/19)+","+evenness(8f/19)+","+evenness(16f/19)+" (expected 1,1,1,1,1)");
		System.out.println("evenness(1/11)="+evenness(1f/11)+","+evenness(2f/11)+","+evenness(4f/11)+","+evenness(8f/11)+","+evenness(16f/11)+" (expected 2,2,2,2,2)");
	}
}
