package org.epici.wavelets.util.ui;

/**
 * Utility class for light based on standard models of physics.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class Light {
	
	/**
	 * The speed of light in a vacuum (<i>c</i>), in metres per second, which is 299792458.
	 * This number is exact because the metre is defined as the distance
	 * travelled by light in a vacuum in 1/299792458 of a second.
	 * <br><br>
	 * The wavelength (<i>&lambda;</i>) and frequency (<i>f</i>)
	 * of a light wave are related by:<br>
	 * <i>&lambda;f=c</i>
	 * <br><br>
	 * It is provided here as an integer.
	 * Please remember to cast to the correct data type before using!
	 */
	public static final int SPEED = 299792458;
	
	// No constructor!
	private Light() {}
	
	/**
	 * x bar function for the CIE 1931 standard observer.
	 * <br>
	 * Given a wavelength of light in metres, gives the responsivity.
	 * <br><br>
	 * This is using a multi-lobed piecewise Gaussian approximation
	 * as described in the paper
	 * <a href="http://jcgt.org/published/0002/02/01/paper.pdf">
	 * <i>Simple Analytic Approximations to the CIE XYZ Color Matching Functions</i>
	 * by Chris Wyman, Peter-Pike Sloan, and Peter Shirley,
	 * which was published in the Journal of Computer Graphics Techniques
	 * in 2013.</a>
	 * <br>
	 * It can be further micro-optimized, but instead it is left
	 * close to the original form so it is clear and recognizable.
	 * Large exponents of 10 in the code are due to the original function having
	 * its input parameter be in nanometres.
	 * 
	 * @param wavelength wavelength of the light in metres
	 * @return responsivity
	 */
	public static double cie1931xbar(double wavelength) {
		double t1 = (wavelength-4.420e-7)*((wavelength<4.420e-7)?0.0624:0.0374);
		double t2 = (wavelength-5.998e-7)*((wavelength<5.998e-7)?0.0264:0.0323);
		double t3 = (wavelength-5.011e-7)*((wavelength<5.011e-7)?0.0490:0.0382);
		return   0.362*Math.exp(-5e17*t1*t1)
				+1.056*Math.exp(-5e17*t2*t2)
				-0.065*Math.exp(-5e17*t3*t3);
	}
	
	/**
	 * y bar function for the CIE 1931 standard observer.
	 * <br>
	 * Given a wavelength of light in metres, gives the responsivity.
	 * For Y specifically, this is also the luminance.
	 * <br><br>
	 * This is using a multi-lobed piecewise Gaussian approximation
	 * as described in the paper
	 * <a href="http://jcgt.org/published/0002/02/01/paper.pdf">
	 * <i>Simple Analytic Approximations to the CIE XYZ Color Matching Functions</i>
	 * by Chris Wyman, Peter-Pike Sloan, and Peter Shirley,
	 * which was published in the Journal of Computer Graphics Techniques
	 * in 2013.</a>
	 * <br>
	 * It can be further micro-optimized, but instead it is left
	 * close to the original form so it is clear and recognizable.
	 * Large exponents of 10 in the code are due to the original function having
	 * its input parameter be in nanometres.
	 * 
	 * @param wavelength wavelength of the light in metres
	 * @return responsivity
	 */
	public static double cie1931ybar(double wavelength) {
		double t1 = (wavelength-5.688e-7)*((wavelength<5.688e-7)?0.0213:0.0247);
		double t2 = (wavelength-5.309e-7)*((wavelength<5.309e-7)?0.0613:0.0322);
		return   0.821*Math.exp(-5e17*t1*t1)
				+0.286*Math.exp(-5e17*t2*t2);
	}
	
	/**
	 * z bar function for the CIE 1931 standard observer.
	 * <br>
	 * Given a wavelength of light in metres, gives the responsivity.
	 * <br><br>
	 * This is using a multi-lobed piecewise Gaussian approximation
	 * as described in the paper
	 * <a href="http://jcgt.org/published/0002/02/01/paper.pdf">
	 * <i>Simple Analytic Approximations to the CIE XYZ Color Matching Functions</i>
	 * by Chris Wyman, Peter-Pike Sloan, and Peter Shirley,
	 * which was published in the Journal of Computer Graphics Techniques
	 * in 2013.</a>
	 * <br>
	 * It can be further micro-optimized, but instead it is left
	 * close to the original form so it is clear and recognizable.
	 * Large exponents of 10 in the code are due to the original function having
	 * its input parameter be in nanometres.
	 * 
	 * @param wavelength wavelength of the light in metres
	 * @return responsivity
	 */
	public static double cie1931zbar(double wavelength) {
		double t1 = (wavelength-4.370e-7)*((wavelength<4.370e-7)?0.0845:0.0278);
		double t2 = (wavelength-4.590e-7)*((wavelength<4.590e-7)?0.0385:0.0725);
		return   1.217*Math.exp(-5e17*t1*t1)
				+0.681*Math.exp(-5e17*t2*t2);
	}
	
}
