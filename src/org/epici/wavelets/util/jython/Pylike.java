package org.epici.wavelets.util.jython;

import java.util.function.Predicate;
import java.util.function.Supplier;
import com.google.common.base.Suppliers;

/**
 * Static utility class for working with Java things
 * but using Python idioms and constructs.
 * Helps bridge the gap between Java and Python users.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public final class Pylike {
	
	// Deny constructor
	private Pylike() {};
	
	// original functions below
	
	/**
	 * Python's {@code or} keyword. If the left hand side is truthy,
	 * return that, otherwise return the right hand side.
	 * 
	 * @param a produces the left hand side
	 * @param b produces the right hand side
	 * @param bool truthiness test
	 * @return a or b
	 */
	public static <T> T or(Supplier<? extends T> a, Supplier<? extends T> b, Predicate<? super T> bool) {
		T ra = a.get();
		return bool.test(ra)?ra:b.get();
	}
	
	/**
	 * Python's {@code and} keyword. If the left hand side is falsy,
	 * return that, otherwise return the right hand side.
	 * 
	 * @param a produces the left hand side
	 * @param b produces the right hand side
	 * @param bool truthiness test
	 * @return a and b
	 */
	public static <T> T and(Supplier<? extends T> a, Supplier<? extends T> b, Predicate<? super T> bool) {
		T ra = a.get();
		return bool.test(ra)?b.get():ra;
	}
	
	// --- lots of aliases below ---
	
	/**
	 * Python's {@code or} keyword. If the left hand side is truthy,
	 * return that, otherwise return the right hand side.
	 * 
	 * @param a the left hand side
	 * @param b produces the right hand side
	 * @param bool truthiness test
	 * @return a or b
	 */
	public static <T> T or(T a, Supplier<? extends T> b, Predicate<? super T> bool) {
		return or(Suppliers.ofInstance(a), b, bool);
	}

	/**
	 * Python's {@code or} keyword. If the left hand side is truthy,
	 * return that, otherwise return the right hand side.
	 * 
	 * @param a the left hand side
	 * @param b the right hand side
	 * @param bool truthiness test
	 * @return a or b
	 */
	public static <T> T or(T a, T b, Predicate<? super T> bool) {
		return or(Suppliers.ofInstance(a), Suppliers.ofInstance(b), bool);
	}

	/**
	 * Python's {@code and} keyword. If the left hand side is falsy,
	 * return that, otherwise return the right hand side.
	 * 
	 * @param a the left hand side
	 * @param b produces the right hand side
	 * @param bool truthiness test
	 * @return a and b
	 */
	public static <T> T and(T a, Supplier<? extends T> b, Predicate<? super T> bool) {
		return and(Suppliers.ofInstance(a), b, bool);
	}

	/**
	 * Python's {@code and} keyword. If the left hand side is falsy,
	 * return that, otherwise return the right hand side.
	 * 
	 * @param a the left hand side
	 * @param b the right hand side
	 * @param bool truthiness test
	 * @return a and b
	 */
	public static <T> T and(T a, T b, Predicate<? super T> bool) {
		return and(Suppliers.ofInstance(a), Suppliers.ofInstance(b), bool);
	}

}
