package org.epici.wavelets.util.waveform;

import org.epici.wavelets.core.Curve;

/**
 * Utility class with methods for sampling primitive waveforms
 * <br><br>
 * The provided sine, square, saw and triangle are normalized
 * and phase shifted to have period 1, minimum -1, maximum 1.
 * They also share several key input-output pairs:
 * <ul>
 * <li>0, 0 (sine<sup>[1]</sup>, square, saw, triangle)</li>
 * <li>1/4, 1 (sine<sup>[1]</sup>, square, triangle)</li>
 * <li>1/2, 0 (sine<sup>[1]</sup>, square<sup>[2]</sup>, triangle)</li>
 * <li>3/4, -1 (sine<sup>[1]</sup>, square, triangle)</li>
 * </ul>
 * In addition, the derivative at input 0 is positive for all of them,
 * so for some small value <i>&epsilon;</i>, <i>f(-&epsilon;) &lt; f(0) &lt; f(&epsilon;)</i>.
 * <br>
 * [1] May be slightly off due to precision problems.
 * <br>
 * [2] May not match due to remainder strangeness.
 * See {@link Math#IEEEremainder(double, double)} for details.
 * 
 * @author EPICI
 * @version 1.0
 */
public final class PrimitiveWaveforms {
	private PrimitiveWaveforms(){}
	
	/**
	 * Standard sine wave. See {@link PrimitiveWaveforms} for usage information.
	 * 
	 * @param phase the phase
	 * @return unit sine
	 */
	public static double unitSine(double phase){
		return Math.sin(phase*(Math.PI*2));
	}
	
	/**
	 * Standard square wave. See {@link PrimitiveWaveforms} for usage information.
	 * 
	 * @param phase the phase
	 * @return unit square
	 */
	public static double unitSquare(double phase){
		return Math.signum(Math.IEEEremainder(phase, 1d));
	}
	
	/**
	 * Standard saw wave. See {@link PrimitiveWaveforms} for usage information.
	 * 
	 * @param phase the phase
	 * @return unit saw
	 */
	public static double unitSaw(double phase){
		return Math.IEEEremainder(phase, 1d)*2d;
	}
	
	/**
	 * Standard triangle wave. See {@link PrimitiveWaveforms} for usage information.
	 * 
	 * @param phase the phase
	 * @return unit triangle
	 */
	public static double unitTriangle(double phase){
		return Math.abs(Math.IEEEremainder(phase+0.25d, 1d)*4d)-1d;
	}
	
	/**
	 * Evaluates the curve at <i>phase</i> modulo 1.
	 * The argument to the curve's {@link Curve#valueAtPosition(double)}
	 * will be in the range [0, 1].
	 * 
	 * @param view the curve to evaluate
	 * @param phase the point at which to evaluate, modulo 1
	 * @return the evaluation of the curve
	 */
	public static double unitCurve(Curve view,double phase){
		return view.valueAtPosition(Math.IEEEremainder(phase-0.5d, 1d)+0.5d);
	}
}
