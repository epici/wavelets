package org.epici.wavelets.util.ds;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import com.google.common.collect.AbstractIterator;

/**
 * An iterator which combines <i>n</i> iterators using "zip" ordering:
 * <br>
 * <i>
 * I<sub>0</sub>[0], I<sub>1</sub>[0], I<sub>2</sub>[0],
 * &#x22EF;, I<sub>n-1</sub>[0],
 * I<sub>0</sub>[1], I<sub>1</sub>[1], I<sub>2</sub>[1],
 * &#x22EF;, I<sub>n-1</sub>[1],
 * &#x22EF;,
 * I<sub>0</sub>[m], I<sub>1</sub>[m], I<sub>2</sub>[m],
 * &#x22EF;, I<sub>n-1</sub>[m]
 * </i>
 * <br>
 * In the case that the iterators are different lengths,
 * will skip the exhausted iterators and continue until all
 * iterators are exhausted.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 *
 * @param <T> type of iterator
 */
public class ZipUngroupedIterator<T> extends AbstractIterator<T> {
	
	/**
	 * All iterators that have not been exhausted yet.
	 * <br>
	 * The most recently used iterator is guaranteed to be at the end,
	 * which is returned by {@link Deque#pollLast()}.
	 * <br>
	 * Currenlty implemented using a {@link ArrayDeque}.
	 */
	protected final ArrayDeque<Iterator<T>> iterators;
	/**
	 * Whether the iterator queue is accessible to outside users.
	 */
	protected final boolean mutable;
	
	/**
	 * Standard constructor, taking the iterators from a collection.
	 * 
	 * @param iterators iterators, in order to be zipped
	 * @param mutable whether the combined iterator should be modifiable
	 */
	public ZipUngroupedIterator(Collection<Iterator<T>> iterators, boolean mutable) {
		this.iterators = new ArrayDeque<>(iterators);
		this.mutable = mutable;
	}
	
	/**
	 * Is it mutable? If true, then {@link #getIterators()}
	 * will return the iterator stack to permit modification.
	 * 
	 * @return whether this iterator concatenation is mutable
	 */
	public boolean isMutable() {
		return mutable;
	}
	
	/**
	 * Get the iterator deque. Returns {@code null} instead if
	 * this is not mutable according to {@link #isMutable()}.
	 * 
	 * @return {@link #iterators} if mutable, otherwise null
	 */
	public Deque<Iterator<T>> getIterators() {
		return isMutable()?iterators:null;
	}
	
	/**
	 * Get the most recently used iterator.
	 * Returns {@code null} instead if
	 * this is not mutable according to {@link #isMutable()},
	 * or if all iterators have been exhausted.
	 * <br>
	 * May produce unexpected results if called before the first fetch.
	 * 
	 * @return the most recently used iterator
	 */
	public Iterator<T> getLastIterator() {
		if(!isMutable())return null;
		return iterators.isEmpty()?null:iterators.peekLast();
	}

	@Override
	protected T computeNext() {
		while(!iterators.isEmpty()) {
			Iterator<T> nextIterator = iterators.pollFirst();
			if(nextIterator.hasNext()) {
				iterators.addLast(nextIterator);
				return nextIterator.next();
			}
		}
		return endOfData();
	}
	
	/**
	 * Tell this combined iterator to drop all iterators,
	 * effectively ending iteration. Will fail if this is
	 * not mutable according to {@link #isMutable()}.
	 * 
	 * @return true on success, false on failure
	 */
	public boolean interrupt() {
		boolean mutable = isMutable();
		if(mutable) {
			iterators.clear();
		}
		return mutable;
	}

}
