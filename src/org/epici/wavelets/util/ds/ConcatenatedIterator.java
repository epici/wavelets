package org.epici.wavelets.util.ds;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.collections4.list.TreeList;
import com.google.common.collect.AbstractIterator;

/**
 * Concatenation of iterators. Will exhaust each iterator
 * before moving on to the next.
 * <br>
 * However, this implementation has the unusual aspect that the iterator
 * list can be modified during iteration, allowing for interruption or
 * injecting new iterators.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 *
 * @param <T> type of iterator
 */
public class ConcatenatedIterator<T> extends AbstractIterator<T> {
	
	/**
	 * All iterators, as a list. When retrieving the next element,
	 * will look through in forward order until a suitable iterator is found,
	 * removing empty iterators along the way.
	 * <br>
	 * The most recently used iterator is guaranteed to be at index 0.
	 * <br>
	 * Currently implemented as a {@link TreeList} to keep the
	 * worst case performance good.
	 */
	protected final TreeList<Iterator<T>> iterators;
	/**
	 * Whether the iterator list is accessible to outside users.
	 */
	protected final boolean mutable;
	
	/**
	 * Standard constructor, supplying iterators in desired order and mutability flag.
	 * 
	 * @param iterators iterators, in order to be concatenated
	 * @param mutable whether the concatenation should be modifiable
	 */
	public ConcatenatedIterator(Collection<Iterator<T>> iterators, boolean mutable) {
		this.iterators = new TreeList<>(iterators);
		Collections.reverse(this.iterators);
		this.mutable = mutable;
	}
	
	/**
	 * Is it mutable? If true, then {@link #getIterators()}
	 * will return the iterator stack to permit modification.
	 * 
	 * @return whether this iterator concatenation is mutable
	 */
	public boolean isMutable() {
		return mutable;
	}
	
	/**
	 * Get the iterator list. Returns {@code null} instead if
	 * this is not mutable according to {@link #isMutable()}.
	 * 
	 * @return {@link #iterators} if mutable, otherwise null
	 */
	public List<Iterator<T>> getIterators() {
		return isMutable()?iterators:null;
	}
	
	/**
	 * Get the most recently used iterator.
	 * Returns {@code null} instead if
	 * this is not mutable according to {@link #isMutable()},
	 * or if all iterators have been exhausted.
	 * <br>
	 * May produce unexpected results if called before the first fetch.
	 * 
	 * @return the most recently used iterator
	 */
	public Iterator<T> getLastIterator() {
		if(!isMutable())return null;
		return iterators.isEmpty()?null:iterators.get(0);
	}

	@Override
	protected T computeNext() {
		while(!iterators.isEmpty()) {
			Iterator<T> nextIterator = iterators.remove(0);
			if(nextIterator.hasNext()) {
				iterators.add(0, nextIterator);
				return nextIterator.next();
			}
		}
		return endOfData();
	}
	
	/**
	 * Tell this concatenation to drop all iterators,
	 * effectively ending iteration. Will fail if this is
	 * not mutable according to {@link #isMutable()}.
	 * 
	 * @return true on success, false on failure
	 */
	public boolean interrupt() {
		boolean mutable = isMutable();
		if(mutable) {
			iterators.clear();
		}
		return mutable;
	}

}
