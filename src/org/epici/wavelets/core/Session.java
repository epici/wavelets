package org.epici.wavelets.core;

import java.awt.Color;
import java.awt.color.ColorSpace;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import javax.swing.*;
import org.apache.pivot.wtk.*;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.core.curve.CurvePolynomial;
import org.epici.wavelets.core.synth.*;
import org.epici.wavelets.core.track.LayeredTrack;
import org.epici.wavelets.core.track.PatternTrack;
import org.epici.wavelets.ui.*;
import org.epici.wavelets.ui.curve.DoubleInput;
import org.epici.wavelets.ui.curve.bezierspline.CurveBezierSplinePreview;
import org.epici.wavelets.ui.curve.bezierspline.CurveBezierSplinePreviewSkin;
import org.epici.wavelets.ui.pattern.PatternEditor;
import org.epici.wavelets.ui.synth.primitives.SynthPrimitivesPreview;
import org.epici.wavelets.ui.synth.primitives.SynthPrimitivesPreviewSkin;
import org.epici.wavelets.ui.track.pattern.PatternTrackEditor;
import org.epici.wavelets.ui.track.pattern.PatternTrackPreview;
import org.epici.wavelets.ui.track.pattern.PatternTrackPreviewSkin;
import org.epici.wavelets.util.Bits;
import org.epici.wavelets.util.jython.Pylike;
import org.epici.wavelets.util.math.Bezier;
import org.epici.wavelets.util.ui.PivotSwingUtils;
import org.hsluv.HUSLColorConverter;

import com.google.common.collect.AbstractIterator;

/**
 * Represents an active session
 * 
 * @author EPICI
 * @version 1.0
 */
public class Session {
	
	/**
	 * The clipboard, used for copying and pasting objects.
	 * Accessed with {@link #setClipBoard(Object)} and {@link #getClipBoard()}.
	 * It is usually used for transferring data rather than creating
	 * copies.
	 * <br>
	 * Hotkey conventions are:
	 * <table>
	 * <tr>
	 *   <th>Key</th>
	 *   <th>Effect</th>
	 * </tr>
	 * <tr>
	 *   <td>Shift</td>
	 *   <td>Copy, target will receive a different reference
	 *   or distinct wrapper</td>
	 * </tr>
	 * <tr>
	 *   <td>Control or Alt</td>
	 *   <td>No copy, target will receive the same reference
	 *   or equivalent wrapper</td>
	 * </tr>
	 * <tr>
	 *   <td>C</td>
	 *   <td>Write reference to clipboard</td>
	 * </tr>
	 * <tr>
	 *   <td>V</td>
	 *   <td>Read reference from clipboard</td>
	 * </tr>
	 * <tr>
	 *   <td>D</td>
	 *   <td>Duplicate reference without accessing clipboard</td>
	 * </tr>
	 * </table>
	 */
	protected Object clipBoard;
	
	/**
	 * Log2 of buffer size
	 */
	protected int bufferSizeShift = 7;
	/**
	 * Samples in buffer
	 */
	protected int bufferSize = 1<<bufferSizeShift;
	/**
	 * Samples per second
	 */
	protected int sampleRate = 44100;
	
	/**
	 * The project being edited
	 */
	public Project project;
	/**
	 * The filename to load from and save to
	 */
	public String filename;
	
	/**
	 * The window manager object
	 */
	public WindowManager windowManager;
	/**
	 * The window for the window manager
	 */
	public JInternalFrame windowManagerFrame;
	
	/**
	 * The desktop pane that contains everything in the UI
	 */
	public JDesktopPane desktopPane;
	/**
	 * The JFrame that holds the JDesktopPane
	 */
	public JFrame mainFrame;
	
	/**
	 * Color scheme, from TerraTheme
	 */
	protected ColorScheme colors;
	
	/**
	 * Preferences/settings
	 */
	protected Preferences preferences;
	
	/**
	 * Pointer to the console or whatever was originally stdout
	 */
	protected PrintStream console;
	
	/**
	 * Audio player
	 */
	protected PlayerBuffered player;
	
	/**
	 * A special marker used as the time for when no audio
	 * is playing, and the point from which audio starts playing
	 * <br>
	 * Always update this together with <i>timeCursorEnd</i>
	 */
	public double timeCursor;
	/**
	 * If there is a time range selection, this is the other end of it
	 * <br>
	 * If this is equal to <i>timeCursor</i> it is assumed that no range
	 * is selected
	 */
	public double timeCursorEnd;
	
	/**
	 * Common names of classes which the user can access.
	 * Removing existing values may break code which assumes it,
	 * so only add names for your own classes.
	 */
	public Map<Class<?>,String> commonNames;
	
	/**
	 * Default constructor, try to find preferences on its own
	 */
	public Session(){
		init(null);
	}
	
	/**
	 * Initialize (reset) with given location at which preferences should be found
	 * 
	 * @param prefSource filename/location, or null for default
	 */
	public void init(String prefSource){
		// Initialize Apache Pivot skins
		Theme theme = Theme.getTheme();
		colors = ColorScheme.getPivotColors();
		colors = Pylike.or(uniqifyColorSchemeDefault(colors), colors, Objects::nonNull);
		theme.set(CircularSlider.class, CircularSliderSkin.class);
		theme.set(PatternTrackPreview.class, PatternTrackPreviewSkin.class);
		theme.set(PatternTrackEditor.LinkedEditorPane.class, PatternTrackEditor.LinkedEditorPaneSkin.class);
		theme.set(PatternEditor.LinkedEditorInnerPane.class, PatternEditor.LinkedEditorInnerPaneSkin.class);
		theme.set(DoubleInput.DoubleSlider.class, DoubleInput.DoubleSliderSkin.class);
		theme.set(
				org.epici.wavelets.ui.curve.bezierspline.GraphicalEditorPane.class,
				org.epici.wavelets.ui.curve.bezierspline.GraphicalEditorPaneSkin.class
				);
		theme.set(SynthPrimitivesPreview.class, SynthPrimitivesPreviewSkin.class);
		theme.set(CurveBezierSplinePreview.class, CurveBezierSplinePreviewSkin.class);
		// create org.epici.wavelets.core UI
		windowManager = PivotSwingUtils.loadBxml(WindowManager.class, "windowManager.bxml");
		windowManager.session = this;
		windowManagerFrame = PivotSwingUtils.wrapPivotWindow(windowManager);
		desktopPane = new JDesktopPane();
		mainFrame = new JFrame("Wavelets");
		mainFrame.setContentPane(desktopPane);
		desktopPane.add(windowManagerFrame);
		PivotSwingUtils.showFrameDefault(windowManagerFrame);
		desktopPane.setVisible(true);
		mainFrame.setSize(1280, 720);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(new java.awt.event.WindowListener(){

			@Override
			public void windowOpened(WindowEvent e) {
			}

			@Override
			public void windowClosing(WindowEvent e) {
				// When you click the close button
				// TODO ask to save changes
				mainFrame.dispose();
				// exit
				System.exit(0);
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}
			
		});
		mainFrame.setResizable(true);
		mainFrame.setVisible(true);
		// make the audio player
		player = new PlayerBuffered(bufferSize);
		player.session = this;
		// make the common names map
		commonNames = new IdentityHashMap<>();
		commonNames.put(Project.class, "Project");
		commonNames.put(Track.class, "Track");
		commonNames.put(PatternTrack.class, "Pattern Track");
		commonNames.put(LayeredTrack.class, "Layered Track");
		commonNames.put(Clip.Template.class, "Clip Template");
		commonNames.put(Curve.class, "1D Curve");
		commonNames.put(Pattern.class, "Pattern");
		commonNames.put(Synthesizer.class, "Synthesizer");
		commonNames.put(SynthPrimitives.class, "Primitives Synth");
		commonNames.put(VarDouble.Single.class, "Constant");
		commonNames.put(VarDouble.Array.class, "Array Indexed Constant");
		commonNames.put(VarDouble.Map.class, "Map Keyed Constant");
		commonNames.put(CurvePolynomial.class, "Polynomial Curve");
		commonNames.put(CurveBezierSpline.class, "Bezier Spline Curve");
	}
	
	/**
	 * Utility method, will open the UI of some track
	 * <br>
	 * JInternalFrame instances get opened as expected,
	 * otherwise it's added to a shared editor window
	 * 
	 * @param track the track to open the UI for
	 */
	public void openUI(Track track){
		MetaComponent<? extends JComponent> ometa = track.getUI();
		JComponent component = ometa.component;
		if(component instanceof JInternalFrame){
			MetaComponent<JInternalFrame> meta = new MetaComponent<>(ometa);
			windowManager.addWindow(meta,true);
		}else{
			// not supported yet
		}
	}
	
	/**
	 * Delegated method for creating a new project object,
	 * primary purpose is to allow customization
	 */
	public void newProject(){
		project = new Project(this);
		filename = null;
	}
	
	/**
	 * Getter for {@link #clipBoard}.
	 * 
	 * @return the current object on the virtual clipboard, which is copied
	 */
	public Object getClipBoard() {
		return clipBoard;
	}

	/**
	 * Setter for {@link #clipBoard}.
	 * 
	 * @param clipBoard the object to set the virtual clipboard to, the new object to copy
	 */
	public void setClipBoard(Object clipBoard) {
		this.clipBoard = clipBoard;
	}

	/**
	 * Method to allow outside users to get the player
	 * in order to play audio or do other things with it
	 * 
	 * @return the current audio player
	 */
	public Player getPlayer(){
		return player;
	}
	
	/**
	 * If the project or a track is currently
	 * being previewed, this gets the current time
	 * in the project, otherwise it returns
	 * the location of the time cursor
	 * 
	 * @return the current time in the project according to the audio player
	 */
	public double getCurrentTime(){
		if(player.isPlaying()){
			return player.currentTime();
		}else{
			return timeCursor;
		}
	}
	
	/**
	 * Get the sample rate in Hz
	 * 
	 * @return the sample rate
	 */
	public int getSampleRate(){
		return sampleRate;
	}
	
	/**
	 * Gets the buffer size in samples
	 * 
	 * @return the buffer size, a power of 2
	 */
	public int getBufferSize(){
		return bufferSize;
	}
	
	/**
	 * Sets the buffer size in samples
	 * 
	 * @param n the buffer size, a power of 2
	 */
	public void setBufferSize(int n){
		setBufferSizeLog(Bits.binLog(n));
	}
	
	/**
	 * Sets the buffer size in samples to 2^n
	 * 
	 * @param n will get set to 2^n
	 */
	public void setBufferSizeLog(int n){
		if(n>=4&&n<=12){
			bufferSizeShift = n;
			bufferSize = 1<<bufferSizeShift;
		}else{
			throw new IllegalArgumentException("n ("+n+") must be between 4 and 12 inclusive");
		}
	}
	
	/**
	 * Get the current color scheme
	 * 
	 * @return the current color scheme
	 */
	public ColorScheme getColors(){
		return colors;
	}
	
	/**
	 * Get the current color scheme used in a session
	 * <br>
	 * If the session or its colors are null, return default colors,
	 * so this will never fail or return null
	 * 
	 * @param session current session
	 * @return color scheme to use for that session
	 */
	public static ColorScheme getColors(Session session){
		ColorScheme result;
		if(session==null||(result=session.getColors())==null)return ColorScheme.getPivotColors();
		return result;
	}
	
	/**
	 * Get the current preferences
	 * 
	 * @return the current preferences
	 */
	public Preferences getPreferences(){
		return preferences;
	}
	
	/**
	 * Get the active {@link PrintStream} which should be used
	 * everywhere else in place of System.out and System.err
	 * 
	 * @return the target output
	 */
	public PrintStream getConsole(){
		return console;
	}
	
	private static final String SESSION_CLASS_NAME = Session.class.getCanonicalName();
	private static final String PREFERENCES_CLASS_NAME = Preferences.class.getCanonicalName();
	private static final String COMPOSITION_CLASS_NAME = Project.class.getCanonicalName();
	private static final String PLAYER_CLASS_NAME = Player.class.getCanonicalName();
	/**
	 * For an options map for {@link BetterClone#copy(int, Map)},
	 * add some common options, including the current session.
	 * 
	 * @param options options map to modify
	 */
	public void setCopyOptions(Map<String,Object> options){
		// add current session
		options.put("session", this);
		// blacklist big classes
		Collection<String> copyBlacklist = (Collection<String>) options.get("blacklist");
		copyBlacklist.add("*"+SESSION_CLASS_NAME);
		copyBlacklist.add("*"+PREFERENCES_CLASS_NAME);
		copyBlacklist.add("*"+COMPOSITION_CLASS_NAME);
		copyBlacklist.add("*"+PLAYER_CLASS_NAME);
	}
	
	/**
	 * Get the human-readable common name for a class.
	 * Currently does not use localization.
	 * 
	 * @param cls class to get name for
	 * @return name string
	 */
	public String getCommonName(Class<?> cls){
		// if it's in the map, return that
		String name = commonNames.get(cls);
		if(name!=null)return name;
		// give up
		return cls.getSimpleName();
	}
	
	/**
	 * Perform a default uniqification on a color scheme using
	 * {@link ColorScheme#uniqify(java.util.function.BiPredicate, Iterator, int)}.
	 * 
	 * @param colorScheme original color scheme
	 * @return derived color scheme, or null if failed
	 */
	public static ColorScheme uniqifyColorSchemeDefault(ColorScheme colorScheme) {
		BiPredicate<Color,Color> similar = (Color a,Color b)->ColorScheme.colorDistance(a, b)<10;
		BiFunction<Integer,Color,Color> mutator = new BiFunction<Integer,Color,Color>() {

			@Override
			public Color apply(Integer iteration, Color original) {
				ColorSpace ciexyz = ColorSpace.getInstance(ColorSpace.CS_CIEXYZ);
				float[] axyzf = new float[4];
				original.getComponents(ciexyz, axyzf);
				double[] axyzd = new double[3];
				for(int i=0;i<3;i++) {
					axyzd[i] = axyzf[i];
				}
				double[] ahsld = HUSLColorConverter.lchToHsluv(
						HUSLColorConverter.luvToLch(
								HUSLColorConverter.xyzToLuv(axyzd)
								)
						);
				ahsld[0] = (ahsld[0] + 360d / (4 + iteration)) % 360;
				ahsld[1] = 100 * jitter01(ahsld[1] * 0.01, 1.5);
				ahsld[2] = 100 * jitter01(ahsld[2] * 0.01, 1.75);
				axyzd = HUSLColorConverter.luvToXyz(
						HUSLColorConverter.lchToLuv(
						HUSLColorConverter.hsluvToLch(ahsld)
						)
						);
				for(int i=0;i<3;i++) {
					axyzf[i] = (float) axyzd[i];
				}
				float[] argbf = ciexyz.toRGB(axyzf);
				return new Color(argbf[0],argbf[1],argbf[2]);
			}
			
		};
		Iterator<BiFunction<Integer,Color,Color>> mutators = new AbstractIterator<BiFunction<Integer,Color,Color>>(){

			@Override
			protected BiFunction<Integer, Color, Color> computeNext() {
				return mutator;
			}
			
		};
		try {
			return colorScheme.uniqify(similar, mutators, 100);
		} catch (IllegalArgumentException err) {
			err.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Helper function for {@link #uniqifyColorSchemeDefault(ColorScheme)}.
	 * Maps a value in the range [0,1] to another value in that range.
	 * Intended to be used as an iterated function.
	 * 
	 * @param x the value to be mapped
	 * @param sf scale factor; less than 1 will cause convergence on 0.5;
	 * more than 1 will cause divergence and jittering; above sqrt(3) will jump
	 * around the whole range and not bounded by halves;
	 * 2 is highest that's still bounded and is very chaotic
	 * @return jittered value
	 */
	private static double jitter01(double x,double sf) {
		return Bezier.bezier2to4(0, 1, 0.5+sf*(x-0.5));
	}
	
}
