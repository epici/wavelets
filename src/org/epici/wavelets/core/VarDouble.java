package org.epici.wavelets.core;

import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.curve.DoubleInput;
import org.epici.wavelets.ui.curve.constant.CurveConstantEditor;

/**
 * A double holder which can vary with offset
 * <br>
 * Preferred over primitive double fields for anything timely
 * 
 * @author EPICI
 * @version 1.0
 */
public interface VarDouble extends Named {
	/**
	 * Get the value held
	 * <br>
	 * Behaviour depends on implementation, for constants,
	 * <i>time</i> is ignored
	 * 
	 * @param time time in seconds
	 * @return the value held at that time
	 */
	public double get(double time);
	/**
	 * Attempt to set the value field as a constant,
	 * equivalent to calling {@link #set(double, double)} with time=0
	 * <br>
	 * Behaviour depends on implementation, may do nothing
	 * 
	 * @param value a constant to set
	 */
	public default void set(double value){
		set(0,value);
	}
	/**
	 * Attempt to set the value field for a specific time
	 * <br>
	 * Behaviour depends on implementation, may do nothing
	 * 
	 * @param time time to set value for
	 * @param value a value to set
	 */
	public default void set(double time,double value){
	}
	
	/**
	 * Make a double array automatable
	 * <br>
	 * Changes to the original will not affect the new array
	 * 
	 * @param original the array to wrap
	 * @return a AutoDouble array mirroring the contents of the original
	 */
	public static VarDouble[] wrapCopy(double[] original){
		int n = original.length;
		VarDouble[] result = new VarDouble[n];
		for(int i=0;i<n;i++)
			result[i] = new Single(original[i]);
		return result;
	}
	
	/**
	 * Make a double array automatable
	 * <br>
	 * As long as the new array isn't overwritten, changes to the original
	 * will reflect in the new array
	 * 
	 * @param original the array to wrap
	 * @return a AutoDouble array mirroring the contents of the original
	 */
	public static VarDouble[] wrap(double[] original){
		int n = original.length;
		VarDouble[] result = new VarDouble[n];
		for(int i=0;i<n;i++)
			result[i] = new Array(original,i);
		return result;
	}
	
	/**
	 * A simple holder
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @see VarDouble
	 */
	public class Single implements VarDouble, DataEditor.Editable<CurveConstantEditor, DoubleInput>{
		/**
		 * The current value
		 */
		public double v;
		/**
		 * A field to track the name, as required by {@link Named}.
		 */
		public String name;
		
		/**
		 * Constructor where all are specified
		 * 
		 * @param v the value
		 */
		public Single(double v){
			this.v = v;
		}
		
		public double get(double time){
			return v;
		}
		
		public void set(double time,double value){
			v = value;
		}
		
		@Override
		public String getName(){
			if(name==null || name.length()==0)name = Named.defaultName("Variable", this);
			return name;
		}
		
		@Override
		public boolean setName(String newName){
			name = newName;
			return true;
		}
		
		/**
		 * Create a {@link Single} object according to default settings.
		 * In the current implementation this will be named
		 * like &quot;Constant&quot; using
		 * {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, Session)}.
		 * 
		 * @param session current session
		 * @return a new {@link Single} object with default values
		 */
		public static Single makeDefaultVarDouble(Session session){
			// create the object
			Single result = new Single(1);
			// set name
			result.setName(session.project.curves.nextName(
					session.getCommonName(Single.class),
					0, false, session));
			// return
			return result;
		}

		@Override
		public CurveConstantEditor getEditor() {
			CurveConstantEditor result = CurveConstantEditor.createNew();
			result.view = this;
			// don't call init()
			return result;
		}

		@Override
		public DoubleInput getPreview() {
			DoubleInput result = new DoubleInput();
			result.value = this;
			result.valueChanged(true, false);
			return result;
		}
		
	}
	
	/**
	 * References a position in an array
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @see VarDouble
	 */
	public class Array implements VarDouble, DataEditor.Editable<CurveConstantEditor, DoubleInput>{
		/**
		 * Referenced array
		 */
		public double[] values;
		/**
		 * Array index
		 */
		public int index;
		/**
		 * A field to track the name, as required by {@link Named}.
		 */
		public String name;
		
		/**
		 * Constructor where all are specified
		 * 
		 * @param vs the array
		 * @param i the index
		 */
		public Array(double[] vs,int i){
			this.values = vs;
			this.index = i;
		}
		
		public double get(double time){
			return values[index];
		}
		
		public void set(double time,double value){
			values[index] = value;
		}
		
		@Override
		public String getName(){
			if(name==null || name.length()==0)name = Named.defaultName("Variable", this);
			return name;
		}
		
		@Override
		public boolean setName(String newName){
			name = newName;
			return true;
		}

		@Override
		public CurveConstantEditor getEditor() {
			CurveConstantEditor result = CurveConstantEditor.createNew();
			result.view = this;
			// don't call init()
			return result;
		}

		@Override
		public DoubleInput getPreview() {
			DoubleInput result = new DoubleInput();
			result.value = this;
			result.valueChanged(true, false);
			return result;
		}
		
	}
	
	/**
	 * References an entry in a map
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @param <T> the key type
	 * @see VarDouble
	 */
	public class Map<T> implements VarDouble, DataEditor.Editable<CurveConstantEditor, DoubleInput>{
		/**
		 * Referenced map
		 */
		public java.util.Map<T,Double> map;
		/**
		 * Map key
		 */
		public T key;
		/**
		 * A field to track the name, as required by {@link Named}.
		 */
		public String name;
		
		/**
		 * Constructor where all are specified
		 * 
		 * @param map the map
		 * @param key the key
		 */
		public Map(java.util.Map<T,Double> map,T key){
			this.map = map;
			this.key = key;
		}
		
		public double get(double time){
			return map.get(key);
		}
		
		public void set(double time,double value){
			map.put(key, value);
		}
		
		@Override
		public String getName(){
			if(name==null || name.length()==0)name = Named.defaultName("Variable", this);
			return name;
		}
		
		@Override
		public boolean setName(String newName){
			name = newName;
			return true;
		}

		@Override
		public CurveConstantEditor getEditor() {
			CurveConstantEditor result = CurveConstantEditor.createNew();
			result.view = this;
			// don't call init()
			return result;
		}

		@Override
		public DoubleInput getPreview() {
			DoubleInput result = new DoubleInput();
			result.value = this;
			result.valueChanged(true, false);
			return result;
		}
		
	}
	
}
