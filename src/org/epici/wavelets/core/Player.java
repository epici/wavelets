package org.epici.wavelets.core;

/**
 * Real time audio player
 * 
 * @author EPICI
 * @version 1.0
 */
public interface Player extends Destructable {
	/**
	 * Set to play a track
	 * <br><br>
	 * The actual sound may start before the apparent start
	 * and end after the apparent end. So we allow for padding on both sides.
	 * If the <i>timePad</i> parameter is specified,
	 * it will be interpreted as follows:
	 * <ul>
	 * <li>If the length is 1, that value is used
	 * for both start and end pad.</li>
	 * <li>If the length is 2, the first value
	 * is the start pad and the second value
	 * is the end pad.</li>
	 * </ul>
	 * If unspecified, both pads are assumed to be 0.
	 * Implementations may choose to use this argument differently
	 * but should do so in a compatible way.
	 * Negative padding works as expected,
	 * it will cut into the normal range.
	 * 
	 * @param track the track to play
	 * @param loop whether to loop
	 * @param timePad optional seconds to pad at start and end
	 */
	public void playTrack(Track track,boolean loop,double[] timePad);
	/**
	 * Set to play samples
	 * 
	 * @param samples the {@link Samples} object to play
	 * @param loop whether to loop
	 */
	public void playSamples(Samples samples,boolean loop);
	/**
	 * The current time for the player relative to the start
	 * of the track/samples
	 * 
	 * @return the current time within the track/samples being played
	 */
	public double currentTime();
	/**
	 * Is it currently playing?
	 * 
	 * @return true if audio is playing
	 */
	public boolean isPlaying();
	/**
	 * Stop all audio playback
	 */
	public void stopPlay();
	
	/**
	 * Set to play a track
	 * 
	 * @param track the track to play
	 * @param loop whether to loop
	 */
	public default void playTrack(Track track,boolean loop){
		playTrack(track,loop,null);
	}
}
