package org.epici.wavelets.core;

import java.util.*;
import java.io.*;

/**
 * A standalone global preferences object.
 * <br><br>
 * Note that types are referred to by what kind of data they represent,
 * which can differ from the type it is stored in:
 * <table>
 * <tr>
 * <th>Kind of data</th>
 * <th>Represented by type</th>
 * <th>Referred to here as</th>
 * </tr>
 * <tr>
 * <td>Boolean/single bit</td>
 * <td><code>boolean</code></td>
 * <td>boolean</td>
 * </tr>
 * <tr>
 * <td>Integer</td>
 * <td><code>long</code></td>
 * <td>int</td>
 * </tr>
 * <tr>
 * <td>Real</td>
 * <td><code>double</code></td>
 * <td>float</td>
 * </tr>
 * <tr>
 * <td>String/characters</td>
 * <td><code>String</code></td>
 * <td>string</td>
 * </tr>
 * </table>
 * 
 * @author EPICI
 * @version 1.0
 */
public class Preferences implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/*
	 * Indexes for various built-in settings
	 * 
	 * Order reflects the order in which they were added,
	 * which roughly reflects the order they were needed,
	 * which roughly reflects the order in which the consuming code was created
	 * 
	 * Order doesn't have any other significance!
	 */
	
	// Booleans (boolean)
	/**
	 * Used by {@link org.epici.wavelets.ui.track.pattern.PatternTrackEditor.LinkedEditorPaneSkin#allowPatternConvert()}.
	 * Explained by its documentation.
	 */
	public static final int INDEX_BOOLEAN_TLS_ALLOW_PATTERN_CONVERT = 0;
	/**
	 * Used by {@link org.epici.wavelets.ui.track.pattern.PatternTrackEditor.LinkedEditorPaneSkin#allowPatternMerge()}.
	 * Explained by its documentation.
	 */
	public static final int INDEX_BOOLEAN_TLS_ALLOW_PATTERN_MERGE = 1;
	/**
	 * Used by {@link org.epici.wavelets.util.ds.NamedMap#rename(String, String, Session)}.
	 * If true, on rename failure will try to use
	 * {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, Session)}
	 * to find an alternate name to rename to.
	 */
	public static final int INDEX_BOOLEAN_NAMEDMAP_RENAME_USE_NEXTNAME = 2;
	/**
	 * Used by {@link org.epici.wavelets.ui.pattern.PatternEditor.LinkedEditorInnerPaneSkin#allowClipMerge()}.
	 * Explained by its documentation.
	 */
	public static final int INDEX_BOOLEAN_PATTERN_ALLOW_CLIP_MERGE = 3;
	
	// Ints (long)
	/**
	 * Used by {@link org.epici.wavelets.ui.track.pattern.PatternTrackEditor.LinkedEditorPaneSkin#paint(java.awt.Graphics2D)}.
	 * Color is calculated at intervals of (this number) pixels,
	 * pixels between will interpolate.
	 * Determines the resolution/granularity/accuracy/detail of the gradient.
	 * A small interval will get fine visualization of where the synthesizer changes,
	 * at the cost of performance. A large interval will get better performance but
	 * can mislead about changes in the synthesizer or miss them entirely.
	 */
	public static final int INDEX_INT_TLS_PATTERN_BAR_GRADIENT = 0;
	/**
	 * Used by {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, int, int, String, int)}.
	 * Determines the minimum number of digits in the numeric suffix.
	 */
	public static final int INDEX_INT_NAMEDMAP_NEXTNAME_DIGITS_INDEX = 1;
	/**
	 * Used by {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, int, int, String, int)}.
	 * Determines the minimum considered number for suffixes.
	 */
	public static final int INDEX_INT_NAMEDMAP_NEXTNAME_MIN_INDEX = 2;
	/**
	 * Used by {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, int, int, String, int)}.
	 * Determines what number is assumed if the suffix of a name is missing.
	 */
	public static final int INDEX_INT_NAMEDMAP_NEXTNAME_BLANK_INDEX = 3;
	/**
	 * Used by {@link org.epici.wavelets.ui.pattern.PatternEditor.LinkedEditorInnerPaneSkin#paint(java.awt.Graphics2D)}.
	 * First 12 bits determine if that offset from 0 to 11 should be drawn darker.
	 * Next 12 bits determine if that offset from 0 to 11 should be drawn shorter.
	 * Remaining bits are ignored.
	 */
	public static final int INDEX_INT_PATTERN_SYNTH_PREVIEW_BAR_STYLE = 4;
	/**
	 * Used by {@link org.epici.wavelets.ui.pattern.PatternEditor.LinkedEditorInnerPaneSkin#paint(java.awt.Graphics2D)}.
	 * Bit 1 determines if background should be gradient or solid tone.
	 * Bit 2 bit determines if there should be horizontal gridlines.
	 * Remaining bits are ignored.
	 */
	public static final int INDEX_INT_PATTERN_BACKGROUND_STYLE = 5;
	/**
	 * Used by {@link org.epici.wavelets.ui.pattern.PatternEditor.LinkedEditorInnerPaneSkin#paint(java.awt.Graphics2D)}.
	 * Color is calculated at intervals of (this number) pixels,
	 * pixels between will interpolate.
	 * Determines the resolution/granularity/accuracy/detail of the gradient.
	 * A small interval will get fine visualization of where the synthesizer changes,
	 * at the cost of performance. A large interval will get better performance but
	 * can mislead about changes in the synthesizer or miss them entirely.
	 */
	public static final int INDEX_INT_PATTERN_BAR_GRADIENT = 6;
	/**
	 * Used by {@link org.epici.wavelets.ui.pattern.PatternEditor.LinkedEditorInnerPaneSkin#paint(java.awt.Graphics2D)}.
	 * If grid lines would be less than this many pixels apart,
	 * they are not drawn.
	 */
	public static final int INDEX_INT_PATTERN_GRID_MIN = 7;
	
	// Floats (double)
	/**
	 * Used by {@link org.epici.wavelets.ui.curve.bezierspline.GraphicalEditorPaneSkin#paint(java.awt.Graphics2D)}
	 * and its commands. Helps determine how the grid will be drawn.
	 * <br><br>
	 * Let <i>D</i> be the diagonal length of the pane.
	 * <br>
	 * Then define <i>L<sub>min</sub> = D<sup>[MIN]</sup>,
	 * L<sub>max</sub> = D<sup>[MAX]</sup></i>.
	 * <br>
	 * At <i>L<sub>min</sub></i> pixels between or less, grid lines are not drawn.
	 * <br>
	 * Between <i>L<sub>min</sub></i> and <i>L<sub>max</sub></i> pixels between,
	 * grid lines are drawn with transparency. Alpha is interpolated.
	 * <br>
	 * At <i>L<sub>max</sub></i> pixels between or more, grid lines are drawn opaque.
	 */
	public static final int INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MIN = 0;
	/**
	 * See {@link #INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MIN} for usage.
	 */
	public static final int INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MAX = 1;
	
	// Strings (String)
	/**
	 * Used by {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, int, int, String, int)}.
	 * Determines what text separates the main body of the name and the suffix.
	 */
	public static final int INDEX_STRING_NAMEDMAP_NEXTNAME_PARTS_SEPARATOR = 0;
	
	/**
	 * Boolean defaults
	 */
	private static final boolean[] BOOLEAN_DEFAULTS = {
			false,//INDEX_BOOLEAN_TLS_ALLOW_PATTERN_CONVERT
			false,//INDEX_BOOLEAN_TLS_ALLOW_PATTERN_MERGE
			true,//INDEX_BOOLEAN_NAMEDMAP_RENAME_USE_NEXTNAME
			false,//INDEX_BOOLEAN_PATTERN_ALLOW_CLIP_MERGE
	};
	/**
	 * Integer defaults
	 */
	private static final long[] INT_DEFAULTS = {
			1<<30,//INDEX_INT_TLS_PATTERN_BAR_GRADIENT_SHIFT
			3,//INDEX_INT_NAMEDMAP_NEXTNAME_DIGITS_INDEX
			0,//INDEX_INT_NAMEDMAP_NEXTNAME_MIN_INDEX
			-2,//INDEX_INT_NAMEDMAP_NEXTNAME_BLANK_INDEX
			0,//INDEX_INT_PATTERN_SYNTH_PREVIEW_BAR_STYLE
			3,//INDEX_INT_PATTERN_BACKGROUND_STYLE
			1<<6,//INDEX_INT_PATTERN_BAR_GRADIENT_SHIFT
			4,//INDEX_INT_PATTERN_GRID_MIN
	};
	/**
	 * Float defaults
	 */
	private static final double[] FLOAT_DEFAULTS = {
			0.15,//INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MIN
			0.85,//INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MAX
	};
	/**
	 * Float defaults
	 */
	private static final String[] STRING_DEFAULTS = {
			".",//INDEX_STRING_NAMEDMAP_NEXTNAME_PARTS_SEPARATOR
	};
	
	/**
	 * Total number of booleans
	 */
	public static final int BOOLEAN_COUNT = BOOLEAN_DEFAULTS.length;
	/**
	 * Total number of integers
	 */
	public static final int INT_COUNT = INT_DEFAULTS.length;
	/**
	 * Total number of floats
	 */
	public static final int FLOAT_COUNT = FLOAT_DEFAULTS.length;
	/**
	 * Total number of strings
	 */
	public static final int STRING_COUNT = STRING_DEFAULTS.length;
	
	/**
	 * Get a default boolean value
	 * @param index index, should be named
	 * @return the default associated boolean value
	 */
	public static boolean getDefaultBoolean(int index){
		return BOOLEAN_DEFAULTS[index];
	}
	
	/**
	 * Get a default integer value
	 * @param index index, should be named
	 * @return the default associated integer value
	 */
	public static long getDefaultInt(int index){
		return INT_DEFAULTS[index];
	}
	
	/**
	 * Get a default float value
	 * @param index index, should be named
	 * @return the default associated float value
	 */
	public static double getDefaultFloat(int index){
		return FLOAT_DEFAULTS[index];
	}
	
	/**
	 * Get a default string value
	 * @param index index, should be named
	 * @return the default associated string value
	 */
	public static String getDefaultString(int index){
		return STRING_DEFAULTS[index];
	}

	/**
	 * All booleans in preferences
	 * <br>
	 * It's recommended to use ints instead since booleans
	 * only allow for one of two values, and more may be needed later
	 */
	protected BitSet booleans;
	/**
	 * The number of booleans in the bitset
	 */
	protected int localBooleanCount;
	/**
	 * All integers in preferences
	 * <br>
	 * Stored as longs instead because while ints may
	 * be convenient in certain contexts (such as indices and switches),
	 * most of the time longs can do the same thing, and have
	 * the advantage of a larger word size
	 */
	protected long[] ints;
	/**
	 * All floats in preferences, with double precision
	 */
	protected double[] floats;
	/**
	 * All strings in preferences
	 * <br>
	 * Some non-primitive data structures or other values can be
	 * represented simply with a string, so they can also be included here,
	 * however, the cost of parsing may make it not worth it
	 */
	protected String[] strings;
	
	/**
	 * Initialize with factory settings
	 */
	public Preferences(){
		restoreDefaults();
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException{
		out.defaultWriteObject();
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
		in.defaultReadObject();
		extendDefaults();//May be deserialized from an older version
	}
	
	/**
	 * The current data is too short, so extend it with defaults
	 */
	protected void extendDefaults(){
		int localBooleanCount = this.localBooleanCount,
				localIntCount = ints.length,
				localFloatCount = floats.length,
				localStringCount = strings.length;
		ints = Arrays.copyOf(ints, INT_COUNT);
		floats = Arrays.copyOf(floats, FLOAT_COUNT);
		strings = Arrays.copyOf(strings, STRING_COUNT);
		for(;localBooleanCount<BOOLEAN_COUNT;localBooleanCount++)
			booleans.set(localBooleanCount,getDefaultBoolean(localBooleanCount));
		for(;localIntCount<INT_COUNT;localIntCount++)
			ints[localIntCount] = getDefaultInt(localIntCount);
		for(;localFloatCount<FLOAT_COUNT;localFloatCount++)
			floats[localFloatCount] = getDefaultFloat(localFloatCount);
		for(;localStringCount<STRING_COUNT;localStringCount++)
			strings[localStringCount] = getDefaultString(localStringCount);
		this.localBooleanCount = localBooleanCount;
		//subprefs.putAll(SUB_DEFAULTS);
	}
	
	/**
	 * Set all data to default values
	 */
	public void restoreDefaults(){
		booleans = new BitSet();
		localBooleanCount = 0;
		ints = new long[0];
		floats = new double[0];
		extendDefaults();
	}
	
	/**
	 * @return bit set containing all boolean preferences
	 */
	public BitSet getBooleans(){
		return booleans;
	}
	
	/**
	 * @return long array containing all integer preferences
	 */
	public long[] getInts(){
		return ints;
	}
	
	/**
	 * @return double array containing all float preferences
	 */
	public double[] getFloats(){
		return floats;
	}
	
	/**
	 * @return {@link String} array containing all string preferences
	 */
	public String[] getStrings() {
		return strings;
	}
	
	/**
	 * Get boolean for {@link Session}'s preferences, null-safe
	 * 
	 * @param session session to fetch preferences for
	 * @param index index of value to fetch
	 * @return value in preferences, or default if null given
	 */
	public static boolean getBooleanSafe(Session session,int index){
		Preferences pref;
		if(session==null || (pref=session.getPreferences())==null)return getDefaultBoolean(index);
		return pref.booleans.get(index);
	}
	
	/**
	 * Get int (actually long) for {@link Session}'s preferences, null-safe
	 * 
	 * @param session session to fetch preferences for
	 * @param index index of value to fetch
	 * @return value in preferences, or default if null given
	 */
	public static long getIntSafe(Session session,int index){
		Preferences pref;
		if(session==null || (pref=session.getPreferences())==null)return getDefaultInt(index);
		return pref.ints[index];
	}
	
	/**
	 * Get float (actually double) for {@link Session}'s preferences, null-safe
	 * 
	 * @param session session to fetch preferences for
	 * @param index index of value to fetch
	 * @return value in preferences, or default if null given
	 */
	public static double getFloatSafe(Session session,int index){
		Preferences pref;
		if(session==null || (pref=session.getPreferences())==null)return getDefaultFloat(index);
		return pref.floats[index];
	}
	
	/**
	 * Get {@link String} for {@link Session}'s preferences, null-safe
	 * 
	 * @param session session to fetch preferences for
	 * @param index index of value to fetch
	 * @return value in preferences, or default if null given
	 */
	public static String getStringSafe(Session session,int index){
		Preferences pref;
		if(session==null || (pref=session.getPreferences())==null)return getDefaultString(index);
		return pref.strings[index];
	}

}
