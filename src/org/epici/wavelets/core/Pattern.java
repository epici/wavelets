package org.epici.wavelets.core;

import java.util.*;
import java.util.function.Predicate;

import org.epici.wavelets.core.synth.SynthPrimitives;
import org.epici.wavelets.util.jython.*;
import org.python.core.*;

/**
 * A standard pattern containing clips
 * 
 * @author EPICI
 * @version 1.0
 */
public class Pattern implements Destructable, TransientContainer<Project>, Named, BetterClone<Pattern> {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Basically, how many steps in a measure
	 */
	public int divisions;
	/**
	 * Basically, number of measures
	 * <br>
	 * Computed as if the start was at 0, even if
	 * it actually starts later
	 * <br>
	 * The true length is a rational, and this is the ceiling
	 * of that rational
	 */
	public transient int length;
	/**
	 * Contains clip objects
	 */
	public final HashSet<Clip> clips;
	
	/**
	 * The synthesizer to be used for everything, may be shared
	 * <br>
	 * Each synthesizer instance has a unique name, so this is the key
	 * which is used to resolve the instance
	 */
	protected String synthName;
	/**
	 * Synthesizer instance
	 */
	protected transient Synthesizer synthesizer;
	/**
	 * VoiceFactory object used to generate audioProducers
	 * <br>
	 * Not a PyVoiceFactory because there are some Java synths
	 */
	protected transient Factory<AudioProducer> audioProducerFactory;
	
	/**
	 * The name of this instance.
	 * Please use the getter and setter instead.
	 */
	public String name;
	
	/**
	 * Destroyed yet?
	 */
	protected transient boolean destroyed = false;
	
	/**
	 * Standard constructor where everything is specified
	 * 
	 * @param divisions number of steps per measure
	 * @param synthName synthesizer name
	 * @param project parent
	 */
	public Pattern(int divisions,String synthName,Project project){
		this.divisions = divisions;
		this.synthName = synthName;
		clips = new HashSet<>();
		initTransient(project);
	}
	
	/**
	 * Create a pattern object according to default settings.
	 * In the current implementation this will be named
	 * like &quot;Pattern&quot; using
	 * {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, Session)}.
	 * <br>
	 * Note that for the default synthesizer this creates an instance of
	 * {@link SynthPrimitives} and adds it to the project's synthesizers for use.
	 * 
	 * @param session current session
	 * @return a new pattern object with default values
	 */
	public static Pattern makeDefaultPattern(Session session){
		// synth
		Synthesizer synth = SynthPrimitives.makeDefaultSynth(session);
		String synthName = synth.getName();
		Synthesizer.Specification synthSpec = Synthesizer.specWrap(synth);
		session.project.addSynth(synthName, synth, synthSpec);
		// create the object
		Pattern result = new Pattern(
				8,
				synthName,
				session.project);
		// set name
		result.setName(
				session.project.patterns.nextName(
						session.getCommonName(Pattern.class),
						0, false, session));
		// return
		return result;
	}
	
	/**
	 * Method to update the divisions and propagate necessary changes
	 * <br>
	 * Except for internal use, this is the
	 * preferred way to set the divisions
	 * 
	 * @param newDivisions new value to set
	 */
	public void setDivisions(int newDivisions){
		if(newDivisions<1)throw new IllegalArgumentException("divisions ("+newDivisions+") must be positive");
		int oldDivisions = divisions;
		if(oldDivisions==newDivisions)return;
		divisions = newDivisions;
		remakeLength();
	}
	
	/**
	 * Recalculate the field <i>length</i> based on current data.
	 * <br>
	 * Since it isn't done automatically but rather by this method,
	 * we can do lazy updating. Also, there are some times when
	 * we intentionally do not update it even if it might be incorrect.
	 */
	public void remakeLength(){
		length = 0;
		for(Clip clip:clips){
			// Offset because floor division
			length=Math.max(length, (clip.getDelay()+clip.getLength()-1)/divisions);
		}
		// Finally add 1 to make it ceiling
		length += 1;
	}
	
	/**
	 * Get the name of this instance. Will never return null.
	 * 
	 * @return the name of this instance
	 */
	public String getName(){
		if(name==null || name.length()==0)name = getDefaultName();
		return name;
	}
	
	/**
	 * Returns a generated name.
	 * 
	 * @return a name
	 */
	public String getDefaultName(){
		return Named.defaultName("Pattern", this);
	}
	
	@Override
	public void destroy() {
		clips.clear();
		synthesizer=null;
		destroyed = true;
	}

	@Override
	public void destroySelf() {
		destroyed = true;
	}
	
	@Override
	public boolean isDestroyed(){
		return destroyed;
	}

	@Override
	public void initTransient(Project parent) {
		remakeLength();
		synthesizer = parent.synths.dualMap.get(synthName);
		setDefaultVoiceFactory();
	}
	
	/**
	 * Set voice factory to default value
	 */
	protected void setDefaultVoiceFactory(){
		if(synthesizer.isPython()){
			PyObject pyData = synthesizer.getPvfInfo();
			PyString stra = pyData.__getitem__(0).__str__();
			PyString strb = pyData.__getitem__(1).__str__();
			PyObject bool = pyData.__getitem__(2);
			setPyVoiceFactory(stra.asString(),strb.asString(),bool.asInt()!=0);
		}else{
			audioProducerFactory = synthesizer.getAudioProducerFactory();
		}
	}
	
	/**
	 * Sets voice factory to a new {@link PyFactory} instance
	 * 
	 * @param a passed to constructor
	 * @param b passed to constructor
	 * @param asScript passed to constructor
	 * @see PyFactory
	 */
	protected void setPyVoiceFactory(String a,String b,boolean asScript){
		audioProducerFactory = new PyFactory<AudioProducer>(AudioProducer.class,a,b,asScript);
	}
	
	/**
	 * @return voice factory object
	 */
	public Factory<AudioProducer> getAudioProducerFactory(){
		if(audioProducerFactory==null){
			setDefaultVoiceFactory();
		}
		return audioProducerFactory;
	}
	
	/**
	 * @return synthesizer object
	 */
	public Synthesizer getSynthesizer(){
		return synthesizer;
	}
	
	/**
	 * @param newSynth the new synthesizer to use
	 * @return true on success, false on failure
	 */
	public boolean setSynthesizer(Synthesizer newSynth) {
		if(newSynth == null)return false;
		this.synthName = newSynth.getName();
		this.synthesizer = newSynth;
		return true;
	}

	@Override
	public boolean setName(String newName) {
		name = newName;
		return true;
	}
	
	private static final String PATTERN_CLASS_NAME = Pattern.class.getCanonicalName();
	@Override
	public Pattern copy(int depth, Map<String,Object> options){
		int nextDepth = depth-1;
		int newDivisions = divisions;
		String newName = name;
		String newSynthName = synthName;
		// the list will be modified anyway
		ArrayList<Clip> newClips = new ArrayList<>(clips);
		Map<String,Object> set = (Map<String,Object>)options.get("set");
		Number val;
		CharSequence sval;
		val = (Number) set.get(PATTERN_CLASS_NAME+".divisions");
		if(val!=null)divisions = val.intValue();
		sval = (CharSequence) set.get(PATTERN_CLASS_NAME+".name");
		if(sval!=null)newName = sval.toString();
		sval = (CharSequence) set.get(PATTERN_CLASS_NAME+".synthName");
		if(sval!=null)newSynthName = sval.toString();
		// filter
		Predicate<? super Clip> pval = (Predicate<? super Clip>) set.get(PATTERN_CLASS_NAME+".clips.filter");
		if(pval!=null){
			newClips.removeIf(pval.negate());
		}
		// any iterable is allowed, valid values override old ones
		Iterable<?> lval = (Iterable<?>) set.get(PATTERN_CLASS_NAME+".clips");
		if(lval!=null){
			int size = newClips.size();
			Iterator<?> iter = lval.iterator();
			// exhaust as many items as possible which don't need extending the list
			int i;
			for(i=0;i<size && iter.hasNext();i++){
				Object value = iter.next();
				if(value!=null && (value instanceof Clip)){
					newClips.set(i, (Clip)value);
				}
			}
			// do remaining items, if any
			while(iter.hasNext()){
				Object value = iter.next();
				if(value!=null && (value instanceof Clip)){
					newClips.add((Clip)value);
				}
			}
			// need to limit length?
			int j;
			if(i<size 
					&& (val = (Number) set.get(PATTERN_CLASS_NAME+".clips.size"))!=null
					&& (j = val.intValue())<i){
				for(size--;size>=j;size--){
					newClips.remove(size);
				}
			}
		}
		Session session = (Session) options.get("session");
		Project project = session.project;
		// correct name
		newName = project.patterns.nextName(newName, -1, false, session);
		// copy properties as needed
		int size = newClips.size();
		for(int i=0;i<size;i++){
			Clip value = newClips.get(i);
			value = BetterClone.copy(value, nextDepth, options);
			newClips.set(i, value);
		}
		// make the copied object
		Pattern result = new Pattern(newDivisions,newSynthName,project);
		result.setName(newName);
		result.clips.addAll(newClips);
		return result;
	}
	
	@Override
	public boolean equals(Object o){
		if(o==this)return true;
		if(o==null || !(o instanceof Pattern))return false;
		Pattern other = (Pattern) o;
		return divisions==other.divisions
				&& synthName.equals(other.synthName)
				&& clips.equals(other.clips);
	}

}
