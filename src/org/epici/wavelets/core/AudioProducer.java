package org.epici.wavelets.core;

import java.util.Collection;

/**
 * A voice, used by audio tracks
 * 
 * @author EPICI
 * @version 1.0
 */
public interface AudioProducer extends Destructable {
	/**
	 * Get the next bit of audio
	 * 
	 * @param sampleCount the buffer size
	 * @return a {@link Samples} object with that length
	 */
	public Samples nextSegment(int sampleCount);
	/**
	 * Check if the voice is still alive or if it should be removed
	 * 
	 * @return true if it is still alive
	 */
	public boolean isAlive();
	/**
	 * Kindly tell it the clip is over, does not need to stop immediately
	 */
	public void requestKill();
	
	/**
	 * Creates a voice object which polls all the given audioProducers
	 * 
	 * @param gvoices audioProducers to flatten
	 * @return a single voice which wraps all the given
	 */
	public static AudioProducer combine(AudioProducer... gvoices){
		return new Combined(gvoices);
	}
	
	/**
	 * 
	 * Creates a voice object which polls all the given audioProducers
	 * 
	 * @param gvoices audioProducers to flatten
	 * @return a single voice which wraps all the given
	 */
	public static AudioProducer combine(Collection<AudioProducer> gvoices){
		AudioProducer[] asArray = gvoices.toArray(new AudioProducer[0]);
		return combine(asArray);
	}
	
	/**
	 * Combined voice class
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class Combined implements AudioProducer{
			
		public AudioProducer[] audioProducers;
		
		/**
		 * Destroyed yet?
		 */
		protected transient boolean destroyed = false;

		public Combined(AudioProducer... gvoices){
			audioProducers=gvoices;
		}

		@Override
		public void destroy() {
			for(AudioProducer audioProducer:audioProducers)
				audioProducer.destroy();
			audioProducers = null;
			destroyed = true;
		}

		@Override
		public void destroySelf() {
			audioProducers = null;
			destroyed = true;
		}
		
		@Override
		public boolean isDestroyed(){
			return destroyed;
		}

		@Override
		public Samples nextSegment(int sampleCount) {
			Samples data = audioProducers[0].nextSegment(sampleCount);
			for(int i=1;i<audioProducers.length;i++)
				data.layerOnThisLazy(audioProducers[i].nextSegment(sampleCount));
			return data;
		}

		@Override
		public boolean isAlive() {
			for(AudioProducer audioProducer:audioProducers)
				if(audioProducer.isAlive())
					return true;
			return false;
		}

		@Override
		public void requestKill() {
			for(AudioProducer audioProducer:audioProducers)
				audioProducer.requestKill();
		}
			
	}
}
