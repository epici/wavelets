package org.epici.wavelets.core;

import java.io.Serializable;
import java.util.function.*;

/**
 * A curve, a function, or anything else that maps
 * one x value to one y value
 * 
 * @author EPICI
 * @version 1.0
 */
public interface Curve extends Serializable, DoubleUnaryOperator, VarDouble {
	/**
	 * Get the value for a specific position
	 * <br>
	 * aka get f(x) for x
	 * 
	 * @param position position to get the corresponding value of
	 * @return that value
	 */
	public double valueAtPosition(double position);
	
	// Standard interface that does the same thing
	@Override
	public default double applyAsDouble(double value){
		return valueAtPosition(value);
	}
	
	// Allow using a curve directly
	@Override
	public default double get(double time){
		return valueAtPosition(time);
	}
	
	/**
	 * Sample some number of points
	 * <br>
	 * Provided here only because certain subclasses cannot
	 * gain any performance advantage with code tailored to them
	 * 
	 * @param positions x values to sample
	 * @return y value for each x
	 */
	public default double[] sample(double[] positions){
		int n = positions.length;
		double[] result = new double[n];
		for(int i=0;i<n;i++)
			result[i] = valueAtPosition(positions[i]);
		return result;
	}
	
	/**
	 * Sample <i>n</i> equally spaced points in the range
	 * [<i>l</i>, <i>r</i>) such that the first point is at
	 * <i>l</i> and the <i>n+1</i>th point, if it existed,
	 * would be at <i>r</i>
	 * <br>
	 * This is not numerically stable, but is fine for
	 * any reasonable parameters
	 * <br>
	 * Provided here only because certain subclasses cannot
	 * gain any performance advantage with code tailored to them
	 * 
	 * @param l first value as described above
	 * @param r would-be last value as described above
	 * @param n number of sample points as described above
	 * @return y value for each sampled x in the range
	 */
	public default double[] bake(double l,double r,int n){
		double[] result = new double[n];
		double d = (r-l)/n;
		for(int i=0;i<n;i++,l+=d)
			result[i] = valueAtPosition(l);
		return result;
	}
	
	/**
	 * If this curve as a function has an inverse, return it.
	 * Otherwise return null.
	 * Details up to the implementor.
	 * <br>
	 * Argument specifies whether it should use a view if possible.
	 * Views are typically inexpensive to create, but create overhead.
	 * If a user wants to check if a curve is invertible, it should
	 * request a view - if null is returned anyway then the user should assume
	 * the curve is not invertible.
	 * 
	 * @param useView whether to use a view if possible
	 * @return inverse curve, or null if not applicable
	 */
	public default Curve curveInvert(boolean useView) {
		return null;
	}
	
	/**
	 * If this curve as a function has an inverse, return it.
	 * Otherwise return null.
	 * Details up to the implementor.
	 * <br>
	 * Redirects to {@link #curveInvert(boolean)} with request to use a view.
	 * 
	 * @return inverse curve, or null if not applicable
	 */
	public default Curve curveInvert() {
		return curveInvert(true);
	}
	
	/**
	 * If this curve supports transformations,
	 * return the transformed curve.
	 * Otherwise return null.
	 * Null mappings are assumed to be the identity function.
	 * Null may also be returned if the transformation is invalid
	 * or unsupported.
	 * <br>
	 * Argument specifies whether it should use a view if possible.
	 * Views are typically inexpensive to create, but create overhead.
	 * If a user wants to check if a curve is transformable, it should
	 * request a view - if null is returned anyway then the user should assume
	 * the curve is not invertible.
	 * <br>
	 * If <i>[X, Y]</i> is an input output pair for this curve,
	 * and <i>T</i> is the requested mapping function,
	 * then the result should contain the input output pairs given
	 * by <i>T([X, Y])</i>.
	 * 
	 * @param mapXY [X, Y] tuple transform
	 * @param useView whether to use a view if possible
	 * @return transformed curve, or null if not applicable
	 */
	public default Curve curveTransform(UnaryOperator<double[]> mapXY, boolean useView) {
		return null;
	}
	
	/**
	 * If this curve supports transformations,
	 * return the transformed curve.
	 * Otherwise return null.
	 * Null mappings are assumed to be the identity function.
	 * Null may also be returned if the transformation is invalid
	 * or unsupported.
	 * <br>
	 * Redirects to {@link #curveTransform(UnaryOperator, boolean)}
	 * with a request to use a view.
	 * <br>
	 * If <i>[X, Y]</i> is an input output pair for this curve,
	 * and <i>T</i> is the requested mapping function,
	 * then the result should contain the input output pairs given
	 * by <i>T([X, Y])</i>.
	 * 
	 * @param mapXY mapXY [X, Y] tuple transform
	 * @return transformed curve, or null if not applicable
	 */
	public default Curve curveTransform(UnaryOperator<double[]> mapXY) {
		return curveTransform(mapXY, true);
	}
	
	/**
	 * If this curve supports transformations,
	 * return the transformed curve.
	 * Otherwise return null.
	 * Null mappings are assumed to be the identity function.
	 * Null may also be returned if the transformation is invalid
	 * or unsupported.
	 * <br>
	 * Argument specifies whether it should use a view if possible.
	 * Views are typically inexpensive to create, but create overhead.
	 * If a user wants to check if a curve is transformable, it should
	 * request a view - if null is returned anyway then the user should assume
	 * the curve is not invertible.
	 * <br>
	 * If <i>[X, Y]</i> is an input output pair for this curve,
	 * and <i>Tx</i> and <i>Ty</i> are the requested mapping functions,
	 * then the result should contain the input output pairs given
	 * by <i>[Tx(X), Ty(Y)]</i>.
	 * 
	 * @param mapX X axis component transform
	 * @param mapY Y axis component transform
	 * @param useView whether to use a view if possible
	 * @return transformed curve, or null if not applicable
	 */
	public default Curve curveTransform(DoubleUnaryOperator mapX, DoubleUnaryOperator mapY, boolean useView) {
		// redirect again
		if(mapX == null)mapX = DoubleUnaryOperator.identity();
		if(mapY == null)mapY = DoubleUnaryOperator.identity();
		final DoubleUnaryOperator fmapX = mapX, fmapY = mapY;// Java complains if not final
		return curveTransform(
				(double[] pxy) -> new double[] {
						fmapX.applyAsDouble(pxy[0]),
						fmapY.applyAsDouble(pxy[1])},
				useView);
	}
	
	/**
	 * If this curve supports transformations,
	 * return the transformed curve.
	 * Otherwise return null.
	 * Null mappings are assumed to be the identity function.
	 * Null may also be returned if the transformation is invalid
	 * or unsupported.
	 * <br>
	 * Redirects to {@link #curveTransform(DoubleUnaryOperator, DoubleUnaryOperator, boolean)}
	 * with a request to use a view.
	 * <br>
	 * If <i>[X, Y]</i> is an input output pair for this curve,
	 * and <i>Tx</i> and <i>Ty</i> are the requested mapping functions,
	 * then the result should contain the input output pairs given
	 * by <i>[Tx(X), Ty(Y)]</i>.
	 * 
	 * @param mapX X axis component transform
	 * @param mapY Y axis component transform
	 * @param useView whether to use a view if possible
	 * @return transformed curve, or null if not applicable
	 */
	public default Curve curveTransform(DoubleUnaryOperator mapX, DoubleUnaryOperator mapY) {
		return curveTransform(mapX, mapY, true);
	}
	
}
