package org.epici.wavelets.core;

import java.awt.Color;
import java.awt.color.ColorSpace;
import java.io.Serializable;
import org.apache.pivot.wtk.*;
import org.apache.pivot.wtk.skin.terra.*;
import org.epici.wavelets.util.Any;
import org.epici.wavelets.util.ds.*;
import org.epici.wavelets.util.hash.*;
import org.epici.wavelets.util.jython.Pylike;
import org.epici.wavelets.util.math.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import org.hsluv.HUSLColorConverter;

/**
 * Represents an immutable color scheme matching Apache Pivot
 * <br>
 * Descriptions here assume a light color scheme
 * 
 * @author EPICI
 * @version 1.0
 */
public class ColorScheme implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Standard web color scheme, used if no others are available
	 */
	public static final ColorScheme DEFAULT_COLORS = new ColorScheme(
			0x000000,
			0xFFFFFF,
			0x999999,
			0xDDDCD5,
			0x336699,
			0x336699,
			0xFFE480,
			0xEB0000
			);
	
	private static ColorScheme pivotColors;
	
	/**
	 * Caches brightening; it's not such an expensive operation
	 * but it's a little speed-up since most colors are derived
	 * from color scheme colors by brightening or darkening
	 */
	private static HashMap<Any.O2<Color,float[]>,Color> memoHsb = new LhmCache<>(1<<10,true);
	
	static{
		refreshDefaultPivotColors();
	}
	
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Text</li>
	 * </ul>
	 */
	public final Color text;
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Background fill</li>
	 * <li>Alternate table row background fill</li>
	 * </ul>
	 */
	public final Color background;
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Outlines</li>
	 * <li>Disabled items' text</li>
	 * <li>Disabled items' fill</li>
	 * </ul>
	 */
	public final Color line;
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Generic component fill/coloration with light/dark gradients/tones</li>
	 * <li>Alternate table row background fill</li>
	 * <li>Would-be selected background fill</li>
	 * <li>Form info</li>
	 * </ul>
	 */
	public final Color gradient;
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Selected items</li>
	 * <li>Form questions</li>
	 * <li>Titles</li>
	 * <li>Link button text</li>
	 * <li>Secondary color</li>
	 * </ul>
	 */
	public final Color highlight;
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Button selections</li>
	 * </ul>
	 */
	public final Color selected;
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Form warnings</li>
	 * <li>Tooltip fill</li>
	 * </ul>
	 */
	public final Color warning;
	/**
	 * Seen in Apache Pivot in:
	 * <ul>
	 * <li>Form errors</li>
	 * </ul>
	 */
	public final Color error;
	/**
	 * Not used in Apache Pivot.
	 * <br>
	 * Indicates a marker,
	 * which is not a {@link #highlight} or a {@link #selected}.
	 * Could be some kind of cursor or label.
	 * <br>
	 * Defaults to the same as {@link #highlight}.
	 */
	public final Color marker;
	
	/**
	 * Hash key for <i>hashCode()</i>
	 */
	public static final long HK_HC = QuickKeyGen.next64();
	
	/**
	 * Copy from the given theme
	 * 
	 * @param theme the theme to copy the colors from
	 */
	public ColorScheme(TerraTheme theme){
		text = theme.getBaseColor(0);
		background = theme.getBaseColor(1);
		line = theme.getBaseColor(2);
		gradient = theme.getBaseColor(3);
		highlight = theme.getBaseColor(4);
		selected = theme.getBaseColor(5);
		warning = theme.getBaseColor(6);
		error = theme.getBaseColor(7);
		marker = highlight;
	}
	
	/**
	 * Get the current theme, and attempt to remake the
	 * active color scheme based on it
	 */
	public static void refreshDefaultPivotColors() {
		Theme theme = Theme.getTheme();
		if(theme!=null && theme instanceof TerraTheme)
			pivotColors = new ColorScheme((TerraTheme) theme);
	}

	/**
	 * Pass all colors in order with RGB hexadecimal colours
	 * <br>
	 * Done with array/varargs for compatiblity and convenience
	 * 
	 * @param colors in order: text, background, line, gradient,
	 * highlight, selected, warning, error [, marker]
	 */
	public ColorScheme(int... colors){
		//Null check
		if(colors.length<8)throw new IllegalArgumentException("Must provide at least 8 colors");
		text = new Color(colors[0]);
		background = new Color(colors[1]);
		line = new Color(colors[2]);
		gradient = new Color(colors[3]);
		highlight = new Color(colors[4]);
		selected = new Color(colors[5]);
		warning = new Color(colors[6]);
		error = new Color(colors[7]);
		marker = Pylike.or(colors.length>8?new Color(colors[8]):null, highlight, Objects::nonNull);
	}
	
	/**
	 * Pass all colors in order pre-made
	 * <br>
	 * Done with array/varargs for compatiblity and convenience,
	 * extra elements will be ignored
	 * 
	 * @param colors in order: text, background, line, gradient,
	 * highlight, selected, warning, error [, marker]
	 */
	public ColorScheme(Color... colors){
		//Null check
		if(colors.length<8)throw new IllegalArgumentException("Must provide at least 8 colors");
		for(int i=0;i<8;i++)
			if(colors[i]==null)
				throw new NullPointerException("Color cannot be null");
		text = colors[0];
		background = colors[1];
		line = colors[2];
		gradient = colors[3];
		highlight = colors[4];
		selected = colors[5];
		warning = colors[6];
		error = colors[7];
		marker = Pylike.or(colors.length>8?colors[8]:null, highlight, Objects::nonNull);
	}
	
	/**
	 * Pass all colors in order with RGB vectors
	 * <br>
	 * Done with array/varargs for compatiblity and convenience
	 * 
	 * @param colors in order: text, background, line, gradient,
	 * highlight, selected, warning, error [, marker]
	 */
	public ColorScheme(float[]... colors){
		text = make3(colors[0]);
		background = make3(colors[1]);
		line = make3(colors[2]);
		gradient = make3(colors[3]);
		highlight = make3(colors[4]);
		selected = make3(colors[5]);
		warning = make3(colors[6]);
		error = make3(colors[7]);
		marker = Pylike.or(colors.length>8?make3(colors[8]):null, highlight, Objects::nonNull);
	}
	
	/**
	 * Pass all colors in order with RGB vectors
	 * <br>
	 * Done with array/varargs for compatiblity and convenience
	 * 
	 * @param colors in order: text, background, line, gradient,
	 * highlight, selected, warning, error [, marker]
	 */
	public ColorScheme(double[]... colors){
		text = make3(colors[0]);
		background = make3(colors[1]);
		line = make3(colors[2]);
		gradient = make3(colors[3]);
		highlight = make3(colors[4]);
		selected = make3(colors[5]);
		warning = make3(colors[6]);
		error = make3(colors[7]);
		marker = Pylike.or(colors.length>8?make3(colors[8]):null, highlight, Objects::nonNull);
	}
	
	/**
	 * Derive a color scheme where all colors are sufficiently unique.
	 * 
	 * @param similar function to check if two colors are too close
	 * and should be mutated.
	 * @param mutators collection of ODE-like functions which will be
	 * used to change the colors until they are all dissimilar. Each
	 * mutator is used for one color, regardless of how many times it actually mutates.
	 * First argument to the function is the iteration index (first time is 0),
	 * second argument is the color to be mutated.
	 * @param tries number of tries for any one color before throwing an exception.
	 * @return a derived color scheme where all colors are sufficiently unique.
	 */
	public ColorScheme uniqify(BiPredicate<Color,Color> similar,Iterator<BiFunction<Integer,Color,Color>> mutators,int tries) {
		Color[] colors = {
				text,
				background,
				line,
				gradient,
				highlight,
				selected,
				warning,
				error,
				marker
		};
		for(int i=0;i<colors.length;i++) {
			Color color = colors[i];
			BiFunction<Integer,Color,Color> mutator = mutators.next();
			for(int j=0;testSimilar(colors,i,color,similar);j++) {
				if(j==tries)throw new IllegalArgumentException("Giving up after "+tries+" tries on color "+i);
				color = mutator.apply(j, color);
				Objects.requireNonNull(mutator, "Mutator returned null");
			}
			colors[i] = color;
		}
		return new ColorScheme(colors);
	}
	
	/**
	 * Helper method for {@link #uniqify(BiPredicate, Iterator, int)}.
	 * Tests whether a certain color is similar to any of the other colors in a range.
	 * 
	 * @param colors all colors
	 * @param n test against the first n colors
	 * @param color reference color
	 * @param similar similarity test
	 * @return true if any were similar
	 */
	private boolean testSimilar(Color[] colors, int n, Color color, BiPredicate<Color,Color> similar) {
		for(int k=0;k<n;k++) {
			if(similar.test(color, colors[k]))return true;
		}
		return false;
	}
	
	/**
	 * Get the Euclidean distance between two colors in the CIELUV
	 * color space. This roughly measures the contrast between the colors.
	 * Alpha is ignored.
	 * 
	 * @param first one of the colors to compare
	 * @param second the other color
	 * @return Euclidean distance in CIELUV color space
	 */
	public static double colorDistance(Color first,Color second) {
		ColorSpace ciexyz = ColorSpace.getInstance(ColorSpace.CS_CIEXYZ);
		float[] axyzf = new float[4];
		float[] bxyzf = new float[4];
		first.getComponents(ciexyz, axyzf);
		second.getComponents(ciexyz, bxyzf);
		double[] axyzd = new double[3];
		double[] bxyzd = new double[3];
		for(int i=0;i<3;i++) {
			axyzd[i] = axyzf[i];
			bxyzd[i] = bxyzf[i];
		}
		double[] aluvd = HUSLColorConverter.xyzToLuv(axyzd);
		double[] bluvd = HUSLColorConverter.xyzToLuv(bxyzd);
		double sumsq = 0;
		for(int i=0;i<3;i++) {
			sumsq += Math.pow(aluvd[i] - bluvd[i], 2);
		}
		return Math.sqrt(sumsq);
	}
	
	/**
	 * Push this color scheme to a theme, modifying it in place, and return it
	 * 
	 * @param theme the theme to modify
	 * @return the same theme after changing
	 */
	public TerraTheme writeTo(TerraTheme theme){
		theme.setBaseColor(0, text);
		theme.setBaseColor(1, background);
		theme.setBaseColor(2, line);
		theme.setBaseColor(3, gradient);
		theme.setBaseColor(4, highlight);
		theme.setBaseColor(5, selected);
		theme.setBaseColor(6, warning);
		theme.setBaseColor(7, error);
		return theme;
	}
	
	public int hashCode(){
		HashTriArx hash = new HashTriArx(HK_HC);
		hash.absorbObj(text,background,line,gradient,highlight,selected,warning,error);
		return hash.squeezeInt();
	}
	
	public boolean equals(Object obj) {
		if (this == obj)return true;
		if (obj == null)return false;
		if (!(obj instanceof ColorScheme))return false;
		ColorScheme other = (ColorScheme) obj;
		return
				text.equals(other.text) &&
				background.equals(other.background) &&
				line.equals(other.line) &&
				gradient.equals(other.gradient) &&
				highlight.equals(other.highlight) &&
				selected.equals(other.selected) &&
				warning.equals(other.warning) &&
				error.equals(other.error);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ColorScheme(");
		sb.append("text=");sb.append(text);sb.append(", ");
		sb.append("background=");sb.append(background);sb.append(", ");
		sb.append("line=");sb.append(line);sb.append(", ");
		sb.append("gradient=");sb.append(gradient);sb.append(", ");
		sb.append("highlight=");sb.append(highlight);sb.append(", ");
		sb.append("selected=");sb.append(selected);sb.append(", ");
		sb.append("warning=");sb.append(warning);sb.append(", ");
		sb.append("error=");sb.append(error);
		sb.append(")");
		return sb.toString();
	}
	
	public static ColorScheme getPivotColors(){
		return pivotColors==null?DEFAULT_COLORS:pivotColors;
	}

	private static Color make3(float[] vec){
		return new Color(vec[0],vec[1],vec[2]);
	}
	
	private static Color make3(double[] vec){
		return new Color((float)vec[0],(float)vec[1],(float)vec[2]);
	}
	
	/**
	 * Taken from {@link TerraTheme}. Brightens or darkens a color <i>linearly</i>.
	 * <br>
	 * Note that {@link TerraTheme} uses <i>+0.1</i> for the next lighter color
	 * and <i>-0.1</i> for the next darker color, so to be consistent with
	 * the rest of pivot, those increments should be used.
	 * 
	 * @param color original color
	 * @param adjustment value between -1 and 1, 0 has no effect
	 * @return brightness-adjusted color
	 */
	public static Color brighten(Color color, float adjustment) {
		return adjustHsb(color,0f,0f,adjustment);
	}
	
	/**
	 * Taken from {@link TerraTheme}. Adjusts hue, saturation and brightness <i>linearly</i>
	 * and independently of the others.
	 * 
	 * @param color original color
	 * @param dhue adjustment value for hue (1=360 degrees)
	 * @param dsat adjustment value for saturation (-1 to greyscale, 1 to color)
	 * @param dbright adjustment value brightness (-1 to black, 1 to white)
	 * @return adjusted color
	 */
	public static Color adjustHsb(Color color, float dhue, float dsat, float dbright){
		float[] fkey = {dhue,dsat,dbright};
		Any.O2<Color,float[]> key = new Any.O2<Color,float[]>(color,fkey);
		Color result = memoHsb.get(key);
		if(result!=null)return result;
		int argb = color.getRGB();
		float[] hsb = Color.RGBtoHSB((argb>>16)&0xff, (argb>>8)&0xff, argb&0xff, null);
		int rgb = Color.HSBtoRGB(hsb[0]+dhue, Floats.median(0, 1, hsb[1]+dsat), Floats.median(0, 1, hsb[2]+dbright));
		result = new Color((argb&0xff000000) | (rgb & 0xffffff), true);
		memoHsb.put(key, result);
		return result;
	}
	
	/**
	 * Convenience method to copy RGB of a color and change
	 * just the alpha value. Even though it's not a lot of code,
	 * it's less confusing to use than the methods or constructors
	 * provided by {@link Color}. So this is provided.
	 * 
	 * @param color original color, RGB will be used
	 * @param alpha alpha of new color
	 * @return color using old RGB and new alpha
	 */
	public static Color setAlpha(Color color,int alpha){
		return new Color(color.getRGB()&0xffffff|(alpha<<24),true);
	}
	
	private static final double XYZ_DESATURATION_FACTOR = 2;
	/**
	 * Get a bounded RGB color which corresponds to unbounded CIE XYZ color.
	 * Bright colors are darkened and desaturated.
	 * <br>
	 * Even though this works with any size possible values,
	 * try to make <i>(1, 1, 1)</i> correspond to a normal white.
	 * In other words, treat it as XYZ 1 but without a hard bound.
	 * 
	 * @param xyz <i>(X, Y, Z)</i> tuple
	 * @return color object
	 */
	public static Color fromCIEXYZUnbounded(double[] xyz) {
		// --- normalize bright colours ---
		// extract CIE XYZ
		double cx = xyz[0], cy = xyz[1], cz = xyz[2];
		// convert to CIE xyY
		double r = cx+cy+cz, rx = cx/r, ry = cy/r;
		// create target Y
		/*
		 * This is the critical piece!
		 * The curve used here is reciprocal based,
		 * but something else is likely more correct
		 * based on human perception.
		 */
		double ty = 1-1/(1+cy);
		// compute ratio
		double t = ty/cy;
		// and a stronger ratio
		double td = 1 - Math.pow(t, XYZ_DESATURATION_FACTOR);
		// rescale xy
		rx = Bezier.bezier2(1d/3, rx, td);
		ry = Bezier.bezier2(1d/3, ry, td);
		// convert to CIE XYZ
		cy = ty;
		double rt = cy/ry;
		cx = rt*rx;
		cz = rt*(1-rx-ry);
		// --- create a Color object ---
		// convert XYZ to RGB
		float[] frgb = ColorSpace.getInstance(ColorSpace.CS_sRGB).fromCIEXYZ(
				new float[] {
						(float) cx,
						(float) cy,
						(float) cz,
				}
				);
		// make integer and construct Color object
		return new Color(frgb[0], frgb[1], frgb[2]);
	}
	
}