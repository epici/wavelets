package org.epici.wavelets.core;

import org.epici.wavelets.util.ds.NamedMap;

/**
 * A named object.
 * 
 * @author EPICI
 * @version 1.0
 */
public interface Named {

	/**
	 * Get the name of the object, or if it has no name,
	 * a default name.
	 * 
	 * @return the name of the object
	 */
	public String getName();
	
	/**
	 * Attempt to change the name to some other value.
	 * If changing to that name is disallowed, returns false,
	 * and no data should change.
	 * 
	 * @param newName new name to give
	 * @return true on success
	 */
	public boolean setName(String newName);
	
	/**
	 * Generate a name for the object based on its
	 * {@link System#identityHashCode(Object)}.
	 * <br>
	 * Roughly follows the style used by {@link NamedMap}.
	 * Suffix will be <i>6</i> digits long with no leading <i>0</i> digit.
	 * Thus, with <i>9&#x00d7;10<sup>5</sup></i> possible suffixes,
	 * we can have around <i>1117</i> objects with <i>&#x2248;50%</i> chance
	 * of a collision.
	 * 
	 * @param type type name
	 * @param obj object
	 * @return a random looking name
	 */
	public static String defaultName(String type,Object obj){
		int id = System.identityHashCode(obj);//Needs more scrambling to look random?
		int shortId = Math.floorMod(id, 900000)+100000;// range: [10**5, 10**6)
		String idstr = Integer.toString(shortId);
		StringBuilder sb = new StringBuilder();
		sb.append(type);
		sb.append(".");
		sb.append(idstr);
		return sb.toString();
	}

	/**
	 * Equivalent to calling {@link #setName(String)} on <i>obj</i>
	 * but with additional checks. If either argument is null
	 * or the new name is blank (after calling {@link String#trim()}),
	 * does nothing. Implemented as a static utility
	 * to take load off of implementors.
	 * <br>
	 * Unless dealing with internal data structures only,
	 * prefer calling this method to calling {@link #setName(String)}.
	 * 
	 * @param obj object to rename
	 * @param newName new name to give
	 * @return true on success
	 */
	public static boolean setName(Named obj,String newName){
		if(obj==null || newName==null || newName.trim().length()==0)return false;
		return obj.setName(newName);
	}
	
}
