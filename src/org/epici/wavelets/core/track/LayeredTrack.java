package org.epici.wavelets.core.track;

import java.util.*;
import javax.swing.*;

import org.epici.wavelets.core.MetaComponent;
import org.epici.wavelets.core.MetaSamples;
import org.epici.wavelets.core.Named;
import org.epici.wavelets.core.Project;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.Track;
import org.epici.wavelets.core.TransientContainer;
import org.epici.wavelets.core.Track.ViewComponent;
import org.epici.wavelets.ui.*;
import org.epici.wavelets.ui.track.layered.LayeredTrackEditor;
import org.epici.wavelets.util.hash.HashTriArx;
import org.epici.wavelets.util.hash.QuickKeyGen;
import org.epici.wavelets.util.ui.PivotSwingUtils;

import org.epici.wavelets.util.*;

/**
 * Contains tracks
 * <br>
 * A new blank {@link MetaSamples} object is created, all tracks process that,
 * and that object is finally layered onto the given one
 * 
 * @author EPICI
 * @version 1.0
 */
public class LayeredTrack implements Track, TransientContainer<LayeredTrackParent>, LayeredTrackParent {
	private static final long serialVersionUID = 1L;
	
	/**
	 * All contained tracks
	 */
	public ArrayList<Track> tracks = new ArrayList<>();
	/**
	 * If this is the root {@link LayeredTrack}, the parent project
	 */
	protected transient Project parentProject;
	/**
	 * If this is not the root {@link LayeredTrack}, the parent {@link LayeredTrack}
	 */
	protected transient LayeredTrack parentTLC;
	/**
	 * If it is the root {@link LayeredTrack}, and therefore
	 * its parent is the project
	 */
	protected transient boolean parentIsComposition;
	/**
	 * The name, if it is named
	 */
	protected String name;
	
	/**
	 * Hash key for <i>hashCode()</i>
	 */
	public static final long HK_HC = QuickKeyGen.next64();
	
	public LayeredTrack(LayeredTrackParent parent){
		initTransient(parent);
	}
	
	@Override
	public void applyTo(MetaSamples current) {
		if(tracks.size()>0){
			MetaSamples toAdd = MetaSamples.blankSamplesFrom(current);
			for(Track track:tracks){
				double[] trackTimeBounds = track.getTimeBounds();
				if(trackTimeBounds[0]!=Double.MAX_VALUE&&trackTimeBounds[1]!=Double.MIN_VALUE&&trackTimeBounds[0]<current.endPos&&trackTimeBounds[1]>current.startPos){
					track.applyTo(toAdd);
				}
			}
			current.layerOnThisMeta(toAdd);
		}
	}
	
	public double[] getTimeBounds(){
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		for(Track track:tracks){
			double[] trackTimeBounds = track.getTimeBounds();
			double start = trackTimeBounds[0];
			double end = trackTimeBounds[1];
			if(start<min){
				min=start;
			}
			if(end>max){
				max=end;
			}
		}
		return new double[]{min,max};
	}

	@Override
	public void initTransient(LayeredTrackParent parent) {
		parentIsComposition=parent instanceof Project;
		if(parentIsComposition){
			parentProject = (Project) parent;
		}else{
			parentTLC = (LayeredTrack) parent;
		}
	}
	
	public Project parentProject(){
		if(parentIsComposition){
			return parentProject;
		}
		return parentTLC.parentProject();
	}
	
	public boolean setParent(Object newParent){
		if(newParent instanceof LayeredTrack){
			parentIsComposition = false;
			parentProject = null;
			parentTLC = (LayeredTrack) newParent;
			return true;
		}
		if(newParent instanceof Project){
			parentIsComposition = true;
			parentProject = (Project) newParent;
			parentTLC = null;
			return true;
		}
		return false;
	}

	@Override
	public MetaComponent<JInternalFrame> getUI() {
		Session session = parentProject().currentSession;
		MetaComponent<JInternalFrame> meta = session.windowManager.getWindow("Layered Track Editor");
		LayeredTrackEditor editor;
		if(meta==null||meta.component.isClosed()){
			editor = LayeredTrackEditor.createNew();
			editor.session = session;
			editor.init();
			JInternalFrame wrapped = PivotSwingUtils.wrapPivotWindow(editor);
			meta = new MetaComponent<>("Default Layered Track Editor","Layered Track Editor",wrapped,null);
			meta.metaData.put("window", editor);
		}
		editor = (LayeredTrackEditor) meta.metaData.get("window");
		editor.addEditorData(this);
		return meta;
	}

	@Override
	public ViewComponent getViewComponent() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean equals(Object o){
		if(o==this)return true;
		if(o==null || !(o instanceof LayeredTrack))return false;
		LayeredTrack other = (LayeredTrack) o;
		return tracks.equals(other.tracks);
	}
	
	public int hashCode(){
		HashTriArx hash = new HashTriArx(HK_HC);
		hash.absorbObj(tracks);
		return hash.squeezeInt();
	}
	
	public String getName(){
		if(name==null || name.length()==0)name = parentIsComposition?"Root Track":Named.defaultName("Layered Track", this);
		return name;
	}
	
	public boolean setName(String newName){
		name=newName;
		return true;
	}

}
