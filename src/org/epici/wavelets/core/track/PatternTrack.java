package org.epici.wavelets.core.track;

import java.util.*;
import javax.swing.*;

import org.epici.wavelets.core.AudioProducer;
import org.epici.wavelets.core.Clip;
import org.epici.wavelets.core.MetaComponent;
import org.epici.wavelets.core.MetaSamples;
import org.epici.wavelets.core.Named;
import org.epici.wavelets.core.Pattern;
import org.epici.wavelets.core.Project;
import org.epici.wavelets.core.Samples;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.Synthesizer;
import org.epici.wavelets.core.Track;
import org.epici.wavelets.core.TransientContainer;
import org.epici.wavelets.core.Track.ViewComponent;
import org.epici.wavelets.ui.*;
import org.epici.wavelets.ui.track.pattern.PatternTrackEditor;
import org.epici.wavelets.ui.track.pattern.PatternTrackPreview;
import org.epici.wavelets.util.hash.*;
import org.epici.wavelets.util.jython.*;
import org.epici.wavelets.util.ui.PivotSwingUtils;
import org.python.core.*;

import org.epici.wavelets.util.*;

/**
 * Contains patterns
 * 
 * @author EPICI
 * @version 1.0
 */
public class PatternTrack implements Track, TransientContainer<LayeredTrack> {
	private static final long serialVersionUID = 1L;

	/**
	 * All the patterns and their respective delays,
	 * encoded as set bits in a bitset
	 */
	public final IdentityHashMap<Pattern,BitSet> patterns;
	/**
	 * List of active audioProducers, used for playback
	 */
	protected transient ArrayList<AudioProducer> audioProducers;
	/**
	 * Parent {@link LayeredTrack}
	 */
	protected transient LayeredTrack parentTLC;
	/**
	 * The name of this track.
	 */
	protected String name;
	
	/**
	 * Hash key for <i>hashCode()</i>
	 */
	public static final long HK_HC = QuickKeyGen.next64();
	
	/**
	 * 
	 * @param parent the parent object
	 * @see LayeredTrack
	 */
	public PatternTrack(LayeredTrack parent){
		patterns = new IdentityHashMap<>();
		initTransient(parent);
	}
	
	@Override
	public synchronized void applyTo(MetaSamples current) {
		Project project = current.project;
		Session session = project.currentSession;
		//Setup
		if(!patterns.isEmpty()){
			//Add new audioProducers if necessary
			for(Pattern pattern:patterns.keySet()){
				PrimitiveIterator.OfInt iter = patterns.get(pattern).stream().iterator();
				while(iter.hasNext()){
					int delay = iter.nextInt();
					double start = project.measuresToSeconds(delay);
					//double end = project.measuresToSeconds(delay+pattern.length);
					if(current.startPos <= start && start < current.endPos){
						double invDivisions = 1d/pattern.divisions;
						ArrayList<double[]> toSendList = new ArrayList<double[]>();
						for(Clip clip:pattern.clips){
							double clipStart = project.measuresToSeconds(delay+clip.getDelay()*invDivisions);
							double clipEnd = project.measuresToSeconds(delay+(clip.getDelay()+clip.getLength())*invDivisions);
							
							int nproperties = clip.countProperties();
							double[] clipData = new double[nproperties+4];
							clipData[0] = clipStart-current.startPos;
							clipData[1] = clipEnd-current.startPos;
							clipData[2] = clip.getPitch();
							clipData[3] = clip.getVolume();
							for(int i=0;i<nproperties;i++){
								clipData[i+4] = clip.getProperty(i);
							}
							toSendList.add(clipData);
						}
						int numToSend = toSendList.size();
						if(numToSend>0){
							double[][] toSend = toSendList.toArray(new double[numToSend][]);
							Synthesizer synthesizer = pattern.getSynthesizer();
							synthesizer.setGlobals(current.vars);
							synthesizer.spawnVoices(toSend, this, session);
						}
					}
				}
			}
		}
		if(audioProducers.size()>0){
			//Process existing audioProducers
			int sampleCount = current.sampleData.length;
			Samples toLayer = Samples.blankSamples(current.sampleRate, sampleCount);
			for(AudioProducer audioProducer:audioProducers){
				Samples toAdd = audioProducer.nextSegment(sampleCount);
				toLayer.layerOnThisLazy(toAdd);
			}
			//Do layering
			current.layerOnThisLazy(toLayer);
			//Remove dead audioProducers
			Iterator<AudioProducer> viter = audioProducers.iterator();
			while(viter.hasNext()){
				AudioProducer audioProducer = viter.next();
				if(!audioProducer.isAlive()){
					viter.remove();
				}
			}
		}
	}

	@Override
	public void initTransient(LayeredTrack parent) {
		audioProducers = new ArrayList<AudioProducer>();
		parentTLC = parent;
	}
	
	public Project parentProject(){
		return parentTLC.parentProject();
	}
	
	public boolean setParent(Object newParent){
		if(newParent instanceof LayeredTrack){
			parentTLC = (LayeredTrack)newParent;
			return true;
		}
		return false;
	}
	
	/**
	 * Adds a voice
	 * 
	 * @param pattern the source pattern
	 * @param args Python args + keyword args
	 * @param keywords Python keywords
	 */
	public synchronized void addVoice(Pattern pattern,PyObject[] args,String[] keywords){
		Factory<AudioProducer> audioProducerFactory = pattern.getAudioProducerFactory();
		addAudioProducer(audioProducerFactory.create(args, keywords));
	}
	
	/**
	 * Adds a single audio producer
	 * 
	 * @param audioProducer the audio producer to add
	 */
	public synchronized void addAudioProducer(AudioProducer audioProducer){
		audioProducers.add(audioProducer);
	}
	
	/**
	 * Call {@link AudioProducer#requestKill()} for all audio producers
	 */
	public synchronized void requestKillAllAudioProducers(){
		for(AudioProducer audioProducer:audioProducers){
			audioProducer.requestKill();
		}
	}
	
	/**
	 * Aggressively removes all audio producers
	 */
	public synchronized void removeAllAudioProducers(){
		for(AudioProducer audioProducer:audioProducers){
			audioProducer.destroy();
		}
		audioProducers.clear();
	}
	
	/**
	 * Adds a pattern
	 * 
	 * @param pattern the pattern to add
	 * @param delay the delay to add with
	 */
	public void addPattern(Pattern pattern,int delay){
		BitSet delays = patterns.get(pattern);
		if(delays==null){
			delays = new BitSet();
			patterns.put(pattern, delays);
		}
		delays.set(delay);
	}
	
	/**
	 * Removes a pattern
	 * 
	 * @param pattern the target pattern
	 * @param delay the delay to remove
	 */
	public void removePattern(Pattern pattern,int delay){
		BitSet delays = patterns.get(pattern);
		if(delays!=null){
			delays.clear(delay);
		}
	}
	
	/**
	 * Attempts to change a delay
	 * <br>
	 * Force flag:
	 * <ul>
	 * <li>If true, functionally equivalent to <i>removePattern</i> followed by <i>addPattern</i></li>
	 * <li>If false, will do nothing if there is no pattern at <i>oldDelay</i> or there is already a pattern at <i>newDelay</i></li>
	 * </ul>
	 * 
	 * @param pattern the keyed pattern
	 * @param oldDelay the original delay
	 * @param newDelay the new delay to set it to
	 * @param force explained in doc
	 * @return true if successful or forced
	 */
	public boolean updateDelay(Pattern pattern,int oldDelay,int newDelay,boolean force){
		BitSet delays = patterns.get(pattern);
		if(force){
			if(delays==null){
				delays=new BitSet();
				patterns.put(pattern, delays);
			}
			delays.clear(oldDelay);//No branch=faster
			delays.set(newDelay);
			return true;
		}else{
			boolean success = delays!=null&&delays.get(oldDelay)&&!delays.get(newDelay);
			if(success&&oldDelay!=newDelay){
				delays.clear(oldDelay);
				delays.set(newDelay);
			}
			return success;
		}
		
	}
	
	/**
	 * Get the current number of patterns
	 * 
	 * @return the current pattern count
	 */
	public int getPatternCount(){
		return patterns.size();
	}
	
	public double[] getTimeBounds(){
		Project project = parentProject();
		double min = Double.POSITIVE_INFINITY;
		double max = Double.NEGATIVE_INFINITY;
		for(Pattern pattern:patterns.keySet()){
			BitSet delays = patterns.get(pattern);
			int first = delays.nextSetBit(0);
			int last = delays.length()-1+pattern.length;
			if(first<min)min=first;
			if(last>max)max=last;
		}
		return new double[]{project.measuresToSeconds(min),project.measuresToSeconds(max)};
	}

	@Override
	public MetaComponent<JInternalFrame> getUI() {
		Session session = parentProject().currentSession;
		MetaComponent<JInternalFrame> meta = session.windowManager.getWindow("Pattern Track Editor");
		PatternTrackEditor editor;
		if(meta==null||meta.component.isClosed()){
			editor = PatternTrackEditor.createNew();
			editor.session = session;
			editor.init();
			JInternalFrame wrapped = PivotSwingUtils.wrapPivotWindow(editor);
			meta = new MetaComponent<>("Default Pattern Track Editor","Pattern Track Editor",wrapped,null);
			meta.metaData.put("window", editor);
		}
		editor = (PatternTrackEditor) meta.metaData.get("window");
		editor.addEditorData(this);
		return meta;
	}

	@Override
	public ViewComponent getViewComponent() {
		return new PatternTrackPreview(this);
	}
	
	public boolean equals(Object o){
		if(o==this)return true;
		if(o==null || !(o instanceof PatternTrack))return false;
		PatternTrack other = (PatternTrack) o;
		return patterns.equals(other.patterns);
	}
	
	public int hashCode(){
		HashTriArx hash = new HashTriArx(HK_HC);
		hash.absorbObj(patterns);
		return hash.squeezeInt();
	}
	
	public String getName(){
		if(name==null || name.length()==0)name = Named.defaultName("Pattern Track", this);
		return name;
	}
	
	public boolean setName(String newName){
		name=newName;
		return true;
	}

}
