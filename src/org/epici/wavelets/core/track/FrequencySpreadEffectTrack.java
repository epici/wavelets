package org.epici.wavelets.core.track;

import java.util.Arrays;
import javax.swing.JComponent;

import org.apache.commons.math3.distribution.LogNormalDistribution;
import org.epici.wavelets.core.MetaComponent;
import org.epici.wavelets.core.MetaSamples;
import org.epici.wavelets.core.Project;
import org.epici.wavelets.core.Samples;
import org.epici.wavelets.core.Track;
import org.epici.wavelets.core.TransientContainer;
import org.epici.wavelets.util.Bits;
import org.epici.wavelets.util.math.FFTRadix2;

/**
 * A track that applies the effect of spreading in the frequency domain.
 * <hr>
 * The exact operation works on a continuous signal, and can be described like so:
 * <ol>
 * <li>Apply the Fourier transform.</li>
 * <li>Map it using the logarithm. In other words, if this is the
 * probability density function of a variable <i>X</i>
 * (ignore un-normalized and bad range), then get the
 * probability density function of <i>ln(X)</i></li>
 * <li>Convolve it by the normal distribution with mean <i>0</i>
 * and standard deviation some constant.</i>
 * <li>Map it using the exponential. In other words, if this is the
 * probability density function of a variable <i>X</i>
 * (ignore un-normalized and bad range), then get the
 * probability density function of <i>e<sup>X</sup></i></li>
 * <li>Apply the inverse Fourier transform.</li>
 * </ol>
 * The preliminary conversion steps bring it from the time domain
 * to the log-frequency domain, where some constant change in pitch
 * becomes the same signed difference in the x axis (log frequency).
 * The convolution spreads it so each pitch bleeds somewhat onto
 * nearby pitches, and is less sharp. The final conversion steps
 * bring it back to the time domain.
 * <br><br>
 * In old music, a similar effect would be accomplished using detune.
 * There would be several audio sources at the same pitch, each with a slightly
 * higher or lower pitch than normal, which resulted in a combined
 * sound that was like the original and at the same pitch, but
 * sounding somewhat different, notably it also sounds louder
 * despite not having the same amount of energy.
 * This new method attempts to improve on that old technique,
 * by having a continuous spread rather than a finite number of points.
 * <hr>
 * In practice, we do not have the entire reals to work with,
 * and the domain is not continuous. We will be working on a small
 * finite discrete sample at a time, this is the audio buffer.
 * This class uses fast rough approximations of the exact version.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class FrequencySpreadEffectTrack implements Track, TransientContainer<LayeredTrack> {
	private static final long serialVersionUID = 1L;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean setName(String newName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void initTransient(LayeredTrack parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void applyTo(MetaSamples current) {
		// TODO Auto-generated method stub

	}

	@Override
	public double[] getTimeBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Project parentProject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean setParent(Object newParent) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public MetaComponent<? extends JComponent> getUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ViewComponent getViewComponent() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Interface for implementations/approximations
	 * of the spread specified by {@link FrequencySpreadEffectTrack}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.1
	 */
	public static interface Spreader{
		
		/**
		 * Apply the spread operation. It should be in place
		 * in the sense that the result is stored back
		 * in the same samples object.
		 * <br><br>
		 * The specification for {@link FrequencySpreadEffectTrack}
		 * includes a convolution by a normal distribution.
		 * The standard deviation is one of the parameters of this function.
		 * However, it is not required that implementations actually
		 * do this convolution, as long as they use the parameter to
		 * be consistent with that definition.
		 * 
		 * @param samples samples to operate on, with metadata
		 * @param standardDeviation standard deviation of
		 * normal distribution in convolution in spread as
		 * specified by {@link FrequencySpreadEffectTrack}
		 */
		public void spread(MetaSamples samples,double standardDeviation);
		
	}
	
	/**
	 * An implementation of the spread. It is a rough approximation
	 * that runs in <i>O(n log n)</i> time and uses <i>O(n)</i> memory.
	 * It only works on power of 2 length samples.
	 * <br><br>
	 * It is relatively close to the original specification. What it does:
	 * <ol>
	 * <li>Apply the Fourier transform to convert it to the frequency domain.</li>
	 * <li>Split it into slices from <i>2^n</i> inclusive to <i>2^(n+1)</i> exclusive.
	 * Each has a power of 2 length and is double the length of the previous.
	 * Ignore index 0.</li>
	 * <li>Convolve each slice by the corresponding log-normal distribution.
	 * This is approximately correct
	 * since <i>e^x</i> is approximately linear in a small range.</li>
	 * <li>Combine the slices back by adding.</li>
	 * <li>Apply the inverse Fourier transform to convert it back to the time domain.</li>
	 * </ol>
	 * There are some more implementation specific details, but this is the general
	 * idea behind this algorithm.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.1
	 */
	public static class DoublingConvolutionSpreader implements Spreader{
		
		/**
		 * The parameter MU (mean) in the log-normal distribution.
		 * <br>
		 * Since an interval extends from 1 to 2 before remapping,
		 * the center should be at <i>1/ln(2)</i>.
		 */
		public static final double CONVOLUTION_MEAN = -Math.log(Math.log(2d));
		
		/**
		 * {@link #CONVOLUTION_MEAN}, as an instance variable so it can
		 * be changed.
		 */
		public double convolutionMean = CONVOLUTION_MEAN;
		
		/**
		 * Remember the most recently returned result of
		 * {@link #getConvolutionVector(Samples, int, double)}.
		 */
		protected double[] lastConv;
		/**
		 * Remember the original array which was used to derive
		 * the values returned by
		 * {@link #getConvolutionVector(Samples, int, double)}.
		 * Contains specific samplings of the CDF of the distribution.
		 */
		protected double[] lastConvSource;
		
		/**
		 * Get the convolution vector as a samples object.
		 * Its length should be double the requested length, but only the first half
		 * should have content. That first half should be the
		 * log-normal distribution for that length, or its
		 * drop in replacement. Values should be normalized.
		 * If the previous one (larger) is provided, it is allowed
		 * to do take some shortcuts using the already computed values.
		 * <br><br>
		 * Note to implementors: avoid doing any FFT work.
		 * Don't call {@link Samples#fft(int)} or anything like that.
		 * 
		 * @param previous optionally the previous samples object
		 * @param length length of segment, must be power of 2
		 * @param standardDeviation standard deviation of distribution
		 * @return convolution vector
		 */
		public Samples getConvolutionVector(Samples previous, int length, double standardDeviation){
			/*
			 * We can't squish directly, because the value halfway must reflect
			 * a range centered on the mean value.
			 * They just don't line up, ever.
			 * This is what the resampling would have to be like:
			 * 
			 * ( 1-length array is just [1.0] )
			 * 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1        2-length
			 *     0 0 0 0 1 1 1 1 2 2 2 2 3 3 3 3    4-length
			 *       0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7  8-length
			 * ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ 19-length (2.5x - 1)
			 *                        | here is mean at 12     (1.5x)
			 * 
			 * It's not possible without knowledge of the original.
			 */
			int slength = length<<1;
			Samples result = Samples.blankSamples(1, slength);
			double[] array = result.sampleData;
			if(length==1) {// hardcoded special case
				array[0] = 1;
				return result;
			}
			double[] lastConv = this.lastConv, lastConvSource = this.lastConvSource;
			double[] parray;
			// see if we can reuse results
			if(lastConv != null && previous != null && lastConv == (parray=previous.sampleData)) {
				int flength = lastConvSource.length;
				int olength = flength;// original length
				olength >>= 1;
				olength ^= olength >> 2;
				olength ^= olength >> 4;
				olength ^= olength >> 8;
				olength ^= olength >> 16;
				int rlength = olength/length;// lengths ratio
				int first = (length>>1)-rlength;
				int skip = rlength<<1;
				double normalize = 1d/(lastConvSource[first+(length<<1)]-lastConvSource[first]);
				for(int i=0;i<length;i++){
					int j = first+(i<<1);
					array[i] = normalize*(lastConvSource[j+skip]-lastConvSource[j]);
				}
				this.lastConv = result.sampleData;
				return result;
			}
			int flength = slength|(length>>1);// 2.5x length
			this.lastConvSource = lastConvSource = new double[flength];
			LogNormalDistribution dist = new LogNormalDistribution(null, convolutionMean, standardDeviation);
			double ilength = 1d/length;
			double ofs = Math.exp(convolutionMean)-0.5;// so 1.5x length hits mean
			for(int i=0;i<flength;i++){
				double p = ofs+i*ilength;
				lastConvSource[i] = dist.cumulativeProbability(p);
			}
			int first = (length>>1)-1;
			int skip = 2;
			double normalize = 1d/(lastConvSource[first+(length<<1)]-lastConvSource[first]);
			for(int i=0;i<length;i++){
				int j = first+(i<<1);
				array[i] = normalize*(lastConvSource[j+skip]-lastConvSource[j]);
			}
			this.lastConv = result.sampleData;
			return result;
		}

		@Override
		public void spread(MetaSamples samples, double standardDeviation) {
			// do initial FFT
			samples.checkFft();
			// get info
			double[] osampleReal = samples.spectrumReal,
					osampleImag = samples.spectrumImag;
			int length = osampleReal.length;
			int loglength = Bits.binLog(length);
			double[] sampleReal = samples.spectrumReal = new double[length],
					sampleImag = samples.spectrumImag = new double[length];
			Samples conv = null;
			// slices loop
			for(int i=loglength-1;i>=0;i--){
				int j = 1<<i, k = j<<1;
				FFTRadix2 fft = FFTRadix2.getFft(i+1);
				int offsetBackward = j>>1;
				// get convolution vector
				conv = getConvolutionVector(conv, j, standardDeviation);
				conv.checkFft();
				// get slice
				double[] sliceReal = new double[k], sliceImag = new double[k];
				System.arraycopy(osampleReal, j, sliceReal, 0, j);
				System.arraycopy(osampleImag, j, sliceImag, 0, j);
				fft.fft(sliceReal, sliceImag);
				// do the convolution, via FFT then complex multiplication then IFFT
				double[] convReal = conv.spectrumReal, convImag = conv.spectrumImag;
				for(int index=0;index<k;index++){
					double sr = sliceReal[index],
							si = sliceImag[index],
							cr = convReal[index],
							ci = convImag[index];
					sliceReal[index] = sr*cr-si*ci;
					sliceImag[index] = sr*ci+si*cr;
				}
				fft.ifft(sliceReal, sliceImag);
				// add the slice
				int offset = j - offsetBackward;
				int lim = Math.min(k, length - offset);
				for(int index=0;index<lim;index++){
					int oindex = index + offset;
					sampleReal[oindex] += sliceReal[index];
					sampleImag[oindex] += sliceImag[index];
				}
			}
			// final IFFT
			samples.checkIfft();
		}
		
	}

}
