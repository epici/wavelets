package org.epici.wavelets.core.track;

import org.epici.wavelets.core.Project;

/**
 * Dummy interface which both {@link LayeredTrack} and {@link Project}
 * implement, since they can both be parents of a TLC
 * 
 * @author EPICI
 * @version 1.0
 */
public interface LayeredTrackParent {

}
