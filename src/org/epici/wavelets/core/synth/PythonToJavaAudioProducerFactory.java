package org.epici.wavelets.core.synth;

import java.util.ArrayList;
import java.util.Objects;
import org.apache.commons.collections4.list.LazyList;
import org.epici.wavelets.core.AudioProducer;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.Synthesizer;
import org.epici.wavelets.core.track.PatternTrack;
import org.epici.wavelets.util.jython.Factory;
import org.python.core.Py;
import org.python.core.PyObject;

/**
 * An implementation of {@link Factory} of {@link AudioProducer}.
 * Suitable as a return value of {@link Synthesizer#getAudioProducerFactory()}.
 * Converts a call to {@link Factory#create(PyObject[], String[])}
 * into a suitable call to {@link Synthesizer#spawnVoices(double[][], PatternTrack, Session)}
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class PythonToJavaAudioProducerFactory implements Factory<AudioProducer> {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The {@link Synthesizer} which will be used to generate
	 * {@link AudioProducer} instances.
	 */
	public Synthesizer view;

	/**
	 * Initialize it to wrap a particular {@link Synthesizer}.
	 * 
	 * @param view the value of {@link #view} to set
	 */
	public PythonToJavaAudioProducerFactory(Synthesizer view) {
		this.view = view;
	}

	@Override
	public AudioProducer create(PyObject[] args, String[] keywords) {
		Objects.requireNonNull(view);
		// positional || keyword
		int pkcount = args.length;
		// keyword
		int kcount = keywords.length;
		// positional
		int pcount = pkcount - kcount;
		// make empty holder variables
		Session session = null;
		// [start, end, pitch, volume, *others]
		LazyList<Double> parameters = LazyList.lazyList(
				new ArrayList<>(),
				()->null
				);
		// put parameters in list, keywords then positional
		for(int i=0;i<kcount;i++) {
			String key = keywords[i];
			PyObject value = args[i+pcount];
			int listIndex = -1;
			switch(key) {
			case "session":{
				session = Py.tojava(value, Session.class);
				break;
			}
			case "start":{
				listIndex = 0;
				break;
			}
			case "end":{
				listIndex = 1;
				break;
			}
			case "pitch":{
				listIndex = 2;
				break;
			}
			case "volume":{
				listIndex = 3;
				break;
			}
			default:{
				listIndex = -2;
				break;
			}
			}
			if(listIndex != -1) {// -1 was signal that nothing happened
				double dvalue = value.asDouble();
				if(listIndex < 0) {// signal to add to back
					parameters.add(Math.max(4, parameters.size()), dvalue);
				} else {// add at index
					parameters.set(listIndex, dvalue);
				}
			}
		}
		for(int i=0,listIndex=0;i<pcount;i++) {
			PyObject value = args[i];
			double dvalue = value.asDouble();
			// find first null slot
			while(parameters.get(listIndex) != null)listIndex++;
			parameters.set(listIndex, dvalue);
		}
		// convert parameters to array
		int nparameters = parameters.size();
		double[] clips = new double[nparameters];
		for(int i=0;i<nparameters;i++) {
			clips[i] = parameters.get(i);
		}
		// make dummy target
		ArrayList<AudioProducer> audioProducers = new ArrayList<>();
		// send audio producers there
		view.spawnVoices(
				new double[][] {clips},
				audioProducers::add,
				session);
		// fetch audio producers and return
		return AudioProducer.combine(audioProducers);
	}

}
