package org.epici.wavelets.core.synth;

import java.awt.Color;
import java.awt.color.ColorSpace;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import org.apache.pivot.wtk.Component;
import org.epici.wavelets.core.AudioProducer;
import org.epici.wavelets.core.BetterClone;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Named;
import org.epici.wavelets.core.Project;
import org.epici.wavelets.core.Samples;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.Synthesizer;
import org.epici.wavelets.core.VarDouble;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.synth.primitives.SynthPrimitivesEditor;
import org.epici.wavelets.ui.synth.primitives.SynthPrimitivesPreview;
import org.epici.wavelets.util.math.Bezier;
import org.epici.wavelets.util.math.Floats;
import org.epici.wavelets.util.ui.Light;
import org.epici.wavelets.util.waveform.PrimitiveWaveforms;
import org.python.core.PyObject;

/**
 * The &quot;Primitives Synthesizer&quot;, which produces sound by adding
 * multiple simpler waveforms. It provides some rough working material
 * which can be turned into less regular and more useful sound by
 * other things.
 * 
 * @author EPICI
 * @version 1.0
 */
public class SynthPrimitives implements BetterClone<SynthPrimitives>, Synthesizer, DataEditor.Editable<SynthPrimitivesEditor, SynthPrimitivesPreview> {
	private static final long serialVersionUID = 1L;

	private static final double SEMITONE = StrictMath.pow(2d, 1d/12d);
	private static final double LSEMITONE = Floats.LOG2/12;
	
	/**
	 * Simple oscillators
	 */
	public final List<Osc> oscillators;
	
	/**
	 * The parent project
	 */
	protected transient Project parentProject;
	
	/**
	 * Destroyed yet?
	 */
	protected transient boolean destroyed = false;
	/**
	 * The name of this synthesizer. Use getter and setter instead of direct access.
	 */
	public String name;
	
	/**
	 * Default constructor
	 * 
	 * @param parent the project this is in
	 */
	public SynthPrimitives(Project parent){
		oscillators = Collections.synchronizedList(new ArrayList<Osc>());
		initTransient(parent);
	}

	@Override
	public void initTransient(Project parent) {
		parentProject = parent;
	}

	@Override
	public void destroy() {
		oscillators.clear();
		destroyed = true;
	}

	@Override
	public void destroySelf() {
		oscillators.clear();
		destroyed = true;
	}
	
	@Override
	public boolean isDestroyed(){
		return destroyed;
	}

	@Override
	public SynthPrimitivesEditor getEditor() {
		return SynthPrimitivesEditor.createNew(this);
	}

	@Override
	public SynthPrimitivesPreview getPreview() {
		SynthPrimitivesPreview result = new SynthPrimitivesPreview();
		result.view = this;
		return result;
	}

	@Override
	public void spawnVoices(double[][] clips, Consumer<AudioProducer> target, Session session) {
		int n = oscillators.size();
		Osc[] losc = oscillators.toArray(new Osc[n]);
		for(double[] clip:clips){
			AudioProducer[] oscvoices = new AudioProducer[n];
			for(int i=0;i<n;i++){
				Osc.OscAudioProducer added = losc[i].spawn(
						clip[2], // second
						clip[0], // second
						clip[1], // 1/12 bit from 440 Hz
						clip[3]); // bel
				added.delay = clip[0];
				oscvoices[i]=added;
			}
			AudioProducer combined = AudioProducer.combine(oscvoices);
			target.accept(combined);
		}
	}

	@Override
	public AudioProducer spawnLiveAudioProducer(double[] params, Session session) {
		double ctime = session.getCurrentTime();
		double pitch = params[0];
		double volume = params[1];
		int n = oscillators.size();
		AudioProducer[] all = new AudioProducer[n];
		for(int i=0;i<n;i++){
			Osc.OscAudioProducer added = oscillators.get(i).spawn(pitch, ctime, ctime+Floats.ID_TINY, volume);
			all[i] = added;
			added.setStepMax(1);
		}
		return AudioProducer.combine(all);
	}

	@Override
	public void setGlobals(HashMap<String, Object> vars) {
		
	}

	@Override
	public boolean isPython() {
		return false;
	}

	@Override
	public PyObject getPvfInfo() {
		return null;
	}

	@Override
	public Color getColorSignature(double time){
		// keep track of sum in CIE XYZ
		double[] xyzsum = new double[3];
		// get partial sums for oscillators
		for(Osc osc:oscillators) {
			double[] ixyz = osc.getColorSignature(time);
			for(int i=0;i<3;i++) {
				xyzsum[i] += ixyz[i];
			}
		}
		return ColorScheme.fromCIEXYZUnbounded(xyzsum);
	}
	
	/**
	 * Characteristic signature
	 * 
	 * @param v a value
	 * @return a compressed view
	 * 
	 * @deprecated
	 */
	private static long sig(double v){
		long bits = Double.doubleToLongBits(v);
		return bits>>>48;
	}
	
	@Override
	public String getName(){
		if(name==null || name.length()==0)name = Named.defaultName("Synthesizer", this);
		return name;
	}
	
	@Override
	public boolean setName(String newName){
		name = newName;
		return true;
	}
	
	/**
	 * Create a synthesizer object according to default settings.
	 * In the current implementation this will be named
	 * like &quot;Primitives Synth&quot; using
	 * {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, Session)}.
	 * 
	 * @param session current session
	 * @return a new synth object with default values
	 */
	public static SynthPrimitives makeDefaultSynth(Session session){
		// create the object
		SynthPrimitives result = new SynthPrimitives(session.project);
		// add saw oscillator
		Osc osc = new Osc(result);
		osc.type = 3;
		result.oscillators.add(osc);
		// set name
		result.setName(
				session.project.synths.nextName(
						session.getCommonName(SynthPrimitives.class),
						0, false, session));
		// return
		return result;
	}
	
	public SynthPrimitives copy(int depth,Map<String,Object> options){
		int nextDepth = depth-1;
		String newName = name;
		// the list will be modified anyway
		ArrayList<Osc> newOscillators = new ArrayList<Osc>(oscillators);
		Map<String,Object> set = (Map<String,Object>)options.get("set");
		Number val;
		CharSequence sval;
		sval = (CharSequence) set.get(SYNTHNOSC_CLASS_NAME+".name");
		if(sval!=null)newName = sval.toString();
		// filter
		Predicate<? super Osc> pval = (Predicate<? super Osc>) set.get(SYNTHNOSC_CLASS_NAME+".oscillators.filter");
		if(pval!=null){
			newOscillators.removeIf(pval.negate());
		}
		// any iterable is allowed, valid values override old ones
		Iterable<?> lval = (Iterable<?>) set.get(SYNTHNOSC_CLASS_NAME+".oscillators");
		int size = newOscillators.size();
		if(lval!=null){
			Iterator<?> iter = lval.iterator();
			// exhaust as many items as possible which don't need extending the list
			int i;
			for(i=0;i<size && iter.hasNext();i++){
				Object value = iter.next();
				if(value!=null && (value instanceof Osc)){
					newOscillators.set(i, (Osc)value);
				}
			}
			// do remaining items, if any
			while(iter.hasNext()){
				Object value = iter.next();
				if(value!=null && (value instanceof Osc)){
					newOscillators.add((Osc)value);
				}
			}
		}
		// need to limit length?
		int j;
		if((val = (Number) set.get(SYNTHNOSC_CLASS_NAME+".oscillators.size"))!=null
				&& (j = val.intValue())<size){
			for(size--;size>=j;size--){
				newOscillators.remove(size);
			}
		}
		// correct name
		Session session = (Session) options.get("session");
		newName = session.project.synths.nextName(newName, -1, false, session);
		// copy properties as needed
		SynthPrimitives result = new SynthPrimitives(parentProject);
		set.put(OSC_CLASS_NAME+".parent", result);
		size = newOscillators.size();
		for(int i=0;i<size;i++){
			Osc value = newOscillators.get(i);
			value = BetterClone.copy(value, nextDepth, options);
			newOscillators.set(i, value);
		}
		// make the copied object
		result.setName(newName);
		result.oscillators.addAll(newOscillators);
		return result;
	}
	
	private static final String SYNTHNOSC_CLASS_NAME = Osc.class.getCanonicalName();
	private static final String OSC_CLASS_NAME = Osc.class.getCanonicalName();
	private static final String[] OSC_PROPERTIES_NAMES = {
			"detune",
			"volume",
			"attackConst",
			"attackFrac",
			"holdConst",
			"holdFrac",
			"decayConst",
			"decayFrac",
			"minVolume",
	};
	
	/**
	 * An oscillator used by this class
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class Osc implements BetterClone<Osc>{
		
		/**
		 * The highest frequency of sound
		 * used normally by humans in Hertz,
		 * <i>5 kHz</i>.
		 */
		public static final double FREQUENCY_MAX_SOUND = 5e3;
		/**
		 * The highest frequency of light
		 * which can be which by humans in Hertz,
		 * <i>770 THz</i>.
		 */
		public static final double FREQUENCY_MAX_LIGHT = 770e12;
		/**
		 * Scale factor which maps {@link FREQUENCY_MAX_SOUND}
		 * to {@link FREQUENCY_MAX_LIGHT}
		 * 
		 * @see org.epici.wavelets.util.ui.Light
		 */
		public static final double SIGNATURE_FREQUENCY_SCALE = FREQUENCY_MAX_LIGHT/FREQUENCY_MAX_SOUND;
		/**
		 * The sound frequency which an offset of 0
		 * will be mapped to in Hertz when generating the signature colours.
		 * This is <i>440 Hz &times; 8</i>.
		 * In classical music terms this is A7.
		 * It gets mapped to a green colour.
		 */
		public static final int SIGNATURE_FREQUENCY_BASE = 3520;
		/**
		 * The color signature is based on sampling sound
		 * that would be produced by this oscillator.
		 * How long should a regular period be, in samples?
		 * That is the value here.
		 */
		public static final int SIGNATURE_PERIOD = 4;
		/**
		 * The color signature is based on sampling sound
		 * that would be produced by this oscillator.
		 * How many periods long should the sample time be?
		 */
		public static final int SIGNATURE_REPETITIONS = 8;
		/**
		 * The color signature is based on sampling sound
		 * that would be produced by this oscillator.
		 * How many periods long should a fake second be?
		 */
		public static final int SIGNATURE_TIME_SCALE = 2;
		
		/**
		 * The parent synthesizer.
		 */
		public SynthPrimitives parent;
		/**
		 * Primitive waveform used
		 * <table>
		 * <caption>Table assigning the integers counting up from 0
		 * to primitive waveforms in a one-to-one correspondence.</caption>
		 * <thead>
		 * <tr><th>Type</th><th>ID</th></tr>
		 * </thead><tbody>
		 * <tr><td>Sine</td><td>0</td></tr>
		 * <tr><td>Square</td><td>1</td></tr>
		 * <tr><td>Triangle</td><td>2</td></tr>
		 * <tr><td>Saw</td><td>3</td></tr>
		 * </tbody>
		 * </table>
		 */
		public volatile int type;
		/**
		 * Holds the other properties
		 * <br>
		 * Arrays allow for easy automation
		 */
		public final VarDouble[] properties;
		
		private static final double[] SIGNATURE_XBAR = new double[16];
		private static final double[] SIGNATURE_YBAR = new double[16];
		private static final double[] SIGNATURE_ZBAR = new double[16];
		
		static {
			// used in color signature
			final int nsamples = SIGNATURE_PERIOD*SIGNATURE_REPETITIONS, samplerate = SIGNATURE_FREQUENCY_BASE/SIGNATURE_REPETITIONS;
			for(int i=1;i<nsamples/2;i++) {
				double wavelength = Light.SPEED/(samplerate*i*SIGNATURE_FREQUENCY_SCALE);
				SIGNATURE_XBAR[i] = Light.cie1931xbar(wavelength);
				SIGNATURE_YBAR[i] = Light.cie1931ybar(wavelength);
				SIGNATURE_ZBAR[i] = Light.cie1931zbar(wavelength);
			}
		}
		
		public Osc(SynthPrimitives parent){
			this.parent = parent;
			type = 0;
			properties = VarDouble.wrapCopy(new double[9]);
			setDetune(0d);
			setVolume(0d);
			setAttackConst(0d);
			setAttackFrac(0d);
			setHoldConst(0d);
			setHoldFrac(1d);
			setDecayConst(0d);
			setDecayFrac(0d);
			setMinVolume(0d);
		}
		
		/**
		 * Get the (min, base, max) for each property
		 * which would be used in a
		 * {@link org.epici.wavelets.ui.curve.DoubleInput.DoubleValidator.BoundedDoubleValidator}.
		 * Index <i>i</i> in the array returned by this function
		 * corresponds to index <i>i</i> in {@link #properties}.
		 * 
		 * @return (min, base, max) for each property
		 */
		public static double[][] getPropertyBounds(){
			// make big array
			double[][] result = new double[9][];
			// fill with common values
			for(int i=0;i<result.length;i++) {
				result[i] = new double[] {-Double.MAX_VALUE, 0d, Double.MAX_VALUE};
			}
			// some have base 1 instead of 0
			result[5][1]
					= 1;
			// some must be positive
			result[2][0]
				= result[3][0]
				= result[4][0]
				= result[5][0]
				= result[6][0]
				= result[7][0]
				= result[8][0]
					= Floats.D_TINY;
			// some must be negative or 0
			// correct bad base
			for(int i=0;i<result.length;i++) {
				result[i][1] = Floats.median(result[i][0], result[i][1], result[i][2]);
			}
			// and done
			return result;
		}
		
		/**
		 * Detune in semitones
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getDetune(double time) {
			return properties[0].get(time);
		}

		/**
		 * Detune in semitones
		 * 
		 * @param detune new value to set
		 */
		public synchronized void setDetune(double detune) {
			properties[0].set(detune);
		}

		/**
		 * Volume offset in B
		 * <br>
		 * Determines peak for envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getVolume(double time) {
			return properties[1].get(time);
		}

		/**
		 * Volume offset in B
		 * <br>
		 * Determines peak for envelope
		 * 
		 * @param volume new value to set
		 */
		public synchronized void setVolume(double volume) {
			properties[1].set(volume);
		}

		/**
		 * Duration in measures to reach full volume
		 * <br>
		 * Part of envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getAttackConst(double time) {
			return properties[2].get(time);
		}

		/**
		 * Duration in measures to reach full volume
		 * <br>
		 * Negative snaps to threshold
		 * <br>
		 * Part of envelope
		 * 
		 * @param attackConst new value to set
		 */
		public synchronized void setAttackConst(double attackConst) {
			final double t=Floats.D_TINY;
			properties[2].set(attackConst<t?t:attackConst);
		}

		/**
		 * Fraction of note length to reach full volume
		 * <br>
		 * Part of envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getAttackFrac(double time) {
			return properties[3].get(time);
		}

		/**
		 * Fraction of note length to reach full volume
		 * <br>
		 * Negative snaps to threshold
		 * <br>
		 * Part of envelope
		 * 
		 * @param attackFrac new value to set
		 */
		public synchronized void setAttackFrac(double attackFrac) {
			final double t=Floats.D_TINY;
			properties[3].set(attackFrac<t?t:attackFrac);
		}

		/**
		 * Duration in measures to hold at full volume
		 * <br>
		 * Part of envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getHoldConst(double time) {
			return properties[4].get(time);
		}

		/**
		 * Duration in measures to hold at full volume
		 * <br>
		 * Negative snaps to threshold
		 * <br>
		 * Part of envelope
		 * 
		 * @param holdConst new value to set
		 */
		public synchronized void setHoldConst(double holdConst) {
			final double t=Floats.D_TINY;
			properties[4].set(holdConst<t?t:holdConst);
		}

		/**
		 * Fraction of note length to hold at full volume
		 * <br>
		 * Part of envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getHoldFrac(double time) {
			return properties[5].get(time);
		}

		/**
		 * Fraction of note length to hold at full volume
		 * <br>
		 * Negative snaps to threshold
		 * <br>
		 * Part of envelope
		 * 
		 * @param holdFrac new value to set
		 */
		public synchronized void setHoldFrac(double holdFrac) {
			final double t=Floats.D_TINY;
			properties[5].set(holdFrac<t?t:holdFrac);
		}

		/**
		 * Duration in measures to fade out
		 * <br>
		 * Part of envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getDecayConst(double time) {
			return properties[6].get(time);
		}

		/**
		 * Duration in measures to fade out
		 * <br>
		 * Negative snaps to threshold
		 * <br>
		 * Part of envelope
		 * 
		 * @param decayConst new value to set
		 */
		public synchronized void setDecayConst(double decayConst) {
			final double t=Floats.D_TINY;
			properties[6].set(decayConst<t?t:decayConst);
		}

		/**
		 * Duration in note lengths to fade out
		 * <br>
		 * Part of envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getDecayFrac(double time) {
			return properties[7].get(time);
		}

		/**
		 * Duration in note lengths to fade out
		 * <br>
		 * Negative snaps to threshold
		 * <br>
		 * Part of envelope
		 * 
		 * @param decayFrac new value to set
		 */
		public synchronized void setDecayFrac(double decayFrac) {
			final double t=Floats.D_TINY;
			properties[7].set(decayFrac<t?t:decayFrac);
		}

		/**
		 * Threshold to kill the voice in B, so when the current
		 * volume is this much lower than the cap, kill the voice
		 * <br>
		 * 0 insta-kills
		 * <br>
		 * Part of envelope
		 * 
		 * @param time applicable if automated
		 * @return the current value
		 * @see VarDouble
		 */
		public synchronized double getMinVolume(double time) {
			return properties[8].get(time);
		}

		/**
		 * Threshold to kill the voice in B, so when the current
		 * volume is this much lower than the cap, kill the voice
		 * <br>
		 * Negative snaps to threshold
		 * <br>
		 * 0 would insta-kill it
		 * <br>
		 * Part of envelope
		 * 
		 * @param minVolume new value to set
		 */
		public synchronized void setMinVolume(double minVolume) {
			final double t=Floats.D_TINY;
			properties[8].set(minVolume<t?t:minVolume);
		}
		
		/**
		 * Allow spawning from outside
		 * 
		 * @param pitch pitch as semitones from A4 (440Hz)
		 * @param start start time in seconds
		 * @param end end time in seconds
		 * @return a voice for this oscillator
		 */
		public OscAudioProducer spawn(double pitch,double start,double end){
			return spawn(pitch,start,end,0);
		}
		
		/**
		 * Allow spawning from outside
		 * 
		 * @param pitch pitch as semitones from A4 (440Hz)
		 * @param start start time in seconds
		 * @param end end time in seconds
		 * @param volume overall volume offset in B
		 * @return a voice for this oscillator
		 */
		public OscAudioProducer spawn(double pitch,double start,double end,double volume){
			return new Osc.OscAudioProducer(pitch,start,end,volume);
		}
		
		@Override
		public Osc copy(int depth,Map<String,Object> options){
			int nextDepth = depth-1;
			int newType = type;
			SynthPrimitives parent = this.parent;
			VarDouble[] newProperties = properties;
			int nproperties = newProperties.length;
			newProperties = Arrays.copyOf(newProperties, nproperties);
			Collection<String> blacklist = (Collection<String>)options.get("blacklist");
			Collection<String> whitelist = (Collection<String>)options.get("whitelist");
			Map<String,Object> set = (Map<String,Object>)options.get("set");
			VarDouble dval;
			for(int i=0;i<nproperties;i++){
				final String FIELD_NAME = OSC_CLASS_NAME+OSC_PROPERTIES_NAMES[i];
				dval = (VarDouble) set.get(FIELD_NAME);
				if(dval!=null){
					newProperties[i] = dval;
				}else if(!BetterClone.fieldIncluded(blacklist, null,
								FIELD_NAME)
						|| BetterClone.fieldIncluded(whitelist, null,
								FIELD_NAME)){
					newProperties[i] = BetterClone.tryCopy(newProperties[i], depth-1, options);
				}
			}
			Number val;
			val = (Number) set.get(OSC_CLASS_NAME+".type");
			if(val!=null)newType = val.intValue();
			SynthPrimitives sval;
			sval = (SynthPrimitives) set.get(OSC_CLASS_NAME+".parent");
			if(sval!=null)parent = sval;
			// make the copied object
			Osc result = new Osc(parent);
			result.type = newType;
			return result;
		}
		
		/**
		 * Get the colour signature of this oscillator.
		 * Result will be a (X, Y, Z) tuple for the CIE 1931 XYZ color space.
		 * Values are not normalized.
		 * 
		 * @param time the specific time to measure at, use 0 if unknown
		 * @return colour signature for this oscillator
		 */
		public double[] getColorSignature(double time) {
			final int nsamples = SIGNATURE_PERIOD*SIGNATURE_REPETITIONS, fakesamplerate = SIGNATURE_PERIOD*SIGNATURE_TIME_SCALE;
			final double dt = 8d/fakesamplerate;
			Samples samples = getQuickSamples(nsamples, time, time+dt, dt, fakesamplerate, SIGNATURE_PERIOD);
			samples.checkFft();
			double[] specreal = samples.spectrumReal;
			double[] specimag = samples.spectrumImag;
			double[] result = new double[3];
			for(int i=1;i<nsamples/2;i++) {
				double mul = Math.hypot(specreal[i], specimag[i]);
				result[0] += mul * SIGNATURE_XBAR[i];
				result[1] += mul * SIGNATURE_YBAR[i];
				result[2] += mul * SIGNATURE_ZBAR[i];
			}
			return result;
		}

		/**
		 * Generate one-off sample data.
		 * 
		 * @param nsamples length of output [samples]
		 * @param start clip start time [seconds]
		 * @param end clip end time [seconds]
		 * @param length of a measure [seconds]
		 * @param fakesamplerate sample rate to use here only [samples/second]
		 * @param period period of the oscillator [samples]
		 * @return sample data
		 */
		public Samples getQuickSamples(int nsamples, double start, double end, double measure, int fakesamplerate, double period) {
			OscAudioProducer ap = spawn(0, start, end);
			ap.freq = fakesamplerate/period;
			ap.sampleRate = fakesamplerate;
			ap.sampleLength = 1d/ap.sampleRate;
			ap.measure = measure;
			Samples result = ap.nextSegment(nsamples);
			ap.destroy();
			return result;
		}
		
		/**
		 * A voice for this oscillator
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public class OscAudioProducer implements AudioProducer{
			
			/**
			 * Thread safe integer used to track the step
			 * <br>
			 * 0: Attack
			 * 1: Hold
			 * 2: Decay
			 * 3: Dead
			 */
			public final AtomicInteger step;
			/**
			 * Frequency pre-adjustment
			 */
			public double freq;
			/**
			 * Time pre-adjustment
			 */
			public double time;
			/**
			 * Phase (for consistent waveforms)
			 */
			public double phase;
			/**
			 * The time of switching to hold
			 */
			public double switched;
			/**
			 * Volume offset in nat logarithmic
			 */
			public double mult;
			/**
			 * Independent volume offset applied after in nat logarithmic
			 */
			public double multOver;
			/**
			 * Sample rate
			 */
			public int sampleRate;
			/**
			 * Seconds per sample
			 */
			public double sampleLength;
			/**
			 * Length of measure in seconds (average)
			 */
			public double measure;
			/**
			 * Length of note in seconds
			 */
			public double note;
			/**
			 * Delay in seconds
			 */
			public double delay;
			
			/**
			 * Destroyed yet?
			 */
			protected transient boolean destroyed = false;
			
			/**
			 * Fill in fields automatically
			 * 
			 * @param pitch pitch as semitones from A4 (440Hz)
			 * @param start start time in seconds
			 * @param end end time in seconds
			 */
			public OscAudioProducer(double pitch,double start,double end){
				this(pitch,start,end,0);
			}
			
			/**
			 * Fill in fields automatically
			 * 
			 * @param pitch pitch as semitones from A4 (440Hz)
			 * @param start start time in seconds
			 * @param end end time in seconds
			 * @param volume overall volume offset
			 */
			public OscAudioProducer(double pitch,double start,double end,double volume){
				note=end-start;
				measure=(parent.parentProject.secondsToMeasures(end)
						-parent.parentProject.secondsToMeasures(start))
						/note;
				sampleRate=parent.parentProject.currentSession.getSampleRate();
				sampleLength=1d/sampleRate;
				time=0d;
				phase=0d;
				step=new AtomicInteger(0);
				mult=-getMinVolume(time)*Floats.LOG10;// note the negative!
				multOver=volume*Floats.LOG10;
				freq=440d*Math.exp(LSEMITONE*pitch);
			}

			@Override
			public void destroy() {
				this.delay = 0;
				setStepMax(3);
				destroyed = true;
			}

			@Override
			public void destroySelf() {
				this.delay = 0;
				setStepMax(3);
				destroyed = true;
			}
			
			@Override
			public boolean isDestroyed(){
				return destroyed;
			}

			@Override
			public Samples nextSegment(int sampleCount) {
				int cstep = step.get();
				double[] data = new double[sampleCount];
				if(cstep==3)return new Samples(sampleRate,data);
				int ltype = type;
				// Load values
				int sampleRate = this.sampleRate;
				double sampleLength = this.sampleLength;
				double phase = this.phase;
				double delay = this.delay;
				double mult = this.mult;
				double time = this.time;
				double switched = this.switched;
				double ldetune = getDetune(time), lvolume = getVolume(time)*Floats.LOG10, lattackConst = getAttackConst(time), lattackFrac = getAttackFrac(time), lholdConst = getHoldConst(time),
						lholdFrac = getHoldFrac(time), ldecayConst = getDecayConst(time), ldecayFrac = getDecayFrac(time), lminVolume = -getMinVolume(time)*Floats.LOG10;
				double afreq = freq*Math.exp(LSEMITONE*ldetune), aattack = -lminVolume/(sampleRate*(lattackConst*measure+lattackFrac*note)),
						ahold = lholdConst*measure+lholdFrac*note, adecay = -lminVolume/(sampleRate*(ldecayConst*measure+ldecayFrac*note)), lpreMult = Math.exp(lvolume+multOver);
				// fill with unscaled values
				double phaseinc = sampleLength * afreq;
				int j=0;
				if(delay>Floats.D_TINY){
					int delayFrames = (int)(delay*sampleRate)+1;
					if(delayFrames>=sampleCount) {
						delay -= sampleLength*sampleCount;
						j = sampleCount;
					} else {
						delay -= sampleLength*delayFrames;
						j = delayFrames;
					}
				}
				this.delay = delay;
				if(j<sampleCount){
					// mark beginning as [i]
					int i=j;
					// follow envelope and write
					if(cstep==0) {// volume increasing
						for(;j<sampleCount;j++) {
							if(mult>=0){
								cstep = 1;
								mult=0;
								switched=time;
								break;
							}
							double vol = lpreMult*Math.exp(mult);
							data[j] = vol;
							time += sampleLength;
							mult += aattack;
						}
					}
					if(cstep==1) {// volume constant
						for(;j<sampleCount;j++) {
							if(time-switched>=ahold){//Not optimized away because the hold can change, and we like real time editing
								cstep = 2;
								break;
							}
							double vol = lpreMult;
							data[j] = vol;
							time += sampleLength;
						}
					}
					if(cstep==2) {// volume decreasing
						for(;j<sampleCount;j++) {
							if(mult<=lminVolume){
								cstep = 3;
								break;
							}
							double vol = lpreMult*Math.exp(mult);
							data[j] = vol;
							time += sampleLength;
							mult -= adecay;
						}
					}
					// cstep == 3 -> do nothing
					this.mult = mult;
					this.time = time;
					this.switched = switched;
					// [j] now marks end
					// then use waveform as scale, from [i] to [j]
					switch(ltype) {
					case 0:{//Sine
						for(;i<j;i++,phase += phaseinc) {
							data[i] *= PrimitiveWaveforms.unitSine(phase);
						}
						break;
					}
					case 1:{//Square
						for(;i<j;i++,phase += phaseinc) {
							data[i] *= PrimitiveWaveforms.unitSquare(phase);
						}
						break;
					}
					case 2:{//Triangle
						for(;i<j;i++,phase += phaseinc) {
							data[i] *= PrimitiveWaveforms.unitTriangle(phase);
						}
						break;
					}
					case 3:{//Saw
						for(;i<j;i++,phase += phaseinc) {
							data[i] *= PrimitiveWaveforms.unitSaw(phase);
						}
						break;
					}
					}
					this.phase = phase;
				}
				setStepMax(cstep);
				return new Samples(sampleRate,data);
			}

			@Override
			public boolean isAlive() {
				return step.get()!=3;
			}

			@Override
			public void requestKill() {
				if(step.get()<2){
					setStepMax(2);// skip to decay step
					note=Math.min(note, time);// may need to cut note shorter
				}
			}
			
			/**
			 * Set {@link #step} to be the maximum of the current value
			 * and the given value.
			 * 
			 * @param newStep new value to possibly set step to
			 */
			public void setStepMax(int newStep){
				int cstep = step.get();
				while(cstep<newStep && !step.compareAndSet(cstep, newStep)){
					cstep = step.get();
				}
			}
			
		}
	}

}
