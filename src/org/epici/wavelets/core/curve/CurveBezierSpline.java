package org.epici.wavelets.core.curve;

import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.epici.wavelets.core.Curve;
import org.epici.wavelets.core.Named;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.curve.bezierspline.CurveBezierSplineEditor;
import org.epici.wavelets.ui.curve.bezierspline.CurveBezierSplinePreview;
import org.epici.wavelets.util.Any;
import org.epici.wavelets.util.hash.HashTriArx;
import org.epici.wavelets.util.hash.QuickKeyGen;
import org.epici.wavelets.util.math.Bezier;
import org.epici.wavelets.util.math.Floats;
import gnu.trove.list.array.TDoubleArrayList;

/**
 * Single input single output spline curve, based on cubic bezier.
 * We treat it as 2D Cartesian,
 * where the x-axis is the input and the y-axis is the output.
 * There are no control points, the derivative at a point
 * is the slope of the line between the neighboring points.
 * It is simple, intuitive, and has some nice properties,
 * so it is used most places a curve is needed.
 * <br>
 * Special cases:
 * <table>
 * <tr>
 * <th>Previous point exists?</th>
 * <th>Next point exists?</th>
 * <th>Occurs when?</th>
 * <th>Derivative from?</th>
 * </tr>
 * <tr>
 * <td>No</td>
 * <td>No</td>
 * <td>There is only one point</td>
 * <td>0 (constant)</td>
 * </tr>
 * <tr>
 * <td>Yes</td>
 * <td>No</td>
 * <td>Last point</td>
 * <td>Previous point and this point</td>
 * </tr>
 * <tr>
 * <td>No</td>
 * <td>Yes</td>
 * <td>First point</td>
 * <td>This point and next point</td>
 * </tr>
 * </table>
 * If there are no points, the entire curve is the 0 constant.
 * <br><br>
 * You can play with the curve
 * <a href="https://www.desmos.com/calculator/yuxzp1eavq">on Desmos</a>.<br>
 * Why is the magic constant <i>1/3</i>? There are various ways to find or prove it,
 * but the easiest is a visual intuition that involves re-parameter-izing this
 * from <i>[x &rarr; y]</i> to the familiar 2D <i>[t &rarr; x, y]</i>.
 * The key realization is that the identity bezier curve
 * for <i>x = B(t) = t</i> uses these control points:<br>
 * <i>[0, 1/n, 2/n, 3/n, &#x22EF;, (n-2)/n, (n-1)/n, 1]</i><br>
 * This curve is cubic (4 control points), so it will be<br>
 * <i>[0, 1/3, 2/3, 1]</i>
 * <br><br>
 * There are several utility methods here for working with streams,
 * however, they will not cover every use. Remember that you can combine them
 * and use {@code ::} to bind methods - {@code abc.def()} becomes {@code abc::def}.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class CurveBezierSpline implements Curve, DataEditor.Editable<CurveBezierSplineEditor, CurveBezierSplinePreview> {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Hash key for <i>hashCode()</i>
	 */
	public static final long HK_HC = QuickKeyGen.next64();
	
	/**
	 * All points, with
	 * x (input) in even positions and
	 * y (output) in odd positions.
	 * The array looks like this:
	 * <br>
	 * [x0, y0, x1, y1, x2, y2, x3, y3, &#x22EF;, xn, yn]
	 * <br>
	 * Therefore, point <i>i</i> will be at
	 * <i>x={@link #points}[2*i]</i>,
	 * <i>y={@link #points}[2*i+1]</i>.
	 */
	protected final TDoubleArrayList points;
	
	/**
	 * A field to track the name, as required by {@link Named}.
	 */
	public String name;
	
	/**
	 * Direct constructor, takes the points list without
	 * modifying it. Faster. For internal use only.
	 * 
	 * @param points value of {@link #points}
	 */
	protected CurveBezierSpline(TDoubleArrayList points) {
		Objects.requireNonNull(points);
		this.points = points;
	}
	
	/**
	 * Blank constructor.
	 */
	public CurveBezierSpline(){
		points = new TDoubleArrayList();
	}
	
	/**
	 * Get a copy of this curve.
	 * 
	 * @return a copy
	 */
	public CurveBezierSpline copy(){
		CurveBezierSpline result = new CurveBezierSpline();
		result.copyFrom(this);
		return result;
	}
	
	/**
	 * Copy another curve's data over to this curve.
	 * 
	 * @param source curve to copy from
	 */
	public void copyFrom(CurveBezierSpline source){
		Objects.requireNonNull(source);
		points.clear(source.points.size());
		points.addAll(source.points);
	}
	
	/**
	 * Create a curve from two arrays which run parallel to each other,
	 * one with the x values and the other with the y values.
	 * 
	 * @param px x (input) values array
	 * @param py y (output) values array
	 * @return curve object
	 */
	public static CurveBezierSpline fromPointsXYArrayParallel(double[] px, double[] py) {
		Objects.requireNonNull(px);
		Objects.requireNonNull(py);
		if(px.length != py.length) {
			throw new IllegalArgumentException("Array length mismatch: |px| ("+px.length+") != |py| ("+py.length+")");
		}
		CurveBezierSpline result = new CurveBezierSpline();
		for(int i=0;i<px.length;i++) {
			result.addPoint(px[i], py[i]);
		}
		return result;
	}
	
	/**
	 * Create a curve from an iterator of points as [X, Y] arrays.
	 * 
	 * @param pxy points iterator
	 * @return curve object
	 */
	public static CurveBezierSpline fromPointsIterator(Iterator<double[]> pxy) {
		CurveBezierSpline result = new CurveBezierSpline();
		while(pxy.hasNext()) {
			result.addPoint(pxy.next());
		}
		return result;
	}
	
	/**
	 * Create a curve from a stream of points as [X, Y] arrays.
	 * 
	 * @param pxy points stream
	 * @return curve object
	 */
	public static CurveBezierSpline fromPointsStream(Stream<double[]> pxy) {
		CurveBezierSpline result = new CurveBezierSpline();
		pxy.forEach(result::addPoint);
		return result;
	}
	
	/**
	 * Get a new curve object which contains the points from this one
	 * in some range. If the range is empty or negative, returns
	 * an empty curve.
	 * <br>
	 * This is optimized to be faster than
	 * {@link #getStreamXYRangeIndex(int, int)}
	 * then
	 * {@link #fromPointsStream(Stream)}.
	 * 
	 * @param start index of first point, inclusive
	 * @param stop index of last point, exclusive
	 * @return curve object using only those points
	 */
	public CurveBezierSpline sliceByIndex(int start, int stop) {
		int pointCount = getPointCount();
		if(start < 0 || stop > pointCount) {
			throw new IllegalArgumentException("Requested fetch range violates inequality: 0 <= start ("+start+") <= stop ("+stop+") <= |points| ("+pointCount+")");
		}
		if(start >= stop) {// empty or negative range -> no points
			return new CurveBezierSpline();
		}
		return new CurveBezierSpline(
				new TDoubleArrayList(
						points.subList(start<<1, stop<<1)
						)
				);
	}
	
	/**
	 * Get a new curve object which contains the points from this one
	 * in some range. If the range is empty or negative, returns
	 * an empty curve.
	 * <br>
	 * This is optimized to be faster than
	 * {@link #getStreamXYRangePosition(double, double)}
	 * then
	 * {@link #fromPointsStream(Stream)}.
	 * 
	 * @param xmin points with X lower than this will be excluded
	 * @param xmax points with X higher than this will be excluded
	 * @return curve object using only those points
	 */
	public CurveBezierSpline sliceByPosition(double xmin, double xmax) {
		return sliceByIndex(getPointInterval(xmin), getPointInterval(xmax));
	}

	@Override
	public double valueAtPosition(double position) {
		int pointCount = getPointCount();
		if(pointCount == 0){// no points -> 0 constant
			return 0;
		}else if(pointCount == 1){// single point -> that value
			return getPointY(0);
		}else{// multiple points
			int index = getPointInterval(position);
			if(index == 0){// before first point -> extend first 2
				double x1 = getPointX(0),
						y1 = getPointY(0),
						x2 = getPointX(1),
						y2 = getPointY(1);
				return Bezier.bezier2(y1, y2, (position-x1)/(x2-x1));
			}else if(index == pointCount){// after last point -> extend last 2
				double x1 = getPointX(pointCount-1),
						y1 = getPointY(pointCount-1),
						x2 = getPointX(pointCount-2),
						y2 = getPointY(pointCount-2);
				return Bezier.bezier2(y1, y2, (position-x1)/(x2-x1));
			}else{// between points
				double x1 = getPointX(index-1),
						y1 = getPointY(index-1),
						x2 = getPointX(index),
						y2 = getPointY(index),
						d1,
						d2;
				final double mul = (x2-x1)*(1d/3);
				if(index == 1){// no point before -> use current points
					d1 = (y2-y1)/(x2-x1);
				}else{// point before exists
					double x3 = getPointX(index-2),
							y3 = getPointY(index-2);
					d1 = (y2-y3)/(x2-x3);
				}
				if(index == pointCount-1){// no point after -> use current points
					d2 = (y2-y1)/(x2-x1);
				}else{// point after exists
					double x3 = getPointX(index+1),
							y3 = getPointY(index+1);
					d2 = (y3-y1)/(x3-x1);
				}
				return Bezier.bezier(
						new double[]{
								y1,
								y1+d1*mul,
								y2-d2*mul,
								y2,
						},
						(position-x1)/(x2-x1)
						);
			}
		}
	}
	
	@Override
	public void set(double time,double value){
		setPoint(time,value);
	}
	
	/**
	 * If it does exist, change the y value and return the index.
	 * If it does not exist, create the point and return the bitwise negation
	 * of the index (~ operator).
	 * <br>
	 * Redirects to {@link #addPoint(double, double)}.
	 * Useful as a stream map.
	 * 
	 * @param ixy point as [X, Y] array
	 * @return index of the point, bitwise negated if it did not exist yet
	 */
	public int setPoint(double[] ixy) {
		return setPoint(ixy[0], ixy[1]);
	}
	
	/**
	 * If it does exist, change the y value and return the index.
	 * If it does not exist, create the point and return the bitwise negation
	 * of the index (~ operator).
	 * <br>
	 * Redirects to {@link #addPoint(double, double, boolean)}.
	 * 
	 * @param ix x of point to add
	 * @param iy y of point to add
	 * @return index of the point, bitwise negated if it did not exist yet
	 */
	public int setPoint(double ix,double iy){
		int pointCountBefore = getPointCount();
		int index = addPoint(ix,iy,true);
		int pointCountAfter = getPointCount();
		/*
		 * in languages that do not differentiate between boolean
		 * and integer types (true=1, false=0)
		 * this can also be written as
		 * index ^ -(pointCountBefore!=pointCountAfter)
		 */
		return pointCountBefore==pointCountAfter?index:~index;
	}
	
	/**
	 * Set the y value of a point.
	 * 
	 * @param index index of point to change
	 * @param iy new y value
	 */
	public void setPointY(int index,double iy){
		int pointCount = getPointCount();
		if(index<0)throw new IndexOutOfBoundsException("Point index ("+index+") is less than the least allowed value (0)");
		if(index>=pointCount)throw new IndexOutOfBoundsException("Point index ("+index+") is greater than the greatest allowed value ("+(pointCount-1)+")");
		points.set((index<<1)|1, iy);
	}
	
	/**
	 * Attempts to add a point, returns the index on success.
	 * Point cannot be too close to other points,
	 * with closeness defined by {@link Floats#isNear(double, double)}.
	 * <br>
	 * Redirects to {@link #addPoint(double, double)}.
	 * Useful as a stream map.
	 * 
	 * @param ixy point as [X, Y] array
	 * @return index if successful, otherwise -1 
	 */
	public int addPoint(double[] ixy) {
		return addPoint(ixy[0], ixy[1]);
	}
	
	/**
	 * Attempts to add a point, returns the index on success.
	 * Point cannot be too close to other points,
	 * with closeness defined by {@link Floats#isNear(double, double)}.
	 * <br>
	 * Redirects to {@link #addPoint(double, double, boolean)}
	 * 
	 * @param ix x of point to add
	 * @param iy y of point to add
	 * @return index if successful, otherwise -1 
	 */
	public int addPoint(double ix,double iy){
		return addPoint(ix, iy, false);
	}
	
	/**
	 * Attempts to add a point, returns the index on success.
	 * Point cannot be too close to other points,
	 * with closeness defined by {@link Floats#isNear(double, double)}.
	 * If it is too close but replace is true, replaces the closer point.
	 * 
	 * @param ix x of point to add
	 * @param iy y of point to add
	 * @param replace whether to replace if exists already
	 * @return index if successful, otherwise -1 
	 */
	public int addPoint(double ix,double iy,boolean replace){
		int pointCount = getPointCount();
		int index = getPointInterval(ix);
		double xleft=Double.NEGATIVE_INFINITY, xright=Double.POSITIVE_INFINITY;
		boolean farLeft = index == 0 // first point, nothing before
				|| !Floats.isNear(ix, xleft = getPointX(index-1));
		boolean farRight = index == pointCount // last point, nothing after
				|| !Floats.isNear(ix, xright = getPointX(index));
		if(farLeft & farRight){
			points.insert(index<<1, new double[]{ix, iy});
			return index;
		}
		if(replace){
			if((ix-xleft) < (xright-ix))index = index - 1;// closer to left
			setPointY(index, iy);
			return index;
		}
		return -1;
	}
	
	/**
	 * Attempts to remove a point, throws on fail.
	 * 
	 * @param index index of point to remove
	 */
	public void removePoint(int index){
		int pointCount = getPointCount();
		if(index<0)throw new IndexOutOfBoundsException("Point index ("+index+") is less than the least allowed value (0)");
		if(index>=pointCount)throw new IndexOutOfBoundsException("Point index ("+index+") is greater than the greatest allowed value ("+(pointCount-1)+")");
		points.remove(index<<1, 2);
	}
	
	/**
	 * Attempts to move a point, returns the index on success.
	 * Point cannot be too close to other points,
	 * with closeness defined by {@link Floats#isNear(double, double)}.
	 * 
	 * @param index index where the point currently is
	 * @param ix new x value of point
	 * @param iy new y value of point
	 * @return index if successful, otherwise -1 
	 */
	public int movePoint(int index,double ix,double iy){
		int pointCount = getPointCount();
		if(index<0)throw new IndexOutOfBoundsException("Point index ("+index+") is less than the least allowed value (0)");
		if(index>=pointCount)throw new IndexOutOfBoundsException("Point index ("+index+") is greater than the greatest allowed value ("+(pointCount-1)+")");
		int newIndex = addPoint(ix, iy);
		if(newIndex<0)return -1;
		if(index<newIndex){
			removePoint(index);
			return newIndex-1;
		}else{
			removePoint(index+1);
			return newIndex;
		}
	}
	
	/**
	 * Get the index of the point with x value nearest
	 * to the input value. If they are equally close,
	 * picks the right/higher one.
	 * 
	 * @param ix x value to search
	 * @return index for the nearest point
	 */
	public int getPointNearest(double ix){
		int index = getPointInterval(ix);
		double prev = getPointX(index-1), next = getPointX(index);
		double prevDiff = ix-prev, nextDiff = next-ix;
		if(prevDiff<nextDiff)return index-1;
		return index;
	}
	
	/**
	 * Get the interval that this point is in.
	 * To be specific, if the return value is <i>i</i>,
	 * then <i>x<sub>i-1</sub> &le; ix &lt; x<sub>i</sub></i>.
	 * If there are <i>n</i> points, it is guaranteed that
	 * <i>0 &le; i &le; n</i>. <i>0</i> would be the interval extending
	 * from the first point to negative infinity,
	 * while <i>n</i> would be the interval extending
	 * from the last point to positive infinity.
	 * In the case that there are no points,
	 * the return value must be <i>0</i> and the interval is
	 * from negative infinity to infinity.
	 * Therefore, the caller should not assume either point is in bounds.
	 * 
	 * @param ix x value to search
	 * @return index for this point's interval
	 */
	public int getPointInterval(double ix){
		// binary search
		int left = 0, right = getPointCount();
		while(left < right){
			int middle = (left+right)>>>1;
			if(ix < getPointX(middle)){
				right = middle;
			}else{
				left = middle + 1;
			}
		}
		return left;
	}
	
	/**
	 * Get the x value for a particular point.
	 * <br>
	 * Special cases:
	 * <ul>
	 * <li>If the input is <i>-1</i>,
	 * the result is <i>{@link Double#NEGATIVE_INFINITY}</i></li>
	 * <li>If the input is the number of points,
	 * the result is <i>{@link Double#POSITIVE_INFINITY}</i></li>
	 * </ul>
	 * These are not actual points, and are only provided
	 * to avoid throwing errors on edge cases.
	 * 
	 * @param index point index
	 * @return x value of that point
	 */
	public double getPointX(int index){
		int pointCount = getPointCount();
		if(index<-1)throw new IndexOutOfBoundsException("Point index ("+index+") is less than the least allowed value (-1)");
		if(index>pointCount)throw new IndexOutOfBoundsException("Point index ("+index+") is greater than the greatest allowed value ("+pointCount+")");
		if(index==-1)return Double.NEGATIVE_INFINITY;
		if(index==pointCount)return Double.POSITIVE_INFINITY;
		return points.getQuick((index<<1));
	}
	
	/**
	 * Get the y value for a particular point.
	 * 
	 * @param index point index
	 * @return y value of that point
	 */
	public double getPointY(int index){
		int pointCount = getPointCount();
		if(index<0)throw new IndexOutOfBoundsException("Point index ("+index+") is less than the least allowed value (0)");
		if(index>=pointCount)throw new IndexOutOfBoundsException("Point index ("+index+") is greater than the greatest allowed value ("+(pointCount-1)+")");
		return points.getQuick((index<<1)|1);
	}
	
	/**
	 * Get a point as an [X, Y] array.
	 * Calls {@link #getPointY(int)} then {@link #getPointX(int)},
	 * which may throw for out of bounds.
	 * 
	 * @param index point index
	 * @return [X, Y] tuple of that point
	 */
	public double[] getPointXY(int index) {
		double py = getPointY(index);
		double px = getPointX(index);
		return new double[] {px,py};
	}
	
	/**
	 * Fetches points x (input) components to an array.
	 * 
	 * @param array array to hold results
	 * @param arrayOffset index to place the first result value in
	 * @param from first index to fetch, inclusive
	 * @param to last index to fetch, exclusive
	 */
	public void getPointsX(double[] array,int arrayOffset,int from,int to) {
		// all the checks
		Objects.requireNonNull(array);
		int pointCount = getPointCount();
		if(from < 0 || to > pointCount || from > to) {
			throw new IllegalArgumentException("Requested fetch range violates inequality: 0 <= from ("+from+") <= to ("+to+") <= |points| ("+pointCount+")");
		}
		int pointsFetched = to-from;
		int arrayLast = arrayOffset + pointsFetched;
		if(arrayOffset < 0 || arrayLast > array.length) {
			throw new IllegalArgumentException("Requested result range violates inequality: 0 <= arrayOffset ("+arrayOffset+"), arrayOffset + to - from ("+arrayLast+") <= |array| ("+array.length+")");
		}
		// the actual fetch
		TDoubleArrayList points = this.points;
		from = (from<<1);
		to = (to<<1);
		for(;from<to;arrayOffset++,from+=2) {
			array[arrayOffset] = points.getQuick(from);
		}
	}
	
	/**
	 * Fetches points y (output) components to an array.
	 * 
	 * @param array array to hold results
	 * @param arrayOffset index to place the first result value in
	 * @param from first index to fetch, inclusive
	 * @param to last index to fetch, exclusive
	 */
	public void getPointsY(double[] array,int arrayOffset,int from,int to) {
		// all the checks
		Objects.requireNonNull(array);
		int pointCount = getPointCount();
		if(from < 0 || to > pointCount || from > to) {
			throw new IllegalArgumentException("Requested fetch range violates inequality: 0 <= from ("+from+") <= to ("+to+") <= |points| ("+pointCount+")");
		}
		int pointsFetched = to-from;
		int arrayLast = arrayOffset + pointsFetched;
		if(arrayOffset < 0 || arrayLast > array.length) {
			throw new IllegalArgumentException("Requested result range violates inequality: 0 <= arrayOffset ("+arrayOffset+"), arrayOffset + to - from ("+arrayLast+") <= |array| ("+array.length+")");
		}
		// the actual fetch
		TDoubleArrayList points = this.points;
		from = (from<<1)|1;
		to = (to<<1)|1;
		for(;from<to;arrayOffset++,from+=2) {
			array[arrayOffset] = points.getQuick(from);
		}
	}
	
	/**
	 * Get the X (input) axis values as a {@link DoubleStream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the size stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @return X (input) axis values as a stream
	 */
	public DoubleStream getStreamX() {
		return getStreamXRangeIndex(0, getPointCount());
	}
	
	/**
	 * Get the X (input) axis values as a {@link DoubleStream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the range stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @param start index of first point, inclusive
	 * @param stop index of last point, exclusive
	 * @return X (input) axis values as a stream
	 */
	public DoubleStream getStreamXRangeIndex(int start,int stop) {
		return getStreamXRangeIndex(IntStream.range(start, stop));
	}
	
	/**
	 * Get the X (input) axis values as a {@link DoubleStream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the points don't move, but will not fetch
	 * values until they are needed.
	 * <br>
	 * Details are up to the underlying {@link IntStream} used.
	 * 
	 * @param indices stream of indices of points to get
	 * @return X (input) axis values as a stream
	 */
	public DoubleStream getStreamXRangeIndex(IntStream indices) {
		return indices.mapToDouble(this::getPointX);
	}
	
	/**
	 * Get the X (input) axis values as a {@link DoubleStream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the indices stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @param xmin points with X lower than this will be excluded
	 * @param xmax points with X higher than this will be excluded
	 * @return X (input) axis values as a stream
	 */
	public DoubleStream getStreamXRangePosition(double xmin,double xmax) {
		return getStreamXRangeIndex(getPointInterval(xmin), getPointInterval(xmax));
	}
	
	/**
	 * Get the Y (output) axis values as a {@link DoubleStream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the size stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @return Y (output) axis values as a stream
	 */
	public DoubleStream getStreamY() {
		return getStreamYRangeIndex(0, getPointCount());
	}
	
	/**
	 * Get the Y (output) axis values as a {@link DoubleStream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the range stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @param start index of first point, inclusive
	 * @param stop index of last point, exclusive
	 * @return Y (output) axis values as a stream
	 */
	public DoubleStream getStreamYRangeIndex(int start,int stop) {
		return getStreamYRangeIndex(IntStream.range(start, stop));
	}
	
	/**
	 * Get the Y (output) axis values as a {@link DoubleStream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the points don't move, but will not fetch
	 * values until they are needed.
	 * <br>
	 * Details are up to the underlying {@link IntStream} used.
	 * 
	 * @param indices stream of indices of points to get
	 * @return Y (output) axis values as a stream
	 */
	public DoubleStream getStreamYRangeIndex(IntStream indices) {
		return indices.mapToDouble(this::getPointY);
	}
	
	/**
	 * Get the [X, Y] tuples as a {@link Stream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the size stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @return point [X, Y] tuples as a stream
	 */
	public Stream<double[]> getStreamXY() {
		return getStreamXYRangeIndex(0, getPointCount());
	}
	
	/**
	 * Get the [X, Y] tuples as a {@link Stream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the range stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @param start index of first point, inclusive
	 * @param stop index of last point, exclusive
	 * @return point [X, Y] tuples as a stream
	 */
	public Stream<double[]> getStreamXYRangeIndex(int start, int stop) {
		return getStreamXYRangeIndex(IntStream.range(start, stop));
	}
	
	/**
	 * Get the [X, Y] tuples as a {@link Stream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the points don't move, but will not fetch
	 * values until they are needed.
	 * 
	 * @param indices stream of indices of points to get
	 * @return point [X, Y] tuples as a stream
	 */
	public Stream<double[]> getStreamXYRangeIndex(IntStream indices) {
		return indices.mapToObj(this::getPointXY);
	}
	
	/**
	 * Get the [X, Y] tuples as a {@link Stream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the indices stays the same, but will not fetch
	 * values until they are needed.
	 * 
	 * @param xmin points with X lower than this will be excluded
	 * @param xmax points with X higher than this will be excluded
	 * @return point [X, Y] tuples as a stream
	 */
	public Stream<double[]> getStreamXYRangePosition(double xmin, double xmax) {
		return getStreamXYRangeIndex(getPointInterval(xmin), getPointInterval(xmax));
	}
	
	/**
	 * Get the [X, Y] tuples as a {@link Stream}.
	 * The stream does not check for co-modifications.
	 * The stream is initialized when this method is called
	 * and assumes the points don't move, but will not fetch
	 * values until they are needed.
	 * 
	 * @param xs X values of points to fetch
	 * @return point [X, Y] tuples as a stream
	 */
	public Stream<double[]> getStreamXYRangePosition(DoubleStream xs) {
		return xs.mapToInt(this::getPointNearest).mapToObj(this::getPointXY);
	}
	
	/**
	 * Static helper method to convert a point [X, Y] tuple from
	 * a primitive array to a boxed object.
	 * 
	 * @param pxy [X, Y] tuple
	 * @return boxed object form
	 */
	public static Any.O2<Double, Double> convertPointArrayToAny(double[] pxy) {
		double py = pxy[1];
		double px = pxy[0];
		return new Any.O2<>(px, py);
	}
	
	/**
	 * Static helper method to convert a point [X, Y] tuple from
	 * a boxed object to a primitive array.
	 * 
	 * @param pxy [X, Y] tuple
	 * @return primitive array form
	 */
	public static double[] convertPointAnyToArray(Any.O2<Double, Double> pxy) {
		double py = pxy.b;
		double px = pxy.a;
		return new double[] {px, py};
	}
	
	/**
	 * Static helper method to convert what may be a [X, Y] tuple in some form
	 * to a primitive array.
	 * 
	 * @param pxy possibly represents a [X, Y] tuple
	 * @return primitive array form, or null on failure
	 */
	public static double[] convertPointUnknownToArray(Object pxy) {
		if (pxy == null) return null;// null is just null
		if (pxy instanceof double[]) {// already an array
			return (double[]) pxy;
		} else if (pxy instanceof Any.O2<?, ?>) {// boxed by any
			Any.O2<?, ?> apxy = (Any.O2<?, ?>) pxy;
			if(apxy.a instanceof Number && apxy.b instanceof Number) {
				return new double[] {((Number)apxy.a).doubleValue(), ((Number)apxy.b).doubleValue()};
			}
		} else if (pxy instanceof Point2D) {// in point object
			Point2D apxy = (Point2D) pxy;
			return new double[] {apxy.getX(), apxy.getY()};
		}
		return null;
	}
	
	/**
	 * Get the total number of points in this instance.
	 * 
	 * @return number of points
	 */
	public int getPointCount(){
		return points.size()>>1;
	}
	
	@Override
	public String toString(){
		int pointCount = getPointCount();
		StringBuilder sb = new StringBuilder();
		sb.append("BSpline(");
		if(pointCount>0){
			sb.append("[");
			sb.append(getPointX(0));
			sb.append(", ");
			sb.append(getPointY(0));
			sb.append("]");
			for(int i=1;i<pointCount;i++){
				sb.append(", [");
				sb.append(getPointX(i));
				sb.append(", ");
				sb.append(getPointY(i));
				sb.append("]");
			}
		}
		sb.append(")");
		return sb.toString();
	}
	
	@Override
	public int hashCode(){
		HashTriArx hash = new HashTriArx(HK_HC);
		int pointsLen = points.size();
		for(int i=0;i<pointsLen;i++){
			hash.absorb(points.get(i));
		}
		return hash.squeezeInt();
	}
	
	@Override
	public boolean equals(Object o){
		if(o==this)return true;
		if(o==null || !(o instanceof CurveBezierSpline))return false;
		CurveBezierSpline other = (CurveBezierSpline)o;
		return points.equals(other.points);
	}
	
	@Override
	public String getName(){
		if(name==null || name.length()==0)name = Named.defaultName("BSpline", this);
		return name;
	}
	
	@Override
	public boolean setName(String newName){
		name = newName;
		return true;
	}
	
	/**
	 * Create a {@link CurveBezierSpline} object according to default settings.
	 * In the current implementation this will be named
	 * like &quot;BSpline Curve&quot; using
	 * {@link org.epici.wavelets.util.ds.NamedMap#nextName(String, int, boolean, Session)}.
	 * 
	 * @param session current session
	 * @return a new {@link CurveBezierSpline} object with default values
	 */
	public static CurveBezierSpline makeDefaultBezierSplineCurve(Session session){
		// create the object
		CurveBezierSpline result = new CurveBezierSpline();
		// set name
		result.setName(session.project.curves.nextName(
				session.getCommonName(CurveBezierSpline.class),
				0, false, session));
		// return
		return result;
	}

	@Override
	public CurveBezierSplineEditor getEditor() {
		return CurveBezierSplineEditor.createNew(this);
	}

	@Override
	public CurveBezierSplinePreview getPreview() {
		CurveBezierSplinePreview result = new CurveBezierSplinePreview();
		result.view = this;
		return result;
	}
	
	/**
	 * Main method, used for testing.
	 * 
	 * @param args ignored
	 */
	public static void main(String[] args){
		int ir=0;double dr=0;
		int hash=0;
		CurveBezierSpline curve = new CurveBezierSpline();
		if((ir = curve.getPointCount()) != 0)throw new AssertionError("Empty curve reported "+ir+" points");
		for(int v=-710;v<=710;v++)if((ir = curve.getPointInterval(Math.sinh(v))) != 0)throw new AssertionError("Interval search reported "+ir+" index for empty curve");
		for(int v=-710;v<=710;v++)if((ir = curve.getPointNearest(Math.sinh(v))) != 0)throw new AssertionError("Nearest search reported "+ir+" index for empty curve");
		for(int v=-710;v<=710;v++)if((dr = curve.valueAtPosition(Math.sinh(v))) != 0)throw new AssertionError("Empty curve evaluated to "+dr);
		if((ir = curve.addPoint(1, 3)) != 0)throw new AssertionError("Point insert reported "+ir+" index");
		if((ir = curve.addPoint(4, 4)) != 1)throw new AssertionError("Point insert reported "+ir+" index");
		if((ir = curve.addPoint(2, 2)) != 1)throw new AssertionError("Point insert reported "+ir+" index");
		if((ir = curve.movePoint(1, 7, 5)) != 2)throw new AssertionError("Point move reported "+ir+" index");
		if((ir = curve.movePoint(2, 2, 2)) != 1)throw new AssertionError("Point move reported "+ir+" index");
		curve.removePoint(1);
		if((ir = curve.getPointCount()) != 2)throw new AssertionError("Curve with 2 points reported "+ir+" points");
		if((ir = curve.addPoint(2, 2)) != 1)throw new AssertionError("Point insert reported "+ir+" index");
		if((ir = curve.addPoint(7, 5)) != 3)throw new AssertionError("Point insert reported "+ir+" index");
		for(int v=100;v>=5;v--)if(curve.setPoint(7, v)<0)throw new AssertionError("Point set reported not exists");
		if((ir = curve.getPointCount()) != 4)throw new AssertionError("Curve with 4 points reported "+ir+" points");
		hash = curve.hashCode();
		System.out.println(curve);
		System.out.println("This session: hash key "+Long.toHexString(HK_HC)+" resulted in hash "+Integer.toHexString(hash));
		for(int[] v:new int[][]{{0,0},{1,1},{2,2},{3,4},{4,7}})for(int u=0;u<100;u++){double w=v[1]+u/100d;if((ir = curve.getPointInterval(w)) != v[0])throw new AssertionError("Interval search reported "+ir+" index for "+w);}
		for(int[] v:new int[][]{{0,1},{1,2},{2,4},{3,7}})for(int u=-49;u<50;u++){double w=v[1]+u/100d;if((ir = curve.getPointNearest(w)) != v[0])throw new AssertionError("Nearest search reported "+ir+" index for "+w);}
		for(int u=0;u<=100;u++){double v=u/100d;double w=u/100d;if(!Floats.isNear(dr = curve.valueAtPosition(w), Bezier.bezier(new double[]{4, 3}, v)))throw new AssertionError("Curve evaluated to "+dr+" at "+w);}
		for(int u=0;u<=100;u++){double v=u/100d;double w=1+u/100d;if(!Floats.isNear(dr = curve.valueAtPosition(w), Bezier.bezier(new double[]{3, 8d/3, 17d/9, 2}, v)))throw new AssertionError("Curve evaluated to "+dr+" at "+w);}
		for(int u=0;u<=100;u++){double v=u/100d;double w=2+u*2/100d;if(!Floats.isNear(dr = curve.valueAtPosition(w), Bezier.bezier(new double[]{2, 20d/9, 18d/5, 4}, v)))throw new AssertionError("Curve evaluated to "+dr+" at "+w);}
		for(int u=0;u<=100;u++){double v=u/100d;double w=4+u*3/100d;if(!Floats.isNear(dr = curve.valueAtPosition(w), Bezier.bezier(new double[]{4, 23d/5, 14d/3, 5}, v)))throw new AssertionError("Curve evaluated to "+dr+" at "+w);}
		for(int u=0;u<=100;u++){double v=u/100d;double w=7+u*3/100d;if(!Floats.isNear(dr = curve.valueAtPosition(w), Bezier.bezier(new double[]{5, 6}, v)))throw new AssertionError("Curve evaluated to "+dr+" at "+w);}
		if(!curve.equals(curve.copy()) || !curve.copy().equals(curve))throw new AssertionError("Curve copy is not equal");
		if(curve.copy().hashCode() != hash)throw new AssertionError("Curve copy hash is different");
		if(curve.hashCode() != hash)throw new AssertionError("Hash changed for no reason");
		System.out.println("Testing done, no problems found!");
	}

}
