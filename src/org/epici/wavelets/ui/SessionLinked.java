package org.epici.wavelets.ui;

import org.apache.pivot.wtk.Component;
import org.epici.wavelets.core.Session;

/**
 * Helper for things that keep track of a {@link Session}
 * or have a way to get one.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public interface SessionLinked {
	
	/**
	 * Get the current session. If it is not stored,
	 * make an attempt to fetch it. Returns null only on failure.
	 * 
	 * @return the current session, as seen by this object
	 */
	public Session getSession();
	
	/**
	 * Traverse up the component hierarchy
	 * searching for an instance of
	 * {@link SessionLinked}.
	 * Returns the session if successful, otherwise null.
	 * 
	 * @param searchFrom component to start searching at
	 * @return the session if found, otherwise null
	 */
	public static Session tryGetSession(Component searchFrom) {
		while(searchFrom != null) {
			if(searchFrom instanceof SessionLinked) {
				Session result = ((SessionLinked) searchFrom).getSession();
				if(result != null)return result;
			}
			searchFrom = searchFrom.getParent();
		}
		return null;
	}

}
