package org.epici.wavelets.ui.pattern;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.awt.Paint;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.net.URL;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.math3.primes.Primes;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.collections.Map;
import org.apache.pivot.util.Resources;
import org.apache.pivot.util.Vote;
import org.apache.pivot.wtk.Border;
import org.apache.pivot.wtk.ButtonPressListener;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.ComponentKeyListener;
import org.apache.pivot.wtk.Container;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Label;
import org.apache.pivot.wtk.ListButton;
import org.apache.pivot.wtk.ListButtonSelectionListener;
import org.apache.pivot.wtk.MenuBar;
import org.apache.pivot.wtk.Mouse.Button;
import org.apache.pivot.wtk.Mouse.ScrollType;
import org.apache.pivot.wtk.PushButton;
import org.apache.pivot.wtk.ScrollPane;
import org.apache.pivot.wtk.TabPane;
import org.apache.pivot.wtk.TabPaneSelectionListener;
import org.apache.pivot.wtk.TablePane;
import org.apache.pivot.wtk.TextInput;
import org.apache.pivot.wtk.content.ButtonData;
import org.apache.pivot.wtk.skin.ContainerSkin;
import org.epici.wavelets.core.BetterClone;
import org.epici.wavelets.core.Clip;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Curve;
import org.epici.wavelets.core.Pattern;
import org.epici.wavelets.core.Player;
import org.epici.wavelets.core.Preferences;
import org.epici.wavelets.core.Project;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.Synthesizer;
import org.epici.wavelets.core.curve.CurvePolynomial;
import org.epici.wavelets.core.track.PatternTrack;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.ui.curve.DoubleInput;
import org.epici.wavelets.ui.synth.SynthPreview;
import org.epici.wavelets.util.Bits;
import org.epici.wavelets.util.math.Floats;
import org.epici.wavelets.util.ui.Draw;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * Editor UI for {@link Pattern}
 * 
 * @author EPICI
 * @version 1.0
 */
public class PatternEditor extends DataEditor.Tabbed<Pattern> {
	
	/**
	 * Width of the floating sidebar in pixels
	 */
	public int sidebarWidth;
	
	public PatternEditor(){
		super();
		sidebarWidth = 100;
	}
	
	@Override
	public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
		super.initialize(namespace, location, resources);
	}
	
	@Override
	public void init(){
		super.init();
		tabPane.getTabPaneSelectionListeners().add(new TabSwitchListener(this));
	}
	
	/**
	 * Convenience method to create a new one from outside
	 * 
	 * @return
	 */
	public static PatternEditor createNew(){
		return PivotSwingUtils.loadBxml(PatternEditor.class, "patternEditor.bxml");
	}
	
	/**
	 * Get the project currently being edited.
	 * 
	 * @return
	 */
	public Project getProject(){
		return session.project;
	}
	
	@Override
	protected void addNewEditorData(Pattern pattern){
		try{
			TabPane.TabSequence tabs = tabPane.getTabs();
			LinkedEditorPane linked = LinkedEditorPane.createNew();
			linked.setEditorParent(this);
			linked.view = pattern;
			linked.init();
			tabs.add(linked);
		}catch(NullPointerException exception){
			exception.printStackTrace();
		}
	}
	
	@Override
	public boolean addEditorDataCasted(Object data){
		if(!(data instanceof Pattern))return false;
		return addEditorData((Pattern)data);
	}
	
	/**
	 * Listens for switching to a different tab and updates the interface
	 * accordingly when that happens
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TabSwitchListener implements TabPaneSelectionListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public PatternEditor parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TabSwitchListener(PatternEditor parent){
			this.parent = parent;
		}
		
		@Override
		public Vote previewSelectedIndexChange(TabPane tabPane, int selectedIndex){
			// approve means nothing changes, so it's the default action
			return Vote.APPROVE;
		}
		
		@Override
		public void selectedIndexChangeVetoed(TabPane tabPane, Vote reason){
			// didn't switch tabs, so do nothing
		}
		
		@Override
		public void selectedIndexChanged(TabPane tabPane, int previousSelectedIndex){
			// get the current tab
			int currentSelectedIndex = tabPane.getSelectedIndex();
			if(currentSelectedIndex<0){
				// negative index -> no selection
				
				return;
			}
			TabPane.TabSequence tabs = tabPane.getTabs();
			LinkedEditorPane editor = (LinkedEditorPane) tabs.get(currentSelectedIndex);
			// update clip template
			String selectedTemplateName = Objects.toString(editor.templateSelector.getSelectedItem(),"");
			editor.updateTemplateList();
			editor.templateSelector.setSelectedItem(selectedTemplateName);
		}
		
	}
	
	/**
	 * The editor interface for an individual {@link org.epici.wavelets.core.Pattern}.
	 * Uses several {@link TablePane} instances along with other components
	 * to provide tools. The specialized editor component for managing the clips
	 * is a {@link LinkedEditorInnerPane}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class LinkedEditorPane extends FillPane implements Bindable, DataEditor.Instance<Pattern, PatternEditor>{
		
		/**
		 * Reserved rows at the top of the clip table
		 */
		public static final int CLIP_TABLE_EXTRA_ROWS_TOP = 9;
		/**
		 * Reserved rows at the bottom of the clip table
		 */
		public static final int CLIP_TABLE_EXTRA_ROWS_BOTTOM = 1;
		/**
		 * Index of the row for the divisions input
		 */
		public static final int INDEX_DIVISIONS_INPUT = 4;
		/**
		 * Index of the row for the clip start input
		 */
		public static final int INDEX_CLIP_START_INPUT = 2;
		/**
		 * Index of the row for the clip duration input
		 */
		public static final int INDEX_CLIP_DURATION_INPUT = 4;
		/**
		 * Index of the row for the clip pitch input
		 */
		public static final int INDEX_CLIP_PITCH_INPUT = 6;
		/**
		 * Index of the row for the clip volume input
		 */
		public static final int INDEX_CLIP_VOLUME_INPUT = 8;
		/**
		 * Index of the row for the template rename button or text field
		 */
		public static final int INDEX_TEMPLATE_RENAME = 2;
		
		/**
		 * Name of the step mode using
		 * {@link DoubleInput.DoubleValidator.BoundedDoubleValidator}
		 */
		public static final String STEP_MODE_NAME_LINEAR = "Add";
		/**
		 * Name of the step mode using
		 * {@link DoubleInput.DoubleValidator.HyperbolicStep}
		 */
		public static final String STEP_MODE_NAME_HYPERBOLIC = "Multiply";
		
		/**
		 * Name of the update mode which
		 * sets the value of the property for all clips
		 * to the new value of the input.
		 */
		public static final String UPDATE_MODE_NAME_SET = "Overwrite";
		/**
		 * Name of the update mode which
		 * additively shifts the value of the property for all clips
		 * so the old value of the input becomes the new value of the input.
		 */
		public static final String UPDATE_MODE_NAME_DIFFERENCE_ADD = "Add";
		/**
		 * Name of the update mode which
		 * multiplicatively shifts the value of the property for all clips
		 * so the old value of the input becomes the new value of the input.
		 */
		public static final String UPDATE_MODE_NAME_DIFFERENCE_MULTIPLY = "Multiply";
		
		/**
		 * The parent {@link PatternEditor}
		 */
		protected PatternEditor parent;
		/**
		 * The {@link Pattern} for which this instance
		 * provides the UI
		 */
		public Pattern view;
		/**
		 * The main part of the pattern editor,
		 * a custom type
		 */
		public LinkedEditorInnerPane editorInnerPane;
		/**
		 * The tab name
		 */
		public ButtonData tabName;
		/**
		 * The table pane that wraps the whole thing
		 */
		public TablePane outerTablePane;
		/**
		 * The top menu
		 */
		public MenuBar menuBar;
		/**
		 * The table pane that wraps the whole thing except for the menu
		 */
		public TablePane innerTablePane;
		/**
		 * Pattern properties scroll pane wrapping a table pane
		 */
		public ScrollPane patternScrollPane;
		/**
		 * Pattern properties table pane, each row is an editable property
		 */
		public TablePane patternTablePane;
		/**
		 * Container for the {@link #synthPreview}.
		 */
		public Border synthContainer;
		/**
		 * The preview component for the {@link Pattern#getSynthesizer()}.
		 */
		public SynthPreview synthPreview;
		/**
		 * Input field for pattern divisions
		 */
		public DoubleInput divisionsInput;
		/**
		 * Clip properties scroll pane wrapping a table pane
		 */
		public ScrollPane clipScrollPane;
		/**
		 * Clip properties table pane, each row is an editable property
		 * <br>
		 * It uses 2 columns: left corresponds to editing the specific
		 * selected clip(s), right corresponds to editing the template
		 * that defines the left's behaviour
		 */
		public TablePane clipTablePane;
		/**
		 * Input field for start of clip
		 */
		public DoubleInput clipStartInput;
		/**
		 * Input field for duration of clip
		 */
		public DoubleInput clipDurationInput;
		/**
		 * Input field for pitch of a clip
		 */
		public DoubleInput clipPitchInput;
		/**
		 * Input field for volume of a clip
		 */
		public DoubleInput clipVolumeInput;
		/**
		 * List to select a template to use
		 */
		public ListButton templateSelector;
		/**
		 * Field to set the name of the current template,
		 * created when the button is pressed
		 */
		public TextInput templateRenameInput;
		/**
		 * Button to temporarily replace the selector with
		 * a text input which is used to rename the template,
		 * or if the text input is active, attempt to commit
		 * the new name
		 */
		public PushButton templateRename;
		/**
		 * Button to switch to a copy of the currently selected
		 * template, or if none are selected, a new template
		 */
		public PushButton templateCopy;
		/**
		 * Button to switch to a new blank template
		 */
		public PushButton templateNew;
		/**
		 * Button to add a new parameter to the bottom
		 */
		public PushButton templateParamNew;
		
		/**
		 * Selection with the original (same object) as keys
		 * and a copy (different object) as values.
		 * <br>
		 * The original (key) is changed by operations.
		 * <br>
		 * When a change is confirmed, value data should be copied from
		 * keys. Similary, when a change is cancelled, key data should be
		 * copied from values.
		 */
		public IdentityHashMap<Clip,Clip> selection;
		/**
		 * Pending paste with the original (same object) as keys
		 * and a copy (different object) as values.
		 * <br>
		 * The copy will be modified to preview the paste result,
		 * without touching the original.
		 * <br>
		 * On confirm or cancel, this map will become null again.
		 * If confirmed, the copy clips will be added to the pattern's
		 * clips and the current selection.
		 */
		public IdentityHashMap<Clip,Clip> paste;
		/**
		 * If any simple command is currently active, what is it?
		 * <br>
		 * See {@link SimpleCommandType} for which commands use this.
		 */
		public SimpleCommandType simpleCommand = SimpleCommandType.NULL;
		
		/**
		 * Possible commands which are simple in the sense that they
		 * do not need their own data.
		 * <br>
		 * In general, <b>left</b> mouse <b>confirms</b> the command,
		 * making the result permanent, and <b>right</b> mouse <b>cancels</b>
		 * the command, reverting all data to the state it was in
		 * before the command started.
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static enum SimpleCommandType {
			/**
			 * PLEASE USE THIS INSTEAD OF {@code null}
			 */
			NULL,
			/**
			 * Translates all the selected clips.
			 * Effect is the same as using
			 * {@link LinkedEditorPane#clipStartInput}
			 * and
			 * {@link LinkedEditorPane#clipPitchInput}.
			 */
			TRANSLATE,
			/**
			 * Scales all the selected clips.
			 * Effect is the same as using
			 * {@link LinkedEditorPane#clipDurationInput}.
			 */
			SCALE,
			/**
			 * User confirmation, then deletes all selected clips.
			 */
			DELETE,
		}
		
		/**
		 * Attempt to retrieve the current template. Returns null
		 * on failure.
		 * 
		 * @return
		 */
		public Clip.Template getTemplate(){
			Project project = getEditorParent().getProject();
			Object oname = templateSelector.getSelectedItem();
			if(oname!=null){
				String name = oname.toString();
				Clip.Template result;
				/*
				 * Here we make attempts to resolve the name.
				 * All should be of the following format:
				 *   result = ...
				 *   if(result!=null)return result;
				 * It's not so efficient, but it's obvious what it does
				 * and it's more easy to add/change attempts later.
				 */
				result = project.clipTemplates.dualMap.get(name);
				if(result!=null)return result;
			}
			return null;
		}
		
		/**
		 * Get the selection, never returns null
		 * <br>
		 * Format: suppose a pair (k,v) exists, k is a reference to the same
		 * object as in the {@link Pattern}, and v is a copy of that object.
		 * If k does not match v, then an operation is pending. After the operation
		 * is confirmed or cancelled, k will match v.
		 * 
		 * @param cleanup flag to iterate through and remove dead clips
		 * @param clear flag to reset and return an empty selection
		 * @return the selection
		 */
		public IdentityHashMap<Clip,Clip> getSelection(boolean cleanup,boolean clear){
			if(clear||selection==null){
				boolean update = selection!=null&&selection.size()>0;
				selection = new IdentityHashMap<>();
				if(update)selectionChanged();
				return selection;
			}
			IdentityHashMap<Clip,Clip> r = selection;
			if(cleanup){
				// Identity hash set
				Set<Clip> patternClips = Collections.newSetFromMap(new IdentityHashMap<>());
				patternClips.addAll(view.clips);
				r.keySet().retainAll(patternClips);
			}
			return r;
		}
		
		/**
		 * Either confirm or cancel pending changes. If confirm,
		 * copy will copy from original. If cancel, original will
		 * copy from copy. In both cases, original and copy will
		 * match after this call.
		 * 
		 * @param confirm true to confirm, false to cancel
		 */
		public void finalizeSelection(boolean confirm){
			IdentityHashMap<Clip,Clip> selection = getSelection(true,false);
			for(java.util.Map.Entry<Clip, Clip> entry:selection.entrySet()){
				Clip original = entry.getKey();
				Clip copy = entry.getValue();
				if(confirm){
					copy.copyFrom(original);
				}else{
					original.copyFrom(copy);
				}
			}
		}
		
		/**
		 * Either confirm or cancel the pending paste. If confirm,
		 * pasted clips will be added and selected. If cancel, nothing happens.
		 * In both cases, {@link #paste} will be <i>null</i> after this.
		 * If it was already <i>null</i>, this does nothing.
		 * 
		 * @param confirm true to confirm, false to cancel
		 */
		public void finalizePaste(boolean confirm){
			IdentityHashMap<Clip,Clip> paste = this.paste;
			if(paste==null)return;
			if(confirm){
				view.clips.addAll(paste.values());
				select(paste.values(),false);
			}
			this.paste = null;
		}
		
		/**
		 * Select some clips.
		 * 
		 * @param clips clips to select
		 * @param invert true to deselect instead
		 */
		public void select(Collection<Clip> clips,boolean invert){
			IdentityHashMap<Clip,Clip> selection = getSelection(false,false);
			int nselected1 = selection.size();
			if(invert){// Deselect given
				selection.keySet().removeAll(clips);
			}else{// Select given
				for(Clip clip:clips){
					if(!selection.containsKey(clip)){
						selection.put(clip, clip.copy());
					}
				}
			}
			int nselected2 = selection.size();
			// this size test does not work with invert select, which isn't used here
			if(nselected1 != nselected2) {
				selectionChanged();
			}
		}
		
		/**
		 * Call this when something is selected or deselected,
		 * will update data or interface as necessary.
		 */
		public void selectionChanged(){
			// Avoid calling getSelection()
			IdentityHashMap<Clip,Clip> selection = this.selection;
			int nselected = selection.size();
			boolean setenabled = nselected>0;
			if(nselected==0){// Empty selection
				clipStartInput.setValueNearest(0);
				clipDurationInput.setValueNearest(1);
				clipPitchInput.setValueNearest(0);
				clipVolumeInput.setValueNearest(0);
			}else if(nselected==1){// Single selection
				Clip single = selection.keySet().iterator().next();
				clipStartInput.setValueNearest(single.getDelay());
				clipDurationInput.setValueNearest(single.getLength());
				clipPitchInput.setValueNearest(single.getPitch());
				clipVolumeInput.setValueNearest(single.getVolume());
			}else{// Multiple selection
				int maxDelay = 0;
				for(Clip clip:selection.keySet()){
					maxDelay = Math.max(maxDelay, clip.getDelay());
				}
				clipStartInput.setValueNearest(maxDelay);
			}
			clipStartInput.valueChanged(true,false);
			clipStartInput.toSliderView(false);
			clipDurationInput.valueChanged(true,false);
			clipDurationInput.toSliderView(false);
			clipPitchInput.valueChanged(true,false);
			clipPitchInput.toSliderView(false);
			clipVolumeInput.valueChanged(true,false);
			clipVolumeInput.toSliderView(false);
			for(TablePane.Row tr:clipTablePane.getRows()){
				// Only look at first column, which is clip
				Component comp = tr.get(0);
				// Inputs should be enabled/disabled
				if(comp instanceof DoubleInput){
					comp.setEnabled(setenabled);
				}
			}
		}
		
		@Override
		public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
			tabName = (ButtonData) namespace.get("tabName");
			editorInnerPane = (LinkedEditorInnerPane) namespace.get("editorInnerPane");
			outerTablePane = (TablePane) namespace.get("outerTablePane");
			menuBar = (MenuBar) namespace.get("menuBar");
			innerTablePane = (TablePane) namespace.get("innerTablePane");
			patternScrollPane = (ScrollPane) namespace.get("patternScrollPane");
			patternTablePane = (TablePane) namespace.get("patternTablePane");
			synthContainer = (Border) namespace.get("synthContainer");
			clipScrollPane = (ScrollPane) namespace.get("clipScrollPane");
			clipTablePane = (TablePane) namespace.get("clipTablePane");
			templateSelector = (ListButton) namespace.get("templateSelector");
			templateRename = (PushButton) namespace.get("templateRename");
			templateCopy = (PushButton) namespace.get("templateCopy");
			templateNew = (PushButton) namespace.get("templateNew");
			templateParamNew = (PushButton) namespace.get("templateParamNew");
		}
		
		/**
		 * Initialize, called after setting fields
		 */
		public void init(){
			editorInnerPane.parent = this;
			
			TablePane.Row tr;
			
			divisionsInput = new DoubleInput(
					new DoubleInput.DoubleValidator.SplitDoubleValidator(
							new DoubleInput.DoubleValidator.BoundedIntegerValidator(1, 1e6, 4),
							new DoubleInput.DoubleValidator.HyperbolicStep(2)),
					view.divisions, 0x1.0p-7);
			divisionsInput.dataListeners.add(new DivisionsInputListener(this));
			tr = patternTablePane.getRows().get(INDEX_DIVISIONS_INPUT);
			tr.update(0, divisionsInput);
			
			clipStartInput = new DoubleInput(
					new DoubleInput.DoubleValidator.BoundedIntegerValidator(-1e6,1e6,0),
					0, 0x1.0p-5);
			clipStartInput.dataListeners.add(new ClipStartInputListener(this));
			tr = clipTablePane.getRows().get(INDEX_CLIP_START_INPUT);
			tr.update(0, clipStartInput);
			
			clipDurationInput = new DoubleInput(
					new DoubleInput.DoubleValidator.HyperbolicStep(1e-6, 1e6, 1, 2),
					1, 0x1.0p-7);
			clipDurationInput.dataListeners.add(new ClipDurationInputListener(this));
			tr = clipTablePane.getRows().get(INDEX_CLIP_DURATION_INPUT);
			tr.update(0, clipDurationInput);
			
			clipPitchInput = new DoubleInput(
					new DoubleInput.DoubleValidator.BoundedIntegerValidator(-80,80,0),
					0, 0x1.0p-5);
			clipPitchInput.dataListeners.add(new ClipPitchInputListener(this));
			tr = clipTablePane.getRows().get(INDEX_CLIP_PITCH_INPUT);
			tr.update(0, clipPitchInput);
			
			clipVolumeInput = new DoubleInput(
					new DoubleInput.DoubleValidator.BoundedDoubleValidator(-10, 10, 0),
					0, 0x1.0p-7);
			clipVolumeInput.dataListeners.add(new ClipVolumeInputListener(this));
			tr = clipTablePane.getRows().get(INDEX_CLIP_VOLUME_INPUT);
			tr.update(0, clipVolumeInput);
			
			templateSelector.getListButtonSelectionListeners().add(new TemplateSelectorInputListener(this));
			
			templateRenameInput = new TextInput();
			templateRename.getButtonPressListeners().add(new TemplateRenameButtonListener(this));
			templateRenameInput.getComponentKeyListeners().add(new TemplateRenameInputListener(this));
			
			templateCopy.getButtonPressListeners().add(new TemplateCopyButtonListener(this));
			
			templateNew.getButtonPressListeners().add(new TemplateNewButtonListener(this));
			
			templateParamNew.getButtonPressListeners().add(new TemplateParamNewButtonListener(this));
			
			Synthesizer synthesizer = view.getSynthesizer();
			synthPreview = new SynthPreview();
			synthPreview.setEditorData(synthesizer);
			synthContainer.setContent(synthPreview);
			synthPreview.init();
			synthPreview.viewSynthListeners.add(new SynthPreviewListener(this));
			
			tabName.setText(view.getName());
			
			// initialize the child
			editorInnerPane.init();
		}
		
		/**
		 * Convenience method to create a new one from outside
		 * 
		 * @return
		 */
		public static LinkedEditorPane createNew(){
			return PivotSwingUtils.loadBxml(LinkedEditorPane.class, "patternEditorPane.bxml");
		}
		
		@Override
		public Pattern getEditorData(){
			return view;
		}

		@Override
		public PatternEditor getEditorParent() {
			return parent;
		}

		@Override
		public boolean setEditorParent(Component newParent) {
			if(newParent instanceof PatternEditor){
				parent = (PatternEditor) newParent;
				return true;
			}
			return false;
		}

		/**
		 * Updates the template selector's list.
		 * Causes the selection to be cleared due to the way
		 * {@link ListButton} is implemented.
		 */
		public void updateTemplateList(){
			// get the original names
			// this may need to be updated in the future to match getTemplate()
			Set<String> source = getEditorParent().getProject().clipTemplates.forwardMap.keySet();
			// make the new list
			org.apache.pivot.collections.ArrayList<String> list =
					new org.apache.pivot.collections.ArrayList<>();
			// add all items
			for(String name:source){
				list.add(name);
			}
			// swap out the old list with the new list
			templateSelector.setListData(list);
		}
		
		/**
		 * Set the current template to a given one,
		 * and update the interface accordingly,
		 * except for the template selector (this is done to
		 * avoid a loop, you should instead select the name
		 * in the template selector and that will trigger this function).
		 * If given null, sets the interface to use no template,
		 * or in other words, clears it.
		 * Returns true on success, if returned false, then
		 * no changes should happen.
		 * 
		 * @param template
		 * @return
		 */
		public boolean updateTemplate(Clip.Template template){
			// fetch data
			TablePane.RowSequence rows = clipTablePane.getRows();
			ArrayList<Clip.Template.Property> properties = null;
			int nproperties = 0;
			if(template!=null){
				properties = template.properties;
				nproperties = properties.size();
			}
			// clear existing rows
			rows.remove(
					CLIP_TABLE_EXTRA_ROWS_TOP,
					rows.getLength()-CLIP_TABLE_EXTRA_ROWS_TOP-CLIP_TABLE_EXTRA_ROWS_BOTTOM
					);
			if(template!=null){
				// remake properties rows
				for(int i=0;i<nproperties;i++){
					Clip.Template.Property property = properties.get(i);
					LinkedClipTableRow row = LinkedClipTableRow.createNew();
					row.parent = this;
					row.view = property;
					row.init();
					rows.insert(row, CLIP_TABLE_EXTRA_ROWS_TOP+i);
				}
			}
			// get rid of pending rename
			renameToTextView(true,false);
			return true;
		}
		
		/**
		 * Convenience method to get all the current properties
		 * from the sliders in the clip table.
		 * 
		 * @return current properties list
		 */
		public ArrayList<Double> getClipCurrentProperties(){
			ArrayList<Double> result = new ArrayList<>();
			TablePane.RowSequence rows = clipTablePane.getRows();
			int loopTo = rows.getLength()-CLIP_TABLE_EXTRA_ROWS_BOTTOM;
			for(int i=CLIP_TABLE_EXTRA_ROWS_TOP;i<loopTo;i++){
				LinkedClipTableRow row = (LinkedClipTableRow) rows.get(i);
				double value = row.propertyInput.getValue();
				result.add(value);
			}
			return result;
		}
		
		/**
		 * Convenience method to get the current volume
		 * from the slider in the clip table.
		 * 
		 * @return current volume
		 */
		public double getClipCurrentVolume(){
			return clipVolumeInput.getValue();
		}
		
		/**
		 * Set the rename component to the text input to begin the rename operation,
		 * or do the reverse, set it to the button to end the rename operation.
		 * Confirming for forward means to set the text to match the current name,
		 * which is usually desired. Confirming for reverse means to attempt the
		 * rename operation.
		 * 
		 * @param reverse false to switch to text input, true to switch to button
		 * @param confirm true to confirm changes, false to cancel
		 */
		public void renameToTextView(boolean reverse,boolean confirm){
			TablePane.Row tr = clipTablePane.getRows().get(LinkedEditorPane.INDEX_TEMPLATE_RENAME);
			Component swapTo;
			if(reverse){
				if(confirm){
					// get old name and new name
					String oldName = getTemplate().getName();
					String newName = templateRenameInput.getText();
					// attempt rename
					String renamedTo = getEditorParent().getProject().clipTemplates.rename(oldName, newName, getEditorParent().session);
					if(renamedTo!=null){
						// need to update list
						updateTemplateList();
						templateSelector.setSelectedItem(renamedTo);
					}
				}
				// swap out for button
				swapTo = templateRename;
			}else{
				if(confirm){
					// set text to match
					String name = Objects.toString(templateSelector.getSelectedItem(),"");
					templateRenameInput.setText(name);
				}
				// swap out for text field
				swapTo = templateRenameInput;
			}
			tr.update(1, swapTo);
		}
		
		/**
		 * Listens for changes to the divisions input field
		 * and updates the pattern accordingly
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class DivisionsInputListener implements DoubleInput.DataListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedEditorPane parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public DivisionsInputListener(LinkedEditorPane parent){
				this.parent = parent;
			}

			@Override
			public void updated(DoubleInput component, boolean commit) {
				// Don't preview for this operation
				if(commit){
					Pattern pattern = parent.view;
					pattern.setDivisions((int)Math.round(component.getValue()));
					parent.repaint();// will look different
				}
			}
			
		}
		
		/**
		 * Listener for the synth preview. When the synth preview is changed
		 * from the UI, updates the pattern's synthesizer accordingly.
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static class SynthPreviewListener implements SynthPreview.ViewSynthListener {

			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedEditorPane parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public SynthPreviewListener(LinkedEditorPane parent){
				this.parent = parent;
			}
			
			@Override
			public void viewSynthChanged(SynthPreview editor, Synthesizer oldView, Synthesizer newView) {
				this.parent.view.setSynthesizer(newView);
			}
			
		}
		
	}
	
	/**
	 * Listens for changes to the clip start input field
	 * and updates the selected clips accordingly
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class ClipStartInputListener implements DoubleInput.DataListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public ClipStartInputListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void updated(DoubleInput component, boolean commit) {
			final double value = component.getValue(),
					lastValue = component.lastValue,
					lastValueCommit = component.lastValueCommit;
			int add = (int)(value-lastValueCommit);
			IdentityHashMap<Clip,Clip> selection = parent.getSelection(false,false);
			for(java.util.Map.Entry<Clip, Clip> entry:selection.entrySet()){
				Clip original = entry.getKey();
				Clip copy = entry.getValue();
				original.setDelay(Math.max(0, copy.getDelay()+add));
			}
			if(commit){
				if(value==lastValueCommit){// Cancel
					parent.finalizeSelection(false);
				}else{// Confirm
					parent.finalizeSelection(true);
				}
			}
			// may need to repaint to show changes
			parent.repaint();
		}
		
	}
	
	/**
	 * Listens for changes to the clip duration input field
	 * and updates the selected clips accordingly
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class ClipDurationInputListener implements DoubleInput.DataListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public ClipDurationInputListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void updated(DoubleInput component, boolean commit) {
			final double value = component.getValue(),
					lastValue = component.lastValue,
					lastValueCommit = component.lastValueCommit;
			double mult = value/lastValueCommit;
			IdentityHashMap<Clip,Clip> selection = parent.getSelection(false,false);
			for(java.util.Map.Entry<Clip, Clip> entry:selection.entrySet()){
				Clip original = entry.getKey();
				Clip copy = entry.getValue();
				original.setLength(Math.max(1, (int)Math.round(copy.getLength()*mult)));
			}
			if(commit){
				if(value==lastValueCommit){// Cancel
					parent.finalizeSelection(false);
				}else{// Confirm
					parent.finalizeSelection(true);
				}
			}
			// may need to repaint to show changes
			parent.repaint();
		}
		
	}
	
	/**
	 * Listens for changes to the clip pitch input field
	 * and updates the selected clips accordingly
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class ClipPitchInputListener implements DoubleInput.DataListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public ClipPitchInputListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void updated(DoubleInput component, boolean commit) {
			final double value = component.getValue(),
					lastValue = component.lastValue,
					lastValueCommit = component.lastValueCommit;
			int add = (int)(value-lastValueCommit);
			IdentityHashMap<Clip,Clip> selection = parent.getSelection(false,false);
			for(java.util.Map.Entry<Clip, Clip> entry:selection.entrySet()){
				Clip original = entry.getKey();
				Clip copy = entry.getValue();
				original.setPitch(copy.getPitch()+add);
			}
			if(commit){
				if(value==lastValueCommit){// Cancel
					parent.finalizeSelection(false);
				}else{// Confirm
					parent.finalizeSelection(true);
				}
			}
			// may need to repaint to show changes
			parent.repaint();
		}
		
	}
	
	/**
	 * Listens for changes to the clip volume input field
	 * and updates the selected clips accordingly
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class ClipVolumeInputListener implements DoubleInput.DataListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public ClipVolumeInputListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void updated(DoubleInput component, boolean commit) {
			final double value = component.getValue(),
					lastValue = component.lastValue,
					lastValueCommit = component.lastValueCommit;
			double add = value-lastValueCommit;// as B, so use add
			IdentityHashMap<Clip,Clip> selection = parent.getSelection(false,false);
			for(java.util.Map.Entry<Clip, Clip> entry:selection.entrySet()){
				Clip original = entry.getKey();
				Clip copy = entry.getValue();
				original.setVolume(copy.getVolume()+add);
			}
			if(commit){
				if(value==lastValueCommit){// Cancel
					parent.finalizeSelection(false);
				}else{// Confirm
					parent.finalizeSelection(true);
				}
			}
			// may need to repaint to show changes
			parent.repaint();
		}
		
	}
	
	/**
	 * Listens for changes to the template selector
	 * and updates the clip and template property interfaces accordingly
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TemplateSelectorInputListener implements ListButtonSelectionListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TemplateSelectorInputListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void selectedIndexChanged(ListButton listButton, int previousSelectedIndex){
			// nothing needs to change
		}
		
		@Override
		public void selectedItemChanged(ListButton listButton, Object previousSelectedItem){
			// may need to remake interface
			Clip.Template template=parent.getTemplate();
			if(template==null){
				// list clearly is outdated
				parent.updateTemplateList();
			}
			parent.updateTemplate(template);
		}
		
	}
	
	/**
	 * Listens for pressing of the template rename button
	 * and swaps it out for the text input when that happens
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TemplateRenameButtonListener implements ButtonPressListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TemplateRenameButtonListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void buttonPressed(org.apache.pivot.wtk.Button button){
			// require a selection
			Clip.Template template=parent.getTemplate();
			if(template!=null){
				// it's in the map, so make the change
				parent.renameToTextView(false, true);
			}else{
				// list clearly is outdated
				parent.updateTemplateList();
			}
		}
		
	}
	
	/**
	 * Listens for key presses for the template rename text input,
	 * swaps out for button and updates accordingly on cancel or confirm
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TemplateRenameInputListener implements ComponentKeyListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TemplateRenameInputListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		public boolean keyTyped(Component component, char character){
			// don't eat the event
			return false;
		}
		
		public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation){
			// don't eat the event
			return false;
		}
		
		public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation){
			switch(keyCode){
			case KeyEvent.VK_ENTER:{// confirm
				parent.renameToTextView(true, true);
				break;
			}
			case KeyEvent.VK_ESCAPE:{// cancel
				parent.renameToTextView(true, false);
				break;
			}
			}
			// don't eat the event
			return false;
		}
		
	}
	
	/**
	 * Listens for pressing of the template copy button
	 * and tries to duplicate the template when that happens
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TemplateCopyButtonListener implements ButtonPressListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TemplateCopyButtonListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void buttonPressed(org.apache.pivot.wtk.Button button){
			// require a selection
			Clip.Template template=parent.getTemplate();
			if(template!=null){
				// it's in the map, so make the copy
				Session session = parent.getEditorParent().session;
				Project project = parent.getEditorParent().getProject();
				java.util.Map<String,Object> copyOptions = BetterClone.fixOptions(null);
				session.setCopyOptions(copyOptions);
				Clip.Template newTemplate = BetterClone.copy(template, 1, copyOptions);
				// add it to the map
				project.clipTemplates.putNamed(newTemplate);
				// select the new name
				parent.updateTemplateList();
				parent.templateSelector.setSelectedItem(newTemplate.getName());
			}else{
				// list clearly is outdated
				parent.updateTemplateList();
			}
		}
		
	}
	
	/**
	 * Listens for pressing of the template add new button
	 * and tries to add a new template when that happens
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TemplateNewButtonListener implements ButtonPressListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TemplateNewButtonListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void buttonPressed(org.apache.pivot.wtk.Button button){
			// fetch needed data
			Session session = parent.getEditorParent().session;
			Project project = parent.getEditorParent().getProject();
			// make the new template
			Clip.Template newTemplate = Clip.Template.makeDefaultTemplate(session);
			// add it to the map
			project.clipTemplates.putNamed(newTemplate);
			// select the new name
			parent.updateTemplateList();
			parent.templateSelector.setSelectedItem(newTemplate.getName());
		}
		
	}
	
	/**
	 * Listens for pressing of the template add new parameter button
	 * and tries to add a new template parameter when that happens
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TemplateParamNewButtonListener implements ButtonPressListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TemplateParamNewButtonListener(LinkedEditorPane parent){
			this.parent = parent;
		}
		
		@Override
		public void buttonPressed(org.apache.pivot.wtk.Button button){
			// fetch needed data
			Clip.Template template = parent.getTemplate();
			// abort if no selection
			if(template==null)return;
			// make the new parameter
			Clip.Template.Property property = new Clip.Template.Property();
			property.min = 0;
			property.base = 0.5;
			property.max = 1;
			property.name = "Property "+(template.properties.size()+1);
			// add it to the list
			template.properties.add(property);
			// remake interface
			parent.updateTemplate(template);
		}
		
	}
	
	public static class LinkedClipTableRow extends TablePane.Row implements Bindable{
		
		/**
		 * Index of the row for the clip property input
		 */
		public static final int INDEX_PROPERTY_INPUT = 1;
		/**
		 * Index of the row for the clip property bounds (min/max/default) inputs
		 */
		public static final int INDEX_BOUNDS_INPUT = 1;
		
		/**
		 * The pane that contains this
		 */
		public LinkedEditorPane parent;
		/**
		 * The property that this edits
		 */
		public Clip.Template.Property view;
		/**
		 * The label on the left side that shows the current name of the property
		 */
		public Label nameLabel;
		/**
		 * Selector for what method the input should use to step values
		 */
		public ListButton stepModeSelector;
		/**
		 * Selector for what method to use for updating the clips' value
		 * based on change in input value
		 */
		public ListButton updateModeSelector;
		/**
		 * Button to swap this property with the one below (after) it
		 */
		public PushButton moveDown;
		/**
		 * Button to swap this property with the one above (before) it
		 */
		public PushButton moveUp;
		/**
		 * Button to remove this property from the template
		 */
		public PushButton remove;
		/**
		 * Table pane containing the min, max, and base inputs
		 */
		public TablePane boundsTablePane;
		/**
		 * Left side table pane, for editing the property for clips
		 */
		public TablePane leftTablePane;
		/**
		 * Table pane containing the buttons for moving and removing
		 */
		public TablePane moveTablePane;
		/**
		 * Right side table pane, for editing the property in the template
		 */
		public TablePane rightTablePane;
		/**
		 * Text field used to edit the name of the property
		 */
		public TextInput nameInput;
		/**
		 * The input for the clip's value for this property
		 */
		public DoubleInput propertyInput;
		/**
		 * The input for the minimum value of the property
		 */
		public DoubleInput minInput;
		/**
		 * The input for the default value of the property
		 */
		public DoubleInput baseInput;
		/**
		 * The input for the maximum value of the property
		 */
		public DoubleInput maxInput;
		
		private static final String SPLITDOUBLEVALIDATOR_CLASS_NAME = DoubleInput.DoubleValidator.SplitDoubleValidator.class.getCanonicalName();
		private static final String BOUNDEDDOUBLEVALIDATOR_CLASS_NAME = DoubleInput.DoubleValidator.BoundedDoubleValidator.class.getCanonicalName();
		private static final String DOUBLEVALIDATOR_CLASS_NAME = DoubleInput.DoubleValidator.class.getCanonicalName();
		
		@Override
		public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
			nameLabel = (Label) namespace.get("nameLabel");
			stepModeSelector = (ListButton) namespace.get("stepModeSelector");
			updateModeSelector = (ListButton) namespace.get("updateModeSelector");
			moveDown = (PushButton) namespace.get("moveDown");
			moveUp = (PushButton) namespace.get("moveUp");
			remove = (PushButton) namespace.get("remove");
			boundsTablePane = (TablePane) namespace.get("boundsTablePane");
			leftTablePane = (TablePane) namespace.get("leftTablePane");
			moveTablePane = (TablePane) namespace.get("moveTablePane");
			rightTablePane = (TablePane) namespace.get("rightTablePane");
			nameInput = (TextInput) namespace.get("nameInput");
		}
		
		/**
		 * Initialize, called after setting fields
		 */
		public void init(){
			TablePane.Row tr;
			
			propertyInput = new DoubleInput(
					new DoubleInput.DoubleValidator.SplitDoubleValidator(
							new DoubleInput.DoubleValidator.BoundedDoubleValidator(view.base),
							new DoubleInput.DoubleValidator.BoundedDoubleValidator(0)),
					view.base, 0x1.0p-5
					);
			propertyInput.dataListeners.add(new ClipPropertyInputListener(this));
			tr = leftTablePane.getRows().get(INDEX_PROPERTY_INPUT);
			tr.update(0, propertyInput);
			
			minInput = new DoubleInput(
					new DoubleInput.DoubleValidator.SplitDoubleValidator(
							new DoubleInput.DoubleValidator.BoundedDoubleValidator(0),
							new DoubleInput.DoubleValidator.HyperbolicStep(2)),
					0, 0x1.0p-7
					);
			minInput.dataListeners.add(new PropertyMinInputListener(this));
			tr = boundsTablePane.getRows().get(INDEX_BOUNDS_INPUT);
			tr.update(0, minInput);
			
			maxInput = new DoubleInput(
					new DoubleInput.DoubleValidator.SplitDoubleValidator(
							new DoubleInput.DoubleValidator.BoundedDoubleValidator(1),
							new DoubleInput.DoubleValidator.HyperbolicStep(2)),
					1, 0x1.0p-7
					);
			maxInput.dataListeners.add(new PropertyMaxInputListener(this));
			tr = boundsTablePane.getRows().get(INDEX_BOUNDS_INPUT);
			tr.update(2, maxInput);
			
			baseInput = new DoubleInput(
					new DoubleInput.DoubleValidator.SplitDoubleValidator(
							new DoubleInput.DoubleValidator.BoundedDoubleValidator(0.5),
							new DoubleInput.DoubleValidator.HyperbolicStep(2)),
					0.5, 0x1.0p-7
					);
			baseInput.dataListeners.add(new PropertyBaseInputListener(this));
			tr = boundsTablePane.getRows().get(INDEX_BOUNDS_INPUT);
			tr.update(1, baseInput);
			
			nameInput.getComponentKeyListeners().add(new PropertyNameInputListener(this));
			
			remove.getButtonPressListeners().add(new PropertyRemoveInputListener(this));
			
			moveUp.getButtonPressListeners().add(new PropertyMoveInputListener(this,-1));
			moveDown.getButtonPressListeners().add(new PropertyMoveInputListener(this,1));
			
			updateModeSelector.getListButtonSelectionListeners().add(new PropertyUpdateModeInputListener(this));
			
			stepModeSelector.getListButtonSelectionListeners().add(new PropertyStepModeInputListener(this));
			
			// fix the mismatches
			updateView(true);
			updatePropertyInput(true,true,true,true,true);
		}
		
		/**
		 * Convenience method to create a new one from outside
		 * 
		 * @return
		 */
		public static LinkedClipTableRow createNew(){
			return PivotSwingUtils.loadBxml(LinkedClipTableRow.class, "patternEditorClipTableRow.bxml");
		}
		
		/**
		 * Given the name of an update mode, return the corresponding
		 * curve instance which maps each value to a new value.
		 * If not recognized, returns null.
		 * 
		 * @param name
		 * @param oldValue old value of the property input
		 * @param newValue new value of the property input
		 * @return
		 */
		public Curve getUpdateInstance(String name,double oldValue,double newValue){
			switch(name){
			case PatternEditor.LinkedEditorPane.UPDATE_MODE_NAME_SET:{
				return new CurvePolynomial(newValue);
			}
			case PatternEditor.LinkedEditorPane.UPDATE_MODE_NAME_DIFFERENCE_ADD:{
				return new CurvePolynomial(newValue-oldValue,1);
			}
			case PatternEditor.LinkedEditorPane.UPDATE_MODE_NAME_DIFFERENCE_MULTIPLY:{
				return new CurvePolynomial(0,newValue/oldValue);
			}
			}
			return null;
		}
		
		/**
		 * Given the name of a step mode, return the corresponding
		 * validator instance. If not recognized, returns null.
		 * 
		 * @param name name for the desired step instance
		 * @param base default value, as specified by {@link DoubleInput.DoubleValidator#base()}.
		 * @return new step instance object if name is recognized, otherwise null
		 */
		public DoubleInput.DoubleValidator getStepInstance(String name, double base){
			switch(name){
			case PatternEditor.LinkedEditorPane.STEP_MODE_NAME_LINEAR:{
				return new DoubleInput.DoubleValidator.BoundedDoubleValidator(base);
			}
			case PatternEditor.LinkedEditorPane.STEP_MODE_NAME_HYPERBOLIC:{
				return new DoubleInput.DoubleValidator.HyperbolicStep(base, 2);
			}
			}
			return null;
		}
		
		/**
		 * Given a stepper instance, return the canonical name of the
		 * corresponding step mode. If not recognized, returns null.
		 * 
		 * @param stepper
		 * @return
		 */
		public String getStepName(DoubleInput.DoubleValidator stepper){
			if(stepper instanceof DoubleInput.DoubleValidator.HyperbolicStep){
				return PatternEditor.LinkedEditorPane.STEP_MODE_NAME_HYPERBOLIC;
			}else if(stepper instanceof DoubleInput.DoubleValidator.BoundedDoubleValidator){
				return PatternEditor.LinkedEditorPane.STEP_MODE_NAME_LINEAR;
			}
			return null;
		}
		
		/**
		 * Calls {@link #updateView(boolean, boolean, boolean, boolean)}
		 * with parameters to update all.
		 * 
		 * @param reverse
		 */
		public void updateView(boolean reverse){
			updateView(reverse,true,true,true);
		}
		
		/**
		 * Calls {@link #updateView(boolean, boolean, boolean, boolean, boolean, boolean, boolean)}
		 * and forward the parameters. The <i>main</i> parameter corresponds to all
		 * of <i>min, max, base, step</i>
		 * 
		 * @param reverse true to update from the view to the property input instead
		 * @param main whether to update the main
		 * @param updateMode whether to update the update mode
		 * @param name whether to update the name
		 */
		public void updateView(boolean reverse,boolean main,boolean updateMode,boolean name){
			updateView(reverse,main,main,main,main,updateMode,name);
		}
		
		/**
		 * Update the view using all properties based on the property input.
		 * Doesn't handle updating between the property input and the other settings.
		 * <br>
		 * Update mode is handled by the list button, not the property input,
		 * and follows the same rules for update direction.
		 * <br>
		 * Name is updated similarly.
		 * 
		 * @param reverse true to update from the view to the property input instead
		 * @param min whether to update the min
		 * @param max whether to update the max
		 * @param base whether to update the default value
		 * @param step whether to update the step
		 * @param updateMode whether to update the update mode
		 * @param name whether to update the name
		 */
		public void updateView(boolean reverse,boolean min,boolean max,boolean base,boolean step,boolean updateMode,boolean name){
			if(reverse){
				Clip.Template.Property view = this.view;
				if(min|max|base|step){
					DoubleInput.DoubleValidator validator = propertyInput.validator;
					double newMin = view.min;
					double newMax = view.max;
					double newBase = view.base;
					java.util.Map<String,Object> copyOptions, copySet;
					Collection<String> copyWhitelist;
					copyOptions = BetterClone.fixOptions(null);
					copySet = (java.util.Map<String,Object>)copyOptions.get("set");
					if(min)copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".min", newMin);
					if(max)copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".max", newMax);
					if(base)copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".base", newBase);
					if(step)copySet.put(SPLITDOUBLEVALIDATOR_CLASS_NAME+".step", getStepInstance(view.step, newBase));
					copyWhitelist = (Collection<String>)copyOptions.get("whitelist");
					copyWhitelist.add("*"+DOUBLEVALIDATOR_CLASS_NAME);
					propertyInput.setValidator(
							BetterClone.copy(validator, 1, copyOptions));
				}
			}else{
				Clip.Template.Property view = this.view;
				if(min|max|base|step){
					DoubleInput.DoubleValidator.SplitDoubleValidator validator =
							(DoubleInput.DoubleValidator.SplitDoubleValidator) propertyInput.validator;
					if(min)view.min = validator.min();
					if(max)view.max = validator.max();
					if(base)view.base = validator.base();
					if(step)view.step = Objects.requireNonNull(getStepName(validator.step), "step is not a recognized type");
				}
			}
			if(updateMode){
				if(reverse){
					updateModeSelector.setSelectedItem(view.update);
				}else{
					view.update = Objects.toString(updateModeSelector.getSelectedItem());// Unsafe?
				}
			}
			if(name){
				String newName;
				if(reverse){
					newName = view.name;
					nameInput.setText(newName);
				}else{
					newName = nameInput.getText();
					view.name = newName;
				}
				nameLabel.setText(newName);
			}
		}
		
		/**
		 * Update to or from the property input. For all items which are to be updated,
		 * the property input and setting input will match after.
		 * <br>
		 * Secondary values such as the minimum of the maximum input are always updated to,
		 * in order to match the others.
		 * 
		 * @param reverse false to update from other settings to the property input,
		 * true to update from property input to other settings
		 * @param min true to update the minimum
		 * @param max true to update the maximum
		 * @param base true to update the default value
		 * @param step true to update the stepping
		 */
		public void updatePropertyInput(boolean reverse,boolean min,boolean max,boolean base,boolean step){
			if(!(min|max|base|step))return;// Skip if no changes
			java.util.Map<String,Object>
					propertyInputCopyOptions = null,
					minInputCopyOptions = null,
					maxInputCopyOptions = null,
					baseInputCopyOptions = null,
					copySet = null;
			Collection<String> copyWhitelist;
			double newMin = 0, newMax = 0, newBase = 0;
			String newStepName = null;
			DoubleInput.DoubleValidator newStepInstance = null;
			if(min|max|base|step)newBase = reverse?propertyInput.validator.base():baseInput.getValue();
			if(min|max|base){
				newMin = reverse?propertyInput.validator.min():minInput.getValue();
				newMax = reverse?propertyInput.validator.max():maxInput.getValue();
				if(min&&reverse){
					minInput.setValue(newMin);
					minInput.valueChanged(true,false);
				}
				if(max&&reverse){
					maxInput.setValue(newMax);
					maxInput.valueChanged(true,false);
				}
				if(base&&reverse){
					baseInput.setValue(newBase);
					baseInput.valueChanged(true,false);
				}
			}
			if(step){
				if(reverse){
					newStepInstance = ((DoubleInput.DoubleValidator.SplitDoubleValidator)
							propertyInput.validator).step;
					newStepName = getStepName(newStepInstance);
					stepModeSelector.setSelectedItem(newStepName);
				}else{
					newStepName = Objects.toString(stepModeSelector.getSelectedItem());
					newStepInstance = getStepInstance(newStepName, newBase);
				}
			}
			if(!reverse){
				propertyInputCopyOptions = BetterClone.fixOptions(null);
				copySet = (java.util.Map<String,Object>)propertyInputCopyOptions.get("set");
				if(min)copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".min", newMin);
				if(max)copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".max", newMax);
				if(base)copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".base", newBase);
				if(step)copySet.put(SPLITDOUBLEVALIDATOR_CLASS_NAME+".step", newStepInstance);
				copyWhitelist = (Collection<String>)propertyInputCopyOptions.get("whitelist");
				copyWhitelist.add("*"+DOUBLEVALIDATOR_CLASS_NAME);
				propertyInput.setValidator(
						BetterClone.copy(propertyInput.validator, 1, propertyInputCopyOptions));
			}
			if(max|base){
				minInputCopyOptions = BetterClone.fixOptions(null);
				copySet = (java.util.Map<String,Object>)minInputCopyOptions.get("set");
				copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".base", newMin);
				copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".max", newBase);
				copyWhitelist = (Collection<String>)minInputCopyOptions.get("whitelist");
				copyWhitelist.add("*"+DOUBLEVALIDATOR_CLASS_NAME);
				minInput.setValidator(
						BetterClone.copy(minInput.validator, 1, minInputCopyOptions));
			}
			if(min|base){
				maxInputCopyOptions = BetterClone.fixOptions(null);
				copySet = (java.util.Map<String,Object>)maxInputCopyOptions.get("set");
				copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".base", newMax);
				copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".min", newBase);
				copyWhitelist = (Collection<String>)maxInputCopyOptions.get("whitelist");
				copyWhitelist.add("*"+DOUBLEVALIDATOR_CLASS_NAME);
				maxInput.setValidator(
						BetterClone.copy(maxInput.validator, 1, maxInputCopyOptions));
			}
			if(min|max){
				baseInputCopyOptions = BetterClone.fixOptions(null);
				copySet = (java.util.Map<String,Object>)baseInputCopyOptions.get("set");
				copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".base", newBase);
				copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".min", newMin);
				copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".max", newMax);
				copyWhitelist = (Collection<String>)baseInputCopyOptions.get("whitelist");
				copyWhitelist.add("*"+DOUBLEVALIDATOR_CLASS_NAME);
				baseInput.setValidator(
						BetterClone.copy(baseInput.validator, 1, baseInputCopyOptions));
			}
		}
		
		/**
		 * Updates just the main set: min, max, base, step. From inputs to
		 * the property's fields.
		 * <br>
		 * They are grouped together for performance reasons - doing all of them
		 * is barely more expensive than doing one, if done together.
		 * Not much surprises are possible because of extra updating,
		 * but the user should be aware of what this method does versus
		 * calling {@link #updateView(boolean, boolean, boolean, boolean, boolean, boolean, boolean)}
		 * directly should it ever cause problems.
		 * 
		 * @param reverse true to update property's fields to inputs instead
		 */
		public void updateMainSet(boolean reverse){
			updateView(reverse,true,false,false);
		}
		
		/**
		 * Update just the update mode, from the update mode selector to the
		 * property's field.
		 * 
		 * @param reverse true to update property's field to update mode selector instead
		 */
		public void updateUpdateMode(boolean reverse){
			updateView(reverse,false,true,false);
		}
		
		/**
		 * Update just the name, from the name input to the property name field,
		 * or the reverse.
		 * <br>
		 * Name field is updated to match.
		 * 
		 * @param reverse true to update property name field to name input instead
		 */
		public void updateName(boolean reverse){
			updateView(reverse,false,false,true);
		}
		
		/**
		 * Convenience method to apply a modification to a property for a clip.
		 * If the property doesn't exist for the clip, fills up to there with
		 * default values, then applies the modification using the curve.
		 * 
		 * @param clip clip to modify
		 * @param template template being used
		 * @param index index of property
		 * @param modify maps old values to new values
		 */
		public static void modifyClipProperty(Clip clip,Clip.Template template,int index,Curve modify){
			// Ensure it has the property we want to modify
			clip.fillWith(template,index);
			// Now we modify the index we want
			clip.setProperty(index, modify.valueAtPosition(clip.getProperty(index)));
		}
		
		/**
		 * Listens for changes to the clip property input field, and accordingly either
		 * updates all selected clips or reverts the changes made.
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class ClipPropertyInputListener implements DoubleInput.DataListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public ClipPropertyInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}
			
			@Override
			public void updated(DoubleInput component, boolean commit) {
				final double value = component.getValue(),
						lastValue = component.lastValue,
						lastValueCommit = component.lastValueCommit;
				if(value!=lastValue){// Change to make
					Clip.Template template = parent.parent.getTemplate();
					Clip.Template.Property property = parent.view;
					int index = template.properties.indexOf(property);
					Curve modify = parent.getUpdateInstance(property.update, lastValue, value);
					IdentityHashMap<Clip,Clip> selection = parent.parent.getSelection(false, false);
					for(Clip clip:selection.keySet()){
						LinkedClipTableRow.modifyClipProperty(clip,template,index,modify);
					}
				}
				if(commit){
					if(value!=lastValueCommit){// Confirm
						parent.parent.finalizeSelection(true);
					}else{// Cancel
						parent.parent.finalizeSelection(false);
					}
				}
			}
			
		}
		
		/**
		 * Listens for changes to the template property minimum value input field
		 * and updates the clip property input accordingly
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyMinInputListener implements DoubleInput.DataListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public PropertyMinInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}
			
			@Override
			public void updated(DoubleInput component, boolean commit) {
				final double value = component.getValue(),
						lastValue = component.lastValue,
						lastValueCommit = component.lastValueCommit;
				if(commit && value!=lastValueCommit){// Confirm
					parent.updatePropertyInput(false, true, false, false, false);
					parent.updateMainSet(false);
					// it's okay that we don't update the clips
				}
			}
			
		}
		
		/**
		 * Listens for changes to the template property maximum value input field
		 * and updates the clip property input accordingly
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyMaxInputListener implements DoubleInput.DataListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public PropertyMaxInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}
			
			@Override
			public void updated(DoubleInput component, boolean commit) {
				final double value = component.getValue(),
						lastValue = component.lastValue,
						lastValueCommit = component.lastValueCommit;
				if(commit && value!=lastValueCommit){// Confirm
					parent.updatePropertyInput(false, false, true, false, false);
					parent.updateMainSet(false);
					// it's okay that we don't update the clips
				}
			}
			
		}
		
		/**
		 * Listens for changes to the template property default value input field
		 * and updates the clip property input accordingly
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyBaseInputListener implements DoubleInput.DataListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public PropertyBaseInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}
			
			@Override
			public void updated(DoubleInput component, boolean commit) {
				final double value = component.getValue(),
						lastValue = component.lastValue,
						lastValueCommit = component.lastValueCommit;
				if(commit && value!=lastValueCommit){// Confirm
					parent.updatePropertyInput(false, false, false, true, false);
					parent.updateMainSet(false);
					// it's okay that we don't update the clips
				}
			}
			
		}
		
		/**
		 * Listens for changes to the template property name input field and update the
		 * property name and name label accordingly
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyNameInputListener implements ComponentKeyListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public PropertyNameInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}

		    @Override
			public boolean keyTyped(Component component, char character){
				return false;
			}

		    @Override
		    public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation){
		    	return false;
		    }

		    @Override
		    public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation){
		    	switch(keyCode){
		    	case KeyEvent.VK_ENTER:{
		    		// Update it
		    		parent.updateName(false);
		    		break;
		    	}
		    	}
		    	return false;
		    }
			
		}
		
		/**
		 * Listens for changes to the template property update mode selector and updates
		 * the property's field accordingly
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyUpdateModeInputListener implements ListButtonSelectionListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public PropertyUpdateModeInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}

		    @Override
			public void selectedIndexChanged(ListButton listButton, int previousSelectedIndex){
			}

		    @Override
		    public void selectedItemChanged(ListButton listButton, Object previousSelectedItem){
		    	parent.updateUpdateMode(false);
		    }
			
		}
		
		/**
		 * Listens for changes to the template property step mode selector and updates
		 * the property's field and property input accordingly
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyStepModeInputListener implements ListButtonSelectionListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public PropertyStepModeInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}

		    @Override
			public void selectedIndexChanged(ListButton listButton, int previousSelectedIndex){
			}

		    @Override
		    public void selectedItemChanged(ListButton listButton, Object previousSelectedItem){
		    	parent.updateMainSet(false);
		    }
			
		}
		
		/**
		 * Listens for presses of the remove button, and removes the property
		 * and updates clips accordingly when that happens
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyRemoveInputListener implements ButtonPressListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 */
			public PropertyRemoveInputListener(LinkedClipTableRow parent){
				this.parent = parent;
			}

		    @Override
		    public void buttonPressed(org.apache.pivot.wtk.Button button){
		    	Clip.Template template = parent.parent.getTemplate();
				Clip.Template.Property property = parent.view;
				int index = template.properties.indexOf(property);
				// Update template
				template.properties.remove(index);
				// Update UI
				parent.parent.clipTablePane.getRows().remove(
						index+LinkedEditorPane.CLIP_TABLE_EXTRA_ROWS_TOP,
						1);
				// Need to update selection as well
				IdentityHashMap<Clip,Clip> selection = parent.parent.getSelection(false, false);
				for(Clip clip:selection.keySet()){
					clip.removeProperty(index);
				}
				parent.parent.finalizeSelection(true);
		    }
			
		}
		
		/**
		 * Listens for presses of the move up or move down button, 
		 * and moves the property and updates the UI accordingly when that happens
		 * 
		 * @author EPICI
		 * @version 1.0
		 */
		public static class PropertyMoveInputListener implements ButtonPressListener{
			
			/**
			 * Remember the parent, other data can be derived from here
			 */
			public LinkedClipTableRow parent;
			/**
			 * Add this value to get the index to swap with
			 */
			public int direction;
			
			/**
			 * Standard constructor
			 * 
			 * @param parent
			 * @param direction
			 */
			public PropertyMoveInputListener(LinkedClipTableRow parent,int direction){
				this.parent = parent;
				this.direction = direction;
			}

		    @Override
		    public void buttonPressed(org.apache.pivot.wtk.Button button){
		    	Clip.Template template = parent.parent.getTemplate();
				Clip.Template.Property property = parent.view;
				int left = template.properties.indexOf(property);
				int right = left+direction;
				// Ensure left<right
				if(direction<0){int temp=left;left=right;right=temp;}
				// Do nothing if out of range
				if(left>=0&&right<template.properties.size()){
					// Update template
					Collections.swap(template.properties, left, right);
					// Update UI
					PivotSwingUtils.swap(parent.parent.clipTablePane.getRows(), left, right);
					// Need to update selection as well
					IdentityHashMap<Clip,Clip> selection = parent.parent.getSelection(false, false);
					for(Clip clip:selection.keySet()){
						clip.fillWith(template, right);
						// Only this property should jump, so direction is reversed
						clip.rotateProperty(left, right+1, direction<0?1:-1);
					}
					parent.parent.finalizeSelection(true);
				}
		    }
			
		}
		
	}
	
	/**
	 * Main editor of the {@link org.epici.wavelets.core.Clip} instances for a {@link org.epici.wavelets.core.Pattern}.
	 * Includes the traditional &quot;piano roll&quot; and a grid to place clips on.
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class LinkedEditorInnerPane extends Container implements Bindable{
		
		/**
		 * The pane that contains this
		 */
		public LinkedEditorPane parent;
		
		/**
		 * Blank constructor. Only does some basic pre-initialization
		 * since {@link #initialize(Map, URL, Resources)}
		 * and {@link #init()} will handle the rest.
		 */
		public LinkedEditorInnerPane(){
			installSkin(LinkedEditorInnerPane.class);
		}
		
		@Override
		public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
		}
		
		/**
		 * Initialize, called after setting fields
		 */
		public void init(){
		}
		
	}
	
	/**
	 * Skin for the {@link LinkedEditorInnerPane}. By convention, it provides the UI
	 * and handles the rendering.
	 * <br>
	 * Note to programmers: +y in graphic space is down, however,
	 * we need it to be up. As such, it is always inverted.
	 * Carefully inverting only once for the y axis is unruly, but required.
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class LinkedEditorInnerPaneSkin extends ContainerSkin implements KeyTracker {
		
		/**
		 * The smallest allowed {@link #scalex}.
		 */
		public static final double SCALE_MIN_X = 4;
		/**
		 * The largest allowed {@link #scalex}.
		 */
		public static final double SCALE_MAX_X = 1<<16;
		/**
		 * The smallest allowed {@link #scaley}.
		 */
		public static final double SCALE_MIN_Y = 4;
		/**
		 * The largest allowed {@link #scaley}.
		 */
		public static final double SCALE_MAX_Y = 1<<8;
		
		/**
		 * The x position (world) which the top left corner
		 * of the pane corresponds to
		 */
		public double anchorx;
		/**
		 * The y position (world) which the top left corner
		 * of the pane corresponds to (+y is down not up)
		 */
		public double anchory;
		/**
		 * Temporary unbounded anchorx
		 */
		public double uanchorx;
		/**
		 * Temporary unbounded anchory
		 */
		public double uanchory;
		/**
		 * The x scale of the UI, maps measures (world) to pixel x
		 */
		public double scalex;
		/**
		 * The y scale of the UI, maps pattern rows (world) to pixel y
		 */
		public double scaley;
		/**
		 * Mouse button being held down.
		 * <br>
		 * <table>
		 * <tr>
		 * <th>Value</th>
		 * <th>Meaning</th>
		 * </tr>
		 * <tr>
		 * <td>-1</td>
		 * <td>No mouse button is actually being pressed,
		 * but treat it as if the mouse is down,
		 * because some command uses it.</td>
		 * </tr>
		 * <tr>
		 * <td>0</td>
		 * <td>No mouse button pressed.</td>
		 * </tr>
		 * <tr>
		 * <td>1</td>
		 * <td><b>Left</b> mouse button pressed.</td>
		 * </tr>
		 * <tr>
		 * <td>2</td>
		 * <td><b>Right</b> mouse button pressed.</td>
		 * </tr>
		 * <tr>
		 * <td>3</td>
		 * <td><b>Middle</b> mouse button pressed.</td>
		 * </tr>
		 * </table>
		 */
		public int mouseDown;
		/**
		 * Has the mouse moved since it was clicked down?
		 */
		public boolean mouseDragged;
		/**
		 * Which keys are held down?
		 */
		protected BitSet keys = new BitSet();
		/**
		 * The mouse x where dragging began
		 */
		public int originMousex;
		/**
		 * The mouse y where dragging began
		 */
		public int originMousey;
		/**
		 * <i>originMousex</i> mapped to world x when dragging began
		 */
		public double originWorldx;
		/**
		 * <i>originMousey</i> mapped to world y when dragging began
		 */
		public double originWorldy;
		/**
		 * The mouse x, where it was last seen
		 */
		public int lastMousex;
		/**
		 * The mouse y, where it was last seen
		 */
		public int lastMousey;
		
		// selection is managed by the LinkedEditorPane
		
		/**
		 * A dummy track which is created when a preview is needed,
		 * and discarded after.
		 */
		public PatternTrack previewTrack;
		/**
		 * If there is a preview currently, is it playing yet?
		 */
		public boolean previewIsPlaying;
		/**
		 * If there is a preview currently, is it playing live from
		 * the synth previwe (true), or is it playing pre-placed
		 * pattern clips (false)?
		 */
		public boolean previewIsLive;
		
		/**
		 * It is possible to drag clips on top of other clips,
		 * and since we can't have overlaps, the duplicate is effectively
		 * destroyed and not recoverable, should we allow it?
		 * 
		 * @return
		 */
		public boolean allowClipMerge(){
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane)getComponent();
			return Preferences.getBooleanSafe(editor.parent.getEditorParent().session,Preferences.INDEX_BOOLEAN_PATTERN_ALLOW_CLIP_MERGE);
		}
		
		public LinkedEditorInnerPaneSkin(){
			anchorx=0;
			anchory=0;
			scalex=300;
			scaley=20;
			/*
			 * No need to add listeners, since Pivot takes care of that
			 * for us as long as we implement the methods here
			 */
		}
		
		@Override
		public BitSet getKeysDown() {
			return keys;
		}
		
		/**
		 * Yes, this is focusable.
		 */
		@Override
		public boolean isFocusable() {
			return true;
		}

		/**
		 * Call this before a preview start operation.
		 * 
		 * @param live whether this will be a live preview
		 */
		public void startPreviewBefore(boolean live){
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			previewIsLive = live;
			if(previewTrack==null){
				previewIsPlaying = false;
				previewTrack = new PatternTrack(session.project.rootTrack);
			}
		}
		
		/**
		 * Call this after a preview start operation.
		 */
		public void startPreviewAfter(){
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			if(!previewIsPlaying){
				previewIsPlaying = true;
				if(previewIsLive){
					session.getPlayer().playTrack(previewTrack, false, new double[]{0,Floats.ID_TINY});
				}else{
					session.getPlayer().playTrack(previewTrack, true);
				}
			}
		}
		
		/**
		 * Start previewing a particular pitch.
		 * 
		 * @param pitch semitones up from A4
		 */
		public void startPreviewPitch(int pitch){
			// fetch some things
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			Pattern pattern = editor.parent.view;
			Synthesizer synthesizer = pattern.getSynthesizer();
			ArrayList<Double> properties = editor.parent.getClipCurrentProperties();
			// make the params to pass
			int nproperties = properties.size();
			double[] clipData = new double[nproperties+2];
			clipData[0] = pitch;
			clipData[1] = editor.parent.getClipCurrentVolume();
			for(int i=0;i<nproperties;i++){
				clipData[i+2] = properties.get(i);
			}
			// call before
			startPreviewBefore(true);
			// add the voice
			previewTrack.addAudioProducer(synthesizer.spawnLiveAudioProducer(clipData, session));
			// call after
			startPreviewAfter();
		}
		
		public void startPreviewPattern(){
			// fetch some things
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			Pattern pattern = editor.parent.view;
			// call before
			startPreviewBefore(false);
			// add the pattern
			previewTrack.addPattern(pattern, 0);
			// call after
			startPreviewAfter();
		}
		
		/**
		 * End the preview operation
		 * 
		 * @param force true to make it end immediately
		 */
		public void endPreview(boolean force){
			if(previewTrack!=null){
				LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
				Session session = editor.parent.getEditorParent().session;
				Player player = session.getPlayer();
				if(!player.isPlaying()){// already not playing
					previewTrack = null;
				}else if(force){// force
					previewTrack.removeAllAudioProducers();
					player.stopPlay();
					previewTrack = null;
				}else{// not force
					previewTrack.requestKillAllAudioProducers();
				}
			}
		}
		
		/**
		 * Attempt to begin a
		 * {@link LinkedEditorPane.SimpleCommandType#TRANSLATE}.
		 * 
		 * @return true if successful
		 */
		public boolean beginCommandTranslate() {
			// can't do this if the mouse is down!
			if(mouseDown!=0)return false;
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			// tracking data
			mouseDown = -1;
			editor.parent.simpleCommand = LinkedEditorPane.SimpleCommandType.TRANSLATE;
			originMousex = lastMousex;
			originMousey = lastMousey;
			// success!
			return true;
		}
		
		/**
		 * Get 2-tuple of x index, y index of button being pressed
		 * <br>
		 * So 3 up after center and 7th from the left would be (6,3)
		 * <br>
		 * If no button is pressed, returns null
		 * 
		 * @param ignore if true, will ignore the current click/drag mouse state
		 * @return
		 */
		public int[] buttonxy(boolean ignore){
			if(!ignore&&(mouseDown!=1||mouseDragged))return null;
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Pattern pattern = editor.parent.view;
			int mousex = lastMousex, mousey = lastMousey;
			if(mousex<0||mousey<0)return null;// Out of bounds signals mouse not over editor
			double anchorx = this.anchorx, anchory = this.anchory,
					scalex = this.scalex, scaley = this.scaley,
					iscalex = 1d/scalex, iscaley = 1d/scaley;
			int swidth = editor.parent.getEditorParent().sidebarWidth;// Width remaining after sidebar
			int[] swidths = sidebarColumnWidths(swidth);
			int swidthn = swidths.length;
			int ry=(int)Math.floor(anchory-mousey*iscaley);
			int rx=0;
			while(rx<swidthn&&swidths[rx]<mousex){
				mousex-=swidths[rx];
				rx++;
			}
			if(rx==swidthn)return null;
			return new int[]{rx,ry};
		}
		
		@Override
		public boolean mouseMove(Component component, int x, int y) {
			// Tracking data
			int lastMousex = this.lastMousex, lastMousey = this.lastMousey;
			this.lastMousex = x;
			this.lastMousey = y;
			mouseDragged = true;
			int mouseDown = this.mouseDown;
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Pattern pattern = editor.parent.view;
			int width = getWidth(), height = getHeight();
			int swidth = editor.parent.getEditorParent().sidebarWidth, ewidth = width-swidth;// Width remaining after sidebar
			// Skip if mouse not held
			if(mouseDown!=0){
				// --- Fetch some data ---
				double anchorx = this.anchorx, anchory = this.anchory,
						scalex = this.scalex, scaley = this.scaley,
						iscalex = 1d/scalex, iscaley = 1d/scaley;
				// Get bounds
				//int[] swidths = sidebarColumnWidths(swidth);
				//int swidtha = swidths[0];
				// Handle the event
				if(editor.parent.simpleCommand != LinkedEditorPane.SimpleCommandType.NULL) {
					// No mouse is actually held down. Some command is happening.
					// This branch needs to be before the regular mouse branches!
					switch(editor.parent.simpleCommand) {
					case TRANSLATE:{
						// Translating clips
						int addx =  (int)Math.round((lastMousex-originMousex)*iscalex*pattern.divisions);
						int addy = -(int)Math.round((lastMousey-originMousey)*iscaley);
						IdentityHashMap<Clip,Clip> selection = editor.parent.getSelection(false,false);
						for(java.util.Map.Entry<Clip, Clip> entry:selection.entrySet()){
							Clip original = entry.getKey();
							Clip copy = entry.getValue();
							original.setDelay(Math.max(0, copy.getDelay()+addx));
							original.setPitch(copy.getPitch()+addy);
						}
						break;
					}
					case SCALE:{
						// TODO
						break;
					}
					case DELETE:{
						// TODO
						break;
					}
					}
				}else if(mouseDown==1){
					// Left mouse held -> drag to create new clip
				}else if(mouseDown==2){
					// Right mouse held -> drag to select
				}else if(mouseDown==3){
					// Middle mouse held -> drag to scroll
					uanchorx -= (x-lastMousex)*iscalex;
					uanchory += (y-lastMousey)*iscaley;// y needs to be inverted
					this.anchorx = Math.max(0, uanchorx);
					this.anchory = Floats.median(-80, uanchory, 80);
				}
			}else if(x<swidth||lastMousex<swidth){
				// nothing yet
			}
			// may need repainting
			editor.repaint();
			return true;// Consume the event
		}
		
		@Override
		public void mouseOut(Component component) {
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			getKeysDown().clear();// Forget which keys are pressed, for safety
			lastMousex = -1;
			lastMousey = -1;
			editor.repaint();// May need repainting
		}
		
		@Override
		public void mouseOver(Component component) {
		}
		
		@Override
		public boolean mouseClick(Component component, Button button, int x, int y, int count) {
			return true;// Consume the event
		}

		@Override
		public boolean mouseDown(Component component, Button button, int x, int y) {
			boolean alreadyDown = mouseDown != 0;
			// if there is an actual mouse press, preserve it
			mouseDown = mouseDown>0?mouseDown:PivotSwingUtils.getMouseButtonCode(button);
			// if it's already down, we don't update anything
			if(alreadyDown)return true;
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			Pattern pattern = editor.parent.view;
			int width = getWidth(), height = getHeight();
			// get focus, so it can respond to key events too
			editor.requestFocus();
			// Get bounds
			int swidth = editor.parent.getEditorParent().sidebarWidth, ewidth = width-swidth;// Width remaining after sidebar
			//int[] swidths = sidebarColumnWidths(swidth);
			//int swidtha = swidths[0];
			double anchorx = this.anchorx, anchory = this.anchory,
					scalex = this.scalex, scaley = this.scaley,
					iscalex = 1d/scalex, iscaley = 1d/scaley;
			uanchorx = anchorx;
			uanchory = anchory;
			originMousex = x;
			originMousey = y;
			originWorldx = anchorx+(x-swidth)*iscalex;
			originWorldy = anchory-y*iscaley;
			lastMousex = x;
			lastMousey = y;
			mouseDragged = false;
			// Local variables are faster + inversion
			HashSet<Clip> clips = pattern.clips;
			int n = clips.size();
			double wx = anchorx+(x-swidth)*iscalex, wy = anchory-y*iscaley;
			int iwx = (int)wx, iwy = (int)Math.floor(wy);
			boolean la = x<swidth;
			// Handle the event
			if(mouseDown==1){
				// Left click
				if(la){// Left click -> begin synth preview
					// spawn voices and play dummy track
					startPreviewPitch(iwy);
				}// Leave nested to allow for easier addition of future behaviours
			}
			// may need to update pending paste
			IdentityHashMap<Clip,Clip> paste = editor.parent.paste;
			if(paste != null){
				int xoffset = (int)Math.round(pattern.divisions*(wx-originWorldx));
				int yoffset = (int)Math.round(wy-originWorldy);
				for(java.util.Map.Entry<Clip,Clip> entry:paste.entrySet()){
					Clip original = entry.getKey();
					Clip copy = entry.getValue();
					copy.setDelay(original.getDelay() + xoffset);
					copy.setPitch(original.getPitch() + yoffset);
				}
			}
			editor.repaint();// May need repainting
			return true;// Consume the event
		}
		
		@Override
		public boolean mouseUp(Component component, Button button, int x, int y) {
			// Mouse is already up? do nothing
			if(mouseDown==0)return true;
			// --- Fetch some data ---
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			Pattern pattern = editor.parent.view;
			int width = getWidth(), height = getHeight();
			// Local variables are faster + inversion
			HashSet<Clip> clips = pattern.clips;
			int n = clips.size();
			double anchorx = this.anchorx, anchory = this.anchory,
					scalex = this.scalex, scaley = this.scaley,
					iscalex = 1d/scalex, iscaley = 1d/scaley;
			boolean mouseDragged = this.mouseDragged;
			// Get bounds
			int swidth = editor.parent.getEditorParent().sidebarWidth, ewidth = width-swidth;// Width remaining after sidebar
			//int[] swidths = sidebarColumnWidths(swidth);
			//int swidtha = swidths[0];
			double wx = anchorx+(x-swidth)*iscalex, wy = anchory-y*iscaley;
			int iwx = (int)wx, ipx = (int)(wx*pattern.divisions), iwy = (int)Math.floor(wy);
			double owx = originWorldx, owy = originWorldy;
			int iowx = (int)owx, iopx = (int)(owx*pattern.divisions), iowy = (int)Math.floor(owy);
			boolean la = x<swidth, lb = originMousex<swidth;
			// Get key modifiers
			boolean shiftHeld = shiftHeld();
			boolean controlHeld = controlHeld();
			// Handle the event
			if(editor.parent.simpleCommand != LinkedEditorPane.SimpleCommandType.NULL) {
				// No mouse is actually held down. Some command is happening.
				// This branch needs to be before the regular mouse branches!
				switch(editor.parent.simpleCommand) {
				case TRANSLATE:{
					// Translating clips -> confirm or cancel
					switch(mouseDown) {
					case 1:{// Left -> confirm
						editor.parent.finalizeSelection(true);
						editor.parent.simpleCommand = LinkedEditorPane.SimpleCommandType.NULL;
						break;
					}
					case 2:{// Right -> cancel
						editor.parent.finalizeSelection(false);
						editor.parent.simpleCommand = LinkedEditorPane.SimpleCommandType.NULL;
						break;
					}
					default:{// Anything else -> revert
						mouseDown = -1;
						break;
					}
					}
					break;
				}
				case SCALE:{
					// TODO
					break;
				}
				case DELETE:{
					// TODO
					break;
				}
				}
			}else if(mouseDown==1){
				// Left click/drag
				// Note: mouse not dragged implies la=lb
				if(mouseDragged){
					if(!(la|lb)){// Left drag -> create new clips
						int xmin = Math.min(ipx, iopx), xmax = Math.max(ipx, iopx);
						Clip newClip = new Clip(xmin, xmax-xmin+1, iwy, editor.parent.getClipCurrentVolume());
						newClip.properties.addAll(editor.parent.getClipCurrentProperties());
						pattern.clips.add(newClip);
						pattern.remakeLength();
						editor.parent.select(Collections.singleton(newClip), false);
					}// Leave nested to allow for easier addition of future behaviours
				}else{
					if(!la){// Left click -> confirm current operation
						if(editor.parent.paste != null){// current operation is paste, so confirm it
							editor.parent.finalizePaste(true);
						}
					}// Leave nested to allow for easier addition of future behaviours
				}
				if(lb){
					// request kill voices from preview
					endPreview(false);
				}
			}else if(mouseDown==2){
				// Right click/drag
				if(mouseDragged){// drag -> select
					if(!(la|lb)){
						// none = set, shift = select, control = deselect
						boolean clear = !(shiftHeld|controlHeld);
						boolean invert = controlHeld;
						IdentityHashMap<Clip,Clip> selection = editor.parent.getSelection(true,clear);
						int xmin=iopx,xmax=ipx,ymin=iowy,ymax=iwy;
						if(xmin>xmax){
							int t=xmin;
							xmin=xmax;
							xmax=t;
						}
						if(ymin>ymax){
							int t=ymin;
							ymin=ymax;
							ymax=t;
						}
						ArrayList<Clip> collectClips = new ArrayList<>();
						for(Clip clip:pattern.clips){
							int cxmin = clip.getDelay(), cxmax = cxmin+clip.getLength()-1, cy = clip.getPitch();
							// test if clip is in selection area
							if(xmin<=cxmax && cxmin<=xmax && ymin<=cy && cy<=ymax){
								collectClips.add(clip);
							}
						}
						editor.parent.select(collectClips, invert);
					}// Leave nested to allow for easier addition of future behaviours
				}else{
					if(!la){// Right click -> cancel current operation or select single
						if(editor.parent.paste != null){// current operation is paste, so cancel
							editor.parent.finalizePaste(false);
						}
					}
				}
			}else if(mouseDown==3){
				// Middle click/drag
				// scroll is already handled in mouseMove so we don't need to do anything here
				uanchorx = anchorx;
				uanchory = anchory;
			}
			// Signal the mouse not being pressed anymore
			// ... unless a command specifically wants it to stay
			mouseDown = mouseDown<0?mouseDown:0;
			// May need repainting
			editor.repaint();
			return true;// Consume the event
		}
		
		@Override
		public boolean mouseWheel(Component component, ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
			// --- Fetch some data ---
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			boolean shiftHeld = shiftHeld();
			double mul = Math.pow(2, (1d/32)*wheelRotation);
			if(shiftHeld){// Vertical zoom
				scaley = Floats.median(SCALE_MIN_Y, scaley*mul, SCALE_MAX_Y);
			}else{// Horizontal zoom
				scalex = Floats.median(SCALE_MIN_X, scalex*mul, SCALE_MAX_X);
			}
			// force repaint
			editor.repaint();
			return true;// Consume the event
		}
		
		@Override
		public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
			getKeysDown().set(keyCode);
			return true;// Consume the event
		}

		@Override
		public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
			getKeysDown().clear(keyCode);
			boolean modified = false;
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			Pattern pattern = editor.parent.view;
			switch(keyCode){
			case KeyEvent.VK_C:{
				if(controlHeld()){// Ctrl + C -> copy
					IdentityHashMap<Clip,Clip> selection = editor.parent.getSelection(true, false);
					HashSet<Clip> selected = new HashSet<>(selection.values());// use the copy here
					session.setClipBoard(selected);
				}
				break;
			}
			case KeyEvent.VK_V:{
				if(controlHeld()){// Ctrl + V -> paste
					// cannot paste if already pasting
					if(editor.parent.paste == null){
						Object clipBoard = session.getClipBoard();
						if(clipBoard instanceof Iterable<?>){
							Iterable<?> iclip = (Iterable<?>)clipBoard;
							Iterator<?> iclipIterator = iclip.iterator();
							ArrayList<Clip> collectClips = new ArrayList<>();
							boolean canPaste = iclipIterator.hasNext();
							while(iclipIterator.hasNext()){
								Object item = iclipIterator.next();
								if(item==null || !(item instanceof Clip)){
									canPaste = false;
									break;
								}
								collectClips.add((Clip)item);
							}
							if(canPaste){
								// log mouse location
								int swidth = editor.parent.getEditorParent().sidebarWidth;
								double anchorx = this.anchorx, anchory = this.anchory,
										scalex = this.scalex, scaley = this.scaley,
										iscalex = 1d/scalex, iscaley = 1d/scaley;
								int x = lastMousex, y = lastMousey;
								originMousex = x;
								originMousey = y;
								originWorldx = anchorx+(x-swidth)*iscalex;
								originWorldy = anchory-y*iscaley;
								// start paste operation
								IdentityHashMap<Clip,Clip> paste = editor.parent.paste = new IdentityHashMap<>();
								for(Clip clip:collectClips){
									paste.put(clip, clip.copy());
								}
								// set modified flag
								modified = true;
							}
						}
					}
				}
				break;
			}
			case KeyEvent.VK_SPACE:{// Start or stop playing
				if(previewTrack!=null){// Currently playing -> stop
					endPreview(true);
				}else{// Not playing -> start
					startPreviewPattern();
				}
				break;
			}
			case KeyEvent.VK_G:{// Grab aka translate clips
				modified = beginCommandTranslate();
				break;
			}
			/*
			 * TODO commands with keybinds
			 * 
			 * delete clips
			 * grab/translate clips (currently WIP, works, but no UI)
			 * scale clips
			 * 
			 * and after that, need to put it in the menu?
			 */
			}
			if(modified){// This flag tracks whether any changes were made
				editor.repaint();
			}
			return true;// Consume the event
		}

		@Override
		public boolean keyTyped(Component component, char character) {
			return true;// Consume the event
		}
		
		@Override
		public void layout() {
			//There are no displayed subcomponents, so nothing needs to be done here
		}
		
		@Override
		public void paint(Graphics2D graphics){
			// --- Fetch some data ---
			LinkedEditorInnerPane editor = (LinkedEditorInnerPane) getComponent();
			Session session = editor.parent.getEditorParent().session;
			Project project = session.project;
			Pattern pattern = editor.parent.view;
			Synthesizer synth = pattern.getSynthesizer();
			int width = getWidth(), height = getHeight();
			int gradientShiftInc = (int)Preferences.getIntSafe(session, Preferences.INDEX_INT_PATTERN_BAR_GRADIENT);
			long backgroundStyle = Preferences.getIntSafe(session, Preferences.INDEX_INT_PATTERN_BACKGROUND_STYLE);
			long synthPreviewStyle = Preferences.getIntSafe(session, Preferences.INDEX_INT_PATTERN_SYNTH_PREVIEW_BAR_STYLE);
			long gridMin = Preferences.getIntSafe(session, Preferences.INDEX_INT_PATTERN_GRID_MIN);
			// Local variables are faster + inversion
			IdentityHashMap<Clip,Clip> selection = editor.parent.getSelection(false, false);
			IdentityHashMap<Clip,Clip> paste = editor.parent.paste;
			int lastMousex = this.lastMousex, lastMousey = this.lastMousey, mouseDown = this.mouseDown;
			double anchorx = this.anchorx, anchory = this.anchory,
					scalex = this.scalex, scaley = this.scaley,
					iscalex = 1d/scalex, iscaley = 1d/scaley;
			boolean mouseDragged = this.mouseDragged;
			// Get bounds
			int swidth = editor.parent.getEditorParent().sidebarWidth, ewidth = width-swidth;// Width remaining after sidebar
			//int[] swidths = sidebarColumnWidths(swidth);
			//int swidtha = swidths[0];
			double wx = anchorx+(lastMousex-swidth)*iscalex, wy = anchory-lastMousey*iscaley;
			int iwx = (int)wx, ipx = (int)(wx*pattern.divisions), iwy = (int)Math.floor(wy);
			double owx = originWorldx, owy = originWorldy;
			int iowx = (int)owx, iopx = (int)(owx*pattern.divisions), iowy = (int)Math.floor(owy);
			boolean la = lastMousex<swidth, lb = originMousex<swidth;
			double cornerx = anchorx+ewidth*iscalex, cornery = anchory-height*iscaley;
			int patternDivisions = pattern.divisions;
			double idivisions = 1d/patternDivisions;
			// create clips list
			ArrayList<Clip> clips = new ArrayList<>(pattern.clips);
			Comparator<Clip> clipSorter = Comparator
					.comparing((Clip clip)->selection.containsKey(clip))
					.thenComparingInt(Clip::getDelay)
					.thenComparingInt(Clip::getLength);
			if(paste != null){// need to show pending paste
				clips.addAll(paste.values());
			}
			if(mouseDown==1 && mouseDragged && !(la|lb)){// drag would create clip
				int xmin = Math.min(ipx, iopx), xmax = Math.max(ipx, iopx);
				Clip newClip = new Clip(xmin, xmax-xmin+1, iwy, editor.parent.getClipCurrentVolume());
				newClip.properties.addAll(editor.parent.getClipCurrentProperties());
				clips.add(newClip);
			}
			clips.sort(clipSorter);
			int nclips = clips.size();
			// calculate y bounds
			int pfirst = (int)Math.floor(cornery), plast = 1+(int)Math.floor(anchory);//By convention it's first inclusive last exclusive
			if(nclips > 0) {
				pfirst = Math.min(
						pfirst,
						clips.stream().mapToInt(Clip::getPitch).reduce(Math::min).getAsInt()
						);
				plast = Math.max(
						plast,
						clips.stream().mapToInt(Clip::getPitch).reduce(Math::max).getAsInt() + 1
						);
			}
			// calculate range after
			int prange = plast-pfirst;
			// Precalculate y values
			int[] ys = new int[prange+1];
			for(int i=pfirst;i<=plast;i++){
				ys[i-pfirst] = (int)(scaley*(anchory-i));
			}
			// --- Pre-draw ---
			Color bgCol, bgColDark, lineCol, alineCol, selectCol, aselectCol;
			ColorScheme colors = Session.getColors(session);
			bgCol = colors.background;
			lineCol = colors.line;
			selectCol = colors.selected;
			bgColDark = ColorScheme.brighten(bgCol, -0.1f);
			alineCol = ColorScheme.setAlpha(lineCol, 30);
			aselectCol = ColorScheme.setAlpha(selectCol, 30);
			// synth colors
			double dxgradientInc = gradientShiftInc*iscalex;
			int xrefBlocks = (int)(anchorx/dxgradientInc);
			double dxref = xrefBlocks*dxgradientInc;
			int xref = -((int)((anchorx-dxref)*scalex));//First x for gradient, cannot be positive
			int xrange = ((ewidth-xref-1)/gradientShiftInc)+1;
			double ixrange = 1d/xrange;
			Color[] sigs = new Color[xrange+1], sigsDark = new Color[xrange+1];
			for(int j=0;j<=xrange;j++){
				double t=project.measuresToSeconds((j*gradientShiftInc)*iscalex+dxref);
				Color sig = sigs[j] = synth.getColorSignature(t);
				sigsDark[j] = ColorScheme.brighten(sig, -0.1f);
			}
			float[] sigKeyframes = new float[xrange+1];
			sigKeyframes[xrange] = 1;
			for(int j=1;j<xrange;j++){
				sigKeyframes[j] = (float)(j*ixrange);
			}
			LinearGradientPaint sigPaint = new LinearGradientPaint(xref,0,xref+gradientShiftInc*xrange,0,sigKeyframes,sigs);
			LinearGradientPaint sigDarkPaint = new LinearGradientPaint(xref,0,xref+gradientShiftInc*xrange,0,sigKeyframes,sigsDark);
			// --- Draw ---
			// Set thicker lines for visibility
			BasicStroke stroke = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			graphics.setStroke(stroke);
			// Draw synth preview
			//int[] swidths = sidebarColumnWidths(swidth);
			//int swidtha = swidths[0];
			int[] buttonxy = buttonxy(false);
			int buttonx = buttonxy==null?-1:buttonxy[0];
			int buttony = buttonxy==null?-1:buttonxy[1];
			// synth preview fills
			for(int i=pfirst,j=Math.floorMod(pfirst, 12);i<plast;i++,j++){
				if(j==12)j=0;// cycle at 12
				int k = i-pfirst;
				int y1 = ys[k], y2 = ys[k+1];
				Color fill = buttonx==0 && buttony==i
						?selectCol
						:Bits.readBit(synthPreviewStyle, j)?bgColDark:bgCol;
				graphics.setColor(fill);
				graphics.fillRect(0, y2, swidth, y1-y2);
			}
			// synth preview borders
			graphics.setColor(lineCol);
			for(int k=0;k<prange;k++){
				int y1 = ys[k], y2 = ys[k+1];
				graphics.drawRect(0, y2, swidth, y1-y2);
			}
			// because this gets drawn before the clip area background, half the vertical line will get cut off
			// done with synth preview, move on to clips
			graphics = (Graphics2D) graphics.create(swidth, 0, ewidth, height);
			// fill background
			if(Bits.readBit(backgroundStyle, 0)){
				for(int i=12*Math.floorDiv(pfirst, 12);i<plast;i+=12){
					int y1 = (int)(scaley*(anchory-i));
					int y2 = (int)(scaley*(anchory-i-12));
					graphics.setPaint(Draw.gradientVertical(bgCol, bgColDark, y2, y1));
					graphics.fillRect(0, y2, ewidth, y1-y2);
				}
			}else{
				graphics.setColor(bgCol);
				graphics.fillRect(0, 0, ewidth, height);
			}
			// overlay
			for(int i=pfirst,j=Math.floorMod(pfirst, 12);i<plast;i++,j++){
				if(j==12)j=0;// cycle at 12
				int k = i-pfirst;
				int y1 = ys[k], y2 = ys[k+1];
				Color fill = buttonx==0 && buttony==i
						?aselectCol
						:Bits.readBit(synthPreviewStyle, j)?alineCol:null;
				if(fill!=null){
					graphics.setColor(fill);
					graphics.fillRect(0, y2, ewidth, y1-y2);
				}
			}
			// draw gridlines
			if(Bits.readBit(backgroundStyle, 1)){// bit 1 toggles horizontal gridlines
				graphics.setColor(lineCol);
				// horizontal lines
				if(scaley>gridMin){
					for(int k=0;k<prange;k++){
						int y = ys[k];
						graphics.drawLine(0, y, ewidth, y);
					}
				}
			}
			// vertical lines
			double ldivisions = Math.log(patternDivisions),
					importanceMult = 1d/(ldivisions+1);
			ArrayList<Integer> divisionsFactors = new ArrayList<>(Primes.primeFactors(patternDivisions));
			divisionsFactors.sort(Comparator.naturalOrder());// not sure if already sorted?
			int nfactors = divisionsFactors.size();
			int[] products = new int[nfactors+1];
			products[0] = 1;
			double[] importance = new double[nfactors+1];
			double lp = 0;
			for(int i=0,pp=1;i<nfactors;i++){
				int p = divisionsFactors.get(i);
				pp *= p;
				lp += Math.log(p);
				products[i+1] = pp;
				importance[i+1] = lp*importanceMult;
			}
			double gradientStart = -anchorx%patternDivisions*scalex
					-1;// for 2px centering, move halfway back
			Color transparentLine = ColorScheme.setAlpha(lineCol, 0);
			double alpha = 0;
			for(int i=nfactors;i>=0;i--){
				int iproduct = products[i];
				double iimportance = importance[i];
				double newAlpha = 1-iimportance;
				double fillAlpha = (newAlpha-alpha)/(1-alpha);
				alpha = newAlpha;
				if((double)iproduct*gridMin>scalex)continue;// very small spacing, would look bad
				Color translucentLine = ColorScheme.setAlpha(lineCol, (int)(255*fillAlpha));
				double gradientWidth = scalex/iproduct;
				double igradientWidth = 2d/gradientWidth;// 2px thick
				LinearGradientPaint paint = new LinearGradientPaint(
						(float)gradientStart,
						0,
						(float)(gradientStart+gradientWidth),
						0,
						new float[]{0,(float)igradientWidth,(float)(igradientWidth+Floats.F_EPSILON),1},
						new Color[]{translucentLine,translucentLine,transparentLine,transparentLine},
						MultipleGradientPaint.CycleMethod.REPEAT
						);
				graphics.setPaint(paint);
				graphics.fillRect(0, 0, ewidth, height);
			}
			// draw clips fill
			graphics.setPaint(sigPaint);
			for(Clip clip:clips){
				double x1 = (clip.getDelay()*idivisions-anchorx)*scalex;
				double x2 = x1+clip.getLength()*idivisions*scalex;
				int ix1 = (int)x1, ix2 = (int)x2;
				int y1 = ys[clip.getPitch()-pfirst  ];
				int y2 = ys[clip.getPitch()-pfirst+1];
				graphics.fillRect(ix1, y2, ix2-ix1, y1-y2);
			}
			// draw clips border
			for(Clip clip:clips){
				boolean selected = selection.containsKey(clip);
				Paint paint = selected?selectCol:sigDarkPaint;
				graphics.setPaint(paint);
				double x1 = (clip.getDelay()*idivisions-anchorx)*scalex;
				double x2 = x1+clip.getLength()*idivisions*scalex;
				int ix1 = (int)x1, ix2 = (int)x2;
				int y1 = ys[clip.getPitch()-pfirst  ];
				int y2 = ys[clip.getPitch()-pfirst+1];
				graphics.drawRect(ix1, y2, ix2-ix1, y1-y2);
			}
			if(mouseDown==2){
				// Right click/drag
				if(mouseDragged){// drag -> select
					if(!(la|lb)){
						// Draw the box for box selecting
						int bxmin = Math.min(lastMousex, originMousex),
								bxmax = Math.max(lastMousex, originMousex),
								bymin = Math.min(lastMousey, originMousey),
								bymax = Math.max(lastMousey, originMousey);
						Rectangle2D.Double dragBox = new Rectangle2D.Double(bxmin-swidth,bymin,bxmax-bxmin,bymax-bymin);
						graphics.setColor(aselectCol);
						graphics.fill(dragBox);
						graphics.setColor(selectCol);
						graphics.draw(dragBox);
					}
				}
			}
		}
		/*
		 * TODO command UI elements
		 */
	}
	
	/**
	 * Widths of the columns for the sidebar
	 * <br>
	 * Will sum to <i>swidth</i>
	 * <br>
	 * May be 0 if <i>swidth</i> is small, but never negative
	 * <br>
	 * Behaviour for negative <i>swidth</i> is unspecified
	 * (it should never happen anyway)
	 * 
	 * @param swidth total width of the sidebar
	 * @return widths of each column
	 */
	public static int[] sidebarColumnWidths(int swidth){
		return new int[]{swidth};
	}

}
