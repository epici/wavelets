package org.epici.wavelets.ui.curve.constant;

import java.net.URL;
import java.util.ArrayList;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.Label;
import org.epici.wavelets.core.Curve;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.VarDouble;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.DataEditor.Instance;
import org.epici.wavelets.ui.curve.CurveEditor;
import org.epici.wavelets.ui.curve.DoubleInput;
import org.epici.wavelets.ui.curve.CurveEditor.LinkedEditorPane;
import org.epici.wavelets.ui.curve.DoubleInput.DoubleValidator;
import org.epici.wavelets.ui.curve.DoubleInput.DoubleValidator.BoundedDoubleValidator;
import org.epici.wavelets.ui.curve.DoubleInput.DoubleValidator.HyperbolicStep;
import org.epici.wavelets.ui.curve.DoubleInput.DoubleValidator.SplitDoubleValidator;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * Editor for {@link VarDouble} instances. Meant only for constants
 * (single variable), not curves.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class CurveConstantEditor extends FillPane implements Bindable, DataEditor.Instance<VarDouble, CurveEditor.LinkedEditorPane> {
	
	/**
	 * The pane which will contain only {@link #constantInput}.
	 */
	public FillPane fillConstantInput;
	/**
	 * The label which displays basic information about the constant.
	 */
	public Label infoLabel;
	/**
	 * The input to change {@link #view}.
	 */
	public DoubleInput constantInput;
	
	/**
	 * The parent editor.
	 */
	public CurveEditor.LinkedEditorPane parent;
	/**
	 * The {@link VarDouble} being edited.
	 */
	public VarDouble view;
	
	public CurveConstantEditor(){
		super();
	}
	
	/**
	 * Convenience method to create a new one from outside
	 * 
	 * @return a new editor instance
	 */
	public static CurveConstantEditor createNew(){
		return PivotSwingUtils.loadBxml(CurveConstantEditor.class, "curveConstantEditor.bxml");
	}
	
	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		fillConstantInput = (FillPane) namespace.get("fillConstantInput");
		infoLabel = (Label) namespace.get("infoLabel");
	}
	
	@Override
	public void init(){
		double value = view.get(0);
		constantInput = new DoubleInput(
				new DoubleInput.DoubleValidator.SplitDoubleValidator(
						new DoubleInput.DoubleValidator.BoundedDoubleValidator(value),
						new DoubleInput.DoubleValidator.HyperbolicStep(2)),
				value, 0x1.0p-7);
		constantInput.value = view;
		
		fillConstantInput.add(constantInput);
		
		updateInfoText();
	}

	@Override
	public VarDouble getEditorData() {
		return view;
	}

	@Override
	public LinkedEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof CurveEditor.LinkedEditorPane))return false;
		parent = (CurveEditor.LinkedEditorPane) newParent;
		return true;
	}
	
	/**
	 * Updates the text in {@link #infoLabel}.
	 */
	public void updateInfoText(){
		// fetch datas
		CurveEditor.LinkedEditorPane parent = getEditorParent();
		Session session = parent.getEditorParent().session;
		// make a list of possible problems
		ArrayList<String> warnings = new ArrayList<>();
		if(view instanceof Curve){
			// curves should not be edited by this
			warnings.add("is not a constant");
		}
		// make the string
		StringBuilder sb = new StringBuilder();
		sb.append(session.getCommonName(view.getClass()));
		if(!warnings.isEmpty()){
			sb.append(" ");
			sb.append(warnings);
		}
		String newText = sb.toString();
		// update the label with the new text
		infoLabel.setText(newText);
	}

}
