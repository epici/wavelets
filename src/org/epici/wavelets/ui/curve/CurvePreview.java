package org.epici.wavelets.ui.curve;

import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.BitSet;
import java.util.Map;

import javax.swing.JInternalFrame;

import org.apache.pivot.beans.Bindable;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.ComponentKeyListener;
import org.apache.pivot.wtk.Container;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Orientation;
import org.apache.pivot.wtk.WTKListenerList;
import org.epici.wavelets.core.BetterClone;
import org.epici.wavelets.core.Curve;
import org.epici.wavelets.core.MetaComponent;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.VarDouble;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.ui.SessionLinked;
import org.epici.wavelets.ui.WindowManager;

/**
 * Generic holder for a preview of a curve.
 * All instances of {@link VarDouble} are allowed rather than
 * only instances of {@link Curve}.
 * Requires that it implements {@link DataEditor.Editable}
 * to propery generate a preview component using
 * {@link DataEditor.Editable#getPreview()}.
 * The curve can be opened in the {@link CurveEditor},
 * and a different curve can be swapped in.
 * Some preview components may also support direct editing.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class CurvePreview extends FillPane implements Bindable, KeyTracker, DataEditor.Instance<VarDouble, Container> {

	/**
	 * The parent of this. Since we want the flexiblity to use it
	 * anywhere, it only needs to be a {@link Container},
	 * which all component containers should be anyway.
	 * Users should attempt a cast to a more specific type
	 * they want to use.
	 */
	public Container parent;
	/**
	 * The curve which this displays a preview for.
	 */
	protected VarDouble view;
	/**
	 * Preview interface supplied by the component. Null only if the component
	 * failed to supply an preview.
	 * <br>
	 * Most things require that it also be an instance of
	 * {@link DataEditor.Instance}. This allows parents
	 * to pass useful data on to the children.
	 * <br>
	 * Similar to {@link CurveEditor.LinkedEditorPane#innerEditor}.
	 */
	public Component innerEditor;
	/**
	 * Which keys are held down?
	 */
	protected BitSet keys = new BitSet();
	/**
	 * The list of listeners of type {@link ViewCurveListener}.
	 * All are notified when {@link #view} changes
	 * through a call to {@link #setEditorData(VarDouble, boolean)}.
	 */
	public ViewCurveListenerList viewCurveListeners = new ViewCurveListenerList();
	
	public CurvePreview() {
		this(Orientation.HORIZONTAL);
	}

	public CurvePreview(Orientation orientation) {
		super(orientation);
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
	}
	
	@Override
	public boolean isFocusable() {
		return this.getLength()==0 || !this.get(0).isFocusable();
	}

	@Override
	public VarDouble getEditorData() {
		return view;
	}
	
	/**
	 * Change the {@link VarDouble} which this displays the preview for.
	 * Updates the user interface and notifies listeners accordingly.
	 * <br>
	 * Call with {@code notify} as {@code true} unless you
	 * really know what you are doing.
	 * Not notifying listeners can result in
	 * invalid state and breaking invariants.
	 * 
	 * @param newView the new value of {@link #view} to set
	 * @param notify whether to notify listeners
	 * @return true if it was successfully set
	 */
	public boolean setEditorData(VarDouble newView, boolean notify) {
		if(newView == null || newView == view)return false;
		VarDouble oldView = view;
		view = newView;
		innerEditor = null;
		this.removeAll();
		trySetInnerEditor();
		viewCurveListeners.viewCurveChanged(this, oldView, newView);
		return true;
	}
	
	/**
	 * Change the {@link VarDouble} which this displays the preview for.
	 * Updates the user interface and notifies listeners accordingly.
	 * 
	 * @param newView the new value of {@link #view} to set
	 * @return true if it was successfully set
	 */
	public boolean setEditorData(VarDouble newView) {
		return setEditorData(newView, true);
	}

	@Override
	public Container getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(newParent instanceof Container){
			parent = (Container) newParent;
			return true;
		}
		return false;
	}
	
	@Override
	public void init(){
		// call super initializer
		DataEditor.Instance.super.init();
		
		// set the parent if not done yet
		if(getEditorParent() == null) {
			setEditorParent(getParent());
		}
		
		// make the combined listener
		ListenerPack listenerPack = new ListenerPack();
		
		// attach the combined listener where applicable
		this.getComponentKeyListeners().add(listenerPack);
		
		// set the preview component based on the "view" field
		trySetInnerEditor();
	}

	@Override
	public BitSet getKeysDown() {
		return keys;
	}
	
	/**
	 * If {@link #innerEditor} is not set, tries to set it.
	 * Returns true if this method sets it.
	 * If it returns false, either it was already set or it
	 * could not be set.
	 * 
	 * @return true on success, false on failure
	 */
	public boolean trySetInnerEditor(){
		// already exists
		if(innerEditor != null)return false;
		// need this interface to fetch UI
		if(view instanceof DataEditor.Editable<?, ?>){
			DataEditor.Editable<?, ?> viewEditable = (DataEditor.Editable<?, ?>) view;
			// set the editor to whatever it returns
			innerEditor = viewEditable.getPreview();
			if(innerEditor == null)return false;// oh no, it failed to supply it
			// most things require implementing this
			if(innerEditor instanceof DataEditor.Instance<?, ?>){
				DataEditor.Instance<?, ?> innerEditorInstance = (DataEditor.Instance<?, ?>) innerEditor;
				// if the parent is accepted, proceed with initialization
				if(innerEditorInstance.setEditorParent(this)){
					innerEditorInstance.init();
				}
			}
			// directly add it, since it did not exist before
			this.add(innerEditor);
			return true;
		}
		// could not get UI
		return false;
	}
	
	/**
	 * Internal helper method to make a copy of a {@link VarDouble}
	 * which is intended to be long lived. Does not rename.
	 * 
	 * @param original make a copy of this
	 * @param session current session which provides other data
	 * @return a copy, or null on failure
	 */
	protected static VarDouble copyOrNull(VarDouble original, Session session) {
		if(original == null)return null;
		Map<String, Object> copyOptions = BetterClone.fixOptions(null);
		session.setCopyOptions(copyOptions);
		VarDouble result = BetterClone.tryCopy(original, 1, copyOptions);
		if(result == original)return null;
		return result;
	}
	
	/**
	 * Combined listener which is attached to a {@link CurvePreview}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ListenerPack implements ComponentKeyListener {

		@Override
		public boolean keyTyped(Component component, char character) {
			return false;
		}

		@Override
		public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
			CurvePreview curvePreview = (CurvePreview) component;
			curvePreview.getKeysDown().set(keyCode);
			return false;
		}

		@Override
		public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
			boolean didSomething = false;
			CurvePreview curvePreview = (CurvePreview) component;
			Session session = SessionLinked.tryGetSession(curvePreview);
			int keyModifiers = curvePreview.getKeyModifiers();
			switch(keyCode) {
			case KeyEvent.VK_C:{
				if(session == null)break;// need a session to do this
				switch(keyModifiers) {
				case 1:{// Shift C -> store copy
					VarDouble toCopy = curvePreview.getEditorData();
					toCopy = CurvePreview.copyOrNull(toCopy, session);
					session.setClipBoard(toCopy);
					didSomething = true;
					break;
				}
				case 2:{// Control C -> store original reference
					VarDouble toCopy = curvePreview.getEditorData();
					session.setClipBoard(toCopy);
					didSomething = true;
					break;
				}
				}
				break;
			}
			case KeyEvent.VK_V:{
				if(session == null)break;// need a session to do this
				switch(keyModifiers) {
				case 1:{// Shift V -> put copy
					Object oclipboard = session.getClipBoard();
					if(oclipboard == null)break;
					VarDouble pasted = null;
					if (oclipboard instanceof VarDouble) {
						pasted = (VarDouble) oclipboard;
						pasted = CurvePreview.copyOrNull(pasted, session);
					} else if (oclipboard instanceof Number) {
						double ovalue = ((Number)oclipboard).doubleValue();
						pasted = new VarDouble.Single(ovalue);
					}
					if(pasted == null)break;
					curvePreview.setEditorData(pasted);
					didSomething = true;
					break;
				}
				case 2:{// Control V -> put original reference
					Object oclipboard = session.getClipBoard();
					if(oclipboard == null)break;
					VarDouble pasted = null;
					if (oclipboard instanceof VarDouble) {
						pasted = (VarDouble) oclipboard;
					} else if (oclipboard instanceof Number) {
						double ovalue = ((Number)oclipboard).doubleValue();
						pasted = new VarDouble.Single(ovalue);
					}
					if(pasted == null)break;
					curvePreview.setEditorData(pasted);
					didSomething = true;
					break;
				}
				}
				break;
			}
			case KeyEvent.VK_I:{// I -> open editor
				// Needs session to work
        		if(session==null)break;
	        	// Open editor
	        	MetaComponent<JInternalFrame> meta = session.windowManager.getWindow(WindowManager.NAME_CURVE_EDITOR, true);
	        	Object ocurveEditor = meta.metaData.get("window");
	        	if(ocurveEditor instanceof DataEditor<?>){
	        		DataEditor<?> curveEditor = (DataEditor<?>) ocurveEditor;
	        		curveEditor.addEditorDataCasted(curvePreview.view);
	        		didSomething = true;
	        	}
				break;
			}
			}
			curvePreview.getKeysDown().clear(keyCode);
			return didSomething;
		}
		
	}
	
	/**
	 * The listener which fires when {@link CurvePreview#view} changes.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static interface ViewCurveListener {
		
		/**
		 * The curve which this component displays, changed.
		 * Now it is displaying a different curve.
		 * 
		 * @param editor the component displaying the curve
		 * @param oldView what curve was being displayed before this event fired
		 * @param newView what curve is being displayed now
		 */
		public void viewCurveChanged(CurvePreview editor, VarDouble oldView, VarDouble newView);
		
	}
	
	/**
	 * Represents a list of {@link ViewCurveListener},
	 * which can also be treated as one.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ViewCurveListenerList extends WTKListenerList<ViewCurveListener> implements ViewCurveListener {

		@Override
		public void viewCurveChanged(CurvePreview editor, VarDouble oldView, VarDouble newView) {
			for(ViewCurveListener listener:this) {
				listener.viewCurveChanged(editor, oldView, newView);
			}
		}
		
	}

}
