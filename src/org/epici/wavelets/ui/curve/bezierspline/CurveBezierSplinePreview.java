package org.epici.wavelets.ui.curve.bezierspline;

import java.net.URL;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Container;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.Orientation;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.SessionLinked;

/**
 * Curve preview, as used by {@link CurveBezierSpline#getPreview()}.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class CurveBezierSplinePreview extends FillPane implements Bindable, DataEditor.Instance<CurveBezierSpline, Container>, SessionLinked {

	/**
	 * The parent editor.
	 */
	public Container parent;
	/**
	 * The curve being edited.
	 */
	public CurveBezierSpline view;
	/**
	 * The current session.
	 */
	public Session session;
	
	/**
	 * Blank constructor, uses vertical orientation.
	 * 
	 * @see FillPane#FillPane()
	 */
	public CurveBezierSplinePreview() {
		this(Orientation.VERTICAL);
	}

	/**
	 * Constructor with orientation.
	 * 
	 * @param orientation layout orientation
	 * 
	 * @see FillPane#FillPane(Orientation)
	 */
	public CurveBezierSplinePreview(Orientation orientation) {
		super(orientation);
		installSkin(CurveBezierSplinePreview.class);
	}

	@Override
	public Session getSession() {
		if(session == null)session = SessionLinked.tryGetSession(this.getParent());
		return session;
	}

	@Override
	public CurveBezierSpline getEditorData() {
		return view;
	}

	@Override
	public Container getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof Container))return false;
		parent = (Container) newParent;
		return true;
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		
	}
	
	@Override
	public void init(){
		
	}
	
}
