package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.Graphics2D;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;

/**
 * Interface for a specific type of command which is an instant action,
 * and thus immediately finalizes itself without giving further time to interact.
 * {@link #finalize(boolean)} should always finish and return true.
 * Implementing this is not required as long as the implementation's
 * {@link #isInstantCommand()} returns true.
 * <br>
 * All listener commands are pre-implemented as default methods to do nothing,
 * since they should never have an opportunity to be triggered anyway
 * and it makes for lots of boilerplate.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public interface GraphicalEditorCommandInstant extends GraphicalEditorCommand {
	
	@Override
	public default boolean isInstantCommand() {
		return true;
	}

	@Override
	public default boolean mouseMove(Component component, int x, int y) {
		return false;
	}

	@Override
	public default void mouseOver(Component component) {
	}

	@Override
	public default void mouseOut(Component component) {
	}

	@Override
	public default boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		return false;
	}

	@Override
	public default boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		return false;
	}

	@Override
	public default boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return false;
	}

	@Override
	public default boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		return false;
	}

	@Override
	public default boolean keyTyped(Component component, char character) {
		return false;
	}

	@Override
	public default boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public default boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public default void paint(Graphics2D graphics) {
		// warning, because this should never get called anyway
		System.out.println("Warning: instant command triggered paint, from instance of "+this.getClass().getCanonicalName());
		// delegate
		getEditorParent().commandNone.paint(graphics);
	}

}
