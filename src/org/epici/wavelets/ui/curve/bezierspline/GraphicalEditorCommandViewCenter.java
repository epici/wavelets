package org.epici.wavelets.ui.curve.bezierspline;

import org.apache.pivot.wtk.Component;
import org.epici.wavelets.core.curve.CurveBezierSpline;

/**
 * Represents an instant command which changes the view center
 * to some special point.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandViewCenter implements GraphicalEditorCommandInstant {
	
	/**
	 * Value of {@link #targetCode} used to center on
	 * the origin.
	 */
	public static final int TARGET_CODE_ORIGIN = 0;
	/**
	 * Value of {@link #targetCode} used to center on
	 * the pivot point for the current selection.
	 */
	public static final int TARGET_CODE_SELECTION = 1;
	/**
	 * Value of {@link #targetCode} used to center on
	 * the mouse cursor location.
	 */
	public static final int TARGET_CODE_CURSOR = 2;
	/**
	 * Value of {@link #targetCode} used to center on
	 * the point nearest to the mouse cursor location.
	 */
	public static final int TARGET_CODE_CURSOR_NEAREST_POINT = 3;
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * Single integer encoding which point to center the view on.
	 * <table>
	 * <tr>
	 *   <th>Value of {@link #targetCode}</th>
	 *   <th>Target name</th>
	 *   <th>Derived from</th>
	 * </tr>
	 * <tr>
	 *   <td>{@link #TARGET_CODE_ORIGIN}</td>
	 *   <td>Origin</td>
	 *   <td>Origin point</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #TARGET_CODE_SELECTION}</td>
	 *   <td>Selection</td>
	 *   <td>Pivot point based on selection and {@link GraphicalEditorPane#getPivotOnPoint(int)}</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #TARGET_CODE_CURSOR}</td>
	 *   <td>Cursor</td>
	 *   <td>Current mouse cursor location</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #TARGET_CODE_CURSOR_NEAREST_POINT}</td>
	 *   <td>Cursor nearest point</td>
	 *   <td>Curve point nearest to current mouse cursor location</td>
	 * </tr>
	 * </table>
	 */
	public int targetCode;

	/**
	 * Full constructor. Specifies parent and recenter target point.
	 * 
	 * @param graphicalEditor parent editor (optional)
	 * @param targetCode value of {@link #targetCode} to set
	 */
	public GraphicalEditorCommandViewCenter(GraphicalEditorPane graphicalEditor, int targetCode) {
		this.parent = graphicalEditor;
		this.targetCode = targetCode;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			GraphicalEditorPane graphicalEditor = getEditorParent();
			CurveBezierSpline curve = graphicalEditor.getEditorData();
			int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
			final int curvePointCount = curve.getPointCount();
			int lastMousex = graphicalEditor.lastMousex, lastMousey = graphicalEditor.lastMousey;
			double anchorx = graphicalEditor.anchorx, anchory = graphicalEditor.anchory,
					scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
					iscalex = 1d/scalex, iscaley = 1d/scaley;
			// change from center to corners
			double halfWorldWidth  = 0.5 *  width * iscalex;
			double halfWorldHeight = 0.5 * height * iscaley;
			anchorx -= halfWorldWidth;// x of left edge
			anchory += halfWorldHeight;// y of top edge
			// Get bounds
			double wx = anchorx+lastMousex*iscalex, wy = anchory-lastMousey*iscaley;
			double rcx = 0, rcy = 0;
			switch(targetCode) {
			case TARGET_CODE_ORIGIN:break;
			case TARGET_CODE_SELECTION:{
				int pivotCode = graphicalEditor.parent.getPivotOn();
				pivotCode = Math.max(1, pivotCode);// default to first
				double[] pivotXY = graphicalEditor.getPivotOnPoint(pivotCode);
				rcx = pivotXY[0];
				rcy = pivotXY[1];
				break;
			}
			case TARGET_CODE_CURSOR:{
				rcx = wx;
				rcy = wy;
				break;
			}
			case TARGET_CODE_CURSOR_NEAREST_POINT:{
				if(curvePointCount == 0)break;// need at least 1 point
				int pindex = curve.getPointNearest(wx);
				rcx = curve.getPointX(pindex);
				rcy = curve.getPointY(pindex);
				break;
			}
			}
			graphicalEditor.anchorx = rcx;
			graphicalEditor.anchory = rcy;
		}
		return true;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

}
