package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.Graphics2D;
import org.apache.pivot.wtk.ComponentKeyListener;
import org.apache.pivot.wtk.ComponentMouseButtonListener;
import org.apache.pivot.wtk.ComponentMouseListener;
import org.apache.pivot.wtk.ComponentMouseWheelListener;
import org.apache.pivot.wtk.Visual;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.DataEditor;

/**
 * Interface for commands in the {@link GraphicalEditorPane}
 * which is used by the {@link CurveBezierSplineEditor}.
 * Most skin things are delegated to the current command in some way.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public interface GraphicalEditorCommand extends
		ComponentMouseListener,
		ComponentMouseButtonListener,
		ComponentMouseWheelListener,
		ComponentKeyListener,
		DataEditor.Instance<CurveBezierSpline, GraphicalEditorPane> {
	
	/**
	 * Tell this command to finalize itself.
	 * <br><br>
	 * Outside users should call {@link GraphicalEditorPane#setCommand(GraphicalEditorCommand, boolean)}
	 * instead, since that method may call this method and handle the rest.
	 * 
	 * @param confirm true to confirm the command, false to cancel it
	 * @return true on success, false if the request was rejected
	 */
	public boolean finalize(boolean confirm);
	
	/**
	 * Optional behaviour when a command is initialized or revived.
	 * 
	 * @param previous the command that was there before, optional
	 */
	public default void reinit(GraphicalEditorCommand previous){};
	
	/**
	 * This will be called by {@link GraphicalEditorPane} to render the UI
	 * and anything else in the area.
	 * <br><br>
	 * This is meant to follow {@link Visual#paint(Graphics2D)},
	 * which is a far up super-interface of skins.
	 * It does not take or produce more advanced information such
	 * as layers/depth.
	 * 
	 * @param graphics graphics rendering context
	 */
	public void paint(Graphics2D graphics);
	
	@Override
	public default CurveBezierSpline getEditorData() {
		return getEditorParent().getEditorData();
	}
	
	/**
	 * Is this an instant command? Instant commands are characterized by being
	 * one time and not requiring further user interaction (such as copy to clipboard),
	 * as opposed to long lived commands which interact more with the user
	 * and have complex behavior (such as box selection). Any command
	 * can return true when this is called to ask to be treated like an instant command,
	 * or false to be treated like a long lived command.
	 * 
	 * @see GraphicalEditorCommandInstant
	 * 
	 * @return true if this is a one off instant command,
	 * false if it is a long lived command with further user interaction
	 */
	public default boolean isInstantCommand() {
		return false;
	}

}
