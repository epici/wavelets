package org.epici.wavelets.ui.curve.bezierspline;

import java.util.Collection;
import org.epici.wavelets.core.Session;
import org.apache.pivot.wtk.Component;

/**
 * Represents an instant command which copies all currently selected
 * points to the clipboard with {@link Session#setClipBoard(Object)}.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandCopy implements GraphicalEditorCommandInstant {
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * [X, Y] arrays for all points that will be copied.
	 */
	public Collection<double[]> points;

	/**
	 * Full constructor. Specifies parent and points to be copied.
	 * 
	 * @param graphicalEditor parent editor (optional)
	 * @param points [X, Y] arrays to be copied
	 */
	public GraphicalEditorCommandCopy(GraphicalEditorPane graphicalEditor, Collection<double[]> points) {
		this.parent = graphicalEditor;
		this.points = points;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			GraphicalEditorPane graphicalEditor = getEditorParent();
			graphicalEditor.getSession().setClipBoard(points);
		}
		return true;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

}
