package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import javax.swing.JInternalFrame;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.ui.WindowManager;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * Represents a search command. In this command, the user searches
 * for a command by name in a pop-up window.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandSearch implements GraphicalEditorCommand {
	
	public static final String FRAME_TITLE = "Command Search";

	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	/**
	 * The pop-up window.
	 */
	public GraphicalEditorSearchWindow window;
	
	/**
	 * Constructor supplying parent.
	 * 
	 * @param parent parent editor (optional)
	 */
	public GraphicalEditorCommandSearch(GraphicalEditorPane parent) {
		this.parent = parent;
	}
	
	/**
	 * Cleans up and destroys any existing windows,
	 * without causing commands to change.
	 * If no window exists currently, does nothing.
	 */
	public void searchWindowDestroy() {
		// do nothing if window already does not exist
		if(window == null)return;
		// let delegation do the work
		window.searchWindowDestroy();
		// original window reference no longer needed
		window = null;
	}
	
	private static final double FRAME_SIZE_MULT_X = 1d/8;
	private static final double FRAME_SIZE_MULT_Y = 1d/4;
	/**
	 * Creates the search dialog pop-up. If any exist already, closes them.
	 * Opens the newly created window and transfers
	 * focus to the search bar in it.
	 */
	public void searchWindowCreate() {
		// get rid of any existing window
		searchWindowDestroy();
		// fetch some useful data
		GraphicalEditorPane graphicalEditor = getEditorParent();
		Session session = graphicalEditor.getSession();
		JInternalFrame frameBase = session.windowManager.getWindow(WindowManager.NAME_CURVE_EDITOR).component;
		Point frameBasePos = frameBase.getLocation();
		Dimension frameBaseSize = frameBase.getSize();
		// do coordinate and sizing calculations
		final double fbx = frameBasePos.getX(),
				fby = frameBasePos.getY(),
				fblx = frameBaseSize.getWidth(),
				fbly = frameBaseSize.getHeight(),
				fbcx = fbx + fblx * 0.5,
				fbcy = fby + fbly * 0.5,
				fbrx = fblx * FRAME_SIZE_MULT_X,
				fbry = fbly * FRAME_SIZE_MULT_Y;
		// load from BXML
		window = GraphicalEditorSearchWindow.createNew();
		// set fields
		window.command = this;
		// make wrapping frame
		JInternalFrame wrapFrame = window.wrapFrame = PivotSwingUtils.wrapPivotWindow(window);
		// now we can initialize
		window.init();
		// show the window in the correct location
		session.desktopPane.add(wrapFrame);
		wrapFrame.setTitle(FRAME_TITLE);
		PivotSwingUtils.showFrame(
				wrapFrame,
				new int[]{
						(int)(fbcx - fbrx),
						(int)(fbcy - fbry)
				},
				new int[]{
						(int)(fbrx * 2),
						(int)(fbry * 2)
				}
				);
		// transfer focus to search bar
		window.searchInput.requestFocus();
	}
	
	@Override
	public void reinit(GraphicalEditorCommand previous){
		GraphicalEditorCommand.super.reinit(previous);
		searchWindowCreate();
	}

	@Override
	public boolean mouseMove(Component component, int x, int y) {
		return false;
	}

	@Override
	public void mouseOver(Component component) {
	}

	@Override
	public void mouseOut(Component component) {
	}

	@Override
	public boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.requestFocus();
		return false;
	}

	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		return false;
	}

	@Override
	public boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return false;
	}

	@Override
	public boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		return false;
	}

	@Override
	public boolean keyTyped(Component component, char character) {
		return false;
	}

	@Override
	public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

	@Override
	public boolean finalize(boolean confirm) {
		/*
		 * The creation of the new command has to occur
		 * after this call, so it cannot occur here.
		 */
		return true;
	}

	@Override
	public void paint(Graphics2D graphics) {
		// --- Regular drawing delegated to empty command ---
		parent.commandNone.paint(graphics);
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		Session session = graphicalEditor.getSession();
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		// --- Pre-draw ---
		Color colorCover, colorCoverAlpha;
		ColorScheme colors = Session.getColors(session);
		colorCover = colors.gradient;
		colorCoverAlpha = ColorScheme.setAlpha(colorCover, 85);
		// draw overlay
		graphics.setColor(colorCoverAlpha);
		graphics.fillRect(0, 0, width, height);
	}

}
