package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.BitSet;

import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Dimensions;
import org.apache.pivot.wtk.Mouse;
import org.apache.pivot.wtk.skin.FillPaneSkin;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Preferences;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.util.math.Floats;
import org.epici.wavelets.util.ui.Draw;

public class CurveBezierSplinePreviewSkin extends FillPaneSkin implements KeyTracker {
	
	/**
	 * What the preferred height will be by default.
	 */
	public static final int DEFAULT_HEIGHT = 50;
	/**
	 * What the preferred width will be by default.
	 */
	public static final int DEFAULT_WIDTH = 150;
	/**
	 * Width/height ratio used to calculate preferred dimension given the other.
	 */
	public static final double DEFAULT_SIZE_RATIO = (double)DEFAULT_WIDTH/DEFAULT_HEIGHT;
	
	/**
	 * Which keys are held down?
	 */
	protected BitSet keys = new BitSet();

	/**
	 * Blank constructor.
	 */
	public CurveBezierSplinePreviewSkin() {
		
	}
	
	@Override
	public BitSet getKeysDown() {
		return keys;
	}
	
	@Override
	public void mouseOut(Component component) {
	}
	
	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		component.requestFocus();
		return false;
	}
	
	/**
	 * Is this focusable?
	 * 
	 * @return {@code true}, because it's a preview
	 */
	@Override
    public boolean isFocusable() {
        return true;
    }

	@Override
	public void layout() {
	}
	
	/**
	 * Returns the visual's unconstrained preferred size.
	 * <br>
	 * Uses the same implementation as
	 * {@link org.apache.pivot.wtk.skin.ComponentSkin#getPreferredSize()},
	 * that is, it calls
	 * {@link #getPreferredWidth(int)} with argument {@code -1}
	 * and
	 * {@link #getPreferredHeight(int)} with argument {@code -1}
	 * and combines them into a {@link Dimensions}.
	 */
	@Override
	public Dimensions getPreferredSize() {
		return new Dimensions(getPreferredWidth(-1),getPreferredHeight(-1));
	}

	@Override
	public int getPreferredHeight(int width) {
		if(width<=0)return DEFAULT_HEIGHT;
		return (int)Math.round(width*(1d/DEFAULT_SIZE_RATIO));
	}

	@Override
	public int getPreferredWidth(int height) {
		if(height<=0)return DEFAULT_WIDTH;
		return (int)Math.round(height*DEFAULT_SIZE_RATIO);
	}
	
	@Override
	public void paint(Graphics2D graphics){
		// --- Fetch some data ---
		CurveBezierSplinePreview graphicalEditor = (CurveBezierSplinePreview) getComponent();
		Session session = graphicalEditor.getSession();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		final double diagonal = Math.hypot(width, height), logDiagonal = Math.log(diagonal);
		double gridDiagonalExponentMin = Preferences.getFloatSafe(session, Preferences.INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MIN);
		double gridDiagonalExponentMax = Preferences.getFloatSafe(session, Preferences.INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MAX);
		// quietly fix mistakes, ensure min < max
		double[] gridDiagonalExponentFix = {
				0,
				Floats.D_EPSILON,
				gridDiagonalExponentMin,
				gridDiagonalExponentMax,
				1-Floats.D_EPSILON,
				1
		};
		Arrays.sort(gridDiagonalExponentFix);
		gridDiagonalExponentMin = gridDiagonalExponentFix[2];
		gridDiagonalExponentMax = gridDiagonalExponentFix[3];
		// Get curve point bounds
		double curveXmin = curve.getStreamX().min().orElse(-1);
		double curveXmax = curve.getStreamX().max().orElse(1);
		double curveYmin = curve.getStreamY().min().orElse(-1);
		double curveYmax = curve.getStreamY().max().orElse(1);
		// Local variables are faster + inversion
		double anchorx = (curveXmin+curveXmax)*0.5, anchory = (curveYmin+curveYmax)*0.5,
				scalex = width/(curveXmax-curveXmin), scaley = height/(curveYmax-curveYmin),
				iscalex = 1d/scalex, iscaley = 1d/scaley,
				logscalex = Math.log(scalex), logscaley = Math.log(scaley),
				cornerx, cornery;
		// change from center to corners
		double halfWorldWidth  = 0.5 *  width * iscalex;
		double halfWorldHeight = 0.5 * height * iscaley;
		cornerx = anchorx + halfWorldWidth;// x of right edge
		cornery = anchory - halfWorldHeight;// y of bottom edge
		anchorx -= halfWorldWidth;// x of left edge
		anchory += halfWorldHeight;// y of top edge
		// get screen position of origin
		double originx = -anchorx*scalex, originy = anchory*scaley;
		// Get bounds
		final int curvePointCount = curve.getPointCount();
		int pfirst = curve.getPointInterval(anchorx), plast = curve.getPointInterval(cornerx);// Indices of points visible, first inclusive, last exclusive
		pfirst = Math.max(pfirst-2, 0);
		plast = Math.min(plast+2, curvePointCount);
		boolean pendfirst = pfirst == 0, pendlast = plast == curvePointCount;
		int prange = plast-pfirst;
		// Get curve points
		double[] pxs = new double[prange];
		double[] pys = new double[prange];
		curve.getPointsX(pxs, 0, pfirst, plast);
		curve.getPointsY(pys, 0, pfirst, plast);
		// --- Pre-draw ---
		Color colorBackground, colorBackgroundDark, colorLine, colorSelect, colorSelectFirst, colorCurve, colorPivot, colorWarn, colorText, colorTextAlpha;
		ColorScheme colors = Session.getColors(session);
		colorBackground = colors.background;
		colorLine = colors.line;
		colorSelect = colors.selected;
		colorCurve = colors.highlight;
		colorPivot = colors.marker;
		colorWarn = colors.warning;
		colorText = colors.text;
		colorBackgroundDark = ColorScheme.brighten(colorBackground, -0.1f);
		colorSelectFirst = ColorScheme.brighten(colorSelect, 0.2f);
		colorTextAlpha = ColorScheme.setAlpha(colorText, 170);
		// rendering hints
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// make strokes
		BasicStroke thinStroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		BasicStroke curveStroke = new BasicStroke((float)GraphicalEditorCommandNone.CURVE_WIDTH, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		// --- Draw ---
		// prepare rotated graphics
		Graphics2D graphicsRotated = (Graphics2D) graphics.create();
		AffineTransform at = graphicsRotated.getTransform();
		at.translate(0, height);
		at.quadrantRotate(-1);
		graphicsRotated.setTransform(at);
		// fill background
		graphics.setPaint(Draw.gradientVertical(colorBackground, colorBackgroundDark, 0, height));
		graphics.fillRect(0, 0, width, height);
		// prepare to make grid lines
		final double logGridSizeMin = logDiagonal * gridDiagonalExponentMin,
				logGridSizeMax = logDiagonal * gridDiagonalExponentMax,
				invLogGridSizeDiff = 1d/(logGridSizeMax - logGridSizeMin);
		Color transparentLine = ColorScheme.setAlpha(colorLine, 0);
		int gridExpOf10, gridExpOf10Verti, gridExpOf10Horiz;
		double logGridSize, gridSize, invGridSize, gridAlpha, gradientStart;
		BigDecimal worldPos, worldGridSize;
		boolean breakAtNext;
		graphics.setStroke(thinStroke);
		// make vertical grid lines
		for(gridExpOf10=(int)Math.floor(Math.log10(iscalex)),breakAtNext=false;!breakAtNext;gridExpOf10++) {
			logGridSize = gridExpOf10*Floats.LOG10+logscalex;
			gridAlpha = Floats.median(0, (logGridSize - logGridSizeMin)*invLogGridSizeDiff, 1);
			if(gridAlpha<=0)continue;
			if(gridAlpha>=1)breakAtNext = true;
			gridSize = Math.exp(logGridSize);
			invGridSize = 1d/gridSize;
			gradientStart = originx % gridSize;
			Color translucentLine = ColorScheme.setAlpha(colorLine, (int)Math.round(255*gridAlpha));
			LinearGradientPaint paint = new LinearGradientPaint(
					(float)gradientStart,
					0,
					(float)(gradientStart+gridSize),
					0,
					new float[]{0,(float)invGridSize,(float)(invGridSize+Floats.F_EPSILON),1},
					new Color[]{translucentLine,translucentLine,transparentLine,transparentLine},
					MultipleGradientPaint.CycleMethod.REPEAT
					);
			graphics.setPaint(paint);
			graphics.fillRect(0, 0, width, height);
		}
		gridExpOf10Verti = gridExpOf10 - 2;
		// make horizontal grid lines
		for(gridExpOf10=(int)Math.floor(Math.log10(iscaley)),breakAtNext=false;!breakAtNext;gridExpOf10++) {
			logGridSize = gridExpOf10*Floats.LOG10+logscaley;
			gridAlpha = Floats.median(0, (logGridSize - logGridSizeMin)*invLogGridSizeDiff, 1);
			if(gridAlpha<=0)continue;
			if(gridAlpha>=1)breakAtNext = true;
			gridSize = Math.exp(logGridSize);
			invGridSize = 1d/gridSize;
			gradientStart = originy % gridSize;
			Color translucentLine = ColorScheme.setAlpha(colorLine, (int)Math.round(255*gridAlpha));
			LinearGradientPaint paint = new LinearGradientPaint(
					0,
					(float)gradientStart,
					0,
					(float)(gradientStart+gridSize),
					new float[]{0,(float)invGridSize,(float)(invGridSize+Floats.F_EPSILON),1},
					new Color[]{translucentLine,translucentLine,transparentLine,transparentLine},
					MultipleGradientPaint.CycleMethod.REPEAT
					);
			graphics.setPaint(paint);
			graphics.fillRect(0, 0, width, height);
		}
		gridExpOf10Horiz = gridExpOf10 - 2;
		// draw the curve
		graphics.setColor(colorCurve);
		graphics.setStroke(curveStroke);
		if(curvePointCount <= 1) {// 0 point/1 point -> constant 0 or point
			final double worldConstantY = curvePointCount==0?0:curve.getPointY(0);
			final double constantY = (-worldConstantY+anchory)*scaley;
			graphics.draw(new Line2D.Double(0, constantY, width, constantY));
		} else {// at least 2 points -> can curve
			Path2D.Double curvePath = new Path2D.Double();
			// draw the first line segment, or nothing
			if(pendfirst) {// need to extend first
				double x3=pxs[0],
						y3=pys[0],
						x4=pxs[1],
						y4=pys[1];
				double sx3 = (x3-anchorx)*scalex,
						sy3 =-(y3-anchory)*scaley;
				double sd = -(y4-y3)/(x4-x3)*scaley*iscalex;
				double sx2 = 0,
						sy2 = sy3+(sx2-sx3)*sd;
				curvePath.moveTo(
						sx2,
						sy2
						);
				curvePath.lineTo(
						sx3,
						sy3
						);
			} else {// nothing to extend
				double x2=pxs[1],
						y2=pys[1];
				double sx2 = (x2-anchorx)*scalex,
						sy2 =-(y2-anchory)*scaley;
				curvePath.moveTo(
						sx2,
						sy2
						);
			}
			// draw the main curve segments
			int ipfirst = pfirst+(pendfirst?1:2);
			int iplast = plast-1;// change to inclusive bound
			for(int i3=ipfirst;i3<=iplast;i3++) {
				int i2 = i3 - 1;
				double x3=pxs[i3-pfirst],
						y3=pys[i3-pfirst],
						x2=pxs[i2-pfirst],
						y2=pys[i2-pfirst];
				double x1,y1,x4,y4;
				if(i2==0) {// first point -> use itself and next
					x1 = x2;
					y1 = y2;
				} else {// not first point -> use previous and next
					int i1 = i2-1;
					x1 = pxs[i1-pfirst];
					y1 = pys[i1-pfirst];
				}
				if(i3>=iplast) {// last point -> use previous and itself
					x4 = x3;
					y4 = y3;
				} else {// not last point -> use previous and next
					int i4 = i3+1;
					x4 = pxs[i4-pfirst];
					y4 = pys[i4-pfirst];
				}
				double dx=(x3-x2)*(1d/3),
						d2=(y3-y1)/(x3-x1)*dx,
						d3=(y4-y2)/(x4-x2)*dx;
				double sx2 = (x2-anchorx)*scalex,
						sy2 =-(y2-anchory)*scaley,
						sx3 = (x3-anchorx)*scalex,
						sy3 =-(y3-anchory)*scaley,
						sdx = dx*scalex,
						sd2 =-d2*scaley,
						sd3 =-d3*scaley;
				curvePath.curveTo(
						sx2+sdx,
						sy2+sd2,
						sx3-sdx,
						sy3-sd3,
						sx3,
						sy3
						);
			}
			// draw the last line segment, or nothing
			if(pendlast) {// need to extend last
				double x1=pxs[prange-2],
						y1=pys[prange-2],
						x2=pxs[prange-1],
						y2=pys[prange-1];
				double sx2 = (x2-anchorx)*scalex,
						sy2 =-(y2-anchory)*scaley;
				double sd = -(y2-y1)/(x2-x1)*scaley*iscalex;
				double sx3 = width,
						sy3 = sy2+(sx3-sx2)*sd;
				curvePath.lineTo(
						sx3,
						sy3
						);
			}
			// finally, draw the path
			graphics.draw(curvePath);
		}
		// set the stroke back for the next user
		graphics.setStroke(curveStroke);
	}

}
