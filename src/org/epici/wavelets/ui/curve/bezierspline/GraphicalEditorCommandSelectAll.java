package org.epici.wavelets.ui.curve.bezierspline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.util.Any;
import org.apache.commons.collections4.list.SetUniqueList;
import org.apache.pivot.wtk.Component;

/**
 * Represents an instant command which selects all points,
 * or does some other selection operation, based on
 * the value of {@link #bitOperator}.
 * Priority is based on distance to pivot point in screen space.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class GraphicalEditorCommandSelectAll implements GraphicalEditorCommandInstant {
	
	/**
	 * The value of {@link #bitOperator} which would correspond to
	 * <i>Toggle</i>
	 * which corresponds to the
	 * <a href="https://en.wikipedia.org/wiki/Boolean_algebras_canonically_defined#Truth_tables">finitary boolean operator</a>
	 * <i><sup>2</sup>f<sub>3</sub><i>
	 * .
	 * <br>
	 * In this case, it causes all existing points to be deselected,
	 * or if none were already selected, all to be selected.
	 */
	public static final int BIT_OPERATOR_TOGGLE = 0;
	/**
	 * The value of {@link #bitOperator} which would correspond to
	 * <i>Invert</i>
	 * which corresponds to the
	 * <a href="https://en.wikipedia.org/wiki/Boolean_algebras_canonically_defined#Truth_tables">finitary boolean operator</a>
	 * <i><sup>2</sup>f<sub>5</sub><i>
	 * .
	 * <br>
	 * In this case, it causes all existing points to be deselected,
	 * and all deselected points to be selected.
	 */
	public static final int BIT_OPERATOR_INVERT = 1;
	
	/**
	 * How the existing selection will be modified.
	 * It is modeled as a bit operator since we can consider the transformation
	 * as a function on [is it selected already?] and [are any selected already?].
	 * <br>
	 * Let A := selected already,
	 * B := any selected already,
	 * C := would be selected after operation;
	 * <table>
	 * <tr>
	 *   <th>Value of {@link #bitOperator}</th>
	 *   <th>Operation name</th>
	 *   <th>Equation</th>
	 * </tr>
	 * <tr>
	 *   <td>{@link #BIT_OPERATOR_TOGGLE}</td>
	 *   <td>Toggle</td>
	 *   <td>C = &#xac; B</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #BIT_OPERATOR_INVERT}</td>
	 *   <td>Invert</td>
	 *   <td>C = &#xac; A</td>
	 * </tr>
	 * </table>
	 */
	public int bitOperator;
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;

	/**
	 * Full constructor. Specifies parent and points to be copied.
	 * 
	 * @param graphicalEditor parent editor (optional)
	 * @param bitOperator value to set {@link #bitOperator} to
	 */
	public GraphicalEditorCommandSelectAll(GraphicalEditorPane graphicalEditor, int bitOperator) {
		this.parent = graphicalEditor;
		this.bitOperator = bitOperator;
	}
	
	/**
	 * Apply the selection. Result of {@link #finalize(boolean)}.
	 */
	public void applySelection() {
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		final int curvePointCount = curve.getPointCount();
		// if no points, we're done
		if(curvePointCount==0)return;
		// --- Apply the selection change ---
		SetUniqueList<Double> selection = graphicalEditor.selection;
		boolean anySelected = !selection.isEmpty();
		switch(bitOperator) {
		case BIT_OPERATOR_TOGGLE:{
			if (anySelected) {
				selection.clear();
			} else {
				SetUniqueList<Double> selectionTarget = getSelectionTarget();
				selection.addAll(selectionTarget);
			}
			break;
		}
		case BIT_OPERATOR_INVERT:{
			SetUniqueList<Double> selectionTarget = getSelectionTarget();
			selectionTarget.removeAll(selection);
			selection.clear();
			selection.addAll(selectionTarget);
			break;
		}
		}
	}
	
	/**
	 * Get the x values of all the points, ordered by distance
	 * from the pivot point. Referred to here as the selection target.
	 * 
	 * @return selection target as list
	 */
	public SetUniqueList<Double> getSelectionTarget() {
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		final int curvePointCount = curve.getPointCount();
		// Local variables are faster + inversion
		double scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
				/* iscalex = 1d/scalex, */ iscaley = 1d/scaley,
				/* scaleRatio = scaley*iscalex, */
				iscaleRatio = iscaley*scalex;
		// --- Create the selection target ---
		SetUniqueList<Double> selectionTarget = SetUniqueList.setUniqueList(new ArrayList<>());
		// key x values by distance to pivot point
		int pivotCode = graphicalEditor.getEditorParent().getPivotOn();
		pivotCode = Math.max(1, pivotCode);
		double[] pivotxy = graphicalEditor.getPivotOnPoint(pivotCode);
		double pivotx = pivotxy[0], pivoty = pivotxy[1];
		ArrayList<Any.Keyed<Double, Double>> distances = new ArrayList<>();
		for(int pindex=0;pindex<curvePointCount;pindex++) {
			double py = curve.getPointY(pindex);
			double px = curve.getPointX(pindex);
			double pxd = Math.abs(px - pivotx);
			double pyd = Math.abs(py - pivoty)*iscaleRatio;
			double pdistance = Math.hypot(pxd, pyd);
			distances.add(new Any.Keyed<>(pdistance, px));
		}
		distances.sort(Comparator.comparing(Any.Keyed::getKeyOf));
		for(Any.Keyed<Double, Double> keyed:distances) {
			selectionTarget.add(keyed.value);
		}
		return selectionTarget;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			applySelection();
		}
		return true;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

}
