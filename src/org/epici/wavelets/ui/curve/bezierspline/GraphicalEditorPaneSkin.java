package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.Graphics2D;
import java.util.BitSet;
import org.apache.pivot.wtk.ComponentKeyListener;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;
import org.apache.pivot.wtk.skin.FillPaneSkin;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.ui.SessionLinked;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * Skin for the {@link GraphicalEditorPane}. As per
 * Apache Pivot standards, it handles painting and events.
 * All of this is actually delegated to the {@link GraphicalEditorPane#command}.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorPaneSkin extends FillPaneSkin implements KeyTracker, SessionLinked, DataEditor.Instance<CurveBezierSpline, GraphicalEditorPane> {

	public GraphicalEditorPaneSkin() {
		super();
	}

	@Override
	public CurveBezierSpline getEditorData() {
		return getEditorParent().view;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return (GraphicalEditorPane) getComponent();
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		install(newParent);
		return true;
	}

	@Override
	public Session getSession() {
		return getEditorParent().getSession();
	}

	@Override
	public BitSet getKeysDown() {
		return getEditorParent().getKeysDown();
	}

	/**
	 * Convenience method to get the command.
	 * Useful because most things delegate to this.
	 * 
	 * @return the command, which things delegate to
	 */
	protected GraphicalEditorCommand getCommand() {
		return getEditorParent().command;
	}
	
	@Override
    public boolean isFocusable() {
        return true;
    }

	// Below here is full of delegates!

	@Override
	public boolean mouseMove(Component component, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.lastMousex = x;
		graphicalEditor.lastMousey = y;
		return getCommand().mouseMove(component, x, y)
				|| super.mouseMove(component, x, y);
	}

	@Override
	public void mouseOver(Component component) {
		getCommand().mouseOver(component);
	}

	@Override
	public void mouseOut(Component component) {
		getCommand().mouseOut(component);
	}

	@Override
	public boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.mouseDown = PivotSwingUtils.getMouseButtonCode(button);
		return getCommand().mouseDown(component, button, x, y)
				|| super.mouseDown(component, button, x, y);
	}

	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.mouseDown = 0;
		return getCommand().mouseUp(component, button, x, y)
				|| super.mouseUp(component, button, x, y);
	}

	@Override
	public boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return getCommand().mouseClick(component, button, x, y, count)
				|| super.mouseClick(component, button, x, y, count);
	}

	@Override
	public boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		return getCommand().mouseWheel(component, scrollType, scrollAmount, wheelRotation, x, y)
				|| super.mouseWheel(component, scrollType, scrollAmount, wheelRotation, x, y);
	}

	@Override
	public boolean keyTyped(Component component, char character) {
		return getCommand().keyTyped(component, character)
				|| super.keyTyped(component, character);
	}

	/**
	 * Updates the {@link #getKeysDown()} set, then
	 * delegates to the {@link #getCommand()}.
	 * If the delegate does not want to consume the event,
	 * passes on to the superclass ({@link FillPaneSkin}).
	 * Returns true if either want to consume the event,
	 * false if they both pass it on.
	 * <br>
	 * Details for when this is called and how to use the return value:
	 * {@link ComponentKeyListener#keyPressed(Component, int, org.apache.pivot.wtk.Keyboard.KeyLocation)}
	 */
	@Override
	public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		getKeysDown().set(keyCode);
		return getCommand().keyPressed(component, keyCode, keyLocation)
				|| super.keyPressed(component, keyCode, keyLocation);
	}

	/**
	 * Updates the {@link #getKeysDown()} set, then
	 * delegates to the {@link #getCommand()}.
	 * If the delegate does not want to consume the event,
	 * passes on to the superclass ({@link FillPaneSkin}).
	 * Returns true if either want to consume the event,
	 * false if they both pass it on.
	 * <br>
	 * Details for when this is called and how to use the return value:
	 * {@link ComponentKeyListener#keyReleased(Component, int, org.apache.pivot.wtk.Keyboard.KeyLocation)}
	 */
	@Override
	public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		getKeysDown().clear(keyCode);
		return getCommand().keyReleased(component, keyCode, keyLocation)
				|| super.keyReleased(component, keyCode, keyLocation);
	}

	@Override
	public void paint(Graphics2D graphics) {
		getCommand().paint(graphics);
	}

}
