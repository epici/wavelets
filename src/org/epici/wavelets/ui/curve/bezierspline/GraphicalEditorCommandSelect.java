package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.util.Any;
import org.epici.wavelets.util.ds.ConcatenatedIterator;
import org.epici.wavelets.util.ds.ZipUngroupedIterator;
import org.apache.commons.collections4.list.SetUniqueList;

/**
 * A command to select near a point, or select inside a box.
 * Supports different behaviors according to {@link #bitOperator}.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandSelect implements GraphicalEditorCommand {
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * The value of {@link #bitOperator} which would correspond to
	 * <i>Set</i>
	 * which corresponds to the
	 * <a href="https://en.wikipedia.org/wiki/Boolean_algebras_canonically_defined#Truth_tables">finitary boolean operator</a>
	 * <i><sup>2</sup>f<sub>12</sub><i>
	 * .
	 */
	public static final int BIT_OPERATOR_SET = 0;
	/**
	 * The value of {@link #bitOperator} which would correspond to
	 * <i>Union</i>
	 * which corresponds to the
	 * <a href="https://en.wikipedia.org/wiki/Boolean_algebras_canonically_defined#Truth_tables">finitary boolean operator</a>
	 * <i><sup>2</sup>f<sub>14</sub><i>
	 * .
	 */
	public static final int BIT_OPERATOR_UNION = 1;
	/**
	 * The value of {@link #bitOperator} which would correspond to
	 * <i>Difference</i>
	 * which corresponds to the
	 * <a href="https://en.wikipedia.org/wiki/Boolean_algebras_canonically_defined#Truth_tables">finitary boolean operator</a>
	 * <i><sup>2</sup>f<sub>2</sub><i>
	 * .
	 */
	public static final int BIT_OPERATOR_DIFFERENCE = 2;
	/**
	 * The value of {@link #bitOperator} which would correspond to
	 * <i>Intersect</i>
	 * which corresponds to the
	 * <a href="https://en.wikipedia.org/wiki/Boolean_algebras_canonically_defined#Truth_tables">finitary boolean operator</a>
	 * <i><sup>2</sup>f<sub>8</sub><i>
	 * .
	 */
	public static final int BIT_OPERATOR_INTERSECT = 3;
	
	/**
	 * How the existing selection will be modified.
	 * It is modeled as a bit operator since we can consider the transformation
	 * as a function on [is it selected already?] and [is it in the selection box?].
	 * <br>
	 * Let A := selected already,
	 * B := in selection box,
	 * C := would be selected after operation;
	 * <table>
	 * <tr>
	 *   <th>Value of {@link #bitOperator}</th>
	 *   <th>Operation name</th>
	 *   <th>Equation</th>
	 * </tr>
	 * <tr>
	 *   <td>{@link #BIT_OPERATOR_SET}</td>
	 *   <td>Set</td>
	 *   <td>C = B</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #BIT_OPERATOR_UNION}</td>
	 *   <td>Union</td>
	 *   <td>C = A &#x2228; B</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #BIT_OPERATOR_DIFFERENCE}</td>
	 *   <td>Difference</td>
	 *   <td>C = A &#x2227; &#xac; B</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #BIT_OPERATOR_INTERSECT}</td>
	 *   <td>Intersect</td>
	 *   <td>C = A &#x2227; B</td>
	 * </tr>
	 * </table>
	 */
	public int bitOperator;
	
	/**
	 * Mouse button which was held down when this command was started
	 * (0 = not pressed, 1 = left, 2 = right, 3 = middle)
	 */
	public int initialMouseDown;
	/**
	 * The mouse x, where it was last seen
	 */
	public int lastMousex;
	/**
	 * The mouse y, where it was last seen
	 */
	public int lastMousey;
	/**
	 * The mouse x where dragging began
	 */
	public int originMousex;
	/**
	 * The mouse y where dragging began
	 */
	public int originMousey;
	
	/**
	 * The x values of the points in the selection box.
	 * <br>
	 * If the mouse is not dragged according to
	 * {@link #getMouseDragged()}, contains a single point closest to the mouse instead.
	 */
	public SetUniqueList<Double> selectionTarget = SetUniqueList.setUniqueList(new ArrayList<>());
	
	/**
	 * Constructor supplying parent and points.
	 * 
	 * @param parent parent editor (optional)
	 * @param bitOperator the value to give to {@link #bitOperator}.
	 */
	public GraphicalEditorCommandSelect(GraphicalEditorPane parent,int bitOperator) {
		this.parent = parent;
		this.bitOperator = bitOperator;
	}
	
	/**
	 * Has the mouse been dragged?
	 * <br><br>
	 * It makes some assumptions:
	 * <ul>
	 * <li>The mouse is down as long as the command is active</li>
	 * <li>Once dragged, the mouse never returns to its original location</li>
	 * <ul>
	 * Normally these would be considered erros, however, in the context of
	 * this command, it is a useful flexibility.
	 * 
	 * @return if the mouse has been dragged
	 */
	public boolean getMouseDragged() {
		return lastMousex != originMousex || lastMousey != originMousey;
	}
	
	@Override
	public void reinit(GraphicalEditorCommand previous) {
		GraphicalEditorCommand.super.reinit(previous);
		GraphicalEditorPane graphicalEditor = previous.getEditorParent();
		copyFrom(graphicalEditor.commandNone);
	}
	
	/**
	 * Copy available information from another command.
	 * 
	 * @param source command to copy from
	 */
	public void copyFrom(GraphicalEditorCommandNone source) {
		initialMouseDown = source.mouseDown;
		lastMousex = originMousex = source.lastMousex;
		lastMousey = originMousey = source.lastMousey;
		recalculateSelectionTarget();
	}
	
	/**
	 * Apply the selection. Result of {@link #finalize(boolean)}.
	 */
	public void applySelection() {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		SetUniqueList<Double> selection = graphicalEditor.selection;
		SetUniqueList<Double> selectionTarget = this.selectionTarget;
		switch(bitOperator) {
		case BIT_OPERATOR_SET:{
			selection.clear();
			selection.addAll(selectionTarget);
			break;
		}
		case BIT_OPERATOR_UNION:{
			selection.addAll(selectionTarget);
			break;
		}
		case BIT_OPERATOR_DIFFERENCE:{
			selection.removeAll(selectionTarget);
			break;
		}
		case BIT_OPERATOR_INTERSECT:{
			selection.retainAll(selectionTarget);
			break;
		}
		}
	}
	
	/**
	 * Recalculate {@link #selectionTarget}.
	 */
	public void recalculateSelectionTarget() {
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		int originMousex = this.originMousex;
		int originMousey = this.originMousey;
		int lastMousex = this.lastMousex;
		int lastMousey = this.lastMousey;
		// Local variables are faster + inversion
		double anchorx = graphicalEditor.anchorx, anchory = graphicalEditor.anchory,
				scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
				iscalex = 1d/scalex, iscaley = 1d/scaley,
				scaleRatio = scaley*iscalex,
				iscaleRatio = iscaley*scalex;
		// change from center to corners
		double halfWorldWidth  = 0.5 *  width * iscalex;
		double halfWorldHeight = 0.5 * height * iscaley;
		anchorx -= halfWorldWidth;// x of left edge
		anchory += halfWorldHeight;// y of top edge
		// Get bounds
		double wx = anchorx+lastMousex*iscalex, wy = anchory-lastMousey*iscaley;
		double owx = anchorx+originMousex*iscalex, owy = anchory-originMousey*iscaley;
		final int curvePointCount = curve.getPointCount();
		// clear the existing set
		selectionTarget.clear();
		// if no points, we're done
		if(curvePointCount==0)return;
		if(getMouseDragged()) {// dragged -> use box
			double wx1 = Math.min(wx, owx),
					wx2 = Math.max(wx, owx),
					wy1 = Math.min(wy, owy),
					wy2 = Math.max(wy, owy);
			int pfirst = curve.getPointInterval(wx1), plast = curve.getPointInterval(wx2);
			// key x values by distance to mouse
			ArrayList<Any.Keyed<Double, Double>> distances = new ArrayList<>();
			for(int pindex=pfirst;pindex<plast;pindex++) {
				double py = curve.getPointY(pindex);
				if(wy1 <= py && py <= wy2) {
					double px = curve.getPointX(pindex);
					double pxd = Math.abs(px - owx);
					double pyd = Math.abs(py - owy)*iscaleRatio;
					double pdistance = Math.hypot(pxd, pyd);
					distances.add(new Any.Keyed<>(pdistance, px));
				}
			}
			distances.sort(Comparator.comparing(Any.Keyed::getKeyOf));
			for(Any.Keyed<Double, Double> keyed:distances) {
				selectionTarget.add(keyed.value);
			}
		} else {// not dragged -> use nearest
			int middle = curve.getPointInterval(wx);
			// iterate both ways to speed up search
			ConcatenatedIterator<Integer> iterForward = new ConcatenatedIterator<>(
					Collections.singleton(
							IntStream
							.range(middle, curvePointCount)
							.boxed()
							.iterator()
							),
					true
					);
			ConcatenatedIterator<Integer> iterBackward = new ConcatenatedIterator<>(
					Collections.singleton(
							IntStream
							.range(0, middle)
							.map((int index)->(middle-index-1))
							.boxed()
							.iterator()
							),
					true
					);
			ArrayList<Iterator<Integer>> iteratorList = new ArrayList<>();
			iteratorList.add(iterForward);
			iteratorList.add(iterBackward);
			ZipUngroupedIterator<Integer> iterator = new ZipUngroupedIterator<>(
					iteratorList,
					true
					);
			int bestIndex = 0;
			double bestDistance = Double.POSITIVE_INFINITY;
			merged:while(iterator.hasNext()) {
				int pindex = iterator.next();
				double px = curve.getPointX(pindex);
				double pxd = Math.abs(px - wx);
				if(pxd >= bestDistance) {// no more in this direction
					ConcatenatedIterator<Integer> lastIterator = (ConcatenatedIterator<Integer>) iterator.getLastIterator();
					lastIterator.interrupt();
					continue merged;
				}
				double py = curve.getPointY(pindex);
				double pyd = Math.abs(py - wy)*iscaleRatio;
				double pdistance = Math.hypot(pxd, pyd);
				if(pdistance < bestDistance) {
					bestIndex = pindex;
					bestDistance = pdistance;
				}
			}
			selectionTarget.add(curve.getPointX(bestIndex));
		}
	}
	
	@Override
	public boolean mouseMove(Component component, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		int lastMousex = this.lastMousex;
		int lastMousey = this.lastMousey;
		this.lastMousex = x;
		this.lastMousey = y;
		recalculateSelectionTarget();
		graphicalEditor.repaint();
		return true;
	}

	@Override
	public void mouseOver(Component component) {
	}

	@Override
	public void mouseOut(Component component) {
	}

	@Override
	public boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.requestFocus();
		return false;
	}

	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		boolean confirm = button == Mouse.Button.LEFT || initialMouseDown != 0;
		graphicalEditor.setCommand(null, confirm);
		graphicalEditor.repaint();
		return true;
	}

	@Override
	public boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return false;
	}

	@Override
	public boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		return false;
	}

	@Override
	public boolean keyTyped(Component component, char character) {
		return false;
	}

	@Override
	public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		boolean didSomething = false;
		GraphicalEditorPane graphicalEditor = getEditorParent();
		switch(keyCode) {
		case KeyEvent.VK_ESCAPE:{// Escape -> cancel
			graphicalEditor.setCommand(null, false);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_ENTER:{// Enter -> confirm
			graphicalEditor.setCommand(null, true);
			didSomething = true;
			break;
		}
		}
		if(didSomething) {
			graphicalEditor.repaint();
		}
		return didSomething;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			applySelection();
		}
		return true;
	}

	private static final double SELECTION_INDICATOR_RADIUS = 6;
	@Override
	public void paint(Graphics2D graphics) {
		// --- Regular drawing delegated to empty command ---
		parent.commandNone.paint(graphics);
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		Session session = graphicalEditor.getSession();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		// Local variables are faster + inversion
		int lastMousex = this.lastMousex,
				lastMousey = this.lastMousey,
				originMousex = this.originMousex,
				originMousey = this.originMousey;
		double anchorx = graphicalEditor.anchorx, anchory = graphicalEditor.anchory,
				scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
				iscalex = 1d/scalex, iscaley = 1d/scaley;
		boolean mouseDragged = getMouseDragged();
		SetUniqueList<Double> selectionTarget = this.selectionTarget;
		// change from center to corners
		double halfWorldWidth  = 0.5 *  width * iscalex;
		double halfWorldHeight = 0.5 * height * iscaley;
		anchorx -= halfWorldWidth;// x of left edge
		anchory += halfWorldHeight;// y of top edge
		// Get bounds
		int mx1 = Math.min(lastMousex, originMousex),
				mx2 = Math.max(lastMousex, originMousex),
						my1 = Math.min(lastMousey, originMousey),
						my2 = Math.max(lastMousey, originMousey);
		double wx = anchorx+lastMousex*iscalex,
				wy = anchory-lastMousey*iscaley,
				owx = anchorx+originMousex*iscalex,
				owy = anchory-originMousey*iscaley;
		double wx1 = Math.min(wx, owx),
				wx2 = Math.max(wx, owx),
				wy1 = Math.min(wy, owy),
				wy2 = Math.max(wy, owy);
		final int curvePointCount = curve.getPointCount(),
				selectionTargetCount = selectionTarget.size();
		// Cache points
		double[] selectionTargetX = new double[selectionTargetCount];
		double[] selectionTargetY = new double[selectionTargetCount];
		int i=0;
		for(double px:selectionTarget) {
			int pindex = curve.getPointNearest(px);
			selectionTargetX[i] = px;
			selectionTargetY[i] = curve.getPointY(pindex);
			i++;
		}
		// --- Pre-draw ---
		Color colorSelect, colorSelectFirst, colorSelectAlpha;
		ColorScheme colors = Session.getColors(session);
		colorSelect = colors.selected;
		colorSelectFirst = ColorScheme.brighten(colorSelect, 0.2f);
		colorSelectAlpha = ColorScheme.setAlpha(colorSelect, 85);
		// we don't set stroke, because empty command already did
		// selection box if mouse dragged
		if(mouseDragged) {
			graphics.setColor(colorSelectAlpha);
			graphics.fill(
					new Rectangle2D.Double(
							mx1,
							my1,
							mx2-mx1,
							my2-my1
							)
					);
		}
		// draw line to first
		if(selectionTargetCount > 0) {
			graphics.setColor(colorSelectFirst);
			graphics.draw(
					new Line2D.Double(
							originMousex,
							originMousey,
							 (selectionTargetX[0] - anchorx) * scalex,
							-(selectionTargetY[0] - anchory) * scaley
							)
					);
		}
		// draw indicators for all points
		graphics.setColor(colorSelect);
		for(i=0;i<selectionTargetCount;i++) {
			double px = selectionTargetX[i];
			double py = selectionTargetY[i];
			double wpx = (px - anchorx) * scalex;
			double wpy =-(py - anchory) * scaley;
			graphics.draw(
					new Line2D.Double(
							wpx - SELECTION_INDICATOR_RADIUS,
							wpy,
							wpx + SELECTION_INDICATOR_RADIUS,
							wpy
							)
					);
			graphics.draw(
					new Line2D.Double(
							wpx,
							wpy - SELECTION_INDICATOR_RADIUS,
							wpx,
							wpy + SELECTION_INDICATOR_RADIUS
							)
					);
		}
	}

}
