package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.geom.Line2D;
import org.apache.commons.collections4.list.SetUniqueList;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.util.ui.Draw;

public class GraphicalEditorCommandDelete implements GraphicalEditorCommand {
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * The mouse x, where it was last seen
	 */
	public int lastMousex;
	/**
	 * The mouse y, where it was last seen
	 */
	public int lastMousey;
	
	/**
	 * Constructor supplying parent.
	 * 
	 * @param parent parent editor (optional)
	 */
	public GraphicalEditorCommandDelete(GraphicalEditorPane parent) {
		this.parent = parent;
	}
	
	/**
	 * Apply the deletion. All currently selected points will be deleted.
	 */
	public void applyDeletion() {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		SetUniqueList<Double> selection = graphicalEditor.selection;
		// remove points from curve
		for(double px:selection) {
			int pindex = curve.getPointNearest(px);
			curve.removePoint(pindex);
		}
		// deleted points can't be selected
		selection.clear();
	}
	
	@Override
	public void reinit(GraphicalEditorCommand previous) {
		GraphicalEditorCommand.super.reinit(previous);
		GraphicalEditorPane graphicalEditor = previous.getEditorParent();
		copyFrom(graphicalEditor.commandNone);
	}
	
	/**
	 * Copy available information from another command.
	 * 
	 * @param source command to copy from
	 */
	public void copyFrom(GraphicalEditorCommandNone source) {
		lastMousex = source.lastMousex;
		lastMousey = source.lastMousey;
	}
	
	@Override
	public boolean mouseMove(Component component, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		int lastMousex = this.lastMousex;
		int lastMousey = this.lastMousey;
		this.lastMousex = x;
		this.lastMousey = y;
		graphicalEditor.repaint();
		return true;
	}

	@Override
	public void mouseOver(Component component) {
	}

	@Override
	public void mouseOut(Component component) {
	}

	@Override
	public boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.requestFocus();
		return false;
	}

	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		boolean confirm = button == Mouse.Button.LEFT;
		graphicalEditor.setCommand(null, confirm);
		graphicalEditor.repaint();
		return true;
	}

	@Override
	public boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return false;
	}

	@Override
	public boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		return false;
	}

	@Override
	public boolean keyTyped(Component component, char character) {
		return false;
	}

	@Override
	public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		boolean didSomething = false;
		GraphicalEditorPane graphicalEditor = getEditorParent();
		switch(keyCode) {
		case KeyEvent.VK_ESCAPE:{// Escape -> cancel
			graphicalEditor.setCommand(null, false);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_ENTER:{// Enter -> confirm
			graphicalEditor.setCommand(null, true);
			didSomething = true;
			break;
		}
		}
		if(didSomething) {
			graphicalEditor.repaint();
		}
		return didSomething;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			applyDeletion();
		}
		return true;
	}

	private static final float FONT_BASE_SIZE = 12;
	private static final float FONT_SHRINK_FACTOR = 2f;
	private static final double TEXT_EXPAND_LARGE = 2d/3;
	private static final double SELECTION_INDICATOR_RADIUS = 6;
	@Override
	public void paint(Graphics2D graphics) {
		// --- Regular drawing delegated to empty command ---
		parent.commandNone.paint(graphics);
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		Session session = graphicalEditor.getSession();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		// Local variables are faster + inversion
		int lastMousex = this.lastMousex,
				lastMousey = this.lastMousey;
		double anchorx = graphicalEditor.anchorx, anchory = graphicalEditor.anchory,
				scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
				iscalex = 1d/scalex, iscaley = 1d/scaley;
		SetUniqueList<Double> selection = graphicalEditor.selection;
		// change from center to corners
		double halfWorldWidth  = 0.5 *  width * iscalex;
		double halfWorldHeight = 0.5 * height * iscaley;
		anchorx -= halfWorldWidth;// x of left edge
		anchory += halfWorldHeight;// y of top edge
		// Get bounds
		double wx = anchorx+lastMousex*iscalex,
				wy = anchory-lastMousey*iscaley;
		final int curvePointCount = curve.getPointCount(),
				selectionPointCount = selection.size();
		// Cache points
		int[] counts = new int[4];
		double[] selectionTargetX = new double[selectionPointCount];
		double[] selectionTargetY = new double[selectionPointCount];
		int i=0;
		for(double px:selection) {
			int pindex = curve.getPointNearest(px);
			double py = curve.getPointY(pindex);
			selectionTargetX[i] = px;
			selectionTargetY[i] = py;
			i++;
			int cindex = (px<wx?0:1)+(py<wy?0:2);
			counts[cindex]++;
		}
		// --- Pre-draw ---
		Color colorDelete, colorDeleteDark, colorDeleteDark2, colorDeleteAlpha;
		ColorScheme colors = Session.getColors(session);
		colorDelete = colors.error;
		colorDeleteDark = ColorScheme.brighten(colorDelete, -0.3f);
		colorDeleteDark2 = ColorScheme.brighten(colorDelete, -0.6f);
		colorDeleteAlpha = ColorScheme.setAlpha(colorDelete, 30);
		// we don't set stroke, because empty command already did
		Stroke ostroke = graphics.getStroke();
		BasicStroke thinStroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		// draw overlay
		graphics.setColor(colorDeleteAlpha);
		graphics.fillRect(0, 0, width, height);
		// draw indicators for all points
		graphics.setColor(colorDelete);
		for(i=0;i<selectionPointCount;i++) {
			double px = selectionTargetX[i];
			double py = selectionTargetY[i];
			double wpx = (px - anchorx) * scalex;
			double wpy =-(py - anchory) * scaley;
			graphics.draw(
					new Line2D.Double(
							wpx - SELECTION_INDICATOR_RADIUS,
							wpy,
							wpx + SELECTION_INDICATOR_RADIUS,
							wpy
							)
					);
			graphics.draw(
					new Line2D.Double(
							wpx,
							wpy - SELECTION_INDICATOR_RADIUS,
							wpx,
							wpy + SELECTION_INDICATOR_RADIUS
							)
					);
		}
		// draw lines across at cursor
		graphics.setStroke(thinStroke);
		graphics.setColor(colorDeleteDark);
		graphics.draw(new Line2D.Double(0, lastMousey, width, lastMousey));
		graphics.draw(new Line2D.Double(lastMousex, 0, lastMousex, height));
		// draw texts
		Font fontBase = graphics.getFont().deriveFont(FONT_BASE_SIZE);
		Draw.drawTextImage(graphics, 0, 0, width, height, "Delete these "+selectionPointCount+" points?", null, fontBase, colorDeleteDark, null, 0.5, 0.5, TEXT_EXPAND_LARGE, 0);
		Font fontSmall = graphics.getFont();
		fontSmall = fontSmall.deriveFont(fontSmall.getSize()/FONT_SHRINK_FACTOR);
		Draw.drawTextImage(graphics, 0, lastMousey, lastMousex, height-lastMousey, Integer.toString(counts[0])+"\u2199 ", null, fontSmall, colorDeleteDark2, null, 1, 0, 0, 0);
		Draw.drawTextImage(graphics, lastMousex, lastMousey, width-lastMousex, height-lastMousey, " \u2198"+Integer.toString(counts[1]), null, fontSmall, colorDeleteDark2, null, 0, 0, 0, 0);
		Draw.drawTextImage(graphics, 0, 0, lastMousex, lastMousey, Integer.toString(counts[2])+"\u2196 ", null, fontSmall, colorDeleteDark2, null, 1, 1, 0, 0);
		Draw.drawTextImage(graphics, lastMousex, 0, width-lastMousex, lastMousey, " \u2197"+Integer.toString(counts[3]), null, fontSmall, colorDeleteDark2, null, 0, 1, 0, 0);
		// set stroke back at the end
		graphics.setStroke(ostroke);
	}

}