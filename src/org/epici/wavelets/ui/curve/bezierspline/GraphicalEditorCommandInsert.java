package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.Objects;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;
import org.epici.wavelets.core.curve.CurveBezierSpline;

/**
 * The command to insert points. Triggered by single insert and paste.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandInsert implements GraphicalEditorCommand {
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * Dummy curve object used to hold the points that would be inserted.
	 * They are translated so the origin becomes the mouse location.
	 */
	public CurveBezierSpline points;
	/**
	 * Remember if the points have been added. If true, the parent curve's data
	 * will include where all the points would go.
	 */
	public boolean pointsAdded;
	/**
	 * The mouse x, where it was last seen
	 */
	public int lastMousex;
	/**
	 * The mouse y, where it was last seen
	 */
	public int lastMousey;
	
	/**
	 * Blank constructor. Optionally sets parent and initializes
	 * {@link #points} to just the origin.
	 * 
	 * @param parent parent editor (optional)
	 */
	public GraphicalEditorCommandInsert(GraphicalEditorPane parent) {
		this.parent = parent;
		points = new CurveBezierSpline();
		points.addPoint(0, 0, true);
	}
	
	/**
	 * Constructor supplying parent and points.
	 * 
	 * @param parent parent editor (optional)
	 * @param points points to insert, wrapped in a curve object
	 */
	public GraphicalEditorCommandInsert(GraphicalEditorPane parent,CurveBezierSpline points) {
		Objects.requireNonNull(points);
		if(points.getPointCount() == 0) {
			throw new IllegalArgumentException("This needs points, but it was given an empty list. (the curve object is just a convenient wrapper)");
		}
		this.parent = parent;
		this.points = points;
	}
	
	@Override
	public void reinit(GraphicalEditorCommand previous) {
		GraphicalEditorCommand.super.reinit(previous);
		GraphicalEditorPane graphicalEditor = previous.getEditorParent();
		copyFrom(graphicalEditor.commandNone);
	}
	
	/**
	 * Copy available information from another command.
	 * 
	 * @param source command to copy from
	 */
	public void copyFrom(GraphicalEditorCommandNone source) {
		checkRemovePoints();
		lastMousex = source.lastMousex;
		lastMousey = source.lastMousey;
		checkAddPoints(true);
	}
	
	/**
	 * Check if the points have been added. If so, remove
	 * all the added points. Effectively a reset.
	 * 
	 * @return true if the points were added and are now removed
	 */
	public boolean checkRemovePoints() {
		int lastMousex = this.lastMousex, lastMousey = this.lastMousey;
		return checkRemovePoints(lastMousex, lastMousey);
	}
	
	/**
	 * Check if the points have been added. If so, remove
	 * all the added points. Effectively a reset.
	 * 
	 * @param lastMousex mouse x to assume
	 * @param lastMousey mouse y to assume
	 * @return true if the points were added and are now removed
	 */
	public boolean checkRemovePoints(int lastMousex, int lastMousey) {
		boolean pointsAdded = this.pointsAdded;
		if(pointsAdded) {
			// --- Fetch some data ---
			GraphicalEditorPane graphicalEditor = getEditorParent();
			CurveBezierSpline curve = graphicalEditor.getEditorData();
			CurveBezierSpline points = this.points;
			int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
			// Local variables are faster + inversion
			double anchorx = graphicalEditor.anchorx, anchory = graphicalEditor.anchory,
					scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
					iscalex = 1d/scalex, iscaley = 1d/scaley;
			// change from center to corners
			double halfWorldWidth  = 0.5 *  width * iscalex;
			double halfWorldHeight = 0.5 * height * iscaley;
			anchorx -= halfWorldWidth;// x of left edge
			anchory += halfWorldHeight;// y of top edge
			// Get bounds
			double wx = anchorx+lastMousex*iscalex, wy = anchory-lastMousey*iscaley;
			final int pointsCount = points.getPointCount();
			// Get original points XY
			double[] pxs = new double[pointsCount];
			points.getPointsX(pxs, 0, 0, pointsCount);
			// calculate the mapped locations, and remove them all
			for(int pindex=0;pindex<pointsCount;pindex++) {
				double px = pxs[pindex] + wx;
				curve.removePoint(curve.getPointNearest(px));
			}
		}
		this.pointsAdded = false;
		return pointsAdded;
	}
	
	/**
	 * Check if the points have been added. If not, may attempt to add
	 * the points.
	 * <br><br>
	 * Usage note: points will be added regardless of whether the
	 * flag is set. However, if the flag is set to
	 * <i>not actually add the points</i>
	 * the points are always removed later in this method.
	 * 
	 * @param doit true to complete the operation if it can,
	 * false to always restore to state before the operation
	 * 
	 * @return true if it could have completed the operation,
	 * regardless of whether it actually did
	 */
	public boolean checkAddPoints(boolean doit) {
		int lastMousex = this.lastMousex, lastMousey = this.lastMousey;
		return checkAddPoints(doit, lastMousex, lastMousey);
	}
	
	/**
	 * Check if the points have been added. If not, may attempt to add
	 * the points.
	 * <br><br>
	 * Usage note: points will be added regardless of whether the
	 * flag is set. However, if the flag is set to
	 * <i>not actually add the points</i>
	 * the points are always removed later in this method.
	 * 
	 * @param doit true to complete the operation if it can,
	 * false to always restore to state before the operation
	 * @param lastMousex mouse x to assume
	 * @param lastMousey mouse y to assume
	 * @return true if it could have completed the operation,
	 * regardless of whether it actually did
	 */
	public boolean checkAddPoints(boolean doit, int lastMousex, int lastMousey) {
		boolean pointsAdded = this.pointsAdded;
		if(pointsAdded)return false;
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		CurveBezierSpline points = this.points;
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		// Local variables are faster + inversion
		double anchorx = graphicalEditor.anchorx, anchory = graphicalEditor.anchory,
				scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
				iscalex = 1d/scalex, iscaley = 1d/scaley;
		// change from center to corners
		double halfWorldWidth  = 0.5 *  width * iscalex;
		double halfWorldHeight = 0.5 * height * iscaley;
		anchorx -= halfWorldWidth;// x of left edge
		anchory += halfWorldHeight;// y of top edge
		// Get bounds
		double wx = anchorx+lastMousex*iscalex, wy = anchory-lastMousey*iscaley;
		final int pointsCount = points.getPointCount();
		// Get original points XY
		double[] pxs = new double[pointsCount];
		double[] pys = new double[pointsCount];
		points.getPointsX(pxs, 0, 0, pointsCount);
		points.getPointsY(pys, 0, 0, pointsCount);
		// calculate the mapped locations, and try to add them
		int[] pis = new int[pointsCount];
		int pindex;
		for(pindex=0;pindex<pointsCount;pindex++) {
			double px = pxs[pindex] + wx;
			double py = pys[pindex] + wy;
			int insertedIndex = pis[pindex]// store the index, if we abort it will be useful
					= curve.addPoint(px, py, false);
			if(insertedIndex == -1) {// we failed! abort!
				break;
			}
		}
		// API says success is counted based on whether it could have completed ...
		boolean success = pindex == pointsCount;
		// ... but this flag can make it abort even on success
		boolean abort = !success || !doit;
		if(abort) {
			/*
			 * Now we reverse iterate over the ones to remove.
			 * We must remove them in the reverse order we added them,
			 * otherwise the indices we stored before will be wrong.
			 */
			for(pindex--;pindex>=0;pindex--) {
				int insertedIndex = pis[pindex];
				curve.removePoint(insertedIndex);
			}
		}
		this.pointsAdded = !abort;
		return success;
	}

	@Override
	public boolean mouseMove(Component component, int x, int y) {
		GraphicalEditorPane editor = getEditorParent();
		int lastMousex = this.lastMousex;
		int lastMousey = this.lastMousey;
		this.lastMousex = x;
		this.lastMousey = y;
		checkRemovePoints(lastMousex, lastMousey);
		checkAddPoints(true, x, y);
		editor.repaint();
		return true;
	}

	@Override
	public void mouseOver(Component component) {
	}

	@Override
	public void mouseOut(Component component) {
	}

	@Override
	public boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.requestFocus();
		return false;
	}

	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		boolean confirm = button == Mouse.Button.LEFT;
		graphicalEditor.setCommand(null, confirm);
		graphicalEditor.repaint();
		return true;
	}

	@Override
	public boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return false;
	}

	@Override
	public boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		return false;
	}

	@Override
	public boolean keyTyped(Component component, char character) {
		return false;
	}

	@Override
	public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		boolean didSomething = false;
		GraphicalEditorPane graphicalEditor = getEditorParent();
		switch(keyCode) {
		case KeyEvent.VK_ESCAPE:{// Escape -> cancel
			graphicalEditor.setCommand(null, false);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_ENTER:{// Enter -> confirm
			graphicalEditor.setCommand(null, true);
			didSomething = true;
			break;
		}
		}
		if(didSomething) {
			graphicalEditor.repaint();
		}
		return didSomething;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			checkAddPoints(true);
			return pointsAdded;
		} else {
			checkRemovePoints();
			return true;
		}
	}

	@Override
	public void paint(Graphics2D graphics) {
		parent.commandNone.paint(graphics);
	}

}
