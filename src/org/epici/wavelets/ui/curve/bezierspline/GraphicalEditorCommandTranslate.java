package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Objects;

import org.apache.commons.collections4.list.SetUniqueList;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.curve.DoubleInput;
import org.epici.wavelets.util.ui.Draw;

/**
 * The command to translate points.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandTranslate implements GraphicalEditorCommand {
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * Ordered X values of points.
	 */
	public double[] pxs;
	/**
	 * Ordered Y values of points.
	 */
	public double[] pys;
	/**
	 * Remember if the points have been added. If true, the parent curve's data
	 * will include where all the points would go.
	 */
	public boolean pointsAdded;
	/**
	 * Mouse button which was held down when this command was started
	 * (0 = not pressed, 1 = left, 2 = right, 3 = middle)
	 */
	public int initialMouseDown;
	/**
	 * The mouse x, where it was last seen
	 */
	public int lastMousex;
	/**
	 * The mouse y, where it was last seen
	 */
	public int lastMousey;
	/**
	 * The mouse x where dragging began
	 */
	public int originMousex;
	/**
	 * The mouse y where dragging began
	 */
	public int originMousey;
	/**
	 * Axis to force. (-1 is none, 0 is X, 1 is Y)
	 */
	public int offsetAxis;
	/**
	 * Whether to use the negative form.
	 */
	public boolean offsetIsNegative;
	/**
	 * Value to offset by.
	 */
	public String offsetExpression;
	
	/**
	 * Constructor supplying parent and points.
	 * 
	 * @param parent parent editor (optional)
	 */
	public GraphicalEditorCommandTranslate(GraphicalEditorPane parent) {
		this.parent = parent;
		CurveBezierSpline curve = parent.getEditorData();
		SetUniqueList<Double> selection = parent.selection;
		int selectionPointCount = selection.size();
		pxs = new double[selectionPointCount];
		pys = new double[selectionPointCount];
		int i=0;
		for(double px:selection) {
			int cpindex = curve.getPointNearest(px);
			double py = curve.getPointY(cpindex);
			pxs[i] = px;
			pys[i] = py;
			i++;
		}
		pointsAdded = true;
		offsetAxis = -1;
		offsetExpression = "";
	}
	
	@Override
	public void reinit(GraphicalEditorCommand previous) {
		GraphicalEditorCommand.super.reinit(previous);
		GraphicalEditorPane graphicalEditor = previous.getEditorParent();
		copyFrom(graphicalEditor.commandNone);
	}
	
	/**
	 * Copy available information from another command.
	 * 
	 * @param source command to copy from
	 */
	public void copyFrom(GraphicalEditorCommandNone source) {
		checkRemovePoints();
		initialMouseDown = source.mouseDown;
		lastMousex = originMousex = source.lastMousex;
		lastMousey = originMousey = source.lastMousey;
		checkAddPoints(true);
	}
	
	/**
	 * Calculate the offsets.
	 * <br>
	 * If {@link #offsetExpression} exists, uses that value, otherwise, uses mouse values.
	 * <br>
	 * If {@link #offsetAxis} exists, restricts offset axis.
	 * 
	 * @return world offsets XY of points
	 */
	public double[] getOffsets() {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		double[] offsets = new double[2];
		try {
			double offsetNumber = DoubleInput.parse(offsetExpression);
			// success! use numeric offset
			if(offsetIsNegative) {
				offsetNumber = -offsetNumber;
			}
			if(offsetAxis == 0) {// x axis must be forced
				offsets[0] = offsetNumber;
			} else {// default use y axis
				offsets[1] = offsetNumber;
			}
		}catch(Exception err) {
			// use mouse offsets instead
			offsets[0] = offsetAxis == 1 ? 0 : (lastMousex - originMousex) / graphicalEditor.scalex;
			offsets[1] = offsetAxis == 0 ? 0 :-(lastMousey - originMousey) / graphicalEditor.scaley;
		}
		return offsets;
	}
	
	/**
	 * Check if the points have been added. If so, remove
	 * all the added points. Effectively a reset.
	 * 
	 * @return true if the points were added and are now removed
	 */
	public boolean checkRemovePoints() {
		return checkRemovePoints(getOffsets());
	}
	
	/**
	 * Check if the points have been added. If so, remove
	 * all the added points. Effectively a reset.
	 * 
	 * @param offsets world offsets XY
	 * @return true if the points were added and are now removed
	 */
	public boolean checkRemovePoints(double[] offsets) {
		boolean pointsAdded = this.pointsAdded;
		if(pointsAdded) {
			// --- Fetch some data ---
			GraphicalEditorPane graphicalEditor = getEditorParent();
			CurveBezierSpline curve = graphicalEditor.getEditorData();
			double wx = offsets[0];
			double[] pxs = this.pxs;
			final int pointsCount = pxs.length;
			// calculate the mapped locations, and remove them all
			for(int pindex=0;pindex<pointsCount;pindex++) {
				double px = pxs[pindex] + wx;
				curve.removePoint(curve.getPointNearest(px));
			}
			graphicalEditor.selection.clear();
		}
		this.pointsAdded = false;
		return pointsAdded;
	}
	
	/**
	 * Check if the points have been added. If not, may attempt to add
	 * the points.
	 * <br><br>
	 * Usage note: points will be added regardless of whether the
	 * flag is set. However, if the flag is set to
	 * <i>not actually add the points</i>
	 * the points are always removed later in this method.
	 * 
	 * @param doit true to complete the operation if it can,
	 * false to always restore to state before the operation
	 * 
	 * @return true if it could have completed the operation,
	 * regardless of whether it actually did
	 */
	public boolean checkAddPoints(boolean doit) {
		return checkAddPoints(doit, getOffsets());
	}
	
	/**
	 * Check if the points have been added. If not, may attempt to add
	 * the points.
	 * <br><br>
	 * Usage note: points will be added regardless of whether the
	 * flag is set. However, if the flag is set to
	 * <i>not actually add the points</i>
	 * the points are always removed later in this method.
	 * 
	 * @param doit true to complete the operation if it can,
	 * false to always restore to state before the operation
	 * @param offsets world offsets XY
	 * @return true if it could have completed the operation,
	 * regardless of whether it actually did
	 */
	public boolean checkAddPoints(boolean doit, double[] offsets) {
		boolean pointsAdded = this.pointsAdded;
		if(pointsAdded)return false;
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		SetUniqueList<Double> selection = graphicalEditor.selection;
		double wx = offsets[0], wy = offsets[1];
		double[] pxs = this.pxs, pys = this.pys;
		final int pointsCount = pxs.length;
		// calculate the mapped locations, and try to add them
		int[] pis = new int[pointsCount];
		int pindex;
		for(pindex=0;pindex<pointsCount;pindex++) {
			double px = pxs[pindex] + wx;
			double py = pys[pindex] + wy;
			int insertedIndex = pis[pindex]// store the index, if we abort it will be useful
					= curve.addPoint(px, py, false);
			if(insertedIndex == -1) {// we failed! abort!
				break;
			}
			selection.add(px);
		}
		// API says success is counted based on whether it could have completed ...
		boolean success = pindex == pointsCount;
		// ... but this flag can make it abort even on success
		boolean abort = !success || !doit;
		if(abort) {
			/*
			 * Now we reverse iterate over the ones to remove.
			 * We must remove them in the reverse order we added them,
			 * otherwise the indices we stored before will be wrong.
			 */
			for(pindex--;pindex>=0;pindex--) {
				int insertedIndex = pis[pindex];
				curve.removePoint(insertedIndex);
			}
			selection.clear();
		}
		this.pointsAdded = !abort;
		return success;
	}

	@Override
	public boolean mouseMove(Component component, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		checkRemovePoints();
		this.lastMousex = x;
		this.lastMousey = y;
		checkAddPoints(true);
		graphicalEditor.repaint();
		return true;
	}

	@Override
	public void mouseOver(Component component) {
	}

	@Override
	public void mouseOut(Component component) {
	}

	@Override
	public boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.requestFocus();
		return false;
	}

	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		boolean confirm = button == Mouse.Button.LEFT;
		graphicalEditor.setCommand(null, confirm);
		graphicalEditor.repaint();
		return true;
	}

	@Override
	public boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return false;
	}

	@Override
	public boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		return false;
	}

	@Override
	public boolean keyTyped(Component component, char character) {
		return false;
	}

	@Override
	public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		boolean didSomething = false;
		GraphicalEditorPane graphicalEditor = getEditorParent();
		int keyCounter = 48;
		switch(keyCode) {
		case KeyEvent.VK_PERIOD:keyCounter=37;
		case KeyEvent.VK_9:keyCounter++;
		case KeyEvent.VK_8:keyCounter++;
		case KeyEvent.VK_7:keyCounter++;
		case KeyEvent.VK_6:keyCounter++;
		case KeyEvent.VK_5:keyCounter++;
		case KeyEvent.VK_4:keyCounter++;
		case KeyEvent.VK_3:keyCounter++;
		case KeyEvent.VK_2:keyCounter++;
		case KeyEvent.VK_1:keyCounter++;
		case KeyEvent.VK_0:{// Digit or period -> numeric offset
			checkRemovePoints();
			offsetExpression += String.valueOf((char)keyCounter);
			checkAddPoints(true);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_BACK_SPACE:{// Backspace -> remove last character
			int slen = offsetExpression.length();
			if(slen>0) {
				checkRemovePoints();
				offsetExpression = offsetExpression.substring(0, slen-1);
				checkAddPoints(true);
			}
			didSomething = true;
			break;
		}
		case KeyEvent.VK_MINUS:{// Minus -> toggle negation
			checkRemovePoints();
			offsetIsNegative = !offsetIsNegative;
			checkAddPoints(true);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_X:{// X -> toggle force x axis
			checkRemovePoints();
			offsetAxis = offsetAxis!=0?0:-1;
			checkAddPoints(true);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_Y:{// Y -> toggle force y axis
			checkRemovePoints();
			offsetAxis = offsetAxis!=1?1:-1;
			checkAddPoints(true);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_ESCAPE:{// Escape -> cancel
			graphicalEditor.setCommand(null, false);
			didSomething = true;
			break;
		}
		case KeyEvent.VK_ENTER:{// Enter -> confirm
			graphicalEditor.setCommand(null, true);
			didSomething = true;
			break;
		}
		}
		if(didSomething) {
			graphicalEditor.repaint();
		}
		return didSomething;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			checkAddPoints(true);
			return pointsAdded;
		} else {
			checkRemovePoints();
			checkAddPoints(true, new double[]{0,0});
			return true;
		}
	}

	private static final String TEST_STRING = "Translating 123 points\nDx: 1.4142135623730951  Dy: 1.4142135623730951";
	private static final float FONT_BASE_SIZE = 12;
	private static final double TEXT_EXPAND = 1d/3;
	@Override
	public void paint(Graphics2D graphics) {
		// --- Regular drawing delegated to empty command ---
		parent.commandNone.paint(graphics);
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		Session session = graphicalEditor.getSession();
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		// get bounds
		int translatePointCount = pxs.length;
		double[] offsets = getOffsets();
		double wdx = offsets[0], wdy = offsets[1];
		// --- Pre-draw ---
		Color colorTransparent, colorText;
		ColorScheme colors = Session.getColors(session);
		colorText = colors.text;
		colorTransparent = ColorScheme.setAlpha(Color.BLACK, 0);
		// make text for offsets
		StringBuilder textsb = new StringBuilder();
		textsb.append("Translating ");
		textsb.append(translatePointCount);
		textsb.append(" points\n");
		switch(offsetAxis) {
		case 0:{
			textsb.append("D: ");
			textsb.append(wdx);
			textsb.append(" along X (input) axis");
			break;
		}
		case 1:{
			textsb.append("D: ");
			textsb.append(wdy);
			textsb.append(" along Y (output) axis");
			break;
		}
		default:{
			textsb.append("Dx: ");
			textsb.append(wdx);
			textsb.append("  Dy: ");
			textsb.append(wdy);
			break;
		}
		}
		// first pass to get font size, next pass to draw actual text
		Font fontBase = graphics.getFont().deriveFont(FONT_BASE_SIZE);
		Draw.drawTextImage(graphics, 0, 0, width, height, TEST_STRING, null, fontBase, colorTransparent, null, 0, 0, TEXT_EXPAND, 0);
		Draw.drawTextImage(graphics, 0, 0, width, height, textsb.toString(), null, null, colorText, null, 0, 0, 0, 0);
	}

}
