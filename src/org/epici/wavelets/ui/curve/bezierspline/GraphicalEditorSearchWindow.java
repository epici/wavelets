package org.epici.wavelets.ui.curve.bezierspline;

import java.net.URL;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.collections.Sequence;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.ComponentKeyListener;
import org.apache.pivot.wtk.ComponentStateListener;
import org.apache.pivot.wtk.Keyboard.KeyLocation;
import org.apache.pivot.wtk.ListView;
import org.apache.pivot.wtk.ListViewItemStateListener;
import org.apache.pivot.wtk.ListViewSelectionListener;
import org.apache.pivot.wtk.Span;
import org.apache.pivot.wtk.TextInput;
import org.apache.pivot.wtk.Window;
import org.epici.wavelets.core.Named;
import org.epici.wavelets.ui.WindowManager;
import org.epici.wavelets.util.text.StringAlgorithms;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * The search dialog pop-up window used by
 * {@link GraphicalEditorCommandSearch}.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorSearchWindow extends Window implements Bindable {
	
	/**
	 * The search results list, where the user can select a command
	 * to start it.
	 */
	public ListView searchSelector;
	/**
	 * The search bar, where the user enters the partial name
	 * of the command they want.
	 */
	public TextInput searchInput;
	/**
	 * The frame which contains this window.
	 */
	public JInternalFrame wrapFrame;
	/**
	 * If true, events will do nothing instead of triggering the normal responses.
	 * Outside code should set this flag to true before doing any clean up to avoid
	 * accidentally triggering events and producing side effects.
	 */
	public boolean silenceEvents;
	/**
	 * The command that is using this window.
	 */
	public GraphicalEditorCommandSearch command;
	/**
	 * Remember the last search, if any.
	 */
	public String lastSearch = null;
	/**
	 * Special counter that counts the number of flips
	 * of the boolean &quot;is it focused?&quot;.
	 * Is always even when not focused and odd when focused,
	 * and never decreases.
	 */
	public int focusCounter = 0;

	/**
	 * Initialize without any components.
	 * Same as {@link Window#Window()}.
	 */
	public GraphicalEditorSearchWindow() {
		this(null);
	}

	/**
	 * Initialize with a component as content.
	 * Same as {@link Window#Window(Component)}.
	 */
	public GraphicalEditorSearchWindow(Component content) {
		super(content);
	}
	
	/**
	 * Create a new instance. It is not initialized,
	 * you will need to call {@link #init()} before you can
	 * do anything with it. This is intended for outside use.
	 * 
	 * @return a new search window instance
	 */
	public static GraphicalEditorSearchWindow createNew() {
		return PivotSwingUtils.loadBxml(GraphicalEditorSearchWindow.class, "graphicalEditorSearchWindow.bxml");
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		searchSelector = (ListView) namespace.get("searchSelector");
		searchInput = (TextInput) namespace.get("searchInput");
	}
	
	/**
	 * Call this after fields are set to finish initialization.
	 */
	public void init() {
		// add the window listener
		wrapFrame.addInternalFrameListener(new InternalFrameListener() {

			@Override
			public void internalFrameOpened(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				// may not fire if flag is set
				if(silenceEvents)return;
				// no more events can fire after this
				silenceEvents = true;
				// cancel command
				command.getEditorParent().setCommand(null, false);
			}

			@Override
			public void internalFrameIconified(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameDeiconified(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameActivated(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameDeactivated(InternalFrameEvent e) {
			}
			
		});
		// add the search input listener
		searchInput.getComponentKeyListeners().add(new ComponentKeyListener() {

			@Override
			public boolean keyTyped(Component component, char character) {
				return false;
			}

			@Override
			public boolean keyPressed(Component component, int keyCode, KeyLocation keyLocation) {
				return false;
			}

			@Override
			public boolean keyReleased(Component component, int keyCode, KeyLocation keyLocation) {
				// may not fire if flag is set
				if(silenceEvents)return false;
				// may need to change results list
				remakeSelector();
				return false;
			}
			
		});
		// add the search list listener
		searchSelector.getListViewSelectionListeners().add(new ListViewSelectionListener() {

			@Override
			public void selectedRangeAdded(ListView listView, int rangeStart, int rangeEnd) {
			}

			@Override
			public void selectedRangeRemoved(ListView listView, int rangeStart, int rangeEnd) {
			}

			@Override
			public void selectedRangesChanged(ListView listView, Sequence<Span> previousSelectedRanges) {
				// may not fire if flag is set
				if(silenceEvents)return;
				// fetch index
				int ifirst = searchSelector.getFirstSelectedIndex(),
						ilast = searchSelector.getLastSelectedIndex();
				// require exactly 1 selected item
				if(ifirst < 0 || ifirst != ilast)return;
				// get the name of the command
				String mergedCommandName = (String) searchSelector.getListData().get(ifirst);
				// get the key bind of the command
				int keyBind = GraphicalEditorPane.extractKeyBindFromCombinedName(mergedCommandName);
				// fetch the new command
				GraphicalEditorPane graphicalEditor = command.getEditorParent();
				Function<GraphicalEditorPane, GraphicalEditorCommand> commandGenerator = GraphicalEditorPane.commandMap.get(keyBind);
				GraphicalEditorCommand newCommand = commandGenerator.apply(graphicalEditor);
				// if command could not be created, undo the selection and return
				if(newCommand == null) {
					searchSelector.clearSelection();
					return;
				}
				// get rid of the search command and clean up
				command.searchWindowDestroy();
				graphicalEditor.setCommand(null, true);
				// return focus to the editor
				JInternalFrame curveEditorFrame = graphicalEditor.getSession().windowManager.getWindow(WindowManager.NAME_CURVE_EDITOR).component;
				curveEditorFrame.toFront();
				curveEditorFrame.requestFocusInWindow();
				graphicalEditor.requestFocus();
				// set the new command
				graphicalEditor.setCommand(newCommand, false);
			}

			@Override
			public void selectedItemChanged(ListView listView, Object previousSelectedItem) {
			}
			
		});
		// add focus change listeners
		ComponentStateListener focusListener = new ComponentStateListener() {

			@Override
			public void enabledChanged(Component component) {
			}

			@Override
			public void focusedChanged(Component component, Component obverseComponent) {
				// may not fire if flag is set
				if(silenceEvents)return;
				// is it focused?
				boolean isFocused = searchInput.isFocused() || searchSelector.isFocused();
				// needs to gain and lose focus once before acting
				// some interactions mean it actually switches twice
				if(focusCounterIncrement(isFocused)<4)return;
				// trigger cancel by closing window
				wrapFrame.dispose();
			}
			
		};
		searchInput.getComponentStateListeners().add(focusListener);
		searchSelector.getComponentStateListeners().add(focusListener);
		// initialize results list
		remakeSelector();
	}
	
	/**
	 * Modify the {@link #focusCounter} and return the new value.
	 * 
	 * @param isFocused is it focused?
	 * @return new value of {@link #focusCounter}
	 */
	public int focusCounterIncrement(boolean isFocused) {
		return focusCounter += (((focusCounter&1)!=0)^isFocused)?1:0;
	}
	
	/**
	 * Does this classes' part of the cleaning up process that occurs
	 * when the search window needs to be closed. Sets {@link #silenceEvents}
	 * to true before it does its work.
	 */
	public void searchWindowDestroy() {
		silenceEvents = true;
		wrapFrame.dispose();
	}
	
	/**
	 * Updates the list items in {@link #searchSelector}.
	 * Commands will be filtered based on the content of {@link #searchInput}.
	 */
	public void remakeSelector() {
		// get the text we want to search by
		String searchText = searchInput.getText();
		// only update on change
		if(lastSearch != null && lastSearch.equals(searchText))return;
		lastSearch = searchText;
		// get the names of all commands as a string
		Stream<String> mergedCommandNames = GraphicalEditorPane
				.commandMap
				.keySet()
				.stream()
				.map(GraphicalEditorPane::getKeyBindNameOfCommandGenerator);
		// we will ignore case by converting all to lower case
		final String lsearchText = searchText.toLowerCase();
		// we instantiate the replacement list
		org.apache.pivot.collections.ArrayList<Object> selectorList = new org.apache.pivot.collections.ArrayList<>();
		// we keep only matches and add those to the list
		mergedCommandNames
		.filter(
				(String name)
				->
				StringAlgorithms.isSubsequenceOf(lsearchText,name.toLowerCase())
				)
		.forEach(
				selectorList::add
				);
		// these list items are used as list data
		searchSelector.setListData(selectorList);
		// need repaint
		repaint();
	}

}
