package org.epici.wavelets.ui.curve.bezierspline;

import org.apache.pivot.wtk.Component;

/**
 * Represents an instant command which changes the scaling factors
 * to some special multipliers.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandViewScale implements GraphicalEditorCommandInstant {

	/**
	 * Value of {@link #targetCode} used to set X and Y scaling factors
	 * to the default scaling factors defined by {@link GraphicalEditorPane#SCALE_DEFAULT}.
	 */
	public static final int TARGET_CODE_DEFAULT = 0;
	/**
	 * Value of {@link #targetCode} used to set X and Y scaling factors
	 * to their current values' geometric mean (multiplicative average).
	 */
	public static final int TARGET_CODE_EQUALIZE = 1;
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * Single integer encoding what new X and Y scale factors to use for the view.
	 * <table>
	 * <tr>
	 *   <th>Value of {@link #targetCode}</th>
	 *   <th>Target name</th>
	 *   <th>Derived from</th>
	 * </tr>
	 * <tr>
	 *   <td>{@link #TARGET_CODE_DEFAULT}</td>
	 *   <td>Default</td>
	 *   <td>Defaults defined by {@link GraphicalEditorPane#SCALE_DEFAULT}</td>
	 * </tr>
	 * <tr>
	 *   <td>{@link #TARGET_CODE_EQUALIZE}</td>
	 *   <td>Equalize</td>
	 *   <td>Multiplicative average of current scaling factors</td>
	 * </tr>
	 * </table>
	 */
	public int targetCode;

	/**
	 * Full constructor. Specifies parent and scaling factors target.
	 * 
	 * @param graphicalEditor parent editor (optional)
	 * @param targetCode value of {@link #targetCode} to set
	 */
	public GraphicalEditorCommandViewScale(GraphicalEditorPane graphicalEditor, int targetCode) {
		this.parent = graphicalEditor;
		this.targetCode = targetCode;
	}

	@Override
	public boolean finalize(boolean confirm) {
		if(confirm) {
			GraphicalEditorPane graphicalEditor = getEditorParent();
			double scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley;
			switch(targetCode) {
			case TARGET_CODE_DEFAULT:{
				double newScale = GraphicalEditorPane.SCALE_DEFAULT;
				graphicalEditor.scalex = graphicalEditor.scaley = newScale;
				break;
			}
			case TARGET_CODE_EQUALIZE:{
				double newScale = Math.sqrt(scalex*scaley);
				graphicalEditor.scalex = graphicalEditor.scaley = newScale;
				break;
			}
			}
		}
		return true;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

}
