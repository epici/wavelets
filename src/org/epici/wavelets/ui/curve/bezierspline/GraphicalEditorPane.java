package org.epici.wavelets.ui.curve.bezierspline;

import java.net.URL;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.*;
import org.epici.wavelets.core.Named;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.ui.SessionLinked;
import org.epici.wavelets.ui.curve.CurveEditor;
import org.epici.wavelets.util.Any;
import org.epici.wavelets.util.Bits;
import com.google.common.collect.MapMaker;
import java.awt.event.KeyEvent;
import org.apache.commons.collections4.list.SetUniqueList;

/**
 * The graphical editor for a {@link CurveBezierSplineEditor}.
 * Shows the curve with control points on top of a grid.
 * <br><br>
 * Note that we use normal graph orientation here:
 * +x is right, and
 * +y is up.
 * <br>
 * This is inverted from graphics space, where
 * +x is right, and
 * +y is down.
 * <br>
 * All graphics and UI code need to invert the Y axis.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorPane extends FillPane implements Bindable, SessionLinked, KeyTracker, DataEditor.Instance<CurveBezierSpline, CurveBezierSplineEditor> {

	/**
	 * The smallest allowed {@link #scalex} or {@link #scaley}.
	 */
	public static final double SCALE_MIN = 0x1.0p-64;
	/**
	 * The largest allowed {@link #scalex} or {@link #scaley}.
	 */
	public static final double SCALE_MAX = 0x1.0p+64;
	/**
	 * What the scale starts at and would be reset to.
	 */
	public static final double SCALE_DEFAULT = 200;
	
	/**
	 * What font size/text size to prefer in displays.
	 */
	public int textSize;
	
	/**
	 * The x position (world) which the centre
	 * of the pane corresponds to
	 */
	public double anchorx;
	/**
	 * The y position (world) which the centre
	 * of the pane corresponds to (+y is down not up)
	 */
	public double anchory;
	/**
	 * The x scale of the UI, maps input (world) to pixel x
	 */
	public double scalex;
	/**
	 * The y scale of the UI, maps output (world) to pixel y
	 */
	public double scaley;
	/**
	 * Which keys are held down?
	 * <br><br>
	 * This is managed by the {@link GraphicalEditorPaneSkin}
	 * and updated <i>before</i> delegation to command.
	 */
	protected BitSet keys = new BitSet();
	/**
	 * Mouse button being held down (0 = not pressed, 1 = left, 2 = right, 3 = middle)
	 * <br><br>
	 * This is managed by the {@link GraphicalEditorPaneSkin}
	 * and updated <i>before</i> delegation to command.
	 */
	public int mouseDown;
	/**
	 * The mouse x, where it was last seen
	 * <br><br>
	 * This is managed by the {@link GraphicalEditorPaneSkin}
	 * and updated <i>before</i> delegation to command.
	 */
	public int lastMousex;
	/**
	 * The mouse y, where it was last seen
	 * <br><br>
	 * This is managed by the {@link GraphicalEditorPaneSkin}
	 * and updated <i>before</i> delegation to command.
	 */
	public int lastMousey;
	
	/**
	 * All selected points on the curve, as the x (input) values.
	 * <br><br>
	 * This is a list of the x (input) values of the selected points.
	 * We could store the indices instead, but some operations
	 * reorder the points, and it becomes a hassle to update the list correctly.
	 * We need to keep the selection in order, because some operations care about
	 * the selection order.
	 * We also need to disallow duplicates, because you can only select a point once.
	 * Thus, it is implemented with a no duplicates (set-like) list.
	 */
	public SetUniqueList<Double> selection = SetUniqueList.setUniqueList(new ArrayList<>());
	
	/**
	 * The parent editor.
	 */
	public CurveBezierSplineEditor parent;
	/**
	 * The curve being edited.
	 */
	public CurveBezierSpline view;
	
	/**
	 * Maps key bindings to commands.
	 * <br><br>
	 * Keys are integers produced by {@link KeyTracker#combineKeyWithModifiers(int, int)}.
	 * They represent a key code and possible modifiers.
	 * <br><br>
	 * Values are functions which take the editor instance and produce a
	 * command instance which is ready to go. If the command cannot be created,
	 * it should return null.
	 * <br>
	 * Though not strictly required, values should also implement {@link Named}.
	 * Their name would be the name of the command.
	 * <br><br>
	 * <i>It does look strange that this is statically bound rather than
	 * associated with a {@link Session}. While partially due to laziness, it is
	 * justified since the function producing the command will be aware of the session
	 * through the editor pane (see {@link #getSession()}), and therefore can
	 * still have different behaviours for different sessions. The convenience of static
	 * access to the map seems to outweigh the inconvenience of packing session-dependent
	 * actions into a function.</i>
	 */
	public static ConcurrentMap<Integer,Function<GraphicalEditorPane,GraphicalEditorCommand>> commandMap = new MapMaker().makeMap();
	
	/**
	 * The current active command, which determines the look
	 * and behaviour of this editor.
	 */
	public GraphicalEditorCommand command;
	/**
	 * Unchanging field uses to restore {@link #command}
	 * after {@link GraphicalEditorCommand#finalize(boolean)}.
	 * 
	 * @see GraphicalEditorCommandNone
	 */
	public GraphicalEditorCommandNone commandNone;
	
	static {
		NamedFunction<GraphicalEditorPane,GraphicalEditorCommand> commandGenerator;
		BiFunction<String, Integer, NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>> metaCommandGenerator1;
		// Space -> search commands
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_SPACE,
						0
						),
				new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
					{
						name = "Command search";
					}
					@Override
					public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
						GraphicalEditorCommandSearch newCommand = new GraphicalEditorCommandSearch(graphicalEditor);
						return newCommand;
					}
				}
				);
		// I -> insert single point
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_I,
						0
						),
				new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
					{
						name = "Insert point";
					}
					@Override
					public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
						GraphicalEditorCommandInsert newCommand = new GraphicalEditorCommandInsert(graphicalEditor);
						return newCommand;
					}
				}
				);
		// B -> box select set
		metaCommandGenerator1 = (String iname,Integer code) -> new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
			{
				name = "Box select ("+iname+")";
			}
			@Override
			public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
				GraphicalEditorCommandSelect newCommand = new GraphicalEditorCommandSelect(
						graphicalEditor,
						code
						);
				return newCommand;
			}
		};
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_B,
						0),
				metaCommandGenerator1.apply(
						"set",
						GraphicalEditorCommandSelect.BIT_OPERATOR_SET
						)
				);
		// Shift B -> box select union
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_B,
						1),
				metaCommandGenerator1.apply(
						"union",
						GraphicalEditorCommandSelect.BIT_OPERATOR_UNION
						)
				);
		// Ctrl B -> box select difference
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_B,
						2),
				metaCommandGenerator1.apply(
						"difference",
						GraphicalEditorCommandSelect.BIT_OPERATOR_DIFFERENCE
						)
				);
		// Ctrl Shift B -> box select intersect
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_B,
						3),
				metaCommandGenerator1.apply(
						"intersect",
						GraphicalEditorCommandSelect.BIT_OPERATOR_INTERSECT
						)
				);
		// X or Delete -> delete selected points
		commandGenerator = new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
			{
				name = "Delete selected";
			}
			@Override
			public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
				// requires a selection
				if(graphicalEditor.selection.isEmpty())return null;
				GraphicalEditorCommandDelete newCommand = new GraphicalEditorCommandDelete(
						graphicalEditor
						);
				return newCommand;
			}
		};
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_X,
						0),
				commandGenerator
				);
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_DELETE,
						0),
				commandGenerator
				);
		// G -> grab/translate
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_G,
						0),
				new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
					{
						name = "Translate selected";
					}
					@Override
					public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
						if(graphicalEditor.selection.isEmpty())return null;// require selection
						GraphicalEditorCommandTranslate newCommand = new GraphicalEditorCommandTranslate(
								graphicalEditor
								);
						return newCommand;
					}
				}
				);
		// S -> scale
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_S,
						0),
				new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
					{
						name = "Scale selected";
					}
					@Override
					public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
						if(graphicalEditor.selection.isEmpty())return null;// require selection
						GraphicalEditorCommandScale newCommand = new GraphicalEditorCommandScale(
								graphicalEditor
								);
						return newCommand;
					}
				}
				);
		// Ctrl V -> paste
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_V,
						2),
				new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
					{
						name = "Paste points";
					}
					@Override
					public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
						Object oclipboard = graphicalEditor.getSession().getClipBoard();
						CurveBezierSpline insertPoints = null;
						int[] fails = new int[1];// track failures
						if(oclipboard instanceof CurveBezierSpline) {// points as curve
							insertPoints = (CurveBezierSpline) oclipboard;
						}else if(oclipboard instanceof Stream<?>) {// points as points stream
							insertPoints = new CurveBezierSpline();
							final CurveBezierSpline lcurve = insertPoints;
							((Stream<?>) oclipboard)
							.map(CurveBezierSpline::convertPointUnknownToArray)
							.forEach((double[] pxy)->{
								if(pxy == null) {
									fails[0]++;
								} else {
									lcurve.addPoint(pxy);
								}
							});
						}else if(oclipboard instanceof Iterator<?>) {// points as points iterator
							insertPoints = new CurveBezierSpline();
							Iterator<?> iter = (Iterator<?>) oclipboard;
							while(iter.hasNext()) {
								Object pxy = iter.next();
								double[] apxy = CurveBezierSpline.convertPointUnknownToArray(pxy);
								if(apxy == null) {
									fails[0] = 1;
									break;
								}
								insertPoints.addPoint(apxy);
							}
						}else if(oclipboard instanceof Iterable<?>) {// points as points iterable
							insertPoints = new CurveBezierSpline();
							for(Object pxy:(Iterable<?>)oclipboard) {
								double[] apxy = CurveBezierSpline.convertPointUnknownToArray(pxy);
								if(apxy == null) {
									fails[0] = 1;
									break;
								}
								insertPoints.addPoint(apxy);
							}
						}
						if(fails[0] != 0)insertPoints = null;// we failed!
						if(insertPoints != null) {// we found a viable insert
							// figure out the pivot point
							double[] pivotPoint = GraphicalEditorPane.getPivotOnPoint(
									graphicalEditor.getEditorParent().getPivotOn(),
									graphicalEditor,
									insertPoints,
									null);
							// make copy which has pivot point translated to origin (subtracted)
							insertPoints = CurveBezierSpline.fromPointsStream(
									insertPoints.getStreamXY().map((double[] oxy)->(new double[]{oxy[0]-pivotPoint[0],oxy[1]-pivotPoint[1]}))
									);
							GraphicalEditorCommandInsert newCommand = new GraphicalEditorCommandInsert(
									graphicalEditor,
									insertPoints
									);
							return newCommand;
						}
						return null;
					}
				}
				);
		// Ctrl C -> copy
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_C,
						2),
				new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
					{
						name = "Copy selected";
					}
					@Override
					public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
						if(!graphicalEditor.selection.isEmpty()) {// require a selection
							// fetch curve
							CurveBezierSpline curve = graphicalEditor.getEditorData();
							// get the selected points, and put it in a list
							ArrayList<double[]> selectedPoints =
									curve.getStreamXYRangePosition(
											graphicalEditor.selection.stream().mapToDouble(Double::doubleValue)
											)
									.collect(Collectors.toCollection(ArrayList::new));
							// don't bother correcting offsets, that'll happen when pasting
							// and then copy it
							GraphicalEditorCommandCopy newCommand = new GraphicalEditorCommandCopy(
									graphicalEditor,
									selectedPoints
									);
							return newCommand;
						}
						return null;
					}
				}
				);
		// . -> center view on origin
		metaCommandGenerator1 = (String iname,Integer code) -> new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
			{
				name = "Center view ("+iname+")";
			}
			@Override
			public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
				GraphicalEditorCommandViewCenter newCommand = new GraphicalEditorCommandViewCenter(
						graphicalEditor,
						code
						);
				return newCommand;
			}
		};
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_PERIOD,
						0),
				metaCommandGenerator1.apply(
						"origin",
						GraphicalEditorCommandViewCenter.TARGET_CODE_ORIGIN
						)
				);
		// Control . -> center view on selection
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_PERIOD,
						2),
				metaCommandGenerator1.apply(
						"selection",
						GraphicalEditorCommandViewCenter.TARGET_CODE_SELECTION
						)
				);
		// Shift . -> center view on cursor
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_PERIOD,
						1),
				metaCommandGenerator1.apply(
						"cursor",
						GraphicalEditorCommandViewCenter.TARGET_CODE_CURSOR
						)
				);
		// Shift Control . -> center view on cursor nearest point
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_PERIOD,
						3),
				metaCommandGenerator1.apply(
						"nearest point to cursor",
						GraphicalEditorCommandViewCenter.TARGET_CODE_CURSOR_NEAREST_POINT
						)
				);
		// 1 -> change zoom to equalized
		metaCommandGenerator1 = (String iname,Integer code) -> new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
			{
				name = "Scale view ("+iname+")";
			}
			@Override
			public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
				GraphicalEditorCommandViewScale newCommand = new GraphicalEditorCommandViewScale(
						graphicalEditor,
						code
						);
				return newCommand;
			}
		};
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_1,
						0),
				metaCommandGenerator1.apply(
						"equalize",
						GraphicalEditorCommandViewScale.TARGET_CODE_EQUALIZE
						)
				);
		// Control 1 -> change zoom to defaults
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_1,
						2),
				metaCommandGenerator1.apply(
						"default",
						GraphicalEditorCommandViewScale.TARGET_CODE_DEFAULT
						)
				);
		// A -> select or deselect all
		metaCommandGenerator1 = (String iname,Integer code) -> new NamedFunction<GraphicalEditorPane,GraphicalEditorCommand>() {
			{
				name = "Select all ("+iname+")";
			}
			@Override
			public GraphicalEditorCommand apply(GraphicalEditorPane graphicalEditor) {
				GraphicalEditorCommandSelectAll newCommand = new GraphicalEditorCommandSelectAll(
						graphicalEditor,
						code
						);
				return newCommand;
			}
		};
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_A,
						0),
				metaCommandGenerator1.apply(
						"toggle",
						GraphicalEditorCommandSelectAll.BIT_OPERATOR_TOGGLE
						)
				);
		// Control I -> invert selection
		commandMap.put(
				KeyTracker.combineKeyWithModifiers(
						KeyEvent.VK_I,
						2),
				metaCommandGenerator1.apply(
						"invert",
						GraphicalEditorCommandSelectAll.BIT_OPERATOR_INVERT
						)
				);
	}
	
	public GraphicalEditorPane() {
		super();
		anchorx = 0;
		anchory = 0;
		scalex = scaley = SCALE_DEFAULT;
		textSize = 16;
		installSkin(GraphicalEditorPane.class);
	}
	
	@Override
	public CurveBezierSpline getEditorData() {
		return view;
	}

	@Override
	public CurveBezierSplineEditor getEditorParent() {
		return parent;
	}

	@Override
	public BitSet getKeysDown() {
		return keys;
	}

	@Override
	public Session getSession() {
		return getEditorParent().getSession();
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof CurveBezierSplineEditor))return false;
		parent = (CurveBezierSplineEditor) newParent;
		return true;
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		
	}
	
	@Override
	public void init(){
		view = parent.view;
		command = commandNone = new GraphicalEditorCommandNone();
		commandNone.setEditorParent(this);
		commandNone.init();
	}
	
	/**
	 * Try to set the command.
	 * 
	 * @param newCommand new command to set, null to reset
	 * @param confirm whether to confirm the existing command if present
	 * @return true on success
	 */
	public boolean setCommand(GraphicalEditorCommand newCommand, boolean confirm) {
		// if there's a current command, we should check first if it can be cancelled
		if(!command.finalize(confirm))return false;
		GraphicalEditorCommand oldCommand = command;
		command = newCommand==null?commandNone:newCommand;
		CurveBezierSplineEditor parent = getEditorParent();
		parent.fillTools.setEnabled(command == commandNone);// toolbar should not be accessible if command is in progress
		command.reinit(oldCommand);
		return true;
	}
	
	/**
	 * Get the point to pivot on in world space as an (x, y) array.
	 * <br>
	 * Returns the origin in an invalid case.
	 * 
	 * @param pivotCode result of {@link CurveBezierSplineEditor#getPivotOn()}
	 * @return point to pivot on in world space as an (x, y) array
	 */
	public double[] getPivotOnPoint(int pivotCode) {
		CurveBezierSpline curve = getEditorData();
		SetUniqueList<Double> selection = this.selection;
		return getPivotOnPoint(pivotCode, this, curve, selection.iterator());
	}
	
	/**
	 * Get the point to pivot on in world space as an (x, y) array.
	 * <br>
	 * Returns the origin in an invalid case.
	 * This includes cases that require knowledge of a
	 * {@link GraphicalEditorPane} or {@link CurveBezierSplineEditor}
	 * if none are specified.
	 * 
	 * @param pivotCode result of {@link CurveBezierSplineEditor#getPivotOn()}
	 * @param graphicalEditor if specified, used to resolve editor-specific cases
	 * @param curve curve to use
	 * @param xs if specified, is the x values to use
	 * @return point to pivot on in world space as an (x, y) array
	 */
	public static double[] getPivotOnPoint(int pivotCode, GraphicalEditorPane graphicalEditor, CurveBezierSpline curve, Iterator<Double> xs) {
		if(xs == null) {// use all x values, if not specified which to use
			xs = curve.getStreamX().iterator();
		}
		switch(pivotCode) {
		case 1:{// first
			if(xs.hasNext()) {// first only makes sense if there is at least 1 point
				double px = xs.next();
				int pindex = curve.getPointNearest(px);
				double py = curve.getPointY(pindex);
				return new double[]{px,py};
			}
			break;
		}
		case 2:{// median
			if(xs.hasNext()) {// median only makes sense if there is at least 1 point
				double sumpx = 0, sumpy = 0;
				int pcounter = 0;
				while(xs.hasNext()) {
					double px = xs.next();
					int pindex = curve.getPointNearest(px);
					double py = curve.getPointY(pindex);
					sumpx += px;
					sumpy += py;
					pcounter += 1;
				}
				double mul = 1d/pcounter;// never divides by zero, because we checked already there is at least 1 point
				sumpx *= mul;
				sumpy *= mul;
				return new double[]{sumpx,sumpy};
			}
			break;
		}
		case 3:{// anchor
			if(graphicalEditor != null) {
				return new double[]{graphicalEditor.anchorx,graphicalEditor.anchory};
			}
			break;
		}
		}
		return new double[]{0,0};
	}
	
	/**
	 * Get the name of a command generator as seen in {@link #commandMap}.
	 * If it is an instance of {@link Named}, it will use that name,
	 * otherwise it will generate a name using {@link Named#defaultName(String, Object)}.
	 * 
	 * @param commandGenerator command generator function
	 * @return name of the command generator
	 */
	public static String getNameOfCommandGenerator(Function<GraphicalEditorPane, GraphicalEditorCommand> commandGenerator) {
		// handle nulls
		if(commandGenerator == null)return "None";
		// use Named if possible
		if(commandGenerator instanceof Named)return ((Named)commandGenerator).getName();
		// otherwise use default name
		return Named.defaultName("Command", commandGenerator);
	}
	
	/**
	 * Get the name of a command generator as seen in {@link #commandMap},
	 * prefixed with the key and key modifiers
	 * and then a colon and then a space.
	 * Will look something like this:
	 * {@code "Control I: Select all (invert)"}
	 * 
	 * @param commandGeneratorKeyBind key bind of command generator
	 * @return merged name
	 */
	public static String getKeyBindNameOfCommandGenerator(int commandGeneratorKeyBind) {
		Function<GraphicalEditorPane, GraphicalEditorCommand> commandGenerator = commandMap.get(commandGeneratorKeyBind);
		StringBuilder sb = new StringBuilder();
		int keyCode = KeyTracker.extractKeyCodeFromCombined(commandGeneratorKeyBind);
		int keyModifiers = KeyTracker.extractKeyModifiersFromCombined(commandGeneratorKeyBind);
		if(Bits.readBit(keyModifiers, 2)) {
			sb.append("Alt ");
		}
		if(Bits.readBit(keyModifiers, 1)) {
			sb.append("Control ");
		}if(Bits.readBit(keyModifiers, 0)) {
			sb.append("Shift ");
		}
		sb.append(KeyEvent.getKeyText(keyCode));
		sb.append(": ");
		sb.append(getNameOfCommandGenerator(commandGenerator));
		return sb.toString();
	}
	
	/**
	 * Inverse function of {@link #getKeyBindNameOfCommandGenerator(int)}.
	 * 
	 * @param combinedName combined name
	 * @return merged key modifiers and key name
	 */
	public static int extractKeyBindFromCombinedName(String combinedName) {
		int splitAt = combinedName.indexOf(':');
		if(splitAt<0)throw new IllegalArgumentException("Wrong combined name format, could not find separator");
		String[] segments = combinedName.substring(0, splitAt).split("\\s+");
		int keyModifiers = 0;
		for(int i=segments.length-2;i>=0;i--) {
			switch(segments[i]) {
			case "Shift":{
				keyModifiers |= 1;
				break;
			}
			case "Control":{
				keyModifiers |= 2;
				break;
			}
			case "Alt":{
				keyModifiers |= 4;
				break;
			}
			default:throw new IllegalArgumentException("Unknown key modifier in combined name: "+segments[i]);
			}
		}
		String keyName = segments[segments.length-1];
		int keyCode = KeyTracker.getKeyCodeFromKeyName(keyName);
		return KeyTracker.combineKeyWithModifiers(keyCode, keyModifiers);
	}
	
	/**
	 * One-off class for interntal use. It's just a combination of
	 * {@link Named} and {@link Function} rolled into one class.
	 * 
	 * @author EPICI
	 *
	 * @param <T> input/argument/parameter type, see {@link Function}
	 * @param <R> output/result/return type, see {@link Function}
	 */
	protected static abstract class NamedFunction<T,R> implements Named, Function<T,R> {
		
		/**
		 * Name, used by {@link Named}.
		 */
		protected String name;
		
		@Override
		public String getName() {
			return name;
		}
		
		@Override
		public boolean setName(String newName) {
			name = newName;
			return true;
		}
		
	}
	
}
