package org.epici.wavelets.ui.curve.bezierspline;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Preferences;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.util.math.Floats;
import org.epici.wavelets.util.ui.Draw;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * The special command for when there is no command.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class GraphicalEditorCommandNone implements GraphicalEditorCommand {
	
	/**
	 * The parent editor.
	 */
	public GraphicalEditorPane parent;
	
	/**
	 * Mouse button being held down (0 = not pressed, 1 = left, 2 = right, 3 = middle)
	 */
	public int mouseDown;
	/**
	 * The mouse x, where it was last seen
	 */
	public int lastMousex;
	/**
	 * The mouse y, where it was last seen
	 */
	public int lastMousey;

	@Override
	public boolean mouseMove(Component component, int x, int y) {
		boolean didSomething = false;
		GraphicalEditorPane graphicalEditor = getEditorParent();
		int lastMousex = this.lastMousex;
		int lastMousey = this.lastMousey;
		this.lastMousex = x;
		this.lastMousey = y;
		boolean controlHeld = graphicalEditor.controlHeld();
		switch(mouseDown) {
		case 3:{// Middle mouse drag -> scroll or zoom
			if(controlHeld) {// axis zoom
				int dx = x - lastMousex;
				int dy = y - lastMousey;
				double xmul = Math.pow(2,  (1d/64)*dx);
				double ymul = Math.pow(2, -(1d/64)*dy);
				graphicalEditor.scalex = Floats.median(
						GraphicalEditorPane.SCALE_MIN,
						graphicalEditor.scalex*xmul,
						GraphicalEditorPane.SCALE_MAX
						);
				graphicalEditor.scaley = Floats.median(
						GraphicalEditorPane.SCALE_MIN,
						graphicalEditor.scaley*ymul,
						GraphicalEditorPane.SCALE_MAX
						);
			} else {// scroll
				graphicalEditor.anchorx -= (x-lastMousex) / graphicalEditor.scalex;
				graphicalEditor.anchory += (y-lastMousey) / graphicalEditor.scaley;
			}
			didSomething = true;
			break;
		}
		}
		if(didSomething) {
			graphicalEditor.repaint();
		}
		return didSomething;
	}

	@Override
	public void mouseOver(Component component) {
	}

	@Override
	public void mouseOut(Component component) {
	}

	@Override
	public boolean mouseDown(Component component, Mouse.Button button, int x, int y) {
		boolean didSomething = false;
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.requestFocus();
		mouseDown = PivotSwingUtils.getMouseButtonCode(button);
		if(mouseDown == 2) {// Right click select
			boolean shiftHeld = graphicalEditor.shiftHeld();
			boolean controlHeld = graphicalEditor.controlHeld();
			int bitOperator;
			if(shiftHeld & controlHeld) {
				bitOperator = GraphicalEditorCommandSelect.BIT_OPERATOR_INTERSECT;
			} else if (shiftHeld) {
				bitOperator = GraphicalEditorCommandSelect.BIT_OPERATOR_UNION;
			} else if (controlHeld) {
				bitOperator = GraphicalEditorCommandSelect.BIT_OPERATOR_DIFFERENCE;
			} else {
				bitOperator = GraphicalEditorCommandSelect.BIT_OPERATOR_SET;
			}
			GraphicalEditorCommandSelect newCommand = new GraphicalEditorCommandSelect(
					graphicalEditor,
					bitOperator
					);
			graphicalEditor.setCommand(newCommand, false);
			didSomething = true;
		}
		if(didSomething) {
			graphicalEditor.repaint();
		}
		return didSomething;
	}

	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		mouseDown = 0;
		return false;
	}

	@Override
	public boolean mouseClick(Component component, Mouse.Button button, int x, int y, int count) {
		return false;
	}

	@Override
	public boolean mouseWheel(Component component, Mouse.ScrollType scrollType, int scrollAmount, int wheelRotation, int x, int y) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		graphicalEditor.requestFocus();
		// don't use the alt key, it seems to defocus the whole thing and ruins our efforts
		//boolean altHeld = graphicalEditor.altHeld();
		boolean shiftHeld = graphicalEditor.shiftHeld();
		boolean controlHeld = graphicalEditor.controlHeld();
		if(shiftHeld|controlHeld) {// keys -> scroll
			double mul = 32*wheelRotation;
			if(!shiftHeld) {// (yes control) no shift -> do horizontal
				// mouse wheel down, scroll right, screen moves left
				graphicalEditor.anchorx += mul / graphicalEditor.scalex;
			}
			if(!controlHeld) {// (yes shift) no control -> do vertical
				// mouse wheel down, scroll down, screen moves up
				graphicalEditor.anchory -= mul / graphicalEditor.scaley;
			}
			// if both shift and control are held, Apache Pivot will zoom the window, we don't get the event. don't do that please
		} else {// no keys -> zoom
			double mul = Math.pow(2, (1d/16)*wheelRotation);
			graphicalEditor.scalex = Floats.median(
					GraphicalEditorPane.SCALE_MIN,
					graphicalEditor.scalex*mul,
					GraphicalEditorPane.SCALE_MAX
					);
			graphicalEditor.scaley = Floats.median(
					GraphicalEditorPane.SCALE_MIN,
					graphicalEditor.scaley*mul,
					GraphicalEditorPane.SCALE_MAX
					);
		}
		graphicalEditor.repaint();
		return true;// consume, because we always do something
	}

	@Override
	public boolean keyTyped(Component component, char character) {
		return false;
	}

	@Override
	public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		return false;
	}

	@Override
	public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
		boolean didSomething = false;
		GraphicalEditorPane graphicalEditor = getEditorParent();
		keycheck:if(!didSomething && mouseDown == 0) {// check for possible keybinds
			int keyModifiers = graphicalEditor.getKeyModifiers();
			int keyCombined = KeyTracker.combineKeyWithModifiers(keyCode, keyModifiers);
			Function<GraphicalEditorPane,GraphicalEditorCommand> commandGenerator = GraphicalEditorPane.commandMap.get(keyCombined);
			if(commandGenerator==null)break keycheck;
			GraphicalEditorCommand newCommand = commandGenerator.apply(graphicalEditor);
			if(newCommand==null)break keycheck;
			graphicalEditor.setCommand(newCommand, false);
			if(newCommand.isInstantCommand()) {// instant commands are immediately confirmed
				graphicalEditor.setCommand(null, true);
			}
			didSomething = true;
		}
		if(didSomething) {
			graphicalEditor.repaint();
		}
		return didSomething;
	}

	@Override
	public GraphicalEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof GraphicalEditorPane))return false;
		parent = (GraphicalEditorPane) newParent;
		return true;
	}

	@Override
	public boolean finalize(boolean confirm) {
		return true;
	}
	
	@Override
	public void reinit(GraphicalEditorCommand previous) {
		GraphicalEditorPane graphicalEditor = getEditorParent();
		mouseDown = graphicalEditor.mouseDown;
		lastMousex = graphicalEditor.lastMousex;
		lastMousey = graphicalEditor.lastMousey;
	}

	/**
	 * Width of the stroke used for curve rendering.
	 * Meant for internal use, but made public so that if another user
	 * really needs it, they can use it. It may be removed
	 * in a later version. Please don't rely on it.
	 */
	public static final double CURVE_WIDTH = 2;
	private static final double CURVE_POINT_RADIUS = CURVE_WIDTH*2;
	@Override
	public void paint(Graphics2D graphics) {
		// --- Fetch some data ---
		GraphicalEditorPane graphicalEditor = getEditorParent();
		Session session = graphicalEditor.getSession();
		CurveBezierSpline curve = graphicalEditor.getEditorData();
		int width = graphicalEditor.getWidth(), height = graphicalEditor.getHeight();
		final double diagonal = Math.hypot(width, height), logDiagonal = Math.log(diagonal);
		int textSize = graphicalEditor.textSize;
		double gridDiagonalExponentMin = Preferences.getFloatSafe(session, Preferences.INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MIN);
		double gridDiagonalExponentMax = Preferences.getFloatSafe(session, Preferences.INDEX_FLOAT_CURVE_BEZIER_SPLINE_EDITOR_GRID_DIAGONAL_MAX);
		// quietly fix mistakes, ensure min < max
		double[] gridDiagonalExponentFix = {
				0,
				Floats.D_EPSILON,
				gridDiagonalExponentMin,
				gridDiagonalExponentMax,
				1-Floats.D_EPSILON,
				1
				};
		Arrays.sort(gridDiagonalExponentFix);
		gridDiagonalExponentMin = gridDiagonalExponentFix[2];
		gridDiagonalExponentMax = gridDiagonalExponentFix[3];
		// Local variables are faster + inversion
		int lastMousex = this.lastMousex, lastMousey = this.lastMousey;
		double anchorx = graphicalEditor.anchorx, anchory = graphicalEditor.anchory,
				scalex = graphicalEditor.scalex, scaley = graphicalEditor.scaley,
				iscalex = 1d/scalex, iscaley = 1d/scaley,
				logscalex = Math.log(scalex), logscaley = Math.log(scaley),
				cornerx, cornery;
		// change from center to corners
		double halfWorldWidth  = 0.5 *  width * iscalex;
		double halfWorldHeight = 0.5 * height * iscaley;
		cornerx = anchorx + halfWorldWidth;// x of right edge
		cornery = anchory - halfWorldHeight;// y of bottom edge
		anchorx -= halfWorldWidth;// x of left edge
		anchory += halfWorldHeight;// y of top edge
		// get screen position of origin
		double originx = -anchorx*scalex, originy = anchory*scaley;
		// get pivot point
		int pivotCode = graphicalEditor.parent.getPivotOn();
		pivotCode = Math.max(1, pivotCode);// default to first
		double[] pivotXY = graphicalEditor.getPivotOnPoint(pivotCode);
		double pivotx = pivotXY[0],
				pivoty = pivotXY[1],
				spivotx = (pivotx - anchorx) * scalex,
				spivoty =-(pivoty - anchory) * scaley;
		// Get bounds
		double wx = anchorx+lastMousex*iscalex, wy = anchory-lastMousey*iscaley;
		final int curvePointCount = curve.getPointCount();
		int pfirst = curve.getPointInterval(anchorx-CURVE_POINT_RADIUS*iscalex), plast = curve.getPointInterval(cornerx+CURVE_POINT_RADIUS*iscalex);// Indices of points visible, first inclusive, last exclusive
		pfirst = Math.max(pfirst-2, 0);
		plast = Math.min(plast+2, curvePointCount);
		boolean pendfirst = pfirst == 0, pendlast = plast == curvePointCount;
		int prange = plast-pfirst;
		// Get curve points
		double[] pxs = new double[prange];
		double[] pys = new double[prange];
		curve.getPointsX(pxs, 0, pfirst, plast);
		curve.getPointsY(pys, 0, pfirst, plast);
		// --- Pre-draw ---
		Color colorBackground, colorBackgroundDark, colorLine, colorSelect, colorSelectFirst, colorCurve, colorPivot, colorWarn, colorText, colorTextAlpha;
		ColorScheme colors = Session.getColors(session);
		colorBackground = colors.background;
		colorLine = colors.line;
		colorSelect = colors.selected;
		colorCurve = colors.highlight;
		colorPivot = colors.marker;
		colorWarn = colors.warning;
		colorText = colors.text;
		colorBackgroundDark = ColorScheme.brighten(colorBackground, -0.1f);
		colorSelectFirst = ColorScheme.brighten(colorSelect, 0.2f);
		colorTextAlpha = ColorScheme.setAlpha(colorText, 170);
		// rendering hints
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// make strokes
		BasicStroke thinStroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		BasicStroke curveStroke = new BasicStroke((float)CURVE_WIDTH, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		// --- Draw ---
		// prepare rotated graphics
		Graphics2D graphicsRotated = (Graphics2D) graphics.create();
		AffineTransform at = graphicsRotated.getTransform();
		at.translate(0, height);
		at.quadrantRotate(-1);
		graphicsRotated.setTransform(at);
		// fill background
		graphics.setPaint(Draw.gradientVertical(colorBackground, colorBackgroundDark, 0, height));
		graphics.fillRect(0, 0, width, height);
		// prepare to make grid lines
		final double logGridSizeMin = logDiagonal * gridDiagonalExponentMin,
				logGridSizeMax = logDiagonal * gridDiagonalExponentMax,
				invLogGridSizeDiff = 1d/(logGridSizeMax - logGridSizeMin);
		Color transparentLine = ColorScheme.setAlpha(colorLine, 0);
		int gridExpOf10, gridExpOf10Verti, gridExpOf10Horiz;
		double logGridSize, gridSize, invGridSize, gridAlpha, gradientStart;
		BigDecimal worldPos, worldGridSize;
		boolean breakAtNext;
		graphics.setStroke(thinStroke);
		// make vertical grid lines
		for(gridExpOf10=(int)Math.floor(Math.log10(iscalex)),breakAtNext=false;!breakAtNext;gridExpOf10++) {
			logGridSize = gridExpOf10*Floats.LOG10+logscalex;
			gridAlpha = Floats.median(0, (logGridSize - logGridSizeMin)*invLogGridSizeDiff, 1);
			if(gridAlpha<=0)continue;
			if(gridAlpha>=1)breakAtNext = true;
			gridSize = Math.exp(logGridSize);
			invGridSize = 1d/gridSize;
			gradientStart = originx % gridSize;
			Color translucentLine = ColorScheme.setAlpha(colorLine, (int)Math.round(255*gridAlpha));
			LinearGradientPaint paint = new LinearGradientPaint(
					(float)gradientStart,
					0,
					(float)(gradientStart+gridSize),
					0,
					new float[]{0,(float)invGridSize,(float)(invGridSize+Floats.F_EPSILON),1},
					new Color[]{translucentLine,translucentLine,transparentLine,transparentLine},
					MultipleGradientPaint.CycleMethod.REPEAT
					);
			graphics.setPaint(paint);
			graphics.fillRect(0, 0, width, height);
		}
		gridExpOf10Verti = gridExpOf10 - 2;
		// make horizontal grid lines
		for(gridExpOf10=(int)Math.floor(Math.log10(iscaley)),breakAtNext=false;!breakAtNext;gridExpOf10++) {
			logGridSize = gridExpOf10*Floats.LOG10+logscaley;
			gridAlpha = Floats.median(0, (logGridSize - logGridSizeMin)*invLogGridSizeDiff, 1);
			if(gridAlpha<=0)continue;
			if(gridAlpha>=1)breakAtNext = true;
			gridSize = Math.exp(logGridSize);
			invGridSize = 1d/gridSize;
			gradientStart = originy % gridSize;
			Color translucentLine = ColorScheme.setAlpha(colorLine, (int)Math.round(255*gridAlpha));
			LinearGradientPaint paint = new LinearGradientPaint(
					0,
					(float)gradientStart,
					0,
					(float)(gradientStart+gridSize),
					new float[]{0,(float)invGridSize,(float)(invGridSize+Floats.F_EPSILON),1},
					new Color[]{translucentLine,translucentLine,transparentLine,transparentLine},
					MultipleGradientPaint.CycleMethod.REPEAT
					);
			graphics.setPaint(paint);
			graphics.fillRect(0, 0, width, height);
		}
		gridExpOf10Horiz = gridExpOf10 - 2;
		// prepare to make grid labels
		double gridLabelPad = textSize*0.5;
		graphics.setColor(colorTextAlpha);
		graphicsRotated.setColor(colorTextAlpha);
		// make top and bottom vertical grid labels
		gridExpOf10 = gridExpOf10Verti;
		logGridSize = gridExpOf10*Floats.LOG10+logscalex;
		gridSize = Math.exp(logGridSize);
		worldGridSize = BigDecimal.ONE.scaleByPowerOfTen(gridExpOf10);
		worldPos = BigDecimal.valueOf(
				Math.rint(anchorx / Math.pow(10, gridExpOf10))
				).scaleByPowerOfTen(gridExpOf10);
		gradientStart = (worldPos.doubleValue()-anchorx)*scalex;
		for(
				breakAtNext=false;
				!breakAtNext;
				gradientStart += gridSize,
						worldPos = worldPos.add(worldGridSize)
				) {
			if(gradientStart>width)breakAtNext=true;
			Draw.drawDecimal(graphicsRotated, worldPos, gridLabelPad, gradientStart-textSize*0.5, height-gridLabelPad*2, textSize, 0, 0.5);
			Draw.drawDecimal(graphicsRotated, worldPos, gridLabelPad, gradientStart-textSize*0.5, height-gridLabelPad*2, textSize, 1, 0.5);
		}
		// make left and right horizontal grid labels
		gridExpOf10 = gridExpOf10Horiz;
		logGridSize = gridExpOf10*Floats.LOG10+logscaley;
		gridSize = Math.exp(logGridSize);
		worldGridSize = BigDecimal.ONE.scaleByPowerOfTen(gridExpOf10);
		worldPos = BigDecimal.valueOf(
				Math.rint(cornery / Math.pow(10, gridExpOf10))
				).scaleByPowerOfTen(gridExpOf10);
		gradientStart = (-worldPos.doubleValue()+anchory)*scaley;
		for(
				breakAtNext=false;
				!breakAtNext;
				gradientStart -= gridSize,
						worldPos = worldPos.add(worldGridSize)
				) {
			if(gradientStart>width)breakAtNext=true;
			if(gradientStart<0)breakAtNext=true;
			Draw.drawDecimal(graphics, worldPos, gridLabelPad, gradientStart-textSize*0.5, width-gridLabelPad*2, textSize, 0, 0.5);
			Draw.drawDecimal(graphics, worldPos, gridLabelPad, gradientStart-textSize*0.5, width-gridLabelPad*2, textSize, 1, 0.5);
		}
		// draw the curve
		graphics.setColor(colorCurve);
		graphics.setStroke(curveStroke);
		if(curvePointCount <= 1) {// 0 point/1 point -> constant 0 or point
			final double worldConstantY = curvePointCount==0?0:curve.getPointY(0);
			final double constantY = (-worldConstantY+anchory)*scaley;
			graphics.draw(new Line2D.Double(0, constantY, width, constantY));
		} else {// at least 2 points -> can curve
			Path2D.Double curvePath = new Path2D.Double();
			// draw the first line segment, or nothing
			if(pendfirst) {// need to extend first
				double x3=pxs[0],
						y3=pys[0],
						x4=pxs[1],
						y4=pys[1];
				double sx3 = (x3-anchorx)*scalex,
						sy3 =-(y3-anchory)*scaley;
				double sd = -(y4-y3)/(x4-x3)*scaley*iscalex;
				double sx2 = 0,
						sy2 = sy3+(sx2-sx3)*sd;
				curvePath.moveTo(
						sx2,
						sy2
						);
				curvePath.lineTo(
						sx3,
						sy3
						);
			} else {// nothing to extend
				double x2=pxs[1],
						y2=pys[1];
				double sx2 = (x2-anchorx)*scalex,
						sy2 =-(y2-anchory)*scaley;
				curvePath.moveTo(
						sx2,
						sy2
						);
			}
			// draw the main curve segments
			int ipfirst = pfirst+(pendfirst?1:2);
			int iplast = plast-1;// change to inclusive bound
			for(int i3=ipfirst;i3<=iplast;i3++) {
				int i2 = i3 - 1;
				double x3=pxs[i3-pfirst],
						y3=pys[i3-pfirst],
						x2=pxs[i2-pfirst],
						y2=pys[i2-pfirst];
				double x1,y1,x4,y4;
				if(i2==0) {// first point -> use itself and next
					x1 = x2;
					y1 = y2;
				} else {// not first point -> use previous and next
					int i1 = i2-1;
					x1 = pxs[i1-pfirst];
					y1 = pys[i1-pfirst];
				}
				if(i3>=iplast) {// last point -> use previous and itself
					x4 = x3;
					y4 = y3;
				} else {// not last point -> use previous and next
					int i4 = i3+1;
					x4 = pxs[i4-pfirst];
					y4 = pys[i4-pfirst];
				}
				double dx=(x3-x2)*(1d/3),
						d2=(y3-y1)/(x3-x1)*dx,
						d3=(y4-y2)/(x4-x2)*dx;
				double sx2 = (x2-anchorx)*scalex,
						sy2 =-(y2-anchory)*scaley,
						sx3 = (x3-anchorx)*scalex,
						sy3 =-(y3-anchory)*scaley,
						sdx = dx*scalex,
						sd2 =-d2*scaley,
						sd3 =-d3*scaley;
				curvePath.curveTo(
						sx2+sdx,
						sy2+sd2,
						sx3-sdx,
						sy3-sd3,
						sx3,
						sy3
						);
			}
			// draw the last line segment, or nothing
			if(pendlast) {// need to extend last
				double x1=pxs[prange-2],
						y1=pys[prange-2],
						x2=pxs[prange-1],
						y2=pys[prange-1];
				double sx2 = (x2-anchorx)*scalex,
						sy2 =-(y2-anchory)*scaley;
				double sd = -(y2-y1)/(x2-x1)*scaley*iscalex;
				double sx3 = width,
						sy3 = sy2+(sx3-sx2)*sd;
				curvePath.lineTo(
						sx3,
						sy3
						);
			}
			// finally, draw the path
			graphics.draw(curvePath);
		}
		// draw the control points
		for(int pindex=pfirst;pindex<plast;pindex++) {
			// get position in world
			double wpx = curve.getPointX(pindex);
			double wpy = curve.getPointY(pindex);
			double incwpx = Math.ulp(wpx);
			double incwpy = Math.ulp(wpy);
			// get position on screen
			double spx = (wpx-anchorx)*scalex;
			double spy =-(wpy-anchory)*scaley;
			// get modifier flags
			boolean selected = graphicalEditor.selection.contains(wpx);
			boolean selectedFirst = selected && graphicalEditor.selection.get(0)==wpx;
			boolean tooSmall = Math.max(incwpx*scalex, incwpy*scaley)>=0x1.0p-52/Floats.D_EPSILON;// too zoomed in, curve does not like things so close
			boolean tooBig = Math.max(iscalex/incwpx, iscaley/incwpy)>=0x1.0p+52;// too zoomed out, may lose fine detail
			boolean warn = tooSmall | tooBig;
			// pick a color to draw with
			// priority: warning > first selected > selected at all > everything else
			Color colorPoint = warn?colorWarn:selected?(selectedFirst?colorSelectFirst:colorSelect):colorCurve;
			graphics.setColor(colorPoint);
			// draw the circle
			graphics.draw(new Ellipse2D.Double(spx-CURVE_POINT_RADIUS, spy-CURVE_POINT_RADIUS, CURVE_POINT_RADIUS*2, CURVE_POINT_RADIUS*2));
		}
		// draw the pivot location
		graphics.setStroke(thinStroke);
		graphics.draw(new Line2D.Double(spivotx, 0, spivotx, height));
		graphics.draw(new Line2D.Double(0, spivoty, width, spivoty));
		// set the stroke back for the next user
		graphics.setStroke(curveStroke);
	}

}
