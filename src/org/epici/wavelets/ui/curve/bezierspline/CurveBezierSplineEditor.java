package org.epici.wavelets.ui.curve.bezierspline;

import java.net.URL;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.*;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurveBezierSpline;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.SessionLinked;
import org.epici.wavelets.ui.curve.CurveEditor;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * Editor for {@link CurveBezierSpline}. Consists of a graphic editor
 * (instance of {@link GraphicalEditorPane})
 * and a toolbar at the bottom.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public class CurveBezierSplineEditor extends FillPane implements Bindable, DataEditor.Instance<CurveBezierSpline, CurveEditor.LinkedEditorPane>, SessionLinked {

	/**
	 * The parent editor.
	 */
	public CurveEditor.LinkedEditorPane parent;
	/**
	 * The curve being edited.
	 */
	public CurveBezierSpline view;
	/**
	 * The current session.
	 */
	public Session session;
	
	/**
	 * The dedicated graphical editor pane.
	 */
	public GraphicalEditorPane graphicalEditor;
	/**
	 * The pane that will hold {@link #graphicalEditor}.
	 */
	public FillPane fillGEP;
	
	/**
	 * Collection of tools.
	 */
	public FillPane fillTools;
	/**
	 * The button group for selecting the method of determining the
	 * pivot points for operations.
	 */
	public ButtonGroup pivotOnWhat;
	/**
	 * Button for the option to pivot on
	 * the first selected point.
	 */
	public PushButton pivotOnFirst;
	/**
	 * Button for the option to pivot on
	 * the median of the selected points.
	 */
	public PushButton pivotOnMedian;
	/**
	 * Button for the option to pivot on
	 * the viewport anchor point.
	 */
	public PushButton pivotOnAnchor;
	
	/**
	 * Blank constructor, which Pivot will use.
	 */
	public CurveBezierSplineEditor(){
		super();
	}
	
	/**
	 * Convenience method to create a new one from outside
	 * 
	 * @param curve the curve to be edited
	 * 
	 * @return a new editor instance
	 */
	public static CurveBezierSplineEditor createNew(CurveBezierSpline curve){
		CurveBezierSplineEditor result = PivotSwingUtils.loadBxml(CurveBezierSplineEditor.class, "curveBezierSplineEditor.bxml");
		result.view = curve;
		return result;
	}
	
	@Override
	public CurveBezierSpline getEditorData() {
		return view;
	}

	@Override
	public CurveEditor.LinkedEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof CurveEditor.LinkedEditorPane))return false;
		parent = (CurveEditor.LinkedEditorPane) newParent;
		return true;
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		fillGEP = (FillPane) namespace.get("fillGEP");
		graphicalEditor = (GraphicalEditorPane) namespace.get("graphicalEditor");
		fillTools = (FillPane) namespace.get("fillTools");
		
		pivotOnWhat = (ButtonGroup) namespace.get("pivotOnWhat");
		pivotOnFirst = (PushButton) namespace.get("pivotOnFirst");
		pivotOnMedian = (PushButton) namespace.get("pivotOnMedian");
		pivotOnAnchor = (PushButton) namespace.get("pivotOnAnchor");
		
		ButtonPressListener onPressRepaintEditors = (Button button)->{
			graphicalEditor.repaint();
			graphicalEditor.requestFocus();// return focus
			};
		
		for(Button button:new Button[]{
				pivotOnFirst,
				pivotOnMedian,
				pivotOnAnchor,
		}) {
			button.getButtonPressListeners().add(onPressRepaintEditors);
		}
	}
	
	@Override
	public void init(){
		session = getEditorParent().getEditorParent().getSession();
		
		graphicalEditor.parent = this;
		graphicalEditor.init();
	}
	
	/**
	 * What should operations pivot on?
	 * <br>
	 * This is a comparatively expensive operation, so please cache the result.
	 * However, do not use a long term cache, since the user may select a different mode.
	 * <br>
	 * <table>
	 * <tr>
	 *   <th>Return value</th>
	 *   <th>Short name</th>
	 *   <th>Description</th>
	 * </tr>
	 * <tr>
	 *   <td>0</td>
	 *   <td>Unknown</td>
	 *   <td>Use some default. This shouldn't happen normally.</td>
	 * </tr>
	 * <tr>
	 *   <td>1</td>
	 *   <td>First</td>
	 *   <td>First (index 0) of selected points, otherwise the origin.</td>
	 * </tr>
	 * <tr>
	 *   <td>2</td>
	 *   <td>Median</td>
	 *   <td>Average of selected points, otherwise the origin.</td>
	 * </tr>
	 * <tr>
	 *   <td>3</td>
	 *   <td>Anchor</td>
	 *   <td>The anchor point of the viewport, which is currently always in the center.</td>
	 * </tr>
	 * </table>
	 * 
	 * @return code for what to pivot on
	 */
	public int getPivotOn() {
		Button selected = pivotOnWhat.getSelection();
		if(selected == pivotOnFirst)return 1;
		if(selected == pivotOnMedian)return 2;
		if(selected == pivotOnAnchor)return 3;
		return 0;
	}

	@Override
	public Session getSession() {
		return session;
	}
	
}
