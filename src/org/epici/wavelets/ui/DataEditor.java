package org.epici.wavelets.ui;

import java.net.URL;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.collections.Map;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.*;
import org.epici.wavelets.core.Named;
import org.epici.wavelets.core.Session;

/**
 * Interface implemented by editors of some data.
 * 
 * @author EPICI
 * @version 1.0
 *
 * @param <T> the type of data this edits.
 */
public interface DataEditor<T> {

	/**
	 * Add the interface for this instance if it isn't already present.
	 * Returns true if it was added, in which case it wasn't already present.
	 * 
	 * @param data object to add editor interface for
	 * @return true if it was successfully added
	 */
	public boolean addEditorData(T data);
	
	/**
	 * Like {@link #addEditorData(Object)}, but used when the generic type
	 * is not known. Will always fail on a bad type.
	 * 
	 * @param data object to add editor interface for
	 * @return true if it was successfully added
	 */
	public default boolean addEditorDataCasted(Object data){
		return addEditorData((T)data);
	}
	
	/**
	 * Call this to initialize the component after setting fields.
	 * Should come after {@link Bindable#initialize(Map, URL, Resources)}.
	 */
	public default void init(){}
	
	/**
	 * Interface for things with their own editor and previews.
	 * This should be used for general classes that can have various
	 * implementations, so that even if the implementations each have
	 * their own unique editors, all those editors can be bundled together.
	 * Usually this is done to abstract away some of the editor functionality
	 * to some general code.
	 * 
	 * @author EPICI
	 *
	 * @param <U> component type for editor
	 * @param <V> component type for preview
	 */
	public static interface Editable<U extends Component,V extends Component>{
		
		/**
		 * Get the editor component. This should include
		 * everything needed to edit this instance and only
		 * this instance.
		 * 
		 * @return editor component
		 */
		public U getEditor();
		
		/**
		 * Get the preview component. This should be a small
		 * lightweight component that shows what this instance is,
		 * and should fit in nicely with other components.
		 * 
		 * @return previe component
		 */
		public V getPreview();
		
	}
	
	/**
	 * Base class for data editor implementations which
	 * assign to each tab an instance to edit,
	 * and have its editor in that tab.
	 * 
	 * @author EPICI
	 * @version 1.0
	 *
	 * @param <T> see documentation for {@link DataEditor}
	 */
	public static abstract class Tabbed<T> extends Window implements DataEditor<T>, Bindable, SessionLinked {
		
		/**
		 * The current/linked session
		 */
		public Session session;
		/**
		 * The tab pane to switch between tracks
		 */
		public TabPane tabPane;
		
		public boolean addEditorData(T data){
			TabPane.TabSequence tabs = tabPane.getTabs();
			for(Component component:tabs){
				if(component instanceof Instance<?, ?>){
					Instance<?, ?> ieditor = (Instance<?, ?>) component;
					Object other = ieditor.getEditorData();
					if(
							// different name may have same data but must be treated as separate
							(data instanceof Named)
							&& (other instanceof Named)
							? ((Named)data).getName().equals(((Named)other).getName())
							// finally, do standard equality test
							: data.equals(other)
							)return false;
				}
			}
			addNewEditorData(data);
			return true;
		}
		
		/**
	     * Called to initialize the class after it has been completely
	     * processed and bound by the serializer.
	     * <br>
	     * Notably for the purposes of {@link DataEditor.Tabbed},
	     * it sets {@link #tabPane}.
	     *
	     * @param namespace
	     * The serializer's namespace. The bindable object can use this to extract named
	     * values defined in the BXML file. Alternatively, the {@link org.apache.pivot.beans.BXML &#64;BXML} annotation
	     * can be used by trusted code to automatically map namespace values to member
	     * variables.
	     *
	     * @param location
	     * The location of the BXML source. May be <tt>null</tt> if the location of the
	     * source is not known.
	     *
	     * @param resources
	     * The resources that were used to localize the deserialized content. May be
	     * <tt>null</tt> if no resources were specified.
	     */
		@Override
		public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
			tabPane = (TabPane) namespace.get("tabPane");
		}
		
		/**
		 * For internal use only: add the editor for a new instance regardless of whether
		 * it is already in {@link #tabPane}.
		 * 
		 * @param data object to edit
		 */
		protected abstract void addNewEditorData(T data);
		
		/**
		 * Call after fields are initialized.
		 * {@link #session} should have been set externally.
		 * {@link #tabPane} should be set by
		 * {@link #initialize(org.apache.pivot.collections.Map, java.net.URL, org.apache.pivot.util.Resources)}.
		 */
		public void init(){
			
		}
		
		@Override
		public Session getSession() {
			return session;
		}
		
	}
	
	/**
	 * Interface implemented by editors of a single instance.
	 * 
	 * @author EPICI
	 * @version 1.0
	 *
	 * @param <T> see documentation for {@link DataEditor}.
	 * @param <P> parent component type
	 */
	public static interface Instance<T,P extends Component>{
		
		/**
		 * Get the instance which is being edited by this editor.
		 * 
		 * @return object which this edits
		 */
		public T getEditorData();
		
		/**
		 * Get the current parent.
		 * <br><br>
		 * Method name is extra verbose to not be confused with
		 * {@link Component#getParent()}.
		 * 
		 * @return this editor's parent.
		 */
		public P getEditorParent();
		
		/**
		 * Set the current parent. May be rejected if
		 * it is the wrong type. Return true on success.
		 * <br><br>
		 * Method name is extra verbose to not be confused with
		 * {@link Component#setParent(Container)}.
		 * 
		 * @param newParent new editor parent to assign.
		 * @return true if successful.
		 */
		public boolean setEditorParent(Component newParent);
		
		/**
		 * Call this to initialize the component after setting fields.
		 * Should come after {@link Bindable#initialize(Map, URL, Resources)}.
		 */
		public default void init(){}
		
	}

}
