package org.epici.wavelets.ui.synth;

import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.concurrent.ConcurrentMap;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.collections.Map;
import org.apache.pivot.util.Resources;
import org.apache.pivot.util.Vote;
import org.apache.pivot.wtk.Action;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.ComponentKeyListener;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Menu;
import org.apache.pivot.wtk.MenuBar;
import org.apache.pivot.wtk.TabPane;
import org.apache.pivot.wtk.TabPaneSelectionListener;
import org.apache.pivot.wtk.TablePane;
import org.apache.pivot.wtk.TextInput;
import org.apache.pivot.wtk.content.ButtonData;
import org.epici.wavelets.core.Project;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.Synthesizer;
import org.epici.wavelets.core.synth.SynthPrimitives;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.util.ui.PivotSwingUtils;
import com.google.common.collect.MapMaker;

public class SynthEditor extends DataEditor.Tabbed<Synthesizer>  {
	
	/**
	 * Map which associates components inside the editor
	 * with their respective parent. Useful because the parent
	 * editor is not always reachable by a container search.
	 * <br><br>
	 * Both the keys and values are weak so that when something
	 * is no longer used it doesn't eat up memory.
	 */
	protected static final ConcurrentMap<Component, SynthEditor> parentMap = new MapMaker().weakKeys().weakValues().makeMap();
	
	/**
	 * Put a mapping in {@link #parentMap}.
	 * 
	 * @param key child component
	 * @param value parent of component
	 */
	public static void putParent(Component key, SynthEditor value){
		parentMap.put(key, value);
	}
	
	/**
	 * Get a mapping from {@link #parentMap}.
	 * 
	 * @param key child component
	 * @return associated parent of component, or null if no mapping exists
	 */
	public static SynthEditor getParent(Component key){
		return parentMap.get(key);
	}
	
	/**
	 * Search for a {@link SynthEditor} which is the component
	 * or related to it. Will check mappings with {@link #getParent(Component)}
	 * and look through the hierarchy with {@link Component#getParent()}.
	 * Returns null only on failure.
	 * 
	 * @param source component to search with
	 * @return parent if found, otherwise null
	 */
	public static SynthEditor getParentSearch(Component source){
		SynthEditor root = null;
		while(source != null){
			if(source instanceof SynthEditor){// it may itself be the root
				root = (SynthEditor) source;
				break;
			}else if((root = SynthEditor.getParent(source)) != null){// it may have a mapping
				break;
			}
			source = source.getParent();
		}
		return root;
	}
	
	/**
	 * The outer table pane used to divide up the space.
	 */
	public TablePane outerTablePane;
	/**
	 * The table pane at the top which separates the menu from
	 * the things that go with the menu but aren't actually part
	 * of the menu (by Pivot logic).
	 */
	public TablePane innerTablePane;
	/**
	 * The top menu. Contains all common functions.
	 */
	public MenuBar menuBar;
	/**
	 * The text field where the user enters the new name to give to the synthesizer.
	 */
	public TextInput renameInput;
	/**
	 * The combined listener used by {@link #renameInput}. 
	 */
	public RenameInputListener renameInputListener;
	/**
	 * The menu item which contains basic options for managing synthesizers.
	 */
	public MenuBar.Item editButton;
	/**
	 * The menu item which on click begins the rename operation.
	 */
	public Menu.Item renameButton;
	/**
	 * The menu item which on click puts the synthesizer on the {@link Session#clipBoard}.
	 */
	public Menu.Item copyButton;
	/**
	 * The menu item which on click tries to bring a synthesizer on the clipboard
	 * into the editor.
	 */
	public Menu.Item importButton;
	/**
	 * The menu item which contains options for creating new synthesizers.
	 */
	public MenuBar.Item newButton;
	/**
	 * The menu item which on click creates a new {@link SynthPrimitives}
	 * and opens an editor for it.
	 */
	public Menu.Item newPrimitivesButton;
	
	/**
	 * Standard no arguments constructor.
	 */
	public SynthEditor() {
		super();
	}
	
	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		super.initialize(namespace, location, resources);
		outerTablePane = (TablePane) namespace.get("outerTablePane");
		innerTablePane = (TablePane) namespace.get("innerTablePane");
		menuBar = (MenuBar) namespace.get("menuBar");
		renameInput = (TextInput) namespace.get("renameInput");
		editButton = (MenuBar.Item) namespace.get("editButton");
		renameButton = (Menu.Item) namespace.get("renameButton");
		copyButton = (Menu.Item) namespace.get("copyButton");
		importButton = (Menu.Item) namespace.get("importButton");
		newButton = (MenuBar.Item) namespace.get("newButton");
		newPrimitivesButton = (Menu.Item) namespace.get("newPrimitivesButton");
	}
	
	@Override
	public void init(){
		super.init();
		
		// put menu items in parent map
		SynthEditor.putParent(editButton, this);
		SynthEditor.putParent(copyButton, this);
		SynthEditor.putParent(importButton, this);
		SynthEditor.putParent(renameButton, this);
		SynthEditor.putParent(newButton, this);
		SynthEditor.putParent(newPrimitivesButton, this);
		
		// disable everything at first
		renameButton.getAction().setEnabled(false);
		copyButton.getAction().setEnabled(false);
		renameInput.setEnabled(false);
		
		// add tab switch listener
		tabPane.getTabPaneSelectionListeners().add(new TabSwitchListener(this));
		
		// add rename field listener
		renameInputListener = new RenameInputListener(this);
		renameInput.getComponentKeyListeners().add(renameInputListener);
	}
	
	/**
	 * Convenience method to create a new one from outside
	 * 
	 * @return
	 */
	public static SynthEditor createNew(){
		// need to put actions in map before loading BXML
		addActionsToMap();
		// do the actual BXML load
		return PivotSwingUtils.loadBxml(SynthEditor.class, "synthEditor.bxml");
	}
	
	/**
	 * Get the project currently being edited.
	 * 
	 * @return
	 */
	public Project getProject(){
		return session.project;
	}

	@Override
	protected void addNewEditorData(Synthesizer synth) {
		try{
			TabPane.TabSequence tabs = tabPane.getTabs();
			LinkedEditorPane linked = LinkedEditorPane.createNew();
			linked.setEditorParent(this);
			linked.view = synth;
			linked.init();
			tabs.add(linked);
		}catch(NullPointerException exception){
			exception.printStackTrace();
		}
	}
	
	@Override
	public boolean addEditorDataCasted(Object data){
		if(!(data instanceof Synthesizer))return false;
		return addEditorData((Synthesizer)data);
	}
	
	/**
	 * Call this on beginning a rename operation.
	 * 
	 * @return true if successful (no issues), false if something went wrong
	 */
	public boolean renameToTextView(){
		Component tab = tabPane.getSelectedTab();
		if(tab == null)return false;
		LinkedEditorPane editorPane = (LinkedEditorPane) tab;
		Synthesizer synth = editorPane.view;
		renameButton.getAction().setEnabled(false);
		renameInput.setEnabled(true);
		renameInput.setText(synth.getName());
		renameInput.selectAll();
		renameInput.requestFocus();
		return true;
	}
	
	/**
	 * Call this on ending a rename operation.
	 * <br>
	 * On cancel, no data is changed, so it cannot fail.
	 * 
	 * @param confirm true to confirm, false to cancel
	 * 
	 * @return true if successful (no issues), false if something went wrong
	 */
	public boolean renameToButtonView(boolean confirm){
		if(confirm){
			Component tab = tabPane.getSelectedTab();
			if(tab == null)return false;
			LinkedEditorPane editorPane = (LinkedEditorPane) tab;
			Synthesizer synth = editorPane.view;
			String newName = session.project.synths.rename(synth.getName(), renameInput.getText(), session);
			if(newName == null)return false;// failed to set name
			editorPane.tabName.setText(newName);
		}
		renameButton.getAction().setEnabled(true);
		renameInput.setEnabled(false);
		renameInput.setText("");
		return true;
	}
	
	/**
	 * Attempt to copy the current synth onto the clipboard.
	 * 
	 * @return true if successful
	 */
	public boolean copyCurrentSynth(){
		Component tab = tabPane.getSelectedTab();
		if(tab == null)return false;
		LinkedEditorPane editorPane = (LinkedEditorPane) tab;
		Synthesizer synth = editorPane.view;
		session.setClipBoard(synth);
		return true;
	}
	
	/**
	 * Puts named actions into the static map.
	 * This must happen before BXML loading,
	 * so it is called statically here.
	 */
	public static void addActionsToMap(){
		Action.NamedActionDictionary actions = Action.getNamedActions();
		
		// all menu buttons have their own action
		if(!actions.containsKey("synthRename"))actions.put("synthRename", new ActionRename());
		if(!actions.containsKey("synthCopy"))actions.put("synthCopy", new ActionCopy());
		if(!actions.containsKey("synthImport"))actions.put("synthImport", new ActionImport());
		if(!actions.containsKey("synthNewPrimitives"))actions.put("synthNewPrimitives", new ActionNewPrimitives());
	}
	
	/**
	 * Listens for switching to a different tab and updates the interface
	 * accordingly when that happens
	 * 
	 * @author EPICI
	 * @version 1.0
	 */
	public static class TabSwitchListener implements TabPaneSelectionListener{
		
		/**
		 * Remember the parent, other data can be derived from here
		 */
		public SynthEditor parent;
		
		/**
		 * Standard constructor
		 * 
		 * @param parent
		 */
		public TabSwitchListener(SynthEditor parent){
			this.parent = parent;
		}
		
		@Override
		public Vote previewSelectedIndexChange(TabPane tabPane, int selectedIndex){
			// approve means nothing changes, so it's the default action
			return Vote.APPROVE;
		}
		
		@Override
		public void selectedIndexChangeVetoed(TabPane tabPane, Vote reason){
			// didn't switch tabs, so do nothing
		}
		
		@Override
		public void selectedIndexChanged(TabPane tabPane, int previousSelectedIndex){
			// get the current tab
			int currentSelectedIndex = tabPane.getSelectedIndex();
			if(currentSelectedIndex<0){
				// negative index -> no selection
				parent.renameButton.getAction().setEnabled(false);
				parent.copyButton.getAction().setEnabled(false);
				parent.renameInput.setEnabled(false);
				return;
			}
			TabPane.TabSequence tabs = tabPane.getTabs();
			LinkedEditorPane editor = (LinkedEditorPane) tabs.get(currentSelectedIndex);
			// nothing to do with inner editor, for now
			// update menu bar
			parent.renameToButtonView(false);
			parent.copyButton.getAction().setEnabled(true);
		}
		
	}
	
	/**
	 * Listener for the rename text input field, {@link #renameInput}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.1
	 */
	public static class RenameInputListener implements ComponentKeyListener{
		
		/**
		 * The parent, this instance listens to the parent's rename field
		 */
		public SynthEditor parent;
		
		/**
		 * Standard constructor with parent.
		 * 
		 * @param parent the parent editor.
		 */
		public RenameInputListener(SynthEditor parent){
			this.parent = parent;
		}

		// Component key events
	    @Override
	    public boolean keyTyped(Component componentArgument, char character) {
	        return false;
	    }

	    @Override
	    public boolean keyPressed(Component componentArgument, int keyCode, Keyboard.KeyLocation keyLocation) {
	    	return false;
	    }
	    
	    @Override
	    public boolean keyReleased(Component componentArgument, int keyCode, Keyboard.KeyLocation keyLocation) {
	        boolean didSomething = false;// did it do something?
	    	switch(keyCode){
	        case KeyEvent.VK_ENTER:{
	        	// Enter -> confirm changes
	        	parent.renameToButtonView(true);
	        	didSomething = true;
	        	break;
	        }
	        case KeyEvent.VK_ESCAPE:{
	        	// Escape -> discard changes
	        	parent.renameToButtonView(false);
	        	didSomething = true;
	        	break;
	        }
	        }
	    	return didSomething;// consume if it did something
	    }
		
	}
	
	/**
	 * Action triggered by the rename button on the menu bar.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.1
	 */
	public static class ActionRename extends Action {
		
		@Override
		public void perform(Component source) {
			SynthEditor root = SynthEditor.getParentSearch(source);
			if(root == null)return;
			root.renameToTextView();
		}
		
	}
	
	/**
	 * Action triggered by the copy button on the menu bar.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.1
	 */
	public static class ActionCopy extends Action {
		
		@Override
		public void perform(Component source) {
			SynthEditor root = SynthEditor.getParentSearch(source);
			if(root == null)return;
			root.copyCurrentSynth();
		}
		
	}
	
	/**
	 * Action triggered by the import button on the menu bar.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.1
	 */
	public static class ActionImport extends Action {
		
		@Override
		public void perform(Component source) {
			SynthEditor root = SynthEditor.getParentSearch(source);
			if(root == null)return;
			Object oclipboard = root.getSession().getClipBoard();
			if(!(oclipboard instanceof Synthesizer))return;
			Synthesizer clipboard = (Synthesizer) oclipboard;
			root.addEditorData(clipboard);
		}
		
	}
	
	/**
	 * Action triggered by the constant button on the create new menu.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ActionNewPrimitives extends Action {

		@Override
		public void perform(Component source) {
			SynthEditor root = SynthEditor.getParentSearch(source);
			if(root == null)return;
			Session session = root.session;
			Project project = session.project;
			SynthPrimitives synth = SynthPrimitives.makeDefaultSynth(session);
			project.addSynth(synth.getName(), synth, null);
			root.addEditorData(synth);
		}
		
	}
	
	/**
	 * Wrapper for the editor interface of an individual {@link Synthesizer}.
	 * Provides some common things in a top menu bar and leaves the rest
	 * to the instance.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.1
	 */
	public static class LinkedEditorPane extends FillPane implements Bindable, DataEditor.Instance<Synthesizer, SynthEditor>{

		/**
		 * The parent {@link SynthEditor}
		 */
		protected SynthEditor parent;
		/**
		 * The {@link Synthesizer} for which this instance
		 * wraps the UI
		 */
		public Synthesizer view;
		
		/**
		 * The tab name
		 */
		public ButtonData tabName;
		/**
		 * The fill pane which will contain the instance's editor
		 * interface, if it exists.
		 */
		public FillPane fillPane;
		/**
		 * Editor interface supplied by the component. Null only if the component
		 * failed to supply an editor.
		 * <br>
		 * Most things require that it also be an instance of
		 * {@link DataEditor.Instance}. This allows parents
		 * to pass useful data on to the children.
		 */
		public Component innerEditor;

		@Override
		public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
			tabName = (ButtonData) namespace.get("tabName");
			fillPane = (FillPane) namespace.get("fillPane");
		}
		
		/**
		 * Initialize, called after setting fields
		 */
		public void init(){
			tabName.setText(view.getName());
			
			trySetInnerEditor();
		}
		
		/**
		 * Convenience method to create a new one from outside
		 * 
		 * @return a new editor wrapper instance
		 */
		public static LinkedEditorPane createNew(){
			return PivotSwingUtils.loadBxml(LinkedEditorPane.class, "synthEditorPane.bxml");
		}
		
		/**
		 * If {@link #innerEditor} is not set, tries to set it.
		 * Returns true if this method sets it.
		 * If it returns false, either it was already set or it
		 * could not be set.
		 * 
		 * @return
		 */
		public boolean trySetInnerEditor(){
			// already exists
			if(innerEditor != null)return false;
			// need this interface to fetch UI
			if(view instanceof DataEditor.Editable<?, ?>){
				DataEditor.Editable<?, ?> viewEditable = (DataEditor.Editable<?, ?>) view;
				// set the editor to whatever it returns
				innerEditor = viewEditable.getEditor();
				if(innerEditor == null)return false;// oh no, it failed to supply it
				// most things require implementing this
				if(innerEditor instanceof DataEditor.Instance<?, ?>){
					DataEditor.Instance<?, ?> innerEditorInstance = (DataEditor.Instance<?, ?>) innerEditor;
					// if the parent is accepted, proceed with initialization
					if(innerEditorInstance.setEditorParent(this)){
						innerEditorInstance.init();
					}
				}
				// directly add it, since it did not exist before
				fillPane.add(innerEditor);
				return true;
			}
			// could not get UI
			return false;
		}
		
		@Override
		public Synthesizer getEditorData() {
			return view;
		}
		
		@Override
		public SynthEditor getEditorParent(){
			return parent;
		}
		
		@Override
		public boolean setEditorParent(Component newParent){
			if(newParent instanceof SynthEditor){
				parent = (SynthEditor) newParent;
				return true;
			}
			return false;
		}
		
	}

}
