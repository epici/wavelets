package org.epici.wavelets.ui.synth.primitives;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.collections.Map;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Border;
import org.apache.pivot.wtk.Button;
import org.apache.pivot.wtk.ButtonPressListener;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.ComponentMouseListener;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.ListButton;
import org.apache.pivot.wtk.ListButtonSelectionListener;
import org.apache.pivot.wtk.PushButton;
import org.apache.pivot.wtk.TablePane;
import org.epici.wavelets.core.BetterClone;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.VarDouble;
import org.epici.wavelets.core.curve.CurvePolynomial;
import org.epici.wavelets.core.synth.SynthPrimitives;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.SessionLinked;
import org.epici.wavelets.ui.curve.CurvePreview;
import org.epici.wavelets.ui.curve.DoubleInput;
import org.epici.wavelets.ui.synth.SynthEditor;
import org.epici.wavelets.util.ui.PivotSwingUtils;

/**
 * Editor user interface for the {@link SynthPrimitives}.
 * Has a vertical list of oscillators as the main component.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class SynthPrimitivesEditor extends FillPane implements Bindable, DataEditor.Instance<SynthPrimitives, SynthEditor.LinkedEditorPane>, SessionLinked {

	/**
	 * Number of table rows always at the top of
	 * {@link #outerTable}
	 * which do not represent a primitive.
	 */
	public static final int EXTRA_ROWS_TOP = 0;
	/**
	 * Number of table rows always at the bottom of
	 * {@link #outerTable}
	 * which do not represent a primitive.
	 */
	public static final int EXTRA_ROWS_BOTTOM = 1;
	
	/**
	 * Locally used thread pool.
	 */
	public static final ScheduledThreadPoolExecutor THREAD_POOL = new ScheduledThreadPoolExecutor(4);
	
	/**
	 * The parent editor.
	 */
	public SynthEditor.LinkedEditorPane parent;
	/**
	 * The synthesizer being edited.
	 */
	public SynthPrimitives view;
	/**
	 * The current session.
	 */
	public Session session;
	
	/**
	 * The outer table. One row is given to each primitive.
	 * The row component is
	 * {@link LinkedTableRow}
	 */
	public TablePane outerTable;
	/**
	 * The button to add a new primitive to the list.
	 */
	public PushButton buttonAddNew;
	/**
	 * The button to select all primitives, or deselect all
	 * if any are selected.
	 */
	public PushButton buttonSelectAll;
	/**
	 * Container for the {@link #preview1}
	 */
	public Border previewContainer1;
	/**
	 * Central preview for this synth.
	 * <br>
	 * Shows it with a longer time.
	 */
	public SynthPrimitivesPreview preview1;
	/**
	 * Container for the {@link #preview2}
	 */
	public Border previewContainer2;
	/**
	 * Central preview for this synth.
	 * <br>
	 * Shows it normally.
	 */
	public SynthPrimitivesPreview preview2;
	/**
	 * Container for the {@link #preview3}
	 */
	public Border previewContainer3;
	/**
	 * Central preview for this synth.
	 * <br>
	 * Shows it with a shorter time.
	 */
	public SynthPrimitivesPreview preview3;
	
	/**
	 * Blank editor, which will be used by BXML.
	 */
	public SynthPrimitivesEditor() {
		super();
	}
	
	/**
	 * Convenience method to create a new one from outside
	 * 
	 * @param synth the synth to be edited
	 * 
	 * @return a new editor instance
	 */
	public static SynthPrimitivesEditor createNew(SynthPrimitives synth){
		SynthPrimitivesEditor result = PivotSwingUtils.loadBxml(SynthPrimitivesEditor.class, "synthPrimitivesEditor.bxml");
		result.view = synth;
		return result;
	}

	@Override
	public Session getSession() {
		return session;
	}

	@Override
	public SynthPrimitives getEditorData() {
		return view;
	}

	@Override
	public SynthEditor.LinkedEditorPane getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof SynthEditor.LinkedEditorPane))return false;
		parent = (SynthEditor.LinkedEditorPane) newParent;
		return true;
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		outerTable = (TablePane) namespace.get("outerTable");
		buttonAddNew = (PushButton) namespace.get("buttonAddNew");
		buttonSelectAll = (PushButton) namespace.get("buttonSelectAll");
		previewContainer1 = (Border) namespace.get("previewContainer1");
		previewContainer2 = (Border) namespace.get("previewContainer2");
		previewContainer3 = (Border) namespace.get("previewContainer3");
	}
	
	@Override
	public void init(){
		// set the session field
		session = getEditorParent().getEditorParent().getSession();
		
		// fetch common data
		SynthPrimitives view = getEditorData();
		
		// --- add listeners to buttons ---
		buttonAddNew.getButtonPressListeners().add(new ButtonAddNewListener(this));
		buttonSelectAll.getButtonPressListeners().add(new ButtonSelectAllListener(this));
		
		// --- add primitives rows ---
		for(SynthPrimitives.Osc osc:view.oscillators) {
			addPrimitiveRow(osc);
		}
		
		// --- make preview ---
		preview1 = view.getPreview();
		previewContainer1.setContent(preview1);
		preview2 = view.getPreview();
		previewContainer2.setContent(preview2);
		preview3 = view.getPreview();
		previewContainer3.setContent(preview3);
		this.validate();
		int previewHeight = preview1.getPreferredHeight()/2+1;
		preview1.setPreferredHeight(previewHeight);
		preview2.setPreferredHeight(previewHeight);
		preview3.setPreferredHeight(previewHeight);
		preview1.timeMap = (x->Math.pow(x, 2d/3) * 0x1.0p+1);
		preview1.periodMap = new CurvePolynomial(-1, 0.5);
		preview3.timeMap = (x->Math.pow(x, 2d/3) * 0x1.0p-1);
		preview3.periodMap = new CurvePolynomial(+1, 0.5);
		
		// --- update previews often ---
		ListenerPack listenerPack = new ListenerPack();
		this.getComponentMouseListeners().add(listenerPack);
	}
	
	/**
	 * Add the {@link LinkedTableRow} for a {@link SynthPrimitives.Osc}.
	 * 
	 * @param newOsc the primitive to add the row for
	 */
	public void addPrimitiveRow(SynthPrimitives.Osc newOsc) {
		LinkedTableRow ltr = LinkedTableRow.createNew(newOsc);
		ltr.setEditorParent(this);
		TablePane.RowSequence rows = outerTable.getRows();
		int nrows = rows.getLength();
		int last = nrows - SynthPrimitivesEditor.EXTRA_ROWS_BOTTOM;
		rows.insert(ltr, last);
		ltr.init();
	}
	
	/**
	 * The listener attached to {@link SynthPrimitivesEditor#buttonAddNew}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ButtonAddNewListener implements ButtonPressListener {
		
		/**
		 * The parent editor.
		 */
		public SynthPrimitivesEditor parent;
		
		/**
		 * Initialize providing a parent.
		 * 
		 * @param parent the parent editor
		 */
		public ButtonAddNewListener(SynthPrimitivesEditor parent) {
			this.parent = parent;
		}

		@Override
		public void buttonPressed(Button button) {
			// fetch the parent
			SynthPrimitives view = parent.view;
			// create the new primitive
			SynthPrimitives.Osc newOsc = new SynthPrimitives.Osc(view);
			// add it to the list
			view.oscillators.add(newOsc);
			// create and add the new row
			parent.addPrimitiveRow(newOsc);
		}
		
	}
	
	/**
	 * The listener attached to {@link SynthPrimitivesEditor#buttonSelectAll}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ButtonSelectAllListener implements ButtonPressListener {
		
		/**
		 * The parent editor.
		 */
		public SynthPrimitivesEditor parent;
		
		/**
		 * Initialize providing a parent.
		 * 
		 * @param parent the parent editor
		 */
		public ButtonSelectAllListener(SynthPrimitivesEditor parent) {
			this.parent = parent;
		}

		@Override
		public void buttonPressed(Button button) {
			// will we select?
			boolean select = true;
			// get the rows and iterate
			TablePane.RowSequence rows = parent.outerTable.getRows();
			int nrows = rows.getLength();
			int first = SynthPrimitivesEditor.EXTRA_ROWS_TOP;
			int last = nrows - SynthPrimitivesEditor.EXTRA_ROWS_BOTTOM;
			for(int i=first;i<last;i++) {
				LinkedTableRow ltr = (LinkedTableRow) rows.get(i);
				// if any are selected, deselect
				if(ltr.getSelected()) {
					select = false;
					break;
				}
			}
			// apply selection or deselection
			for(int i=first;i<last;i++) {
				LinkedTableRow ltr = (LinkedTableRow) rows.get(i);
				ltr.setSelected(select);
			}
		}
		
	}
	
	/**
	 * Combined listener for the {@link SynthPrimitivesEditor}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ListenerPack implements ComponentMouseListener {
		
		/**
		 * The update delay, in milliseconds.
		 */
		public static final long UPDATE_DELAY = 50;
		/**
		 * Track the last update with delay.
		 * Update anyway if it's been long enough.
		 */
		public AtomicLong lastUpdate = new AtomicLong(0);
		/**
		 * Track the next scheduled update. Abort if we're late.
		 */
		public AtomicLong nextScheduledUpdate = new AtomicLong(0);

		@Override
		public boolean mouseMove(Component component, int x, int y) {
			updateNow((SynthPrimitivesEditor)component);
			return false;
		}

		@Override
		public void mouseOver(Component component) {
		}

		@Override
		public void mouseOut(Component component) {
		}
		
		/**
		 * Change the value to be at least some value.
		 * 
		 * @param target the holder
		 * @param value the minimum value to set
		 */
		public static void setMax(AtomicLong target, long value) {
			long get;
			do {
				value = Math.max(value, get=target.get());
			} while (!target.compareAndSet(get, value));
		}

		/**
		 * Schedule an update.
		 * 
		 * @param editor
		 */
		public void updateNow(SynthPrimitivesEditor editor) {
			long ptime = System.currentTimeMillis();
			long ctime = ptime + UPDATE_DELAY;
			UpdateEvent uevent = new UpdateEvent(this, editor, ctime);
			if(ptime > lastUpdate.get()) {
				uevent.delayed = false;
				uevent.run();
			} else {
				setMax(nextScheduledUpdate, ctime);
				THREAD_POOL.schedule(uevent, UPDATE_DELAY, TimeUnit.MILLISECONDS);
			}
		}
		
		/**
		 * Scheduled update event.
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static class UpdateEvent implements Runnable {
			
			/**
			 * The combined listener.
			 */
			public ListenerPack listenerPack;
			/**
			 * The parent editor.
			 */
			public SynthPrimitivesEditor editor;
			/**
			 * Time when this event was created.
			 */
			public long ctime;
			/**
			 * Was this done with a delay?
			 */
			public boolean delayed = true;
			
			/**
			 * Basic constructor.
			 * 
			 * @param listenerPack the listener pack
			 * @param editor the editor
			 * @param ctime the current time
			 */
			public UpdateEvent(ListenerPack listenerPack, SynthPrimitivesEditor editor, long ctime) {
				this.listenerPack = listenerPack;
				this.editor = editor;
				this.ctime = ctime;
			}

			@Override
			public void run() {
				if(!delayed || ctime >= listenerPack.nextScheduledUpdate.get()) {
					ListenerPack.setMax(listenerPack.lastUpdate, ctime+UPDATE_DELAY);
					// actual updating
					editor.preview1.repaint();
					editor.preview2.repaint();
					editor.preview3.repaint();
				}
			}
			
		}
		
	}
	
	public static class LinkedTableRow extends TablePane.Row implements Bindable, DataEditor.Instance<SynthPrimitives.Osc, SynthPrimitivesEditor>, SessionLinked  {

		/**
		 * The parent editor.
		 */
		public SynthPrimitivesEditor parent;
		/**
		 * The synthesizer primitive being edited.
		 */
		public SynthPrimitives.Osc view;
		/**
		 * The current session.
		 */
		public Session session;
		
		// TODO doc all these things
		
		public Border containerDetune;
		public Border containerVolume;
		public Border containerAttackConst;
		public Border containerAttackFrac;
		public Border containerHoldConst;
		public Border containerHoldFrac;
		public Border containerDecayConst;
		public Border containerDecayFrac;
		public Border containerMinVolume;
		public ListButton typeSelector;
		public PushButton buttonMoveUp;
		public PushButton buttonSelect;
		public PushButton buttonDelete;
		public PushButton buttonMoveDown;
		
		public CurvePreview previewDetune;
		public CurvePreview previewVolume;
		public CurvePreview previewAttackConst;
		public CurvePreview previewAttackFrac;
		public CurvePreview previewHoldConst;
		public CurvePreview previewHoldFrac;
		public CurvePreview previewDecayConst;
		public CurvePreview previewDecayFrac;
		public CurvePreview previewMinVolume;
		
		/**
		 * Convenience method to create a new one from outside
		 * 
		 * @param osc the synth primitive to be edited
		 * 
		 * @return a new editor instance
		 */
		public static LinkedTableRow createNew(SynthPrimitives.Osc osc){
			LinkedTableRow result = PivotSwingUtils.loadBxml(SynthPrimitivesEditor.class, "synthPrimitivesEditorTableRow.bxml");
			result.view = osc;
			return result;
		}

		@Override
		public Session getSession() {
			return session;
		}

		@Override
		public SynthPrimitives.Osc getEditorData() {
			return view;
		}

		@Override
		public SynthPrimitivesEditor getEditorParent() {
			return parent;
		}

		@Override
		public boolean setEditorParent(Component newParent) {
			if(!(newParent instanceof SynthPrimitivesEditor))return false;
			parent = (SynthPrimitivesEditor) newParent;
			return true;
		}

		@Override
		public void initialize(Map<String, Object> namespace, URL location, Resources resources) {
			containerDetune = (Border) namespace.get("containerDetune");
			containerVolume = (Border) namespace.get("containerVolume");
			containerAttackConst = (Border) namespace.get("containerAttackConst");
			containerAttackFrac = (Border) namespace.get("containerAttackFrac");
			containerHoldConst = (Border) namespace.get("containerHoldConst");
			containerHoldFrac = (Border) namespace.get("containerHoldFrac");
			containerDecayConst = (Border) namespace.get("containerDecayConst");
			containerDecayFrac = (Border) namespace.get("containerDecayFrac");
			containerMinVolume = (Border) namespace.get("containerMinVolume");
			typeSelector = (ListButton) namespace.get("typeSelector");
			buttonMoveUp = (PushButton) namespace.get("buttonMoveUp");
			buttonSelect = (PushButton) namespace.get("buttonSelect");
			buttonDelete = (PushButton) namespace.get("buttonDelete");
			buttonMoveDown = (PushButton) namespace.get("buttonMoveDown");
		}
		
		private static final String BOUNDEDDOUBLEVALIDATOR_CLASS_NAME = DoubleInput.DoubleValidator.BoundedDoubleValidator.class.getCanonicalName();
		private static final String DOUBLEVALIDATOR_CLASS_NAME = DoubleInput.DoubleValidator.class.getCanonicalName();
		
		@Override
		public void init() {
			// set session field
			session = getEditorParent().getSession();
			
			// fetch parent by method
			SynthPrimitives.Osc parent = getEditorData();
			
			// fetch bounds array
			double[][] propertyBounds = SynthPrimitives.Osc.getPropertyBounds();
			
			// --- initialize individual properties previews ---
			int pindex = 0;
			DoubleInput pinput = null;
			PropertyPreviewListener propertyListener = null;
			
			previewDetune = new CurvePreview();
			previewDetune.setEditorData(parent.properties[pindex]);
			containerDetune.setContent(previewDetune);
			previewDetune.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewDetune.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewDetune.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewVolume = new CurvePreview();
			previewVolume.setEditorData(parent.properties[pindex]);
			containerVolume.setContent(previewVolume);
			previewVolume.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewVolume.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewVolume.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewAttackConst = new CurvePreview();
			previewAttackConst.setEditorData(parent.properties[pindex]);
			containerAttackConst.setContent(previewAttackConst);
			previewAttackConst.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewAttackConst.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewAttackConst.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewAttackFrac = new CurvePreview();
			previewAttackFrac.setEditorData(parent.properties[pindex]);
			containerAttackFrac.setContent(previewAttackFrac);
			previewAttackFrac.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewAttackFrac.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewAttackFrac.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewHoldConst = new CurvePreview();
			previewHoldConst.setEditorData(parent.properties[pindex]);
			containerHoldConst.setContent(previewHoldConst);
			previewHoldConst.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewHoldConst.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewHoldConst.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewHoldFrac = new CurvePreview();
			previewHoldFrac.setEditorData(parent.properties[pindex]);
			containerHoldFrac.setContent(previewHoldFrac);
			previewHoldFrac.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewHoldFrac.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewHoldFrac.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewDecayConst = new CurvePreview();
			previewDecayConst.setEditorData(parent.properties[pindex]);
			containerDecayConst.setContent(previewDecayConst);
			previewDecayConst.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewDecayConst.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewDecayConst.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewDecayFrac = new CurvePreview();
			previewDecayFrac.setEditorData(parent.properties[pindex]);
			containerDecayFrac.setContent(previewDecayFrac);
			previewDecayFrac.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewDecayFrac.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewDecayFrac.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			previewMinVolume = new CurvePreview();
			previewMinVolume.setEditorData(parent.properties[pindex]);
			containerMinVolume.setContent(previewMinVolume);
			previewMinVolume.init();
			propertyListener = new PropertyPreviewListener(this, pindex);
			previewMinVolume.viewCurveListeners.add(propertyListener);
			pinput = (DoubleInput) previewMinVolume.innerEditor;
			setDoubleInputBounds(pinput, propertyBounds[pindex]);
			pindex++;

			// --- set correct type and add listener ---
			typeSelector.setSelectedIndex(view.type);
			typeSelector.getListButtonSelectionListeners().add(new TypeSelectorListener(this));;
			
			// --- add listeners to list order changing buttons ---
			buttonMoveUp.getButtonPressListeners().add(new ButtonMoveUpListener(this));
			buttonMoveDown.getButtonPressListeners().add(new ButtonMoveDownListener(this));
			buttonDelete.getButtonPressListeners().add(new ButtonDeleteListener(this));
		}
		
		/**
		 * Change a {@link DoubleInput#validator} to have the specified
		 * {@link DoubleInput.DoubleValidator#min()},
		 * {@link DoubleInput.DoubleValidator#base()}, and
		 * {@link DoubleInput.DoubleValidator#max()}.
		 * 
		 * @param pinput {@link DoubleInput} instance to change
		 * @param minbasemax (min, base, max) tuple
		 */
		public static void setDoubleInputBounds(DoubleInput pinput,double[] minbasemax) {
			java.util.Map<String,Object> copyOptions = null, copySet = null;
			Collection<String> copyWhitelist;
			copyOptions = BetterClone.fixOptions(null);
			copySet = (java.util.Map<String,Object>)copyOptions.get("set");
			copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".min", minbasemax[0]);
			copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".max", minbasemax[2]);
			copySet.put(BOUNDEDDOUBLEVALIDATOR_CLASS_NAME+".base", minbasemax[1]);
			copyWhitelist = (Collection<String>)copyOptions.get("whitelist");
			copyWhitelist.add("*"+DOUBLEVALIDATOR_CLASS_NAME);
			pinput.setValidator(
					BetterClone.copy(pinput.validator, 1, copyOptions));
		}
		
		/**
		 * Is this primitive selected?
		 * Checks the {@link #buttonSelect} to find out.
		 * 
		 * @return true if it is selected, false if it is not selected
		 */
		public boolean getSelected() {
			return buttonSelect.isSelected();
		}
		
		/**
		 * Select or deselect this primitive.
		 * Tells the {@link #buttonSelect} to change its selected state.
		 * 
		 * @param selected true to select, false to deselect
		 */
		public void setSelected(boolean selected) {
			buttonSelect.setSelected(selected);
		}
		
		/**
		 * Tracks a property of a {@link SynthPrimitives.Osc}
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static class PropertyPreviewListener implements CurvePreview.ViewCurveListener {

			/**
			 * Default constructor.
			 * 
			 * @param parent parent editor
			 * @param propertyIndex index of property to track
			 */
			public PropertyPreviewListener(SynthPrimitivesEditor.LinkedTableRow parent, int propertyIndex) {
				super();
				this.parent = parent;
				this.propertyIndex = propertyIndex;
			}

			/**
			 * The parent editor
			 */
			public SynthPrimitivesEditor.LinkedTableRow parent;
			/**
			 * Index of property being tracked
			 */
			public int propertyIndex;
			
			@Override
			public void viewCurveChanged(CurvePreview editor, VarDouble oldView, VarDouble newView) {
				parent.view.properties[propertyIndex] = newView;
			}
			
		}
		
		/**
		 * The listener attached to {@link LinkedTableRow#typeSelector}.
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static class TypeSelectorListener implements ListButtonSelectionListener {
			
			/**
			 * The parent editor.
			 */
			public LinkedTableRow parent;
			
			/**
			 * Initialize providing a parent.
			 * 
			 * @param parent the parent editor
			 */
			public TypeSelectorListener(LinkedTableRow parent) {
				this.parent = parent;
			}

			@Override
			public void selectedIndexChanged(ListButton listButton, int previousSelectedIndex) {
				parent.view.type = listButton.getSelectedIndex();
			}

			@Override
			public void selectedItemChanged(ListButton listButton, Object previousSelectedItem) {
			}
			
		}
		
		/**
		 * The listener attached to {@link LinkedTableRow#buttonMoveUp}.
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static class ButtonMoveUpListener implements ButtonPressListener {
			
			/**
			 * The parent editor.
			 */
			public LinkedTableRow parent;
			
			/**
			 * Initialize providing a parent.
			 * 
			 * @param parent the parent editor
			 */
			public ButtonMoveUpListener(LinkedTableRow parent) {
				this.parent = parent;
			}

			@Override
			public void buttonPressed(Button button) {
				// get higher editor
				SynthPrimitivesEditor speditor = parent.getEditorParent();
				// get the rows
				TablePane.RowSequence rows = speditor.outerTable.getRows();
				// get index in table
				int index = rows.indexOf(parent) - 1;
				// get index in synth list
				int rindex = index - SynthPrimitivesEditor.EXTRA_ROWS_TOP;
				// Can't move past first row, so check
				if(index >= SynthPrimitivesEditor.EXTRA_ROWS_TOP){
					Collections.swap(speditor.view.oscillators, rindex, rindex+1);
					PivotSwingUtils.swap(rows, index, index+1);
				}
			}
			
		}
		
		/**
		 * The listener attached to {@link LinkedTableRow#buttonMoveDown}.
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static class ButtonMoveDownListener implements ButtonPressListener {
			
			/**
			 * The parent editor.
			 */
			public LinkedTableRow parent;
			
			/**
			 * Initialize providing a parent.
			 * 
			 * @param parent the parent editor
			 */
			public ButtonMoveDownListener(LinkedTableRow parent) {
				this.parent = parent;
			}

			@Override
			public void buttonPressed(Button button) {
				// get higher editor
				SynthPrimitivesEditor speditor = parent.getEditorParent();
				// get the rows
				TablePane.RowSequence rows = speditor.outerTable.getRows();
				int nrows = rows.getLength();
				// get index in table
				int index = rows.indexOf(parent);
				// get index in synth list
				int rindex = index - SynthPrimitivesEditor.EXTRA_ROWS_TOP;
				// Can't move past last row, so check
				if(index < nrows - SynthPrimitivesEditor.EXTRA_ROWS_BOTTOM - 1){
					Collections.swap(speditor.view.oscillators, rindex, rindex+1);
					PivotSwingUtils.swap(rows, index, index+1);
				}
			}
			
		}
		
		/**
		 * The listener attached to {@link LinkedTableRow#buttonDelete}.
		 * 
		 * @author EPICI
		 * @version 1.0
		 * @since 0.2.2
		 */
		public static class ButtonDeleteListener implements ButtonPressListener {
			
			/**
			 * The parent editor.
			 */
			public LinkedTableRow parent;
			
			/**
			 * Initialize providing a parent.
			 * 
			 * @param parent the parent editor
			 */
			public ButtonDeleteListener(LinkedTableRow parent) {
				this.parent = parent;
			}

			@Override
			public void buttonPressed(Button button) {
				// get higher editor
				SynthPrimitivesEditor speditor = parent.getEditorParent();
				// get the rows
				TablePane.RowSequence rows = speditor.outerTable.getRows();
				// get index in table
				int index = rows.indexOf(parent);
				// get index in synth list
				int rindex = index - SynthPrimitivesEditor.EXTRA_ROWS_TOP;
				// do the removal
				speditor.view.oscillators.remove(rindex);
				rows.remove(index, 1);
			}
			
		}
		
	}

}
