package org.epici.wavelets.ui.synth.primitives;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Comparator;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Dimensions;
import org.apache.pivot.wtk.Mouse;
import org.apache.pivot.wtk.skin.FillPaneSkin;
import org.epici.wavelets.core.ColorScheme;
import org.epici.wavelets.core.Samples;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.synth.SynthPrimitives;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.util.Bits;
import org.epici.wavelets.util.math.Floats;
import org.epici.wavelets.util.ui.Draw;

/**
 * Skin for the {@link SynthPrimitivesPreview}.
 * <br>
 * Shows either the entire {@link SynthPrimitives}'s result
 * or the breakdown of individual {@link SynthPrimitives#oscillators}.
 * All show waveforms with false colour.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class SynthPrimitivesPreviewSkin extends FillPaneSkin implements KeyTracker {
	
	/**
	 * How many pixels of horizontal distance should there be between
	 * the visual for one sample and the visual for the next?
	 * <br>
	 * This is the target distance.
	 * It may not actually draw with this distance.
	 */
	public static final int TARGET_SAMPLE_PIXELS = 2;
	
	/**
	 * What the preferred height will be by default.
	 */
	public static final int DEFAULT_HEIGHT = 30;
	/**
	 * What the preferred width will be by default.
	 */
	public static final int DEFAULT_WIDTH = 150;
	/**
	 * Width/height ratio used to calculate preferred dimension given the other.
	 */
	public static final double DEFAULT_SIZE_RATIO = (double)DEFAULT_WIDTH/DEFAULT_HEIGHT;
	/**
	 * If the width is shorter than this, it
	 * will try to become vertical by asking for a higher height.
	 */
	public static final double DEFAULT_SIZE_THRESHOLD = Math.sqrt(DEFAULT_WIDTH*DEFAULT_HEIGHT);
	
	/**
	 * True to show the entire {@link SynthPrimitives}'s result,
	 * false to show the individual {@link SynthPrimitives#oscillators}.
	 * <br>
	 * Gets toggled by a double click.
	 */
	protected boolean showCombined = true;
	/**
	 * Click count, used to detect double clicks.
	 * Counter is reset when the mouse leaves.
	 */
	protected int clickCount = 0;
	/**
	 * Which keys are held down?
	 */
	protected BitSet keys = new BitSet();

	/**
	 * Blank constructor.
	 */
	public SynthPrimitivesPreviewSkin() {
		
	}

	@Override
	public BitSet getKeysDown() {
		return keys;
	}
	
	@Override
	public void mouseOut(Component component) {
		clickCount = 0;
	}
	
	@Override
	public boolean mouseUp(Component component, Mouse.Button button, int x, int y) {
		component.requestFocus();
		clickCount++;
		// double click to toggle
		if(clickCount == 2) {
			showCombined ^= true;
			component.repaint();
		}
		return false;
	}
	
	/**
	 * Is this focusable?
	 * 
	 * @return {@code true}, because it's a preview
	 */
	@Override
    public boolean isFocusable() {
        return true;
    }

	@Override
	public void layout() {
	}
	
	/**
	 * Returns the visual's unconstrained preferred size.
	 * <br>
	 * Uses the same implementation as
	 * {@link org.apache.pivot.wtk.skin.ComponentSkin#getPreferredSize()},
	 * that is, it calls
	 * {@link #getPreferredWidth(int)} with argument {@code -1}
	 * and
	 * {@link #getPreferredHeight(int)} with argument {@code -1}
	 * and combines them into a {@link Dimensions}.
	 */
	@Override
	public Dimensions getPreferredSize() {
		return new Dimensions(getPreferredWidth(-1),getPreferredHeight(-1));
	}

	@Override
	public int getPreferredHeight(int width) {
		if(width<=0)return DEFAULT_HEIGHT;
		return (int)Math.round(width*(width<=DEFAULT_SIZE_THRESHOLD?DEFAULT_SIZE_RATIO:1d/DEFAULT_SIZE_RATIO));
	}

	@Override
	public int getPreferredWidth(int height) {
		if(height<=0)return DEFAULT_WIDTH;
		return (int)Math.round(height*DEFAULT_SIZE_RATIO);
	}
	
	@Override
	public void paint(Graphics2D graphics){
		// --- Fetch some data ---
		SynthPrimitivesPreview preview = (SynthPrimitivesPreview) getComponent();
		SynthPrimitives synth = preview.getEditorData();
		Session session = preview.getSession();
		ColorScheme colors = Session.getColors(session);
		int width = getWidth(), height = getHeight();
		AffineTransform at = graphics.getTransform();
		// Ensure width > height
		if(width<height){// Quarter turn CCW
			at.translate(0, height);
			at.quadrantRotate(-1);
			graphics.setTransform(at);
			int tmp = width;width = height;height = tmp;
		}
		// Get some numbers
		double time = session.getCurrentTime();
		ArrayList<SynthPrimitives.Osc> oscCopy = new ArrayList<>(synth.oscillators);
		int noscs = oscCopy.size();
		// Sort oscs list by frequency
		oscCopy.sort(Comparator.comparing((SynthPrimitives.Osc iosc)->iosc.getDetune(time)));
		// Get sample data and color signatures
		int ln = Bits.binLog(width);
		int lp = (int)Math.round(Floats.median(0, preview.periodMap.applyAsDouble(ln), 30));
		int period = 1<<lp;
		int nsamples = 1<<ln;
		double[][] oscColors = new double[noscs][];
		Samples[] oscSamples = new Samples[noscs];
		double dt = preview.timeMap.applyAsDouble(nsamples)/880d;
		for(int i=0;i<noscs;i++) {
			SynthPrimitives.Osc iosc = oscCopy.get(i);
			oscColors[i] = iosc.getColorSignature(time);
			oscSamples[i] = iosc.getQuickSamples(nsamples, time, time+dt, dt, 880, period);
		}
		// --- Pre-draw ---
		// Get colors
		Color bgColUpper, bgColLower, textCol, lineCol;
		bgColLower = colors.gradient;
		bgColUpper = ColorScheme.brighten(bgColLower, 0.1f);
		textCol = colors.text;
		lineCol = colors.line;
		// --- Draw ---
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// The base is a rectangle
		Rectangle2D.Double shapeOuter = new Rectangle2D.Double(0, 0, width, height);
		graphics.setPaint(new GradientPaint(0,0,bgColUpper,0,height,bgColLower));
		graphics.fill(shapeOuter);
		// skip the drawing steps if there are no oscs
		if(noscs > 0) {
			// Set the stroke
			BasicStroke stroke = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			graphics.setStroke(stroke);
			// Branch
			if(showCombined) {// Draw the combined
				double[] sumColors = new double[3];
				Samples sumSamples = Samples.blankSamples(
						oscSamples[0].sampleRate,
						oscSamples[0].sampleData.length
						);
				for(int i=0;i<noscs;i++) {
					for(int j=0;j<3;j++) {
						sumColors[j] += oscColors[i][j];
					}
					sumSamples.layerOnThisLazy(oscSamples[i]);
				}
				drawSamplesInBox(
						graphics,
						new Rectangle2D.Double(0, 0, width, height),
						sumColors,
						sumSamples
						);
			} else {// Draw individual oscs
				double boxFrac = 1/Math.sqrt(noscs);
				double boxSize = boxFrac * height;
				double spaceSize = (height - boxSize) / Math.max(1, noscs - 1);
				for(int i=0;i<noscs;i++) {
					drawSamplesInBox(
							graphics,
							new Rectangle2D.Double(0, spaceSize*i, width, boxSize),
							oscColors[i],
							oscSamples[i]
							);
				}
			}
		}
		// Outline comes last so it is always above
		graphics.setColor(lineCol);
		graphics.draw(shapeOuter);
		// Overlay the name of the synth
		Font textFont = graphics.getFont().deriveFont(10f);
		Draw.drawTextImage(graphics, 0, 0, width, height, synth.getName(), null, textFont, textCol, null, 0, 0, 0.5, 0);
	}
	
	/**
	 * Draw sample data in a box using stroke of a path.
	 * May actually go outside of the box.
	 * 
	 * @param graphics graphics drawing context
	 * @param bounds where to try to draw in
	 * @param colorxyz CIE XYZ unbounded color
	 * @param samples sample data
	 */
	protected static void drawSamplesInBox(Graphics2D graphics, Rectangle2D.Double bounds, double[] colorxyz, Samples samples) {
		double[] sampleData = samples.sampleData;
		int n = sampleData.length;
		if(n<2)return;
		Path2D.Double path = new Path2D.Double();
		double x = bounds.getX(), y = bounds.getY(), lx = bounds.getWidth(), ly = bounds.getHeight();
		double[] offsetAmplitude = getOffsetAmplitude(samples);
		double offset = offsetAmplitude[0], amplitude = offsetAmplitude[1];
		if(amplitude==0)amplitude = 1;
		double sx = lx/(n-1), dy = offset, sy = -0.5*ly/amplitude, oy = y+ly*0.5;
		for(int i=0;i<n;i++) {
			double px = x + i * sx;
			double py = oy + (sampleData[i] - dy) * sy;
			if (i==0) {// first no stroke
				path.moveTo(px, py);
			} else {// then join
				path.lineTo(px, py);
			}
		}
		graphics.setColor(ColorScheme.fromCIEXYZUnbounded(colorxyz));
		graphics.draw(path);
	}
	
	/**
	 * Calculate the <i>(offset, amplitude)</i> tuple for some sample data.
	 * Assumes it is generated by <i>amplitude &times; wave(time) + offset</i>
	 * for some kind of wave function.
	 * Amplitude does not follow any strict mathematical definition.
	 * It is definitely not:
	 * <ul>
	 * <li>Half the size of the range</li>
	 * <li>The average energy</li>
	 * </ul>
	 * <br>
	 * If all values are {@link java.lang.Double#NaN}, returns the tuple
	 * <i>(0, 1)</i>.
	 * <br>
	 * Used by {@link #paint(Graphics2D)}.
	 * 
	 * @param samples sample data
	 * @return<i>(offset, amplitude)</i> tuple
	 */
	protected static double[] getOffsetAmplitude(Samples samples) {
		// fetch
		double[] sampleData = samples.sampleData;
		int nsamples = 0;
		// offset
		double sum = 0;
		for(double value:sampleData) {
			if(!Double.isFinite(value))continue;
			sum += value;
			nsamples += 1;
		}
		nsamples = Math.max(nsamples, 1);
		double offset = sum/nsamples;
		// amplitude
		double sumsq = 0;
		for(double value:sampleData) {
			sumsq += Math.pow(value - offset, 2);
		}
		double amplitude = 4 * Math.sqrt(sumsq/nsamples);
		// return
		return new double[] {offset,amplitude};
	}

}
