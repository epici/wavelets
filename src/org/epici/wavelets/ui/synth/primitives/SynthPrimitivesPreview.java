package org.epici.wavelets.ui.synth.primitives;

import java.net.URL;
import java.util.function.DoubleUnaryOperator;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.Container;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.Orientation;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.core.curve.CurvePolynomial;
import org.epici.wavelets.core.synth.SynthPrimitives;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.SessionLinked;

/**
 * Preview interface for the {@link SynthPrimitives}.
 * <br>
 * Behaviour determined by its skin, an instance of
 * {@link SynthPrimitivesPreviewSkin}.
 * <br>
 * Created by {@link SynthPrimitives#getPreview()}.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class SynthPrimitivesPreview extends FillPane implements Bindable, DataEditor.Instance<SynthPrimitives, Container>, SessionLinked {

	/**
	 * The parent editor.
	 */
	public Container parent;
	/**
	 * The synthesizer being edited.
	 */
	public SynthPrimitives view;
	/**
	 * The current session.
	 */
	public Session session;
	/**
	 * Maps the total length to the length of a measure as used
	 * by the skin.
	 */
	public DoubleUnaryOperator timeMap;
	/**
	 * Maps the log2 of the total length to
	 * the log2 of the period as used
	 * by the skin.
	 */
	public DoubleUnaryOperator periodMap;
	
	/**
	 * Blank constructor, uses vertical orientation.
	 * 
	 * @see FillPane#FillPane()
	 */
	public SynthPrimitivesPreview() {
		this(Orientation.VERTICAL);
	}

	/**
	 * Constructor with orientation.
	 * 
	 * @param orientation layout orientation
	 * 
	 * @see FillPane#FillPane(Orientation)
	 */
	public SynthPrimitivesPreview(Orientation orientation) {
		super(orientation);
		installSkin(SynthPrimitivesPreview.class);
		timeMap = (x->Math.pow(x, 2d/3));
		periodMap = new CurvePolynomial(0,0.5);
	}

	@Override
	public Session getSession() {
		if(session == null)session = SessionLinked.tryGetSession(this.getParent());
		return session;
	}

	@Override
	public SynthPrimitives getEditorData() {
		return view;
	}

	@Override
	public Container getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(!(newParent instanceof Container))return false;
		parent = (Container) newParent;
		return true;
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
		
	}
	
	@Override
	public void init(){
		
	}

}
