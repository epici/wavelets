package org.epici.wavelets.ui.synth;

import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.BitSet;
import java.util.Map;
import javax.swing.JInternalFrame;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.Component;
import org.apache.pivot.wtk.ComponentKeyListener;
import org.apache.pivot.wtk.ComponentMouseButtonListener;
import org.apache.pivot.wtk.Container;
import org.apache.pivot.wtk.FillPane;
import org.apache.pivot.wtk.Keyboard;
import org.apache.pivot.wtk.Mouse.Button;
import org.apache.pivot.wtk.Orientation;
import org.apache.pivot.wtk.WTKListenerList;
import org.epici.wavelets.core.BetterClone;
import org.epici.wavelets.core.MetaComponent;
import org.epici.wavelets.core.Synthesizer;
import org.epici.wavelets.core.Session;
import org.epici.wavelets.ui.DataEditor;
import org.epici.wavelets.ui.KeyTracker;
import org.epici.wavelets.ui.SessionLinked;
import org.epici.wavelets.ui.WindowManager;

/**
 * Generic holder for a preview of a synth.
 * Requires that it implements {@link DataEditor.Editable}
 * to propery generate a preview component using
 * {@link DataEditor.Editable#getPreview()}.
 * The synth can be opened in the {@link SynthEditor},
 * and a different synth can be swapped in.
 * Some preview components may also support direct editing.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.2
 */
public class SynthPreview extends FillPane implements Bindable, KeyTracker, DataEditor.Instance<Synthesizer, Container> {

	/**
	 * The parent of this. Since we want the flexiblity to use it
	 * anywhere, it only needs to be a {@link Container},
	 * which all component containers should be anyway.
	 * Users should attempt a cast to a more specific type
	 * they want to use.
	 */
	public Container parent;
	/**
	 * The synth which this displays a preview for.
	 */
	protected Synthesizer view;
	/**
	 * Preview interface supplied by the component. Null only if the component
	 * failed to supply an preview.
	 * <br>
	 * Most things require that it also be an instance of
	 * {@link DataEditor.Instance}. This allows parents
	 * to pass useful data on to the children.
	 * <br>
	 * Similar to {@link SynthEditor.LinkedEditorPane#innerEditor}.
	 */
	public Component innerEditor;
	/**
	 * Which keys are held down?
	 */
	protected BitSet keys = new BitSet();
	/**
	 * The list of listeners of type {@link ViewSynthListener}.
	 * All are notified when {@link #view} changes
	 * through a call to {@link #setEditorData(Synthesizer, boolean)}.
	 */
	public ViewSynthListenerList viewSynthListeners = new ViewSynthListenerList();
	
	public SynthPreview() {
		this(Orientation.HORIZONTAL);
	}

	public SynthPreview(Orientation orientation) {
		super(orientation);
	}

	@Override
	public void initialize(org.apache.pivot.collections.Map<String, Object> namespace, URL location, Resources resources) {
	}
	
	@Override
	public boolean isFocusable() {
		return this.getLength()==0 || !this.get(0).isFocusable();
	}

	@Override
	public Synthesizer getEditorData() {
		return view;
	}
	
	/**
	 * Change the {@link Synthesizer} which this displays the preview for.
	 * Updates the user interface and notifies listeners accordingly.
	 * <br>
	 * Call with {@code notify} as {@code true} unless you
	 * really know what you are doing.
	 * Not notifying listeners can result in
	 * invalid state and breaking invariants.
	 * 
	 * @param newView the new value of {@link #view} to set
	 * @param notify whether to notify listeners
	 * @return true if it was successfully set
	 */
	public boolean setEditorData(Synthesizer newView, boolean notify) {
		if(newView == null || newView == view)return false;
		Synthesizer oldView = view;
		view = newView;
		innerEditor = null;
		this.removeAll();
		trySetInnerEditor();
		viewSynthListeners.viewSynthChanged(this, oldView, newView);
		return true;
	}
	
	/**
	 * Change the {@link Synthesizer} which this displays the preview for.
	 * Updates the user interface and notifies listeners accordingly.
	 * 
	 * @param newView the new value of {@link #view} to set
	 * @return true if it was successfully set
	 */
	public boolean setEditorData(Synthesizer newView) {
		return setEditorData(newView, true);
	}

	@Override
	public Container getEditorParent() {
		return parent;
	}

	@Override
	public boolean setEditorParent(Component newParent) {
		if(newParent instanceof Container){
			parent = (Container) newParent;
			return true;
		}
		return false;
	}
	
	@Override
	public void init(){
		// call super initializer
		DataEditor.Instance.super.init();
		
		// set the parent if not done yet
		if(getEditorParent() == null) {
			setEditorParent(getParent());
		}
		
		// make the combined listener
		ListenerPack listenerPack = new ListenerPack();
		
		// attach the combined listener where applicable
		this.getComponentKeyListeners().add(listenerPack);
		this.getComponentMouseButtonListeners().add(listenerPack);
		
		// set the preview component based on the "view" field
		trySetInnerEditor();
	}

	@Override
	public BitSet getKeysDown() {
		return keys;
	}
	
	/**
	 * If {@link #innerEditor} is not set, tries to set it.
	 * Returns true if this method sets it.
	 * If it returns false, either it was already set or it
	 * could not be set.
	 * 
	 * @return true on success, false on failure
	 */
	public boolean trySetInnerEditor(){
		// already exists
		if(innerEditor != null)return false;
		// need this interface to fetch UI
		if(view instanceof DataEditor.Editable<?, ?>){
			DataEditor.Editable<?, ?> viewEditable = (DataEditor.Editable<?, ?>) view;
			// set the editor to whatever it returns
			innerEditor = viewEditable.getPreview();
			if(innerEditor == null)return false;// oh no, it failed to supply it
			// most things require implementing this
			if(innerEditor instanceof DataEditor.Instance<?, ?>){
				DataEditor.Instance<?, ?> innerEditorInstance = (DataEditor.Instance<?, ?>) innerEditor;
				// if the parent is accepted, proceed with initialization
				if(innerEditorInstance.setEditorParent(this)){
					innerEditorInstance.init();
				}
			}
			// directly add it, since it did not exist before
			this.add(innerEditor);
			return true;
		}
		// could not get UI
		return false;
	}
	
	/**
	 * Internal helper method to make a copy of a {@link Synthesizer}
	 * which is intended to be long lived. Does not rename.
	 * 
	 * @param original make a copy of this
	 * @param session current session which provides other data
	 * @return a copy, or null on failure
	 */
	protected static Synthesizer copyOrNull(Synthesizer original, Session session) {
		if(original == null)return null;
		Map<String, Object> copyOptions = BetterClone.fixOptions(null);
		session.setCopyOptions(copyOptions);
		Synthesizer result = BetterClone.tryCopy(original, 1, copyOptions);
		if(result == original)return null;
		return result;
	}
	
	/**
	 * Combined listener which is attached to a {@link SynthPreview}.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ListenerPack implements ComponentKeyListener, ComponentMouseButtonListener {

		@Override
		public boolean keyTyped(Component component, char character) {
			return false;
		}

		@Override
		public boolean keyPressed(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
			SynthPreview synthPreview = (SynthPreview) component;
			synthPreview.getKeysDown().set(keyCode);
			return false;
		}

		@Override
		public boolean keyReleased(Component component, int keyCode, Keyboard.KeyLocation keyLocation) {
			boolean didSomething = false;
			SynthPreview synthPreview = (SynthPreview) component;
			Session session = SessionLinked.tryGetSession(synthPreview);
			int keyModifiers = synthPreview.getKeyModifiers();
			switch(keyCode) {
			case KeyEvent.VK_C:{
				if(session == null)break;// need a session to do this
				switch(keyModifiers) {
				case 1:{// Shift C -> store copy
					Synthesizer toCopy = synthPreview.getEditorData();
					toCopy = SynthPreview.copyOrNull(toCopy, session);
					session.setClipBoard(toCopy);
					didSomething = true;
					break;
				}
				case 2:{// Control C -> store original reference
					Synthesizer toCopy = synthPreview.getEditorData();
					session.setClipBoard(toCopy);
					didSomething = true;
					break;
				}
				}
				break;
			}
			case KeyEvent.VK_V:{
				if(session == null)break;// need a session to do this
				switch(keyModifiers) {
				case 1:{// Shift V -> put copy
					Object oclipboard = session.getClipBoard();
					if(oclipboard == null)break;
					Synthesizer pasted = null;
					if (oclipboard instanceof Synthesizer) {
						pasted = (Synthesizer) oclipboard;
						pasted = SynthPreview.copyOrNull(pasted, session);
					}
					if(pasted == null)break;
					synthPreview.setEditorData(pasted);
					didSomething = true;
					break;
				}
				case 2:{// Control V -> put original reference
					Object oclipboard = session.getClipBoard();
					if(oclipboard == null)break;
					Synthesizer pasted = null;
					if (oclipboard instanceof Synthesizer) {
						pasted = (Synthesizer) oclipboard;
					}
					if(pasted == null)break;
					synthPreview.setEditorData(pasted);
					didSomething = true;
					break;
				}
				}
				break;
			}
			case KeyEvent.VK_I:{// I -> open editor
				// Needs session to work
        		if(session==null)break;
	        	// Open editor
	        	MetaComponent<JInternalFrame> meta = session.windowManager.getWindow(WindowManager.NAME_SYNTH_EDITOR, true);
	        	Object osynthEditor = meta.metaData.get("window");
	        	if(osynthEditor instanceof DataEditor<?>){
	        		DataEditor<?> synthEditor = (DataEditor<?>) osynthEditor;
	        		synthEditor.addEditorDataCasted(synthPreview.view);
	        		didSomething = true;
	        	}
				break;
			}
			}
			synthPreview.getKeysDown().clear(keyCode);
			return didSomething;
		}

		@Override
		public boolean mouseDown(Component component, Button button, int x, int y) {
			return false;
		}

		@Override
		public boolean mouseUp(Component component, Button button, int x, int y) {
			component.requestFocus();// backup focus
			return false;
		}

		@Override
		public boolean mouseClick(Component component, Button button, int x, int y, int count) {
			return false;
		}
		
	}
	
	/**
	 * The listener which fires when {@link SynthPreview#view} changes.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static interface ViewSynthListener {
		
		/**
		 * The synth which this component displays, changed.
		 * Now it is displaying a different synth.
		 * 
		 * @param editor the component displaying the synth
		 * @param oldView what synth was being displayed before this event fired
		 * @param newView what synth is being displayed now
		 */
		public void viewSynthChanged(SynthPreview editor, Synthesizer oldView, Synthesizer newView);
		
	}
	
	/**
	 * Represents a list of {@link ViewSynthListener},
	 * which can also be treated as one.
	 * 
	 * @author EPICI
	 * @version 1.0
	 * @since 0.2.2
	 */
	public static class ViewSynthListenerList extends WTKListenerList<ViewSynthListener> implements ViewSynthListener {

		@Override
		public void viewSynthChanged(SynthPreview editor, Synthesizer oldView, Synthesizer newView) {
			for(ViewSynthListener listener:this) {
				listener.viewSynthChanged(editor, oldView, newView);
			}
		}
		
	}

}
