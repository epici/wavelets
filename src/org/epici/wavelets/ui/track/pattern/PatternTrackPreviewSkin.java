package org.epici.wavelets.ui.track.pattern;

import java.util.*;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Color;
import java.awt.GradientPaint;
import org.apache.pivot.wtk.*;
import org.apache.pivot.wtk.skin.*;
import org.apache.pivot.wtk.skin.terra.*;
import org.epici.wavelets.core.*;
import org.epici.wavelets.core.track.PatternTrack;
import org.epici.wavelets.ui.SessionLinked;

import javax.swing.JInternalFrame;

import org.epici.wavelets.util.*;

/**
 * A skin for {@link PatternTrackPreview}
 * <br>
 * Necessary because rendering is delegated to skin
 * 
 * @author EPICI
 * @version 1.0
 */
public class PatternTrackPreviewSkin extends ComponentSkin implements SessionLinked {
	
	/**
	 * Extra space to leave above and below, in semitones
	 */
	public static final int PITCH_MARGIN = 4;
	
	public transient Preferences preferences;
	protected transient PatternTrack tls;
	protected transient Project comp;
	protected transient Session session;
	protected transient int width, height;
	protected transient double[] gbounds, lbounds;
	protected transient boolean highlighted = false;
	
	public PatternTrackPreviewSkin(){
		
	}
	
	@Override
	public Session getSession() {
		return session;
	}

	@Override
	public void layout() {
		
	}
	
	@Override
	public void mouseOver(Component component){
		highlighted = true;
		component.repaint();
	}
	
	@Override
	public void mouseOut(Component component){
		highlighted = false;
		component.repaint();
	}
	
	@Override
	public boolean mouseClick(Component component,Mouse.Button button,int x,int y,int count){
		PatternTrackPreview target = (PatternTrackPreview) component;
		tls = target.target;
		MetaComponent<JInternalFrame> meta = tls.getUI();
		comp = tls.parentProject();
		Session session = comp.currentSession;
		session.windowManager.addWindow(meta,true);
		return true;
	}
	
	/**
	 * Calculate the correct width, set the transient field, and return it
	 * 
	 * @return the correct width
	 */
	public int updateWidth(){
		PatternTrackPreview target = (PatternTrackPreview) getComponent();
		tls = target.target;
		lbounds = tls.getTimeBounds();
		comp = tls.parentProject();
		session = comp.currentSession;
		gbounds = comp.rootTrack.getTimeBounds();
		double measureDiff = comp.secondsToMeasures(gbounds[1])
				-comp.secondsToMeasures(gbounds[0]);
		return width = (int) (measureDiff*target.pixelsPerMeasure);
	}
	
	/**
	 * Calculate the correct height, set the transient field, and return it
	 * 
	 * @return the correct height
	 */
	public int updateHeight(){
		PatternTrackPreview target = (PatternTrackPreview) getComponent();
		return height = target.height;
	}

	@Override
	public int getPreferredHeight(int width) {
		return updateHeight();
	}

	@Override
	public int getPreferredWidth(int height) {
		return updateWidth();
	}

	@Override
	public void paint(Graphics2D graphics) {
		/*
		 * Determines resolution of gradient:
		 * Call this x, a range of 2^x pixels is a solid tone
		 * 0 is individual pixels (highest resolution)
		 * 30 makes everything the same tone (lowest resolution), 31 would cause overflow
		 */
		int gradientShiftInc = (int)Preferences.getIntSafe(session, Preferences.INDEX_INT_TLS_PATTERN_BAR_GRADIENT);
		PatternTrackPreview target = (PatternTrackPreview) getComponent();
		PatternTrack tls = target.target;
		updateWidth();updateHeight();
		int width = target.getWidth(), height = target.getHeight();
		Color bg;
		ColorScheme colors = Session.getColors(session);
		bg = colors.background;
		Color bgdark = TerraTheme.darken(bg);
		IdentityHashMap<Pattern,BitSet> patterns = tls.patterns;
		int minPitch = 0, maxPitch = 0;
		// Fetch synth colors at t=0 and pitch bounds at the same time
		for(Pattern pattern:patterns.keySet()){
			int lminPitch = Integer.MAX_VALUE, lmaxPitch = Integer.MIN_VALUE;
			for(Clip clip:pattern.clips){
				int pitch = clip.getPitch();
				if(pitch<lminPitch)lminPitch=pitch;
				if(pitch>lmaxPitch)lmaxPitch=pitch;
			}
			if(lminPitch<minPitch)minPitch=lminPitch;
			if(lmaxPitch>maxPitch)maxPitch=lmaxPitch;
		}
		minPitch -= PITCH_MARGIN;maxPitch += PITCH_MARGIN + 1;// Because they are bars, not lines, and take up a space
		int pitchRange = maxPitch - minPitch;
		double clipHeight = height/(double)pitchRange;
		int iclipHeight = (int)Math.ceil(clipHeight);
		// Precalculate y values (redundancy is okay because it's inexpensive)
		int[] ys = new int[pitchRange];
		for(int i=0;i<pitchRange;i++){
			int iy=(int)Math.round(clipHeight*(pitchRange-i-1));
			ys[i] = iy;
		}
		// Octave indicators (each doubling is one gradient repetition)
		for(int i=12*Math.floorDiv(minPitch, 12);i<maxPitch;i+=12){
			int y1 = (int)Math.round(clipHeight*(maxPitch-i)), y2 = (int)Math.round(clipHeight*(maxPitch-i-12));// Recalculate because it can be out of bounds
			graphics.setPaint(new GradientPaint(0,y1,bg,0,y2,bgdark));
			graphics.fillRect(0, y2, width, y1-y2);
		}
		double anchorx = comp.secondsToMeasures(gbounds[0]), cornerx = comp.secondsToMeasures(gbounds[1]);
		double scalex = width/(cornerx-anchorx), iscalex = 1d/scalex;
		IdentityHashMap<Synthesizer,Color[]> synthSigs = new IdentityHashMap<>();
		IdentityHashMap<Synthesizer,LinearGradientPaint> synthSigPaints = new IdentityHashMap<>();
		double dxgradientInc = gradientShiftInc*iscalex;
		int xrefBlocks = (int)(anchorx/dxgradientInc);
		double dxref = xrefBlocks*dxgradientInc;
		int xref = -((int)((anchorx-dxref)*scalex));//First x for gradient, cannot be positive
		int xrange = ((width-xref-1)/gradientShiftInc)+1;
		double ixrange = 1d/xrange;
		float[] sigKeyframes = new float[xrange+1];
		sigKeyframes[xrange] = 1;
		for(int j=1;j<xrange;j++){
			sigKeyframes[j] = (float)(j*ixrange);
		}
		// Draw bars
		for(Pattern pattern:patterns.keySet()){
			Synthesizer synth = pattern.getSynthesizer();
			Color[] sigs = synthSigs.get(synth);
			LinearGradientPaint sigPaints = synthSigPaints.get(synth);
			if(sigs==null){
				sigs = new Color[xrange+1];
				synthSigs.put(synth, sigs);
				for(int j=0;j<=xrange;j++){
					double t=comp.measuresToSeconds((j*gradientShiftInc)*iscalex+dxref);
					sigs[j] = synth.getColorSignature(t);
				}
				sigPaints = new LinearGradientPaint(xref,0,xref+gradientShiftInc*xrange,0,sigKeyframes,sigs);
				synthSigPaints.put(synth, sigPaints);
			}
			graphics.setPaint(sigPaints);
			double ixmult = scalex/pattern.divisions;
			PrimitiveIterator.OfInt iter = patterns.get(pattern).stream().iterator();
			while(iter.hasNext()){
				int delay = iter.nextInt();
				double xoffset = (delay-anchorx)*scalex;
				for(Clip clip:pattern.clips){
					graphics.fillRect((int)Math.round(xoffset+clip.getDelay()*ixmult), ys[clip.getPitch()-minPitch], (int)Math.ceil(clip.getLength()*ixmult), iclipHeight);
				}
			}
		}
		if(highlighted){
			graphics.setColor(new Color(0xff,0xff,0xff,0x99));
			graphics.fillRect(0, 0, width, height);
		}
	}

}
