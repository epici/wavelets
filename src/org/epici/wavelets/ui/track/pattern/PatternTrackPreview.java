package org.epici.wavelets.ui.track.pattern;

import java.awt.*;
import org.apache.pivot.wtk.*;
import org.epici.wavelets.core.*;
import org.epici.wavelets.core.track.PatternTrack;
import org.epici.wavelets.util.ui.*;

import org.epici.wavelets.util.*;

/**
 * Row sized preview for a {@link PatternTrack}
 * 
 * @author EPICI
 * @version 1.0
 */
public class PatternTrackPreview extends Track.ViewComponent {

	/**
	 * {@link PatternTrack} to preview
	 */
	public PatternTrack target;
	/**
	 * Horizontal length allotted to one measure
	 * <br>
	 * Non-positive signals uninitialized
	 */
	public transient double pixelsPerMeasure = 0d;
	/**
	 * Height
	 * <br>
	 * Non-positive signals uninitialized
	 */
	public transient int height = 0;
	
	public PatternTrackPreview(PatternTrack track){
		target = track;
		installSkin(PatternTrackPreview.class);
	}
	
	@Override
	public void setPixelsPerMeasure(double v) {
		pixelsPerMeasure = v;
	}

}
