package org.epici.wavelets.ui;

import java.util.BitSet;
import java.util.Objects;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;
import org.epici.wavelets.util.Bits;
import com.google.common.collect.MapMaker;
import java.awt.event.KeyEvent;

/**
 * Helper interface for things that track keys.
 * Intended for use by skins.
 * 
 * @author EPICI
 * @version 1.0
 * @since 0.2.1
 */
public interface KeyTracker {
	
	/**
	 * Mapping which contains known inverses of
	 * {@link KeyEvent#getKeyText(int)},
	 * used by {@link #getKeyCodeFromName(String)}.
	 * <br>
	 * We use a one-off {@link Supplier} to initialize this,
	 * since apparently static initializers in an interface class
	 * are not allowed.
	 */
	public static final ConcurrentMap<String,Integer> keyNameToKeyCode = new Supplier<ConcurrentMap<String, Integer>>(){
		public ConcurrentMap<String,Integer> get() {
			// Make the map
			ConcurrentMap<String,Integer> result = new MapMaker().makeMap();
			// Scan ranges for named keys
			for(int i=0;i<1<<16;i++) {
				String keyName = KeyEvent.getKeyText(i);
				// Single character is just that
				// 0x will appear in unmapped key codes
				if(keyName.length() > 1 && !keyName.contains("0x")) {
					result.put(keyName, i);
				}
			}
			// Done
			return result;
		}
	}.get();
	
	/**
	 * Get the keys currently held down, as a bit set.
	 * If a bit is set, it means the corresponding key code
	 * (as would be returned by
	 * {@link KeyEvent#getKeyCode()})
	 * is held down.
	 * 
	 * @return keys currently being pressed
	 */
	public BitSet getKeysDown();
	
	/**
	 * Is the shift key held down?
	 * 
	 * @return true if the shift key is held down
	 */
	public default boolean shiftHeld(){
		return getKeysDown().get(KeyEvent.VK_SHIFT);
	}
	
	/**
	 * Is the control key held down?
	 * 
	 * @return true if the control key is held down
	 */
	public default boolean controlHeld(){
		return getKeysDown().get(KeyEvent.VK_CONTROL);
	}
	
	/**
	 * Is the alt key held down?
	 * 
	 * @return true if the alt key is held down
	 */
	public default boolean altHeld(){
		return getKeysDown().get(KeyEvent.VK_ALT);
	}
	
	/**
	 * Return a bitmask of special keys' state,
	 * in order: shift, control, alt
	 * 
	 * @return bitmask for specific keys
	 */
	public default int getKeyModifiers() {
		int result = 0;
		result = Bits.writeBit(result, 0, shiftHeld());
		result = Bits.writeBit(result, 1, controlHeld());
		result = Bits.writeBit(result, 2, altHeld());
		return result;
	}
	
	/**
	 * Bodge together a key and a modifier bitmask.
	 * Modifiers will be put in reverse order, so they'll go in the top bits.
	 * Remaining lower bits are the original key.
	 * 
	 * @param keyCode key code
	 * @param keyModifiers key modifiers
	 * @return value which contains data from both
	 */
	public static int combineKeyWithModifiers(int keyCode,int keyModifiers) {
		return keyCode|Integer.reverse(keyModifiers);
	}
	
	/**
	 * Get back the key code from a result of {@link #combineKeyWithModifiers(int, int)}.
	 * 
	 * @param combined combined value
	 * @return just the key code
	 */
	public static int extractKeyCodeFromCombined(int combined) {
		return combined & 0x0fffffff;
	}
	
	/**
	 * Get back the key modifiers from a result of {@link #combineKeyWithModifiers(int, int)}.
	 * Key modifiers are as would be returned by {@link #getKeyModifiers()}.
	 * 
	 * @param combined combined value
	 * @return just the key modifiers
	 */
	public static int extractKeyModifiersFromCombined(int combined) {
		return Integer.reverse(combined & 0xf0000000);
	}
	
	/**
	 * Inverse of {@link KeyEvent#getKeyText(int)}.
	 * 
	 * @param keyName name of key
	 * @return the key code
	 */
	public static int getKeyCodeFromKeyName(String keyName) {
		Objects.requireNonNull(keyName);
		if(keyName.length() == 0) {
			return keyName.codePointAt(0);
		} else {// multiple characters, is a wordy name
			Integer result = keyNameToKeyCode.get(keyName);
			if(result == null) {
				throw new IllegalArgumentException("Unknown key name: "+keyName);
			}
			return result;
		}
	}
	
}
