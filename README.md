# Wavelets
Open source DAW. Ditching analog for creative freedom.

Started 2016-12-24. Migrated from GitHub to GitLab on 2018-06-11.

Some History
---
Inspired by Blender and other open source projects that came before it, Wavelets aims to liberate another industry. Though originally meant for audio in general, it was respecialized for music.

The first version was sort of a test run. The current version is the second, targeting a usable state, though without the goal of competing with the current industry powerhouses.

Abandoned? Why?
---
I started this project with very big goals. It was ambitious. In theory at least, it could work, and I could work fast enough to make it happen. Over time, it became apparent that my actual work rate is nowhere near that fast. Sure, I sometimes I sat down for hours at a time to work on Wavelets. But I couldn't do that every day. It wasn't even an issue of time. It was just a lack of motivation.

I had plans for the future. I planned very far ahead. I planned for the future of Wavelets and for projects which followed. But I almost always worked too slow to meet my goals. At some point, I saw that continuing my plans as is was simply not an option anymore. The timings were unrealistic and implausible. If I continued working on Wavelets, I might never get to try my other ideas. And after a little over 2 years, I finally made the call to drop Wavelets.

I will be moving on to other projects now, and taking better care of myself.

Wavelets is dead. It's not coming back. Not by me anyway.

I am keeping Wavelets under GPLv3, since it does what I want and I've already found it to work. If you need it under a different license, please contact me, I will check the license compatibility, and if it works, I can relicense it.

**Thank you to everyone who supported me and tracked this project up to now.**
